run_setup:
	cp .env.staging .env
	yarn install

run_local:
	yarn build:vendor && yarn build:prod
	node server.js

run_docker:
	docker build --tag mitraaca-dashboard:dev .
	docker run --name mitraacadashboardserver -d -p 3000:3000 mitraaca-dashboard:dev


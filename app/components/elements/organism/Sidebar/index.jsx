import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Layout, Menu, Avatar, Button,
} from 'antd'
import { capitalize } from 'lodash'

const { Sider } = Layout
const { SubMenu } = Menu

const SideBar = ({
  handleKeys, site, user, logout,
  isCollapsed, toggleCollapsed,
  imgAvatar, setAvatar,
}) => (
  <Sider
    collapsible
    breakpoint="sm"
    onCollapse={collapsed => toggleCollapsed(collapsed)}
    className="site-layout-background"
  >
    <Menu
      theme="light"
      mode="inline"
      selectedKeys={[site.activePage]}
      defaultOpenKeys={(window.localStorage.getItem('isPath') || '').split(',')}
      style={{ height: '100%', borderRight: 0 }}
      onClick={(e) => {
        if (e.key === '#') {
          logout()
        } else {
          handleKeys(e)
        }
      }}
      onOpenChange={e => handleKeys(e, true)}
    >
      <div className="py-4 text-center bg-blue">
        <Link to="/dashboard">
          <img src="/assets/logo-mitra-aca.png" alt="logo" />
        </Link>
      </div>
      <div className="py-4 text-center">
        <Avatar shape="circle" size={isCollapsed ? 'small' : 120} src={imgAvatar || '/assets/avatar-on-error.jpg'} onError={() => setAvatar('')} />
        <div className="pt-3">
          <p className="profile-name text-truncate">{user.name.toUpperCase()}</p>
          <p className="profile-role text-truncate">{capitalize(user.role && user.role.group.name)}</p>
        </div>
      </div>
      {/*
      <Menu.Item key="dashboard" className="zoom-icon">
        <span>
          <i className="las la-home mr-2" />
          <span className="nav-text">Dashboard</span>
        </span>
      </Menu.Item>
      */}
      <Menu.Item key="users" className="zoom-icon">
        <span>
          {/* <i className="las la-user-tie mr-2" /> */}
          <img src="/assets/ic-search-agent.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">USER MANAGEMENT</span>
        </span>
      </Menu.Item>
      <Menu.Item key="role-group" className="zoom-icon">
        <span>
          {/* <i className="las la-box mr-2" /> */}
          <img src="/assets/ic-search-policy.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">ROLE GROUP</span>
        </span>
      </Menu.Item>
      <Menu.Item key="role" className="zoom-icon">
        <span>
          {/* <i className="las la-box mr-2" /> */}
          <img src="/assets/ic-search-policy.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">ROLE</span>
        </span>
      </Menu.Item>
      <Menu.Item key="products" className="zoom-icon">
        <span>
          {/* <i className="las la-box mr-2" /> */}
          <img src="/assets/ic-search-policy.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">PRODUCTS</span>
        </span>
      </Menu.Item>
      <Menu.Item key="product-setting" className="zoom-icon">
        <span>
          {/* <i className="las la-box mr-2" /> */}
          <img src="/assets/ic-box.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">PRODUCT SETTING</span>
        </span>
      </Menu.Item>
      <SubMenu
        key="reports"
        className="zoom-icon"
        title={(
          <span>
            {/* <i className="las la-chalkboard mr-2" /> */}
            <img src="/assets/ic-report.svg" alt="logo" className="icon-menu" />
            <span className="nav-text">REPORTS</span>
          </span>
        )}
      >
        <SubMenu key="reports-administrator" title="Administrator" className="side-submenu">
          <Menu.Item key="reports/contest-winner">Agent Contest Winner</Menu.Item>
          <Menu.Item key="reports/restricted-product">Agent Restricted Product</Menu.Item>
          <Menu.Item key="reports/agent-list">Agent List</Menu.Item>
          <Menu.Item key="reports/approval">Approval</Menu.Item>
        </SubMenu>
        <SubMenu key="reports-produksi" title="Produksi" className="side-submenu">
          <Menu.Item key="reports/sppa">SPPA</Menu.Item>
          <Menu.Item key="reports/production">Produksi</Menu.Item>
          <Menu.Item key="reports/claim-settle">Claim Settle</Menu.Item>
          <Menu.Item key="reports/outstanding-claim">Outstanding Claim</Menu.Item>
          <Menu.Item key="reports/loss-business">Loss Business</Menu.Item>
        </SubMenu>
        <SubMenu key="reports-finance" title="Finance" className="side-submenu">
          <Menu.Item key="reports/finance-commission">Komisi Finance</Menu.Item>
          <Menu.Item key="reports/benefit-monthly">Benefit Finance Monthly</Menu.Item>
          <Menu.Item key="reports/benefit-yearly">Benefit Finance Yearly</Menu.Item>
          <Menu.Item key="reports/career">Jenjang Karir</Menu.Item>
        </SubMenu>
        <SubMenu key="reports-agent" title="Agent" className="side-submenu">
          <Menu.Item key="reports/agent/detail-commision">Detail komisi</Menu.Item>
          <Menu.Item key="reports/agent/expired-policy">Expired Policy</Menu.Item>
        </SubMenu>
        <SubMenu key="reports-training" title="Training" className="side-submenu">
          <Menu.Item key="reports/training">Training</Menu.Item>
        </SubMenu>
      </SubMenu>
      <Menu.Item key="my-password" className="zoom-icon">
        <Button type="link" className="px-0 btn-signout">
          {/* <i className="las la-box mr-2" /> */}
          <img src="/assets/ic-password.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">CHANGE PASSWORD</span>
        </Button>
      </Menu.Item>
      <Menu.Item key="#" className="zoom-icon">
        <Button type="link" className="px-0 btn-signout">
          {/* <i className="las la-box mr-2" /> */}
          <img src="/assets/ic-signout.svg" alt="logo" className="icon-menu" />
          <span className="nav-text">LOGOUT</span>
        </Button>
      </Menu.Item>
    </Menu>
  </Sider>
)

SideBar.propTypes = {
  handleKeys: PropTypes.func,
  site: PropTypes.object,
  user: PropTypes.object,
  logout: PropTypes.func,
  isCollapsed: PropTypes.bool,
  toggleCollapsed: PropTypes.func,
  imgAvatar: PropTypes.string,
  setAvatar: PropTypes.func,
}

export default SideBar

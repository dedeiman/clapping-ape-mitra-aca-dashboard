/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormProduct = ({
  detailProduct, handleSubmit,
  form, isEdit, handleSelect,
  selectOption, loadCOB, isFetching,
  errorsField, setErrorsField,
  goToDetailCountry,
  handleGetCountries, getCountries,
  handlePage, metaCountries,
  handleFilter,
}) => {
  const { getFieldDecorator, validateFields, getFieldValue } = form

  return (
    <Card title={`Form ${isEdit ? 'Edit' : 'Create'} Product`}>
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item label="Code" {...errorsField.code}>
              {getFieldDecorator('code', {
                initialValue: detailProduct.code,
                rules: [
                  ...Helper.fieldRules(['required'], 'Kode'),
                  { pattern: /^[a-zA-Z0-9 ]+$/, message: '*Tidak boleh menggunakan spesial karakter' },
                ],
                getValueFromEvent: (e) => {
                  setErrorsField({
                    ...errorsField,
                    code: undefined,
                  })
                  return e.target.value
                },
              })(
                <Input placeholder="Input Code" disabled={isEdit} />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Name" {...errorsField.name}>
              {getFieldDecorator('name', {
                initialValue: detailProduct.display_name,
                rules: Helper.fieldRules(['required', 'notSpecial'], 'Nama'),
              })(
                <Input placeholder="Input Name" disabled={isEdit} />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Product Type" {...errorsField.product_type_id}>
              {getFieldDecorator('product_type_id', {
                initialValue: (detailProduct.type && detailProduct.type.id) ? detailProduct.type.id : undefined,
                rules: Helper.fieldRules(['requiredDropdown'], 'Product Type'),
              })(
                <Select
                  onChange={handleSelect}
                  disabled={isEdit}
                  placeholder="Select Product Type"
                >
                  <Select.Option value={1}>Konvensional</Select.Option>
                  <Select.Option value={2}>Syariah</Select.Option>
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Class of Business" {...errorsField.cob_id}>
              {getFieldDecorator('cob_id', {
                initialValue: detailProduct.cob_id ? detailProduct.cob_id : undefined,
                rules: Helper.fieldRules(['requiredDropdown'], 'COB'),
              })(
                <Select
                  onChange={() => handleGetCountries(1, '')}
                  loading={loadCOB}
                  disabled={isEdit}
                  placeholder="Select Class of Business"
                >
                  {selectOption.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Max Discount" {...errorsField.max_discount}>
              {getFieldDecorator('max_discount', {
                initialValue: detailProduct.max_discount,
                rules: [
                  ...Helper.fieldRules(['required'], 'Diskon'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      if (Number(value) < 1) return Promise.reject('*Diskon minimal 1 %')

                      if (Number(value) > 100) {
                        return Promise.reject('*Diskon tidak boleh lebih dari 100%')
                      }

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input placeholder="Max Discount" maxLength={9} />,
              )}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Max Out Go" {...errorsField.max_commision}>
              {getFieldDecorator('max_commision', {
                initialValue: detailProduct.max_commision,
                rules: [
                  ...Helper.fieldRules(['required'], 'Komisi'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      if (Number(value) < 1) return Promise.reject('*Komisi minimal 1 %')

                      if (Number(value) > 100) {
                        return Promise.reject('*Diskon tidak boleh lebih dari 100%')
                      }

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input placeholder="Max Out Go" maxLength={9} />,
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            {(getFieldValue('cob_id') === 5) && (
              <Form.Item label="Countries" {...errorsField.countries_ids}>
                {getFieldDecorator('countries_ids', {
                })(
                  <>
                    <Row>
                      <Col span={12}>
                        <Input.Search
                          allowClear
                          placeholder="Search..."
                          className="w-md-50 mb-3"
                          onChange={e => handleFilter(e.target.value)}
                        />
                      </Col>
                      <Col span={12}>
                        <Button
                          type="primary"
                          className="float-md-right mt-3 mt-md-0"
                          onClick={() => history.push('/detail-country/add')}
                        >
                          Add User
                        </Button>
                      </Col>
                    </Row>

                    <Table
                      columns={columns}
                      rowKey="id"
                      bordered
                      loading={getCountries.isLoadCountries}
                      onRow={dataCountry => ({
                        onClick: () => goToDetailCountry(dataCountry),
                      })}
                      dataSource={getCountries.list}
                      pagination={{
                        total: metaCountries ? metaCountries.total_count : getCountries.list.length,
                        onChange: handlePage,
                      }}
                    />
                  </>,
                )}
              </Form.Item>
            )}
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">{!isEdit ? 'Add Product' : 'Submit Change'}</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormProduct.propTypes = {
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormProduct)

import PropTypes from 'prop-types'
import {
  Card, Table, Col, Row,
  Button, Alert, Select,
  Popconfirm, Input,
} from 'antd'
import history from 'utils/history'

const ProductsPage = ({
  isFetching, handlePage,
  metaProduct, dataProduct,
  handleDelete, errorMessage,
  closeError, handleSearch,
  keyword, currentPage, setKeyword,
  stateFilter, setFilter,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'no',
      render: (text, row, idx) => {
        const numb = idx + 1
        const pageCount = (currentPage && currentPage > 1) ? (currentPage - 1) : ''
        if (numb < 10) return `${pageCount}${numb}`
        if (numb === 10) return `${pageCount + 1}0`
        return numb
      },
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
      // width: 110,
      render: text => (text || '-'),
    },
    {
      title: 'Name',
      dataIndex: 'display_name',
      key: 'name',
      render: text => (text || '-'),
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      render: text => ((text && text.name) ? text.name : '-'),
    },
    {
      title: 'Class of Business',
      dataIndex: 'class_of_business',
      key: 'class_of_business',
      render: text => ((text && text.name) ? text.name : '-'),
    },
    {
      title: 'Max Discount',
      dataIndex: 'max_discount',
      key: 'max_discount',
      render: text => (text || '-'),
    },
    {
      title: 'Max Out Go',
      dataIndex: 'max_commision',
      key: 'max_commision',
      render: text => (text || '-'),
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'action',
      width: 110,
      render: (text, record) => (
        <Button.Group>
          <Button onClick={() => history.push(`/product/${record.id}/edit`)} className="px-2">
            <i className="lar la-edit text-primary" style={{ fontSize: '20px' }} />
          </Button>
          {/*<Popconfirm*/}
          {/*  title="Are you sure delete this item?"*/}
          {/*  okText="Yes"*/}
          {/*  cancelText="No"*/}
          {/*  placement="topLeft"*/}
          {/*  onConfirm={() => handleDelete(record.id)}*/}
          {/*>*/}
          {/*  <Button className="px-2">*/}
          {/*    <i className="las la-trash text-danger" style={{ fontSize: '20px' }} />*/}
          {/*  </Button>*/}
          {/*</Popconfirm>*/}
        </Button.Group>
      ),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h4 className="mb-3 text-primary2">Product List</h4>
            <div className="d-flex align-items-center">
              <Select
                value={stateFilter.type}
                style={{ width: '200px' }}
                onChange={(val) => {
                  setFilter({ ...stateFilter, type: val })
                  setKeyword('')
                }}
              >
                <Select.Option value="name">Name</Select.Option>
                <Select.Option value="code">Code</Select.Option>
                <Select.Option value="type">Type</Select.Option>
                <Select.Option value="coba">Class of Business</Select.Option>
                <Select.Option value="max_discount">Max Discount</Select.Option>
                <Select.Option value="max_commision">Max Out Go</Select.Option>
              </Select>
              <Input.Search
                allowClear
                placeholder="Search..."
                value={keyword}
                onChange={(e) => {
                  const { value } = e.target
                  if (value === '' || /^[a-zA-Z0-9-+@. ]+$/.test(value)) {
                    setKeyword(value)
                  }
                }}
                onSearch={handleSearch}
                className="w-md-50 ml-3"
              />
            </div>
          </Col>
          <Col xs={24} md={4}>
            {/*<Button*/}
            {/*  type="primary"*/}
            {/*  className="float-md-right mt-3 mt-md-0"*/}
            {/*  onClick={() => history.push('/product/add')}*/}
            {/*>*/}
            {/*  Add Product*/}
            {/*</Button>*/}
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}

      <Table
        bordered
        rowKey="id"
        columns={columns}
        dataSource={dataProduct}
        loading={isFetching}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaProduct ? metaProduct.total_count : dataProduct.length,
          current: currentPage || 1,
          onChange: handlePage,
          simple: isMobile,
        }}
      />
    </Card>
  )
}

ProductsPage.propTypes = {
  isFetching: PropTypes.bool,
  dataProduct: PropTypes.array,
  metaProduct: PropTypes.object,
  handlePage: PropTypes.func,
  handleDelete: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleSearch: PropTypes.func,
  keyword: PropTypes.string,
  setFilter: PropTypes.func,
  currentPage: PropTypes.string,
  setKeyword: PropTypes.func,
  stateFilter: PropTypes.object,
}

export default ProductsPage

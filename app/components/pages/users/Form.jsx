/* eslint-disable no-unused-vars */
/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-shadow */
import React from 'react'
import PropTypes from 'prop-types'
import Helper from 'utils/Helper'
import {
  Card, Form, Input, Button,
  Upload, Icon, Avatar, Divider,
  Select, Alert, Row, Col, Radio, Checkbox,
} from 'antd'
import { capitalize, isEmpty } from 'lodash'
import { PO_ID, BRANCH_ID } from 'constants/ActionTypes'
import { Loader } from 'components/elements'

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 5,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 12,
    },
  },
}

const FormUser = ({
  detailUser, currentState,
  stateBranches, stateProducts,
  handleSubmit, form, handleUpload,
  avatarFileList, handleSelect,
  selectOption, loadRole,
  selectGroupOption, loadGroupRole,
  selectCOBOption, loadCOB,
  selectBranchesOption, loadBranches,
  isEdit, isFetching, errorMessage,
  errorsField, setErrorsField,
  removeCard, addCard, stateCountry,
  statePermission, checkedList,
  productType, handlePermission,
  onChange, onCheckAllChange, checkAll,
  addAll, handleResendVerify, match,
  isVerifyOtp,
}) => {
  const {
    getFieldDecorator, getFieldsValue, getFieldValue, setFieldsValue,
  } = form

  getFieldDecorator('keys', { initialValue: currentState.keys })

  const {
    role_group_id: group,
    branch_id: branchValues,
    class_of_business: cobValues,
    product_ids: productsValues,
    branch_perwakilan_ids: branchDelegateValues,
    head_user: headUserValues,
  } = getFieldsValue()

  const isBranch = (group === BRANCH_ID)
  const isPO = (group === PO_ID)

  let fieldItems
  if ((group === PO_ID) || (group === BRANCH_ID)) {
    fieldItems = (getFieldValue('keys') || []).map((item, idx) => {
      const elementTags = {}
      const initValue = {}

      if (isBranch) {
        elementTags.parent = !isEmpty(currentState.branch) ? currentState.branch[idx] : undefined
        elementTags.child = !isEmpty(currentState.perwakilan) ? currentState.perwakilan[idx] : undefined
        elementTags.load = !isEmpty(stateBranches[idx]) ? stateBranches[idx].load : false
        initValue.parent = !isEmpty(detailUser.branches) ? !isEmpty((detailUser.branches || []).find((val, index) => index === idx)) ? (detailUser.branches || []).find((val, index) => index === idx).id : undefined : undefined
        initValue.child = !isEmpty(detailUser.branches) ? ((!isEmpty((detailUser.branches || []).find((val, index) => index === idx)) ? (detailUser.branches || []).find((val, index) => index === idx).branch_perwakilan : undefined) || []).map(child => child.id) : undefined

        const dataInit = !isEmpty(detailUser.branches) ? ((!isEmpty((detailUser.branches || []).find((val, index) => index === idx)) ? (detailUser.branches || []).find((val, index) => index === idx).id : undefined) || []) : undefined
        initValue.nik = selectBranchesOption.filter(data => data.id === dataInit)[0]

        if (item) {
          const dataListPerwakilan = !isEmpty(stateBranches) ? (stateBranches || []).find(val => val.id === initValue.parent) : []
          elementTags.list = !isEmpty(dataListPerwakilan) ? dataListPerwakilan.list : []
        } else {
          elementTags.list = !isEmpty(stateBranches[idx]) ? stateBranches[idx].list : []
        }
        elementTags.list = !isEmpty(stateBranches[idx]) ? stateBranches[idx].list : []
      } else {
        elementTags.parent = !isEmpty(currentState.cob) ? currentState.cob[idx] : undefined
        elementTags.child = !isEmpty(currentState.productIds) ? currentState.productIds[idx] : undefined
        elementTags.load = !isEmpty(stateProducts[idx]) ? stateProducts[idx].load : false

        if (!isEmpty(detailUser.user_cobs)) {
          initValue.parent = !isEmpty((detailUser.user_cobs || []).find((val, index) => index === idx)) ? (detailUser.user_cobs || []).find((val, index) => index === idx).id : undefined
          initValue.type = !isEmpty((detailUser.user_cobs || []).find((val, index) => index === idx)) ? ((detailUser.user_cobs || []).find((val, index) => index === idx).product_type || {}).id : undefined
          initValue.child = !isEmpty((detailUser.user_cobs || []).find((val, index) => index === idx)) ? ((detailUser.user_cobs || []).find((val, index) => index === idx).product_type.products || []).map(child => child.id) : undefined
        }

        const loopNik = (stateProducts || [])[idx]
        initValue.nik = loopNik

        if (item) {
          const existProducts = stateProducts.find(o => o.cob === item)
          elementTags.list = existProducts ? existProducts.list : []
        } else {
          elementTags.list = !isEmpty(stateProducts[idx]) ? stateProducts[idx] : []
        }
        elementTags.list = !isEmpty(stateProducts[idx]) ? stateProducts[idx].list : []
      }

      return (
        <React.Fragment key={`field_${Math.random()}`}>
          {idx > 0 && <Divider />}
          {
            ((getFieldValue('role_id') === 33 || getFieldValue('role_id') === 29 || getFieldValue('role_id') === 32 || getFieldValue('role_id') === 24) && idx === 0) && (
              <Button
                type="primary"
                className="d-block ml-auto"
                onClick={() => addAll(isBranch)}
              >
                Select All
                {isBranch ? ' Branch' : ' COB'}
              </Button>
            )
          }
          <b>{`${isBranch ? 'Branch' : 'COB'} ${((getFieldValue('keys') || []).length > 1) ? (idx + 1) : ''}`}</b>
          {!isBranch && (
            <Form.Item
              label="Product Type"
              labelCol={{ sm: 24 }}
              wrapperCol={{ sm: 24 }}
              labelAlign="left"
            >
              {getFieldDecorator(`product_type[${idx}]`, {
                initialValue: initValue.type || 1,
                rules: [{ required: true, message: 'Product Type Is Required' }],
              })(
                <Radio.Group buttonStyle="solid">
                  {(productType.options || []).map(opt => (
                    <Radio value={opt.id}>{opt.name}</Radio>
                  ))}
                </Radio.Group>,
              )}
            </Form.Item>
          )}
          <Form.Item
            label={isBranch ? 'Branch' : 'Class of Business'}
            wrapperCol={{ sm: 24 }}
            labelCol={{ sm: 24 }}
            labelAlign="left"
          >
            {getFieldDecorator(`${isBranch ? 'branch_id' : 'class_of_business'}[${idx}]`, {
              initialValue: initValue.parent || undefined,
              rules: [{ required: true, message: `${isBranch ? 'Branch' : 'Class of Business'} Is Required` }],
              getValueFromEvent: (val) => {
                handleSelect(val, `${isBranch ? 'branch_id_' : 'class_of_business_'}${idx}`)
                if (isBranch) {
                  const headBranch = selectBranchesOption.filter(item => item.id === val)
                  if (getFieldValue('role_id') === 32) {
                    if (isEdit && initValue.parent === val) {
                      setFieldsValue({
                        [`head_user[${idx}]`]: headBranch[0].head_user.nik,
                        [`branch_perwakilan_ids[${idx}]`]: initValue.child,
                      })
                    } else {
                      setFieldsValue({
                        [`head_user[${idx}]`]: headBranch[0].head_user.nik,
                        [`branch_perwakilan_ids[${idx}]`]: undefined,
                      })
                    }
                  } else {
                    setFieldsValue({
                      head_user: headBranch[0].head_user ? headBranch[0].head_user.nik : '',
                      branch_perwakilan_ids: initValue.child,
                    })
                  }
                } else if (isPO) {
                  const headProduct = stateProducts.filter(item => item.id === val)
                  if (isEdit && initValue.parent === val) {
                    setFieldsValue({
                      [`product_ids[${idx}]`]: initValue.child,
                    })
                  } else {
                    setFieldsValue({
                      [`product_ids[${idx}]`]: undefined,
                    })
                  }
                }

                return val
              },
            })(
              <Select
                loading={isBranch ? loadBranches : loadCOB}
                disabled={isBranch ? isEmpty(selectBranchesOption) : isEmpty(selectCOBOption)}
                placeholder={`Select ${isBranch ? 'Branch' : 'Class of Business'}`}
              >
                {(isBranch ? selectBranchesOption : selectCOBOption).map((opt) => {
                  const fieldValues = isBranch ? branchValues : cobValues

                  return (
                    <Select.Option key={opt.id} value={opt.id} disabled={(fieldValues || []).includes(opt.id)}>{opt.name}</Select.Option>
                  )
                })}
              </Select>,
            )}
          </Form.Item>
          <Form.Item
            label={isBranch ? 'Branch Perwakilan' : 'Product'}
            labelCol={{ sm: 24 }}
            wrapperCol={{ sm: 24 }}
            labelAlign="left"
          >
            {getFieldDecorator(`${isBranch ? (getFieldValue('role_id') === 32 || getFieldValue('role_id') === 24 ? `branch_perwakilan_ids[${idx}]` : 'branch_perwakilan_ids') : `product_ids[${idx}]`}`, {
              initialValue: initValue.child || undefined,
              rules: [{ required: !isEmpty(elementTags.list), message: `${isBranch ? 'Branch Perwakilan' : 'Product'} Is Required` }],
              getValueFromEvent: (value) => {
                handleSelect(value, 'product')
                const all = (elementTags.list || []).map(val => val.id)

                if (value && value.length && value.includes('all')) {
                  if (value.length === (all.length + 1)) {
                    return []
                  }
                  return [...all]
                }
                return value
              },
            })(
              <Select
                showArrow
                allowClear
                mode="multiple"
                loading={elementTags.load}
                disabled={isEmpty(elementTags.list)}
                placeholder={`Select ${isBranch ? 'Branch Perwakilan' : 'Product'}`}
              >
                <Select.Option
                  value="all"
                  disabled={(getFieldValue(`${isBranch ? 'branch_perwakilan_ids' : 'product_ids'}[${idx}]`) || []).length === (elementTags.list || []).length}
                >
                  <span className="font-weight-bold d-flex align-items-center">
                    <Icon type="unordered-list" className="mr-2" />
                    Select All
                  </span>
                </Select.Option>
                {
                  (elementTags.list || []).map(opt => (
                    <Select.Option key={opt.id} value={opt.id}>{opt[isBranch ? 'name' : 'display_name']}</Select.Option>
                  ))
                }
              </Select>,
            )}
            {(() => {
              switch (group) {
                case BRANCH_ID:
                  return (
                    <Form.Item label="NIK Atasan">
                      {getFieldDecorator(`${isBranch ? (getFieldValue('role_id') === 32 ? `head_user[${idx}]` : 'head_user') : 'head_user'}`, {
                        initialValue: !isEmpty(initValue.nik) ? initValue.nik.head_user.nik : '',
                        rules: [
                          { pattern: new RegExp('^[a-zA-Z0-9]{1,20}$'), message: 'Cannot input with special character and longer than 20 character' },
                          { required: true, message: 'NIK Is Required' },
                        ],
                        getValueFromEvent: (e) => {
                          setErrorsField({
                            ...errorsField,
                            head_user: undefined,
                          })
                          return e.target.value
                        },
                      })(
                        <Input placeholder="Input NIK Atasan" disabled={isBranch} />,
                      )}
                    </Form.Item>
                  )
                case PO_ID:
                  return (
                    !isEmpty(getFieldValue(`product_ids[${idx}]`)) && (
                      getFieldValue(`product_ids[${idx}]`).map((id) => {
                        const dataNik = !isEmpty(initValue.nik) ? (!isEmpty(initValue.nik.list) ? initValue.nik.list.filter(idp => idp.id === id) : []) : []
                        const dataCheck = dataNik[0]

                        return (
                          <Form.Item label={`NIK Atasan ${!isEmpty(dataCheck) ? !isEmpty(dataCheck.head_user) ? dataCheck.display_name : '' : ''}`}>
                            {getFieldDecorator(`head_user[${id}]`, {
                              initialValue: !isEmpty(dataCheck) ? ((dataCheck.head_user) ? dataCheck.head_user.nik : '') : '',
                              getValueFromEvent: (e) => {
                                setErrorsField({
                                  ...errorsField,
                                  head_user: undefined,
                                })
                                return e.target.value
                              },
                            })(
                              <Input placeholder="Input NIK Atasan" disabled={isPO} />,
                            )}
                          </Form.Item>
                        )
                      })
                    )
                  )
                default:
                  return (
                    <Form.Item label="NIK Atasan">
                      {getFieldDecorator('head_user', {
                        initialValue: !isEmpty(initValue.nik) ? initValue.nik.head_user.nik : '',
                        rules: [
                          { pattern: new RegExp('^[a-zA-Z0-9]{1,20}$'), message: 'Cannot input with special character and longer than 20 character' },
                          { required: true, message: 'NIK Is Required' },
                        ],
                        getValueFromEvent: (e) => {
                          setErrorsField({
                            ...errorsField,
                            head_user: undefined,
                          })
                          return e.target.value
                        },
                      })(
                        <Input placeholder="Input NIK Atasan" />,
                      )}
                    </Form.Item>
                  )
              }
            })()}
            {((getFieldValue('keys') || []).length > 1)
              ? (
                <Icon
                  type="delete"
                  onClick={() => removeCard(idx)}
                />
              ) : null
            }
          </Form.Item>
        </React.Fragment>
      )
    })
  }

  const prefixSelector = getFieldDecorator('phone_code_id', {
    initialValue: !isEmpty(detailUser) ? (detailUser.phone_code) ? (detailUser.phone_code.phone_code) : '+62' : '+62',
  })(
    <Select style={{ width: 70 }} showSearch>
      {(stateCountry.list.slice(0, 1) || []).map(item => (
        <Select.Option key={Math.random()} value={item.phone_code}>{item.phone_code}</Select.Option>
      ))}
    </Select>,
  )

  return (
    <Card
      title={`Form ${isEdit ? 'Edit' : 'Add'} User`}
      extra={(
        match.params.id && (
          <Button
            shape="round"
            onClick={() => handleResendVerify(match.params.id)}
          >
            Resend Verification
          </Button>
        )
      )}
    >
      {isFetching && <Loader />}
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          className="mb-3"
        />
      )}
      <Form {...formItemLayout} onSubmit={handleSubmit}>
        <Form.Item label="Profile Picture">
          {getFieldDecorator('profile_pic_file', {
            rules: [{ required: !isEdit, message: 'Profile Picture Is Required' }],
          })(
            <Upload
              name="profile_pic_file"
              listType="picture-card"
              className="avatar-uploader banner-content"
              showUploadList={false}
              beforeUpload={() => false}
              onChange={info => handleUpload(info)}
            >
              {avatarFileList
                ? (
                  <Avatar src={avatarFileList} shape="square" className="banner-preview" />
                )
                : (
                  <div>
                    <p className="ant-upload-drag-icon">
                      <Icon type="inbox" style={{ fontSize: '24px' }} />
                    </p>
                    <p className="ant-upload-text">Upload Profile Picture</p>
                  </div>
                )
              }
            </Upload>,
          )}
        </Form.Item>
        <Form.Item label="Role Group">
          {getFieldDecorator('role_group_id', {
            initialValue: detailUser.role ? detailUser.role.group.id : undefined,
            rules: [{ required: true, message: 'Role Group Is Required' }],
            getValueFromEvent: (val) => {
              handleSelect(val, 'role-group')

              return val
            },
          })(
            <Select
              loading={loadGroupRole}
              disabled={isEmpty(selectGroupOption)}
              placeholder="Select Role Group"
            >
              {selectGroupOption.map(item => <Select.Option key={item.id} value={item.id}>{item.display_name.split('-').join(' ').toUpperCase()}</Select.Option>)}
            </Select>,
          )}
        </Form.Item>
        <Form.Item label="Role">
          {getFieldDecorator('role_id', {
            initialValue: detailUser.role ? detailUser.role.id : undefined,
            rules: [{ required: true, message: 'Role Is Required' }],
            getValueFromEvent: (val) => {
              handleSelect(val, 'role')
              handlePermission(val)
              return val
            },
          })(
            <Select
              loading={loadRole}
              disabled={isEmpty(selectOption)}
              placeholder="Select Role"
            >
              {selectOption.map(item => <Select.Option key={item.id} value={item.id}>{item.display_name.split('-').join(' ').toUpperCase()}</Select.Option>)}
            </Select>,
          )}
        </Form.Item>
        {(getFieldValue('role_group_id') === 8 || isBranch) && (
          <Row style={{ marginBottom: '24px' }}>
            <Col xs={24} sm={5} />
            <Col xs={24} sm={12}>
              <Card className="child-item">
                {fieldItems}
                <div className="d-flex align-items-center" style={{ marginTop: '24px', borderTop: '0.5px solid #ddd' }}>
                  {(getFieldValue('role_group_id') === 8 || getFieldValue('role_id') === 32) && (
                    <Form.Item className="mr-3">
                      <Button
                        shape="round"
                        onClick={() => addCard()}
                      >
                        <Icon type="plus" />
                        {` Add ${getFieldValue('role_id') === 32 ? 'Branch' : 'COB'}`}
                      </Button>
                    </Form.Item>
                  )}
                </div>
              </Card>
            </Col>
          </Row>
        )}
        <Form.Item label="NIK" {...errorsField.nik}>
          {getFieldDecorator('nik', {
            initialValue: detailUser.nik || '',
            rules: [
              { pattern: new RegExp('^[a-zA-Z0-9., ]+$'), message: 'Cannot input with special character' },
              { required: true, message: 'NIK Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                nik: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input NIK" maxLength={20} />,
          )}
        </Form.Item>
        <Form.Item label="Name" {...errorsField.name}>
          {getFieldDecorator('name', {
            initialValue: detailUser.name || '',
            rules: [
              { required: true, message: 'Name Is Required' },
              { pattern: new RegExp('^[a-zA-Z ]+$'), message: 'Name is not alphabet' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                name: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input Name" />,
          )}
        </Form.Item>
        <Form.Item label="Email" {...errorsField.email}>
          {getFieldDecorator('email', {
            initialValue: detailUser.email || '',
            rules: [
              { type: 'email', message: 'Email Is Not Valid' },
              { required: true, message: 'Email Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                email: undefined,
              })
              return e.target.value
            },
          })(
            <Input placeholder="Input Email" />,
          )}
        </Form.Item>
        <Form.Item label="Phone Number">
          {getFieldDecorator('phone_number',
            {
              initialValue: detailUser ? detailUser.phone_number : '',

              rules: [
                ...Helper.fieldRules(['required'], 'Phone number'),
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    if (!(/^[0-9+]+$/).test(value)) {
                      return Promise.reject('*Phone number harus menggunakan angka')
                    }

                    if (value.charAt(0) == 0) {
                      return Promise.reject('*Cannot input first number with "0"')
                    }

                    if (value.length < 8) {
                      return Promise.reject('*Minimal 8 digit')
                    }

                    return Promise.resolve()
                  },
                },
              ],
            })(
            <Input
              className="field-lg"
              maxLength={16}
              size="large"
              autoComplete="off"
              addonBefore={prefixSelector}
              placeholder="Input Phone Number"
            />,
          )}
        </Form.Item>

        {getFieldValue('role_id') && (
          <Form.Item label="Permission" {...errorsField.permission_ids}>
            {getFieldDecorator('permission_ids', {
              initialValue: detailUser.permissions ? detailUser.permissions.map(o => o.id) : statePermission.list.map(o => o.id),
            })(
              <Checkbox.Group onChange={onChange}>
                <Row gutter={[12, 12]}>
                  {statePermission.list.map(item => (
                    <Col xs={24} md={12} sm={24} key={`chkb_${item.id}`}>
                      <Checkbox value={item.id} disabled={!!['auth-login', 'forgot-password', 'change-password', 'signout', 'password-update', 'password-menu'].includes(item.name)}>{capitalize((item.name).split('-').join(' '))}</Checkbox>
                    </Col>
                  ))}
                </Row>
              </Checkbox.Group>,
            )}
          </Form.Item>
        )}
        <Form.Item>
          <Button type="primary" htmlType="submit">{!isEdit ? 'Add User' : 'Submit Change'}</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormUser.propTypes = {
  handleSubmit: PropTypes.func,
  handleUpload: PropTypes.func,
  avatarFileList: PropTypes.any,
  handleSelect: PropTypes.func,
  selectOption: PropTypes.array,
  loadRole: PropTypes.bool,
  selectGroupOption: PropTypes.array,
  loadGroupRole: PropTypes.bool,
  selectCOBOption: PropTypes.array,
  loadCOB: PropTypes.bool,
  selectBranchesOption: PropTypes.array,
  loadBranches: PropTypes.bool,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  isFetching: PropTypes.bool,
  errorMessage: PropTypes.string,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  stateBranches: PropTypes.array,
  stateProducts: PropTypes.array,
  currentState: PropTypes.object,
  removeCard: PropTypes.func,
  addCard: PropTypes.func,
  addAll: PropTypes.func,
  getBranches: PropTypes.func,
  handlePermission: PropTypes.func,
  productType: PropTypes.object,
  statePermission: PropTypes.object,
  stateCountry: PropTypes.object,
  detailUser: PropTypes.object,
  checkedList: PropTypes.array,
  onChange: PropTypes.func,
  checkAll: PropTypes.bool,
  match: PropTypes.object,
  isVerifyOtp: PropTypes.object,
  handleResendVerify: PropTypes.func,
  onCheckAllChange: PropTypes.func,
}

export default FormUser

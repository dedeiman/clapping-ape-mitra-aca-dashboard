import PropTypes from 'prop-types'
import {
  Card, Table, Popconfirm,
  Button, Avatar, Tag,
  Alert, Input, Row, Col,
  Select,
} from 'antd'
import history from 'utils/history'

const UserPage = ({
  handleDelete, handleSearch,
  setFilter, stateFilter,
  metaUser, dataUser,
  isFetching, handlePage,
  errorMessage, closeError,
  keyword, currentPage, setKeyword,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'Avatar',
      dataIndex: 'profile_pic',
      key: 'profile_pic',
      width: 100,
      render: text => (
        text
          ? <Avatar shape="square" size={50} src={text} className="img-contain icon-error"><img src="/assets/avatar-on-error.jpg" alt="on-error" /></Avatar>
          : <Avatar shape="square" size={50} src="/assets/avatar-on-error.jpg" className="img-contain" />
      ),
    },
    {
      title: 'NIK',
      dataIndex: 'nik',
      key: 'nik',
      width: 150,
      render: text => text || '-',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => (record.display_name || text).split('-').join(' ').toUpperCase(),
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      render: text => text || '-',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phone_number',
      key: 'phone_number',
      width: 150,
      render: text => text || '-',
    },
    {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
      width: 100,
      render: (text, record) => (record.role.display_name || text.display_name).split('-').join(' ').toUpperCase(),
    },
    {
      title: 'Created at',
      dataIndex: 'created_at',
      key: 'created_at',
      width: 170,
      render: text => (text || '-'),
    },
    {
      title: 'Last Login',
      dataIndex: 'last_login',
      key: 'last_login',
      width: 170,
      render: text => (text || '-'),
    },
    {
      title: 'Token Reset Exp',
      dataIndex: 'token_reset_expired',
      key: 'token_reset_expired',
      width: 170,
      render: text => (text || '-'),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      width: 100,
      render: text => <Tag>{text || '-'}</Tag>,
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'action',
      className: '',
      width: 110,
      render: (text, record) => (
        <Button.Group>
          <Button onClick={() => history.push(`/user/${record.id}/edit`)} className="px-2">
            <i className="lar la-edit text-primary" style={{ fontSize: '20px' }} />
          </Button>
          <Popconfirm
            title="Are you sure delete this user?"
            okText="Yes"
            cancelText="No"
            placement="topLeft"
            onConfirm={() => handleDelete(record.id)}
          >
            <Button className="px-2">
              <i className="las la-trash text-danger" style={{ fontSize: '20px' }} />
            </Button>
          </Popconfirm>
        </Button.Group>
      ),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="space-between" align="bottom">
          <Col xs={24} md={20}>
            <h4 className="mb-3 text-primary2 upp">User List</h4>
            <div className="d-flex align-items-center">
              <Select
                value={stateFilter.type}
                style={{ width: '200px' }}
                onChange={(val) => {
                  setFilter({ ...stateFilter, type: val, prefix: '+62' })
                  setKeyword('')
                }}
              >
                <Select.Option value="name">Name</Select.Option>
                <Select.Option value="email">Email</Select.Option>
                <Select.Option value="status">Status</Select.Option>
                <Select.Option value="role">Role</Select.Option>
                <Select.Option value="nik">NIK</Select.Option>
                <Select.Option value="phone_number">Phone Number</Select.Option>
              </Select>
              {stateFilter.type === 'phone_number'
                ? (
                  <Input.Group compact className="ml-3">
                    <Select value={stateFilter.prefix} onChange={val => setFilter({ ...stateFilter, prefix: val })}>
                      {(stateFilter.countries || []).map(item => (
                        <Select.Option value={item.phone_code}>{item.phone_code}</Select.Option>
                      ))}
                    </Select>
                    <Input.Search
                      allowClear
                      value={keyword}
                      placeholder="Search..."
                      onChange={(e) => {
                        const { value } = e.target
                        if (stateFilter.prefix === '+62') {
                          if (value === '' || /^[1-9][0-9]{0,16}$/.test(value)) {
                            setKeyword(value)
                          }
                        } else if (value === '' || /^[0-9]{0,16}$/.test(value)) {
                          setKeyword(value)
                        }
                      }}
                      onSearch={handleSearch}
                      className="w-md-50"
                    />
                  </Input.Group>
                ) : (
                  <Input.Search
                    allowClear
                    placeholder="Search..."
                    value={keyword}
                    onChange={(e) => {
                      const { value } = e.target
                      if (stateFilter.type === 'nik' && (value === '' || /^[a-zA-Z0-9]/.test(value))) {
                        setKeyword(value)
                      } else if (stateFilter.type === 'email' && (value === '' || /^[a-zA-Z0-9@._+-]+$/.test(value))) {
                        setKeyword(value)
                      } else if (value === '' || /^[a-zA-Z- ]+$/.test(value)) {
                        setKeyword(value)
                      }
                    }}
                    onSearch={handleSearch}
                    className="w-md-50 ml-3"
                  />
                )
              }
            </div>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="bg-blue float-md-right mt-3 mt-md-0"
              onClick={() => history.push('/user/add')}
            >
              Add User
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}
      <Table
        bordered
        rowKey="id"
        columns={columns}
        dataSource={dataUser}
        loading={isFetching}
        scroll={{ x: 2000 }}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaUser ? metaUser.total_count : dataUser.length,
          current: currentPage || 1,
          onChange: handlePage,
          simple: isMobile,
        }}
      />
    </Card>
  )
}

UserPage.propTypes = {
  isFetching: PropTypes.bool,
  dataUser: PropTypes.array,
  metaUser: PropTypes.object,
  handlePage: PropTypes.func,
  handleDelete: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleSearch: PropTypes.func,
  keyword: PropTypes.string,
  currentPage: PropTypes.number,
  setKeyword: PropTypes.func,
  stateFilter: PropTypes.object,
  setFilter: PropTypes.func,
}

export default UserPage

import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Row, Col,
} from 'antd'
import history from 'utils/history'
import { Loader } from '../../elements'

const FormPassword = ({
  isFetching, form,
  handleSubmit, errorsField,
  setErrorsField, onVerify,
}) => {
  const { getFieldDecorator } = form

  return (
    <Row>
      <Col xs={24} md={12}>
        <Card title="Change Password">
          {isFetching && <Loader />}
          <Form
            onSubmit={handleSubmit}
          >
            <Row gutter={24}>
              <Col span={24}>
                <Form.Item label="Old Password" {...errorsField.old_password}>
                  {getFieldDecorator('old_password', {
                    rules: [
                      { required: true },
                      // { pattern: /^.{8,50}$/, message: '*Field must consist of 8 to 50 characters' },
                      // { pattern: /(?=.*[A-Z])/, message: '*Field must contain at least 1 uppercase letter' }
                    ],
                    getValueFromEvent: (e) => {
                      setErrorsField({
                        ...errorsField,
                        old_password: undefined,
                      })
                      onVerify(e.target.value)
                      return e.target.value
                    },
                  })(
                    <Input.Password placeholder="Input Ol Password" />,
                  )}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label="New Password">
                  {getFieldDecorator('password', {
                    rules: [
                      { required: true },
                      { pattern: /^.{8,50}$/, message: '*Field must consist of 8 to 50 characters' },
                      { pattern: /(?=.*[a-z])/, message: '*Field must contain at least 1 lowercase letter' },
                    ],
                  })(
                    <Input.Password placeholder="Input New Password" />,
                  )}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label="Confirm New Password">
                  {getFieldDecorator('password_confirmation', {
                    rules: [
                      { required: true },
                      { pattern: /^.{8,50}$/, message: '*Field must consist of 8 to 50 characters' },
                      { pattern: /(?=.*\d)/, message: '*Field must contain at least 1 number' },
                    ],
                  })(
                    <Input.Password placeholder="Input Password Confirmation" />,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Form.Item className="text-right">
              <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
              <Button type="primary" htmlType="submit">Submit</Button>
            </Form.Item>
          </Form>
        </Card>
      </Col>
    </Row>
  )
}

FormPassword.propTypes = {
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isFetching: PropTypes.bool,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  onVerify: PropTypes.func,
}

export default FormPassword

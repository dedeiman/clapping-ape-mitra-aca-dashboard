import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'antd'
import PrimaryPassword from 'containers/pages/password/PrimaryPassword'
import SecondaryPassword from 'containers/pages/password/SecondaryPassword'

const Password = ({ isSecondary, toggleSecondary }) => (
  <React.Fragment>
    <Row gutter={24} className="mb-5">
      <Col span={24} className="d-flex justify-content-between align-items-center">
        <h4 className="text-primary2">Change Password</h4>
      </Col>
    </Row>
    <Row gutter={24} justify="center" className="mb-5">
      <Col xs={24} md={12}>
        <PrimaryPassword isSecondary={isSecondary} toggleSecondary={toggleSecondary} />
      </Col>

      {isSecondary && (
      <Col xs={24} md={12}>
        <SecondaryPassword />
      </Col>
      )}
    </Row>
  </React.Fragment>
)

Password.propTypes = {
  isSecondary: PropTypes.bool,
  toggleSecondary: PropTypes.func,
}

export default Password

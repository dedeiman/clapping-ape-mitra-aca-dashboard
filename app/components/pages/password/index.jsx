import PropTypes from 'prop-types'
import PasswordChange from 'containers/pages/password/Password'
import FirstPassword from 'containers/pages/password/FirstPassword'

const ProfileRoutes = ({
  match,
  location,
}) => {
  if (location.hash === '#first') {
    return <FirstPassword match={match} location={location} />
  } else {
    return <PasswordChange match={match} location={location} />
  }
}

ProfileRoutes.propTypes = {
  location: PropTypes.object,
  match: PropTypes.object,
}

export default ProfileRoutes

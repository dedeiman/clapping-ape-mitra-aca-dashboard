/* eslint-disable prefer-promise-reject-errors */
import React from 'react'
import PropTypes from 'prop-types'
import { Prompt } from 'react-router-dom'
import { Form } from '@ant-design/compatible'
import { LoadingOutlined } from '@ant-design/icons'
import {
  Row, Col,
  Input, Button,
  Card, Alert,
} from 'antd'
import Helper from 'utils/Helper'

const PrimaryPasswordChange = ({
  onSubmit, form, isLoading,
  isBlocking, handleBlockNavigation,
  onVerify, stateError, setStateError,
}) => {
  const { getFieldDecorator } = form

  return (
    <React.Fragment>
      <Row gutter={24} className="mb-5">
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="text-primary2">Change Password</div>
        </Col>
      </Row>
      <Card>
        <Form onSubmit={onSubmit}>
          <Row gutter={24} justify="center" className="mb-5">
            <Col xs={24} md={12}>
              <Card className="h-100 shadow-none">
                <p className="title-card mb-4">Primary Password</p>
                <p className="font-weight-bold mb-0">Password Anda Saat Ini</p>
                <Form.Item {...stateError.old_password}>
                  {getFieldDecorator('old_password', {
                    rules: [
                      ...Helper.fieldRules(['required'], 'Password'),
                      { pattern: /^.{0,50}$/, message: '*Maksimal 50 karakter' },
                    ],
                    initialValue: undefined,
                    getValueFromEvent: (e) => {
                      setStateError({
                        ...stateError,
                        old_password: undefined,
                      })
                      onVerify((e.target.value), true)
                      return e.target.value
                    },
                  })(
                    <Input.Password
                      placeholder="Masukan Password..."
                      className="field-lg mb-4"
                      iconRender={visible => (
                        visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                      )}
                    />,
                  )}
                </Form.Item>
                <p className="font-weight-bold mb-0">Password Baru Anda</p>
                <Form.Item>
                  {getFieldDecorator('password', {
                    rules: [
                      ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'New Password'),
                      {
                        validator: (rule, value) => {
                          // eslint-disable-next-line prefer-promise-reject-errors
                          if (value && value.length < 8) return Promise.reject('*Minimal 8 karakter')
                          // eslint-disable-next-line prefer-promise-reject-errors
                          if (value && value.includes(' ')) return Promise.reject('*Opps.. Password mengandung space')
                          // eslint-disable-next-line prefer-promise-reject-errors
                          return Promise.resolve()
                        },
                      },
                    ],
                    initialValue: undefined,
                  })(
                    <Input.Password
                      placeholder="Password Baru"
                      className="field-lg mb-4"
                      maxLength={50}
                      iconRender={visible => (
                        visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                      )}
                    />,
                  )}
                </Form.Item>
                <p className="font-weight-bold mb-0">Konfirmasi Password</p>
                <Form.Item>
                  {getFieldDecorator('password_confirmation', {
                    initialValue: undefined,
                    rules: [
                      ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'Confirmation Password'),
                      {
                        validator: (rule, value) => {
                          if (value && value.length < 8) return Promise.reject('*Minimal 8 karakter')
                          if (value && value.includes(' ')) return Promise.reject('*Opps.. Password mengandung space')

                          return Promise.resolve()
                        },
                      },
                    ],
                  })(
                    <Input.Password
                      placeholder="Konfirmasi Password"
                      className="field-lg mb-4"
                      maxLength={50}
                      iconRender={visible => (
                        visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                      )}
                    />,
                  )}
                </Form.Item>
                {stateError.message && (
                  <Alert
                    message=""
                    description={stateError.message}
                    type="error"
                    closable
                    // onClose={onCloseError}
                    className="mb-3 text-left"
                  />
                )}
              </Card>
            </Col>
            <Col xs={24} md={12}>
              <Card className="h-100 shadow-none">
                <p className="title-card mb-4">Secondary Password</p>
                <p className="font-weight-bold mb-0">Password Anda Saat Ini</p>
                <Form.Item {...stateError.old_alternative_password}>
                  {getFieldDecorator('old_alternative_password', {
                    initialValue: undefined,
                    rules: [
                      ...Helper.fieldRules(['required'], 'Password'),
                      { pattern: /^.{0,50}$/, message: '*Maksimal 50 karakter' },
                    ],
                    getValueFromEvent: (e) => {
                      setStateError({
                        ...stateError,
                        old_alternative_password: undefined,
                      })
                      onVerify(e.target.value)
                      return e.target.value
                    },
                  })(
                    <Input.Password
                      placeholder="Masukan Password..."
                      className="field-lg mb-4"
                      iconRender={visible => (
                        visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                      )}
                    />,
                  )}
                </Form.Item>
                <p className="font-weight-bold mb-0">Password Baru Anda</p>
                <Form.Item>
                  {getFieldDecorator('alternative_password', {
                    rules: [
                      ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'New Password'),
                      {
                        validator: (rule, value) => {
                          if (value && value.length < 8) return Promise.reject('*Minimal 8 karakter')
                          if (value && value.includes(' ')) return Promise.reject('*Opps.. Password mengandung space')
                          return Promise.resolve()
                        },
                      },
                    ],
                    initialValue: undefined,
                  })(
                    <Input.Password
                      placeholder="Password Baru"
                      className="field-lg mb-4"
                      maxLength={50}
                      iconRender={visible => (
                        visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                      )}
                    />,
                  )}
                </Form.Item>
                <p className="font-weight-bold mb-0">Konfirmasi Password</p>
                <Form.Item>
                  {getFieldDecorator('alternative_password_confirmation', {
                    rules: [
                      ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'Confirmation Password'),
                      {
                        validator: (rule, value) => {
                          if (value && value.length < 8) return Promise.reject('*Minimal 8 karakter')
                          if (value && value.includes(' ')) return Promise.reject('*Opps.. Password mengandung space')
                          return Promise.resolve()
                        },
                      },
                    ],
                    initialValue: undefined,
                  })(
                    <Input.Password
                      placeholder="Konfirmasi Password"
                      className="field-lg mb-4"
                      maxLength={50}
                      iconRender={visible => (
                        visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                      )}
                    />,
                  )}
                </Form.Item>
                {stateError.message && (
                  <Alert
                    message=""
                    description={stateError.message}
                    type="error"
                    closable
                    // onClose={onCloseError}
                    className="mb-3 text-left"
                  />
                )}
              </Card>
            </Col>
          </Row>
          <Form.Item>
            <Button type="primary button-lg float-right" htmlType="submit" disabled={isLoading}>
              Ubah Password
              {isLoading && <LoadingOutlined className="ml-2" />}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </React.Fragment>
  )
}

PrimaryPasswordChange.propTypes = {
  groupRole: PropTypes.object,
  location: PropTypes.object,
  form: PropTypes.any,
  isLoading: PropTypes.bool,
  onVerify: PropTypes.func,
  stateError: PropTypes.object,
  setStateError: PropTypes.func,
  onSubmit: PropTypes.func,
  isBlocking: PropTypes.bool,
  handleBlockNavigation: PropTypes.func,
}

export default PrimaryPasswordChange

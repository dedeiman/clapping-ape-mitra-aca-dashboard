import React from 'react'
import PropTypes from 'prop-types'
import {
  Form, Divider,
  Input, Button,
  Card, Alert,
  Modal,
} from 'antd'

const SecondaryPasswordChange = ({
  onSubmit, form, onVerify,
  isVisible, toggleVisible,
  currentUser, sendEmail, fetchForgot,
  isLoading, stateError,
}) => {
  const { getFieldDecorator } = form
  return (
    <Card className="h-100">
      <Form onSubmit={onSubmit}>
        <p className="title-card mb-4">Secondary Password</p>
        <p className="font-weight-bold">Password Anda Saat Ini</p>
        <Form.Item {...stateError.old_alternative_password}>
          {getFieldDecorator('old_alternative_password', {
            rules: [
              { required: true, message: '*Wajib diisi' },
              { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' },
            ],
            initialValue: undefined,
          })(
            <Input.Password
              placeholder="Masukan Password..."
              className="field-lg"
              maxLength={50}
              iconRender={visible => (
                visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
              )}
              onChange={async (e) => {
                await onVerify(e.target.value)
              }}
            />,
          )}
        </Form.Item>
        <p className="font-weight-bold">Password Baru Anda</p>
        <Form.Item>
          {getFieldDecorator('alternative_password', {
            rules: [
              { required: true, message: '*Wajib diisi' },
              { pattern: /(?=.*[A-Z])/, message: '*Wajib memiliki minimal 1 huruf besar' },
              { pattern: /(?=.*[a-z])/, message: '*Wajib memiliki minimal 1 huruf kecil' },
              { pattern: /(?=.*\d)/, message: '*Wajib memiliki minimal 1 angka' },
              { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' }, {
                validator: (rule, value) => {
                  // eslint-disable-next-line prefer-promise-reject-errors
                  if (value && value.includes(' ')) return Promise.reject('*Tidak boleh menggunakan space')
                  // eslint-disable-next-line prefer-promise-reject-errors
                  return Promise.resolve()
                },
              },
            ],
            initialValue: undefined,
          })(
            <Input.Password
              placeholder="Password Baru"
              className="field-lg"
              maxLength={50}
              iconRender={visible => (
                visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
              )}
            />,
          )}
        </Form.Item>
        <p className="font-weight-bold">Konfirmasi Password</p>
        <Form.Item>
          {getFieldDecorator('alternative_password_confirmation', {
            rules: [
              { required: true, message: '*Wajib diisi' },
              { pattern: /(?=.*[A-Z])/, message: '*Wajib memiliki minimal 1 huruf besar' },
              { pattern: /(?=.*[a-z])/, message: '*Wajib memiliki minimal 1 huruf kecil' },
              { pattern: /(?=.*\d)/, message: '*Wajib memiliki minimal 1 angka' },
              { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' }, {
                validator: (rule, value) => {
                  // eslint-disable-next-line prefer-promise-reject-errors
                  if (value && value.includes(' ')) return Promise.reject('*Tidak boleh menggunakan space')
                  // eslint-disable-next-line prefer-promise-reject-errors
                  return Promise.resolve()
                },
              },
            ],
            initialValue: undefined,
          })(
            <Input.Password
              placeholder="Konfirmasi Password"
              className="field-lg"
              maxLength={50}
              iconRender={visible => (
                visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
              )}
            />,
          )}
        </Form.Item>
        {stateError.message && (
          <Alert
            message="Error"
            description={stateError.message}
            type="error"
            closable
            className="mb-3 text-left"
          />
        )}
        <div className="d-flex justify-content-between align-items-center">
          <Button
            type="link"
            onClick={() => toggleVisible(true)}
          >
            Lupa Second Password
          </Button>
          <Form.Item>
            <Button type="primary button-lg float-right mt-4" htmlType="submit" disabled={isLoading}>
              Ubah Second Password
            </Button>
          </Form.Item>
        </div>
        <Divider />
        <p>Second Password is used for some menu that needed this password</p>
      </Form>
      <Modal
        title={<p className="mb-0 title-card">Lupa second password Anda, kami akan bantu</p>}
        visible={isVisible}
        footer={null}
        closable={false}
        onCancel={() => toggleVisible(false)}
      >
        <Input
          disabled
          size="large"
          value={currentUser.email}
        />
        <div className="mt-4 d-flex justify-content-end align-items-center">
          <Button
            type="primary"
            onClick={() => sendEmail()}
            disabled={fetchForgot}
          >
            Kirim Link
          </Button>
          <Button
            type="default"
            className="ml-3"
            onClick={() => toggleVisible(false)}
          >
            Cancel
          </Button>
        </div>
      </Modal>
    </Card>
  )
}

SecondaryPasswordChange.propTypes = {
  form: PropTypes.any,
  isLoading: PropTypes.bool,
  onSubmit: PropTypes.func,
  stateError: PropTypes.object,
  setStateError: PropTypes.func,
  onVerify: PropTypes.func,
  currentUser: PropTypes.object,
  sendEmail: PropTypes.func,
  isVisible: PropTypes.bool,
  toggleVisible: PropTypes.func,
  fetchForgot: PropTypes.bool,
}

export default SecondaryPasswordChange

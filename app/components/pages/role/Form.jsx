import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input, Button,
  Alert, Checkbox, Select,
  Col, Row,
} from 'antd'
import { Loader } from 'components/elements'
import { capitalize } from 'lodash'

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 5,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 12,
    },
  },
}

const FormRole = ({
  currentData, handlePermission,
  handleSubmit, form,
  isEdit, isFetching, errorMessage,
  onChange, errorsField, setErrorsField, statePermission, stateGroup,
}) => {
  const { getFieldDecorator, getFieldValue } = form

  return (
    <Card title={`Form ${isEdit ? 'Edit' : 'Add'} Role`}>
      {isFetching && <Loader />}
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          className="mb-3"
        />
      )}
      <Form {...formItemLayout} onSubmit={handleSubmit}>
        <Form.Item label="Name" {...errorsField.name}>
          {getFieldDecorator('name', {
            initialValue: (currentData.display_name || currentData.name) || '',
            rules: [
              { required: true, message: 'Name Is Required' },
            ],
            getValueFromEvent: (e) => {
              setErrorsField({
                ...errorsField,
                name: undefined,
              })
              return e.target.value
            },
          })(
            <Input className="uppercase" placeholder="Input Name" disabled={isEdit} />,
          )}
        </Form.Item>
        {JSON.stringify(currentData.group_id)}
        <Form.Item label="Group" {...errorsField.group_id}>
          {getFieldDecorator('group_id', {
            initialValue: currentData.group ? currentData.group.id : '',
            rules: [
              { required: true, message: 'Group Is Required' },
            ],
            getValueFromEvent: (val) => {
              handlePermission(val)
              return val
            },
          })(
            <Select
              disabled={isEdit}
              showSearch
              placeholder="Group"
            >
              {(stateGroup.list || []).map(item => (
                <Select.Option key={item.id} value={item.id}>{item.display_name.split('-').join(' ').toUpperCase()}</Select.Option>
              ))}
            </Select>,
          )}
        </Form.Item>
        {getFieldValue('group_id') && (
        <Form.Item label="Permission" {...errorsField.permission_ids}>
          {getFieldDecorator('permission_ids', {
            initialValue: currentData.permissions ? currentData.permissions.map(o => o.id) : statePermission.list.map(o => o.id),
          })(
            <Checkbox.Group onChange={onChange}>
              <Row gutter={[12, 12]}>
                {statePermission.list.map(item => (
                  <Col xs={24} md={12} sm={24} key={`chkb_${item.id}`}>
                    <Checkbox value={item.id} disabled={!!['auth-login', 'forgot-password', 'change-password', 'signout', 'password-update', 'password-menu'].includes(item.name)}>{capitalize((item.name).split('-').join(' '))}</Checkbox>
                  </Col>
                ))}
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        )}
        <Form.Item>
          <Button type="primary" htmlType="submit">{!isEdit ? 'Create Role' : 'Submit Change'}</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormRole.propTypes = {
  handleSubmit: PropTypes.func,
  handlePermission: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  isFetching: PropTypes.bool,
  errorMessage: PropTypes.string,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  currentData: PropTypes.object,
  checkedList: PropTypes.array,
  checkAll: PropTypes.bool,
  statePermission: PropTypes.object,
  stateGroup: PropTypes.object,
  onChange: PropTypes.func,
  onCheckAllChange: PropTypes.func,
}

export default FormRole

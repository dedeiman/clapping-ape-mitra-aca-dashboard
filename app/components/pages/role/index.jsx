import PropTypes from 'prop-types'
import {
  Card, Table, Col, Row,
  Button, Alert,
  Popconfirm, Input,
} from 'antd'
import history from 'utils/history'

const RolePage = ({
  isFetching, handlePage,
  metaRole, dataRole,
  handleDelete, errorMessage,
  closeError, handleSearch,
  keyword, currentPage, setKeyword,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'no',
      width: 110,
      render: (text, row, idx) => {
        const numb = idx + 1
        const pageCount = (currentPage && currentPage > 1) ? (currentPage - 1) : ''
        if (numb < 10) return `${pageCount}${numb}`
        if (numb === 10) return `${pageCount + 1}0`
        return numb
      },
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => (record.display_name || text).split('-').join(' ').toUpperCase(),
    },
    {
      title: 'Group',
      dataIndex: '',
      key: '',
      // width: 110,
      render: (text, record) => (record.group.display_name || text).split('-').join(' ').toUpperCase(),
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'action',
      width: 110,
      render: (text, record) => (
        <Button.Group>
          <Button onClick={() => history.push(`/role/${record.id}/edit`)} className="px-2">
            <i className="lar la-edit text-primary" style={{ fontSize: '20px' }} />
          </Button>
        </Button.Group>
      ),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h4 className="mb-3 text-primary2">Role List</h4>
            <Input.Search
              allowClear
              placeholder="Search..."
              value={keyword}
              onChange={(e) => {
                const { value } = e.target
                if (value === '' || /^[a-zA-Z0-9-_+@. ]+$/.test(value)) {
                  setKeyword(value)
                }
              }}
              onSearch={handleSearch}
              className="w-md-50"
            />
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="float-md-right mt-3 mt-md-0"
              onClick={() => history.push('/role/add')}
            >
              Add Role
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}

      <Table
        bordered
        rowKey="id"
        columns={columns}
        dataSource={dataRole}
        loading={isFetching}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaRole ? metaRole.total_count : dataRole.length,
          current: currentPage || 1,
          onChange: handlePage,
          simple: isMobile,
        }}
      />
    </Card>
  )
}

RolePage.propTypes = {
  isFetching: PropTypes.bool,
  dataRole: PropTypes.array,
  metaRole: PropTypes.object,
  handlePage: PropTypes.func,
  handleDelete: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleSearch: PropTypes.func,
  keyword: PropTypes.string,
  currentPage: PropTypes.string,
  setKeyword: PropTypes.func,
}

export default RolePage

/* eslint-disable no-nested-ternary */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Avatar,
  Descriptions, Typography,
  Row, Col, Button, Empty, Divider,
  Select, Modal, Skeleton, Tag,
} from 'antd'
import history from 'utils/history'
import moment from 'moment'
import { isEmpty } from 'lodash'
import Helper from 'utils/Helper'
import { AGENCY_CODE, AGENT_CODE } from 'constants/ActionTypes'
import {DoubleRightOutlined, LeftOutlined} from '@ant-design/icons'
import ChangeStatus from 'containers/pages/customers/ChangeStatus'
import ChangeAgent from 'containers/pages/customers/ChangeAgent'
import CardForm from 'containers/pages/customers/CardForm'

const DetailCustomer = ({
  detailCustomer, stateButton, handleDelete,
  setStateButton, handleDetail, donwloadDocument,
  stateCard, setStateCard, collectDataCard,
  match, stateModalStatus, setStateModalStatus,
  setStateModalAgent, stateModalAgent,
  isFetching, isBlocking, handleUpdateCard,
  groupRole, handleCancelCard, currentUser,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
      <Row gutter={[24, 24]}>
        <Col span={24}>
          <Button
            type="link"
            className="p-0 d-flex align-items-center"
            onClick={() => history.goBack()}
          >
            <Tag color="#2b57b7" size="small" className="px-1 mr-2 rounded">
              <LeftOutlined />
            </Tag>
            <b style={{ fontSize: '18px' }}>Back</b>
          </Button>
        </Col>
      </Row>
      <Row gutter={[24, 24]}>
        <Col span={12} className="d-flex justify-content-between align-items-center">
          <div className="text-primary2">
            Customer
            <span className="text-muted ml-4 mb-0" style={{ fontSize: '18px' }}>{(detailCustomer.name || '').toUpperCase()}</span>
          </div>
        </Col>
        {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
          <Col span={12} className="d-flex justify-content-end align-items-center">
            <Button
              type="link"
              size="large"
              className="d-flex align-items-center"
              onClick={() => setStateModalAgent({ ...stateModalAgent, visible: true })}
            >
              <i className="las la-random mr-2" />
              Perpindahan Nasabah
            </Button>
            <Button
              type="link"
              size="large"
              className="d-flex align-items-center"
              onClick={() => setStateModalStatus({ ...stateModalStatus, visible: true })}
            >
              <i className="las la-exchange-alt mr-2" />
              Change Status
            </Button>
          </Col>
        )}
      </Row>
      <Row gutter={24} className="mb-5">
        <Col span={24} className="mb-4">
          <Card loading={isFetching}>
            <p className="title-card">General Info Customer</p>
            <Descriptions layout="vertical" colon={false} column={4}>
              <Descriptions.Item span={2} label="Profile ID" className="px-1">
                <p>{(detailCustomer.candidate_customer_number || '-').toUpperCase()}</p>
              </Descriptions.Item>
              <Descriptions.Item span={1} label="Name" className="px-1">
                <p>{(detailCustomer.name || '-').toUpperCase()}</p>
              </Descriptions.Item>
              <Descriptions.Item span={1} label="Status" className="px-1">
                <p>{(detailCustomer.status_active || '-').toUpperCase()}</p>
              </Descriptions.Item>
              <Descriptions.Item span={2} label="Type Customer" className="px-1">
                <p>{(detailCustomer.customer_type || '-').toUpperCase()}</p>
              </Descriptions.Item>
              { detailCustomer.customer_type !== 'corporate'
                ? (
                  <Descriptions.Item span={1} label="Day of Birth" className="px-1">
                    <p>{detailCustomer.dob ? moment(detailCustomer.dob).format('DD MMMM YYYY') : '-'}</p>
                  </Descriptions.Item>
                ) : null
              }
              { detailCustomer.customer_type !== 'corporate'
                ? (
                  <Descriptions.Item span={1} label="Birth Place" className="px-1">
                    <p>{detailCustomer.birthplace || '-'}</p>
                  </Descriptions.Item>
                ) : null
              }
              <Descriptions.Item span={2} label="Customer Number" className="px-1">
                <p>
                  {!isEmpty(detailCustomer) ? detailCustomer.customer_number : '-'}
                </p>
              </Descriptions.Item>
              { detailCustomer.customer_type !== 'corporate'
                ? (
                  <Descriptions.Item span={1} label="Gender" className="px-1">
                    <p>{(detailCustomer.gender || '-').toUpperCase()}</p>
                  </Descriptions.Item>
                ) : null
              }
              <Descriptions.Item span={1} label="Kewarganegaraan" className="px-1">
                <p>{(detailCustomer.citizenship || '-').toUpperCase()}</p>
              </Descriptions.Item>
              { detailCustomer.customer_type !== 'corporate'
                ? (
                  <Descriptions.Item span={2} label="Type ID" className="px-1">
                    <p>{(detailCustomer.type_id || '-').toUpperCase()}</p>
                  </Descriptions.Item>
                ) : null
              }
              { detailCustomer.customer_type !== 'corporate'
                ? (
                  <Descriptions.Item span={1} label="Marital Status" className="px-1">
                    <p>{(detailCustomer.martial_status || '-').toUpperCase()}</p>
                  </Descriptions.Item>
                ) : null
              }
              <Descriptions.Item span={1} label="NPWP Number" className="px-1">
                <p>{(detailCustomer.npwp || '-').toUpperCase()}</p>
              </Descriptions.Item>
              { detailCustomer.customer_type !== 'corporate'
                ? (
                  <Descriptions.Item span={1} label="ID Number" className="px-1">
                    <p>{detailCustomer.number_id || '-'}</p>
                  </Descriptions.Item>
                ) : null
              }
              { (detailCustomer.citizenship !== 'wna')
                ? (
                  <Descriptions.Item span={1} label="" className="px-1" />
                ) : (
                  <Descriptions.Item span={1} label="Expiry Date" className="px-1">
                    <p>{!isEmpty(detailCustomer.expiry_date_identity_id) ? moment(detailCustomer.expiry_date_identity_id).format('DD MMMM YYYY') : '-'}</p>
                  </Descriptions.Item>
                )
              }
              <Descriptions.Item span={1} label="Email" className="px-1">
                <p>{(detailCustomer.email || '-').toUpperCase()}</p>
              </Descriptions.Item>
              <Descriptions.Item span={1} label="Phone Number" className="px-1">
                <p>
                  {!isEmpty(detailCustomer) ? !isEmpty(detailCustomer.phone_code) ? detailCustomer.phone_code.phone_code : '0' : '0'}
                  {!isEmpty(detailCustomer) ? !isEmpty(detailCustomer.phone_code) ? ' ' : '' : ''}
                  {!isEmpty(detailCustomer) ? detailCustomer.phone_number.charAt(0) === 0 ? detailCustomer.phone_number : detailCustomer.phone_number.slice(1, (detailCustomer.phone_number).length) : ''}
                </p>
              </Descriptions.Item>
            </Descriptions>
          </Card>
        </Col>
        <Col xs={24} md={12}>
          <Card className="h-100" loading={isFetching}>
            <p className="title-card mb-4">Alamat Customer</p>
            <Descriptions layout="vertical" colon={false} column={2}>
              <Descriptions.Item label="Provinsi" className="px-1">
                <p>{detailCustomer.province ? detailCustomer.province.name : '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Kota" className="px-1">
                <p>{detailCustomer.city ? detailCustomer.city.name : '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Kecamatan" className="px-1">
                <p>{detailCustomer.sub_district ? detailCustomer.sub_district.name : '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Kelurahan" className="px-1">
                <p>{detailCustomer.urban_village ? detailCustomer.urban_village.name : '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Kode Pos" className="px-1" span={2}>
                <p>{detailCustomer.postal_code || '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Latitude" className="px-1">
                <p>{detailCustomer.lat || '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Longitude" className="px-1">
                <p>{detailCustomer.lng || '-'}</p>
              </Descriptions.Item>
              <Descriptions.Item label="Alamat Lengkap" className="px-1" span={2}>
                <Typography.Paragraph ellipsis={{ rows: 4, expandable: true }} className="mb-0">
                  {`${
                    detailCustomer.address || '-'
                  }`}
                  {(!isEmpty(detailCustomer.rt) || !isEmpty(detailCustomer.rt)) && (
                    <p>
                      <span>
                        RT.
                        {' '}
                        {detailCustomer.rt || '-'}
                      </span>
                      <span className="ml-5">
                        RW.
                        {' '}
                        {detailCustomer.rw || '-'}
                      </span>
                    </p>
                  )}
                </Typography.Paragraph>
              </Descriptions.Item>
            </Descriptions>
          </Card>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={0} className="h-100">
            <Col xs={24} className="mb-4">
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Pekerjaan Customer</p>
                <Descriptions layout="vertical" colon={false} column={2}>
                  { detailCustomer.customer_type !== 'corporate'
                    ? (
                      <Descriptions.Item label="Pekerjaan" className="px-1">
                        <p>{((detailCustomer.employment || '-').split('_').join(' ')).toUpperCase()}</p>
                      </Descriptions.Item>
                    ) : null
                  }
                  <Descriptions.Item label="Range Penghasilan" className="px-1">
                    <p>{detailCustomer.income_range ? detailCustomer.income_range.display_name : '-'}</p>
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
            <Col xs={24}>
              <Card className="h-100" loading={isFetching}>
                <p className="title-card">Data PIC</p>
                <Descriptions layout="vertical" colon={false} column={2}>
                  <Descriptions.Item label="Nama PIC" className="px-1">
                    <p>{detailCustomer.pic_name || '-'}</p>
                  </Descriptions.Item>
                  <Descriptions.Item label="PIC Phone" className="px-1">
                    <p>{detailCustomer.pic_phone_number || '-'}</p>
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={24}>
          <Card loading={isFetching}>
            <Row gutter={24}>
              <Col xs={24} md={6} xl={4}>
                {isMobile && (
                  <Select
                    onChange={(val) => {
                      setStateButton({ ...stateButton, active: val })
                      handleDetail(val)
                    }}
                    value={stateButton.active}
                    className="field-lg w-100"
                  >
                    {(stateButton.list || []).map(item => (
                      <Select.Option key={`select_${item}`} value={item}>{item}</Select.Option>
                    ))}
                  </Select>
                )}
                {!isMobile && (stateButton.list || []).map(item => (
                  <Button
                    className="button-profile shadow"
                    key={`button_${item}`}
                    type={stateButton.active === item ? 'primary' : ''}
                    onClick={() => {
                      setStateButton({ ...stateButton, active: item })
                      handleDetail(item)
                    }}
                  >
                    {item}
                    {stateButton.active === item ? <DoubleRightOutlined /> : null}
                  </Button>
                ))}
              </Col>
              <Col xs={24} md={18} xl={20} className="px-0 px-md-5 h-100">
                <div className="d-flex justify-content-between align-items-center">
                  <p className="title-card pt-3 pt-md-0">
                    {((stateButton.active === 'Training') || (stateButton.active === 'Contest'))
                      ? `History ${stateButton.active}`
                      : stateButton.active
                    }
                  </p>
                  {(() => {
                    switch (stateButton.active) {
                      case 'Product':
                      case 'Contract':
                      case 'License':
                      case 'Document':
                      case 'Bank':
                        return (
                          (() => (
                            <div className="d-flex">
                              {(() => {
                                switch (groupRole.code) {
                                  case AGENCY_CODE:
                                    return (
                                      <>
                                        <Button
                                          type="link"
                                          className="p-0 d-flex mr-3 align-items-center"
                                          onClick={() => (
                                            setStateCard({
                                              ...stateCard,
                                              visible: true,
                                              isEdit: false,
                                              data: {},
                                              title: (stateButton.active).toLowerCase(),
                                            })
                                          )}
                                        >
                                          {stateButton.active !== 'Document' && (
                                          <>
                                            <i className="las la-plus-circle mr-1" style={{ fontSize: '24px' }} />
                                            {stateButton.active !== 'Product' ? 'Add New' : 'Restricted'}
                                            {' '}
                                            {stateButton.active}
                                          </>
                                          )}
                                        </Button>
                                        { stateButton.active === 'Document' && (
                                          <Button
                                            ghost
                                            type="primary"
                                            className="p-10 d-flex align-items-center"
                                            onClick={donwloadDocument}
                                          >
                                            Download Document
                                          </Button>
                                        ) }
                                      </>
                                    )
                                  case AGENT_CODE:
                                    return (
                                      <>
                                        <Button
                                          type="link"
                                          className="p-0 d-flex mr-3 align-items-center"
                                          onClick={() => (
                                            setStateCard({
                                              ...stateCard,
                                              visible: true,
                                              isEdit: false,
                                              data: {},
                                              title: (stateButton.active).toLowerCase(),
                                            })
                                          )}
                                        >
                                          {stateButton.active === 'Document' && (
                                            <>
                                              <i className="las la-plus-circle mr-1" style={{ fontSize: '24px' }} />
                                              {stateButton.active !== 'Product' ? 'Add New' : 'Restricted'}
                                              {' '}
                                              {stateButton.active}
                                            </>
                                          )}
                                        </Button>
                                      </>
                                    )
                                  default:
                                }

                                return true
                              })()}
                            </div>
                          ))()
                        )
                      default:
                        return ''
                    }
                  })()}
                </div>
                <Card className="border-0 card-description shadow-none" loading={stateButton.isFetching}>
                  {(
                    ((stateButton.active === 'Policy') && isEmpty(stateButton.policy))
                    || ((stateButton.active === 'Product') && isEmpty(stateButton['restricted-products']))
                    || ((stateButton.active === 'Agent') && isEmpty(stateButton.agents))
                    || ((stateButton.active === 'Document') && isEmpty(stateButton.documents))
                    || ((stateButton.active === 'History') && isEmpty(stateButton.histories))
                  ) && (
                  <Empty description="Tidak ada data" />
                  )}
                  {stateButton.active === 'Policy' && (
                    isFetching.setStateButton
                      ? <Skeleton />
                      : (stateButton.policy || []).map(item => (
                        <React.Fragment key={`policy${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="name" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.product.display_name) || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="nomor polis" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.policy_number) || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="periode pertanggungan" className="px-1 text-uppercase profile-detail pb-0">
                              {item.start_period ? moment(item.start_period).format('DD MMM YYYY') : ''}
                              {' '}
                              -
                              {item.end_period ? moment(item.end_period).format('DD MMM YYYY') : ''}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="agen penutupan" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.product.display_name) || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                          <Divider />
                        </React.Fragment>
                      ))
                  )}

                  {stateButton.active === 'Product' && (
                    isFetching.setStateButton
                      ? <Skeleton />
                      : (stateButton['restricted-products'] || []).map(item => (
                        <React.Fragment key={`policy${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="name" className="px-1 text-uppercase profile-detail pb-0">
                              {item.product ? Helper.getValue(item.product.display_name) : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="restricted date" className="px-1 text-uppercase profile-detail pb-0">
                              {item.restricted_date ? moment(item.restricted_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="un-restricted date" className="px-1 text-uppercase profile-detail pb-0">
                              {item.unrestricted_date ? moment(item.unrestricted_date).format('DD MMMM YYYY') : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="reason" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.reason) || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="status" className="px-1 text-uppercase profile-detail pb-0">
                              {(item.status || '-').toUpperCase()}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
                              <Descriptions.Item span={isMobile ? 5 : 2} label="action" className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  className="p-0"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'product',
                                    })
                                  )}
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                        </React.Fragment>
                      ))
                  )}
                  {stateButton.active === 'Agent' && (
                    isFetching.setStateButton
                      ? <Skeleton />
                      : (stateButton.agents || []).map(item => (
                        <React.Fragment key={`policy${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="agent id" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.agent.agent_id) || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="agent name" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.agent.name) || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="branch" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.agent.branch.name) || '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
                              <Descriptions.Item span={isMobile ? 5 : 1} label="action" className="px-1 text-uppercase profile-detail pb-0">
                                <Button
                                  type="link"
                                  className="p-0"
                                  onClick={() => (
                                    setStateCard({
                                      ...stateCard,
                                      visible: true,
                                      isEdit: true,
                                      data: item,
                                      title: 'agent',
                                    })
                                  )}
                                >
                                  <i className="las la-edit" style={{ fontSize: '24px' }} />
                                </Button>
                              </Descriptions.Item>
                            )}
                          </Descriptions>
                          <Divider />
                        </React.Fragment>
                      ))
                  )}

                  {stateButton.active === 'History' && (
                    isFetching.setStateButton
                      ? <Skeleton />
                      : (stateButton.histories || []).map(item => (
                        <React.Fragment key={`policy${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="tanggal history" className="px-1 text-uppercase profile-detail pb-0">
                              {moment(item.user_action.updated_at).format('DD MMM YYYY') || ''}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="action" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.action) || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 1} label="user action" className="px-1 text-uppercase profile-detail pb-0">
                              {Helper.getValue(item.user_action.name) || '-'}
                            </Descriptions.Item>
                          </Descriptions>
                          <Divider />
                        </React.Fragment>
                      ))
                  )}

                  {stateButton.active === 'Document' && (
                    isFetching.setStateButton
                      ? <Skeleton />
                      : (stateButton.documents || []).map(item => (
                        <React.Fragment key={`policy${item.id}`}>
                          <Descriptions layout="vertical" colon={false} column={5}>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="Image" className="px-1 text-uppercase profile-detail pb-0">
                              <Avatar
                                shape="square"
                                size={150}
                                src={item.file_url}
                                className="img-contain"
                              />
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="kategori" className="px-1 text-uppercase profile-detail pb-0">
                              {item.document_category ? item.document_category.name : '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="deskripsi" className="px-2 text-uppercase profile-detail pb-0">
                              {item.description || '-'}
                            </Descriptions.Item>
                            <Descriptions.Item span={isMobile ? 5 : 2} label="uploaded by" className="px-1 text-uppercase profile-detail pb-0">
                              {!isEmpty(item.uploader) ? item.uploader.name : '-'}
                            </Descriptions.Item>
                            {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
                              <>
                                <Descriptions.Item span={isMobile ? 5 : 2} label="action" className="px-1 text-uppercase profile-detail pb-0">
                                  <Button
                                    type="link"
                                    className="p-0"
                                    onClick={() => (
                                      setStateCard({
                                        ...stateCard,
                                        visible: true,
                                        isEdit: true,
                                        data: item,
                                        title: 'document',
                                      })
                                    )}
                                  >
                                    <i className="las la-edit" style={{ fontSize: '24px' }} />
                                  </Button>
                                </Descriptions.Item>
                                <Descriptions.Item span={isMobile ? 5 : 2} label="delete" className="px-1 text-uppercase profile-detail pb-0">
                                  <Button
                                    type="link"
                                    className="p-0"
                                    onClick={() => handleDelete(item.id)}
                                  >
                                    <i className="las la-trash" style={{ fontSize: '24px' }} />
                                  </Button>
                                </Descriptions.Item>
                              </>
                            )}
                          </Descriptions>
                          <Divider />
                        </React.Fragment>
                      ))
                  )}
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      {(currentUser.permissions && currentUser.permissions.indexOf('customer-detail') > -1) && (
        <Row className="my-3">
          <Col span={18} className="d-flex justify-content-end align-items-center ml-3" />
          <Col className="d-flex justify-content-end align-items-center ml-3">
            <Button
              size="large"
              type="primary"
              disabled={isBlocking}
              onClick={handleUpdateCard}
            >
              Update
            </Button>
          </Col>
          <Col className="d-flex justify-content-end align-items-center ml-3">
            <Button
              size="large"
              type="primary"
              disabled={isBlocking}
              onClick={handleCancelCard}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      )}

      <Modal
        title={<p className="mb-0 title-card">Perpindahan Nasabah</p>}
        visible={stateModalAgent.visible}
        footer={null}
        closable={false}
        width={650}
        onCancel={() => setStateModalAgent({ ...stateModalAgent, visible: false })}
      >
        <ChangeAgent
          toggle={() => setStateModalAgent({ ...stateModalAgent, visible: false })}
          detailCustomer={detailCustomer}
        />
      </Modal>

      <Modal
        title={<p className="mb-0 title-card">Change Status</p>}
        visible={stateModalStatus.visible}
        footer={null}
        closable={false}
        width={650}
        onCancel={() => setStateModalStatus({ ...stateModalStatus, visible: false })}
      >
        <ChangeStatus
          toggle={() => setStateModalStatus({ ...stateModalStatus, visible: false })}
          id={detailCustomer.id}
        />
      </Modal>

      <Modal
        title={<p className="mb-0 title-card text-capitalize">{`${stateCard.isEdit ? 'Edit' : 'Add'} ${stateCard.title}`}</p>}
        visible={stateCard.visible}
        footer={null}
        closable={false}
        onCancel={() => setStateCard({ ...stateCard, isEdit: false, visible: false })}
      >
        <CardForm
          toggle={() => setStateCard({ ...stateCard, visible: false })}
          data={stateCard}
          stateCard={stateButton}
          submitForm={(data, title, isEdit) => collectDataCard(data, title, isEdit)}
          match={match}
        />
      </Modal>
    </React.Fragment>
  )
}

DetailCustomer.propTypes = {
  detailCustomer: PropTypes.object,
  stateButton: PropTypes.object,
  setStateButton: PropTypes.func,
  handleDetail: PropTypes.object,
  stateCard: PropTypes.object,
  setStateCard: PropTypes.func,
  collectDataCard: PropTypes.any,
  match: PropTypes.any,
  handleDelete: PropTypes.func,
  isBlocking: PropTypes.bool,
  handleUpdateCard: PropTypes.func,
  stateModalStatus: PropTypes.object,
  setStateModalStatus: PropTypes.func,
  stateModalAgent: PropTypes.object,
  setStateModalAgent: PropTypes.func,
  donwloadDocument: PropTypes.func,
  isFetching: PropTypes.bool,
  groupRole: PropTypes.object,
  currentUser: PropTypes.object,
  handleCancelCard: PropTypes.func,
}

export default DetailCustomer

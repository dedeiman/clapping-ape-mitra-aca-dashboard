import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Table, Collapse,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse

function callback(key) {
  console.log(key)
}

const columns = [
  {
    title: 'Uang Pertanggungan',
    dataIndex: 'jaminan',
    key: 'jaminan',
    render: (text, row, index) => {
      if (index > 0) {
        return <a>{text}</a>;
      }
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: 1,
        },
      };
    },
  },
  {
    title: 'Comprehensive',
    children: [
      {
        title: 'Wilayah 1 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb1',
            key: 'bb1',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt1',
            key: 'bt1',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba1',
            key: 'ba1',
          },
        ],
      },
      {
        title: 'Wilayah 2 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb2',
            key: 'bb2',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt2',
            key: 'bt2',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba2',
            key: 'ba2',
          },
        ],
      },
      {
        title: 'Wilayah 3 (%)',
        children: [
          {
            title: 'Batas Bawah',
            dataIndex: 'bb3',
            key: 'bb3',
          },
          {
            title: 'Batas Tengah',
            dataIndex: 'bt3',
            key: 'bt3',
          },
          {
            title: 'Batas Atas',
            dataIndex: 'ba3',
            key: 'ba3',
          },
        ],
      },
    ],
  },
]

const columnsBencana = [
  {
    title: '',
    dataIndex: 'bencanaAlam',
    key: 'bencanaAlam',
  },
  {
    title: 'Wilayah 1 (%)',
    children: [
      {
        title: 'Batas Bawah',
        dataIndex: 'bb1',
        key: 'bb1',
      },
      {
        title: 'Batas Tengah',
        dataIndex: 'bt1',
        key: 'bt1',
      },
      {
        title: 'Batas Atas',
        dataIndex: 'ba1',
        key: 'ba1',
      },
    ],
  },
  {
    title: 'Wilayah 2 (%)',
    children: [
      {
        title: 'Batas Bawah',
        dataIndex: 'bb2',
        key: 'bb2',
      },
      {
        title: 'Batas Tengah',
        dataIndex: 'bt2',
        key: 'bt2',
      },
      {
        title: 'Batas Atas',
        dataIndex: 'ba2',
        key: 'ba2',
      },
    ],
  },
  {
    title: 'Wilayah 3 (%)',
    children: [
      {
        title: 'Batas Bawah',
        dataIndex: 'bb3',
        key: 'bb3',
      },
      {
        title: 'Batas Tengah',
        dataIndex: 'bt3',
        key: 'bt3',
      },
      {
        title: 'Batas Atas',
        dataIndex: 'ba3',
        key: 'ba3',
      },
    ],
  },
]

const columnsTPLBus = [
  {
    title: ' ',
    dataIndex: 'tplBus',
    key: 'tplBus',
    width: 500,
  },
  {
    title: 'Rate',
    dataIndex: 'rate',
    key: 'rate',
  },
]

const columnsTPLNonBus = [
  {
    title: ' ',
    dataIndex: 'tplBus',
    key: 'tplBus',
    width: 500,
  },
  {
    title: 'Rate',
    dataIndex: 'rate',
    key: 'rate',
  },
]

const columnsPA = [
  {
    title: 'PA Driver',
    dataIndex: 'pad',
    key: 'pad',
    width: 500,
  },
  {
    title: 'PA Passenger',
    dataIndex: 'pap',
    key: 'pap',
  },
]

const ComprehensiveSetting = ({
                      stateLiability, dataCustomer,
                      isFetching,
                    }) => {
  const isMobile = window.innerWidth < 768
  const ub1 = Object.assign({}, stateLiability.list.comprehensive_tlo_premi_rates)
  const ub2 = Object.assign({}, stateLiability.list.comprehensive_tlo_natural_disaster_rates)
  const ub3 = Object.assign({}, stateLiability.list.comprehensive_tlo_third_party_liability_rates)
  const ub4 = Object.assign({}, stateLiability.list.personal_accident_rates)
  const tpl1 = Object.assign({}, ub3['bus_tjhx'])
  const tpl2 = Object.assign({}, ub3['nonbus_tjh'])
  const r1 = Object.assign({}, ub1['>100jt-125jt'])
  const r2 = Object.assign({}, ub1['>125jt-200jt'])
  const r3 = Object.assign({}, ub1['>200jt-400jt'])
  const r4 = Object.assign({}, ub1['>400jt-800jt'])
  const r5 = Object.assign({}, ub1['>800jt'])
  const r6 = Object.assign({}, ub1['Bus'])
  const r7 = Object.assign({}, ub1['Kendaraan roda dua'])
  const r8 = Object.assign({}, ub1['Truk & Pick up'])
  const b1 = Object.assign({}, ub2.eq)
  const b2 = Object.assign({}, ub2.flood)
  const b3 = Object.assign({}, ub2.rscc)
  const b4 = Object.assign({}, ub2.ts)

  const dataPA = [
    {
      key: '1',
      pad: Object.assign({}, ub4.pad).rate,
      pap: Object.assign({}, ub4.pap).rate,
    },
  ]

  const data = [
    {
      key: '1',
      jaminan: '> 100 s/d 125 juta',
      bb1: Object.assign({}, Object.assign({}, r1['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r1['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r1['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r1['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r1['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r1['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r1['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r1['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r1['area_3'])[2]).rate,
    },
    {
      key: '2',
      jaminan: '> 125 s/d 200 juta',
      bb1: Object.assign({}, Object.assign({}, r2['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r2['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r2['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r2['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r2['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r2['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r2['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r2['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r2['area_3'])[2]).rate,
    },
    {
      key: '3',
      jaminan: '> 200 s/d 400 juta',
      bb1: Object.assign({}, Object.assign({}, r3['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r3['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r3['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r3['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r3['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r3['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r3['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r3['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r3['area_3'])[2]).rate,
    },
    {
      key: '4',
      jaminan: '> 400 s/d 800 juta',
      bb1: Object.assign({}, Object.assign({}, r4['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r4['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r4['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r4['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r4['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r4['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r4['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r4['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r4['area_3'])[2]).rate,
    },
    {
      key: '5',
      jaminan: '> 800 juta',
      bb1: Object.assign({}, Object.assign({}, r5['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r5['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r5['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r5['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r5['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r5['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r5['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r5['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r5['area_3'])[2]).rate,
    },
    {
      key: '6',
      jaminan: 'Bus',
      bb1: Object.assign({}, Object.assign({}, r6['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r6['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r6['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r6['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r6['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r6['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r6['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r6['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r6['area_3'])[2]).rate,
    },
    {
      key: '6',
      jaminan: 'Kendaraan roda dua',
      bb1: Object.assign({}, Object.assign({}, r7['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r7['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r7['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r7['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r7['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r7['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r7['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r7['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r7['area_3'])[2]).rate,
    },
    {
      key: '6',
      jaminan: 'Truk & Pick up',
      bb1: Object.assign({}, Object.assign({}, r8['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, r8['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, r8['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, r8['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, r8['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, r8['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, r8['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, r8['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, r8['area_3'])[2]).rate,
    },
  ]
  const dataBencana = [
    {
      key: '1',
      bencanaAlam: 'Angin Topan, Badai, Hujan Es, Banjir dan atau Tanah Longsor',
      bb1: Object.assign({}, Object.assign({}, b2['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b2['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b2['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b2['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b2['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b2['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b2['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b2['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b2['area_3'])[2]).rate,
    },
    {
      key: '2',
      bencanaAlam: 'Gempa Bumi, Tsunami, dan atau Letusan Gunung Berapi',
      bb1: Object.assign({}, Object.assign({}, b1['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b1['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b1['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b1['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b1['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b1['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b1['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b1['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b1['area_3'])[2]).rate,
    },
    {
      key: '3',
      bencanaAlam: 'Huru-hara dan Kerusuhan (SRCC)',
      bb1: Object.assign({}, Object.assign({}, b3['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b3['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b3['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b3['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b3['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b3['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b3['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b3['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b3['area_3'])[2]).rate,
    },
    {
      key: '4',
      bencanaAlam: 'Terorisme dan Sabotase',
      bb1: Object.assign({}, Object.assign({}, b4['area_1'])[0]).rate,
      bt1: Object.assign({}, Object.assign({}, b4['area_1'])[1]).rate,
      ba1: Object.assign({}, Object.assign({}, b4['area_1'])[2]).rate,
      bb2: Object.assign({}, Object.assign({}, b4['area_2'])[0]).rate,
      bt2: Object.assign({}, Object.assign({}, b4['area_2'])[1]).rate,
      ba2: Object.assign({}, Object.assign({}, b4['area_2'])[2]).rate,
      bb3: Object.assign({}, Object.assign({}, b4['area_3'])[0]).rate,
      bt3: Object.assign({}, Object.assign({}, b4['area_3'])[1]).rate,
      ba3: Object.assign({}, Object.assign({}, b4['area_3'])[2]).rate,
    },
  ]
  const dataTPLBus = [
    {
      key: '1',
      tplBus: 'UP > 25 jt - 50 jt',
      rate: Object.assign({}, tpl1['UP > 25 jt - 50 jt']).rate,
    },
    {
      key: '2',
      tplBus: 'UP > 50 jt - 100 jt',
      rate: Object.assign({}, tpl1['UP > 50 jt - 100 jt']).rate,
    },
    {
      key: '3',
      tplBus: 'UP hingga 25 jt',
      rate: Object.assign({}, tpl1['UP hingga 25 jt']).rate,
    },
  ]
  const dataTPLNonBus = [
    {
      key: '1',
      tplBus: 'UP > 25 jt - 50 jt',
      rate: Object.assign({}, tpl2['UP > 25 jt - 50 jt']).rate,
    },
    {
      key: '2',
      tplBus: 'UP > 50 jt - 100 jt',
      rate: Object.assign({}, tpl2['UP > 50 jt - 100 jt']).rate,
    },
    {
      key: '3',
      tplBus: 'UP hingga 25 jt',
      rate: Object.assign({}, tpl2['UP hingga 25 jt']).rate,
    },
  ]
  return (
      <React.Fragment>
        <Card>
          <Row type="flex" justify="space-between" className="mb-2 mr-4">
            <Col xs={24} md={20}>
              <div className="text-primary2">Product - Comprehensive</div>
            </Col>
            <Col xs={24} md={4}>
              <Button type="primary" className="float-md-right mt-3 mt-md-0">
                Edit Comprehensive
              </Button>
            </Col>
          </Row>
          {isFetching && (
              <Loader />
          )}
          {(!isFetching && isEmpty(dataCustomer)) && (
              <>
                <Table columns={columns} dataSource={data} bordered size="small" pagination={false} className="mb-4" />
                <Collapse defaultActiveKey={['1']} accordion="true" onChange={callback} expandIconPosition="right">
                  <Panel header="Bencana Alam" key="1">
                    <Table columns={columnsBencana} dataSource={dataBencana} pagination={false} bordered size="large" />
                  </Panel>
                </Collapse>
                <Collapse defaultActiveKey={['2']} onChange={callback} className="mt-4" expandIconPosition="right">
                  <Panel header="Persnal Accident" key="2">
                    <Table columns={columnsPA} dataSource={dataPA} pagination={false} bordered size="small" />
                  </Panel>
                </Collapse>
                <Collapse defaultActiveKey={['3']} onChange={callback} className="mt-4" expandIconPosition="right">
                  <Panel header="TPL Bus" key="3">
                    <Table columns={columnsTPLBus} dataSource={dataTPLBus} bordered pagination={false} size="small" />
                  </Panel>
                </Collapse>
                <Collapse defaultActiveKey={['4']} onChange={callback} className="mt-4" expandIconPosition="right">
                  <Panel header="TPL Non Bus" key="4">
                    <Table columns={columnsTPLNonBus} dataSource={dataTPLNonBus} bordered pagination={false} size="small" />
                  </Panel>
                </Collapse>
              </>
          )}
          {!isEmpty(dataCustomer)
              ? (
                  <>
                    <Table columns={columns} dataSource={data} bordered size="small" pagination={false} className="mb-4" />
                    <Collapse defaultActiveKey={['1']} accordion="true" onChange={callback} expandIconPosition="right">
                      <Panel header="Bencana Alam" key="1">
                        <Table columns={columnsBencana} dataSource={dataBencana} pagination={false} bordered size="large" />
                      </Panel>
                    </Collapse>
                    <Collapse defaultActiveKey={['2']} onChange={callback} className="mt-4" expandIconPosition="right">
                      <Panel header="Persnal Accident" key="2">
                        <Table columns={columnsPA} dataSource={dataPA} pagination={false} bordered size="small" />
                      </Panel>
                    </Collapse>
                  </>
              )
              : <div className="py-5" />
          }
        </Card>
      </React.Fragment>
  )
}

ComprehensiveSetting.propTypes = {
  stateLiability: PropTypes.object,
  dataCustomer: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default ComprehensiveSetting

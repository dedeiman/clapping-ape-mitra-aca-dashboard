import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Table, Collapse,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse

function callback(key) {
  console.log(key)
}

const columns = [
  {
    title: 'Jaminan',
    dataIndex: 'jaminan',
    key: 'jaminan',
    render: (text, row, index) => {
      if (index > 0) {
        return <a>{text}</a>;
      }
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: 1,
        },
      };
    },
  },
  {
    title: 'Otomate',
    children: [
      {
        title: 'Wilayah 1 (%)',
        dataIndex: 'wilayah1',
        key: 'wilayah1',
      },
      {
        title: 'Wilayah 2 (%)',
        dataIndex: 'wilayah2',
        key: 'wilayah2',
      },
      {
        title: 'Wilayah 3 (%)',
        dataIndex: 'wilayah3',
        key: 'wilayah3',
      },
    ],
  },
]

const columnsBencana = [
  {
    title: 'TSI Bencana Alam : 10% dari TSI',
    children: [
      {
        title: '',
        dataIndex: 'bencanaAlam',
        key: 'bencanaAlam',
      },
      {
        title: 'Wilayah 1 (%)',
        dataIndex: 'bencanaAlam1',
        key: 'bencanaAlam1',
      },
      {
        title: 'Wilayah 2 (%)',
        dataIndex: 'bencanaAlam2',
        key: 'bencanaAlam2',
      },
      {
        title: 'Wilayah 3 (%)',
        dataIndex: 'bencanaAlam3',
        key: 'bencanaAlam3',
      },
    ],
  },
]

const columnsTPL = [
  {
    title: 'TPL',
    dataIndex: 'tpl',
    key: 'tpl',
    width: 500,
  },
  {
    title: 'TPL 25jt jumlah premi minimum 250.000',
    dataIndex: 'tpl1',
    key: 'tpl1',
  },
]

const columnsPA = [
  {
    title: 'PA Driver',
    dataIndex: 'pad',
    key: 'pad',
    width: 500,
  },
  {
    title: 'PA Passenger',
    dataIndex: 'pap',
    key: 'pap',
  },
]

const OtomateSetting = ({
stateLiability, dataCustomer,
isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const ub1 = Object.assign({}, stateLiability.list.premi_rates)
  const ub2 = Object.assign({}, stateLiability.list.natural_disaster_rates)
  const ub3 = Object.assign({}, stateLiability.list.third_party_liability_rates)
  const ub4 = Object.assign({}, stateLiability.list.personal_accident_rates)
  const r1 = Object.assign({}, ub1['>100jt-125jt'])
  const r2 = Object.assign({}, ub1['>125jt-200jt'])
  const r3 = Object.assign({}, ub1['>200jt-400jt'])
  const r4 = Object.assign({}, ub1['>400jt-800jt'])
  const r5 = Object.assign({}, ub1['>800jt'])
  const b1 = Object.assign({}, ub2.eq)
  const b2 = Object.assign({}, ub2.flood)
  const b3 = Object.assign({}, ub2.rscc)
  const b4 = Object.assign({}, ub2.ts)

  const data = [
    {
      key: '1',
      jaminan: 'Rate Jual',
    },
    {
      key: '2',
      jaminan: '< 100 juta',
      wilayah1: '0',
      wilayah2: '0',
      wilayah3: '0',
    },
    {
      key: '3',
      jaminan: '> 100 s/d 125 juta',
      wilayah1: Object.assign({}, r1[0]).rate,
      wilayah2: Object.assign({}, r1[1]).rate,
      wilayah3: Object.assign({}, r1[2]).rate,
    },
    {
      key: '4',
      jaminan: '> 125 s/d 200 juta',
      wilayah1: Object.assign({}, r2[0]).rate,
      wilayah2: Object.assign({}, r2[1]).rate,
      wilayah3: Object.assign({}, r2[2]).rate,
    },
    {
      key: '5',
      jaminan: '> 200 s/d 400 juta',
      wilayah1: Object.assign({}, r3[0]).rate,
      wilayah2: Object.assign({}, r3[1]).rate,
      wilayah3: Object.assign({}, r3[2]).rate,
    },
    {
      key: '6',
      jaminan: '> 400 s/d 800 juta',
      wilayah1: Object.assign({}, r4[0]).rate,
      wilayah2: Object.assign({}, r4[1]).rate,
      wilayah3: Object.assign({}, r4[2]).rate,
    },
    {
      key: '7',
      jaminan: '> 800 juta',
      wilayah1: Object.assign({}, r5[0]).rate,
      wilayah2: Object.assign({}, r5[1]).rate,
      wilayah3: Object.assign({}, r5[2]).rate,
    },
  ]
  const dataBencana = [
    {
      key: '1',
      bencanaAlam: 'Angin Topan, Badai, Hujan Es, Banjir dan atau Tanah Longsor',
      bencanaAlam1: Object.assign({}, b2[0]).rate,
      bencanaAlam2: Object.assign({}, b2[1]).rate,
      bencanaAlam3: Object.assign({}, b2[2]).rate,
    },
    {
      key: '2',
      bencanaAlam: 'Gempa Bumi, Tsunami, dan atau Letusan Gunung Berapi',
      bencanaAlam1: Object.assign({}, b1[0]).rate,
      bencanaAlam2: Object.assign({}, b1[1]).rate,
      bencanaAlam3: Object.assign({}, b1[2]).rate,
    },
    {
      key: '3',
      bencanaAlam: 'Huru-hara dan Kerusuhan (SRCC)',
      bencanaAlam1: Object.assign({}, b3[0]).rate,
      bencanaAlam2: Object.assign({}, b3[1]).rate,
      bencanaAlam3: Object.assign({}, b3[2]).rate,
    },
    {
      key: '4',
      bencanaAlam: 'Terorisme dan Sabotase',
      bencanaAlam1: Object.assign({}, b4[0]).rate,
      bencanaAlam2: Object.assign({}, b4[1]).rate,
      bencanaAlam3: Object.assign({}, b4[2]).rate,
    },
  ]
  const dataPA = [
    {
      key: '1',
      pad: Object.assign({}, ub4.pad).rate,
      pap: Object.assign({}, ub4.pap).rate,
    },
  ]
  const dataTPL = [
    {
      key: '1',
      tpl: Object.assign({}, ub3['25 jt']).max_price,
      tpl1: Object.assign({}, ub3['25 jt']).amount,
    },
    {
      key: '2',
      tpl: Object.assign({}, ub3['30 jt']).max_price,
      tpl1: Object.assign({}, ub3['30 jt']).amount,
    },
    {
      key: '3',
      tpl: Object.assign({}, ub3['35 jt']).max_price,
      tpl1: Object.assign({}, ub3['35 jt']).amount,
    },
    {
      key: '4',
      tpl: Object.assign({}, ub3['40 jt']).max_price,
      tpl1: Object.assign({}, ub3['40 jt']).amount,
    },
    {
      key: '5',
      tpl: Object.assign({}, ub3['45 jt']).max_price,
      tpl1: Object.assign({}, ub3['45 jt']).amount,
    },
    {
      key: '6',
      tpl: Object.assign({}, ub3['50 jt']).max_price,
      tpl1: Object.assign({}, ub3['50 jt']).amount,
    },
    {
      key: '7',
      tpl: Object.assign({}, ub3['55 jt']).max_price,
      tpl1: Object.assign({}, ub3['55 jt']).amount,
    },
    {
      key: '8',
      tpl: Object.assign({}, ub3['60 jt']).max_price,
      tpl1: Object.assign({}, ub3['60 jt']).amount,
    },
    {
      key: '9',
      tpl: Object.assign({}, ub3['65 jt']).max_price,
      tpl1: Object.assign({}, ub3['65 jt']).amount,
    },
    {
      key: '10',
      tpl: Object.assign({}, ub3['70 jt']).max_price,
      tpl1: Object.assign({}, ub3['70 jt']).amount,
    },
    {
      key: '11',
      tpl: Object.assign({}, ub3['75 jt']).max_price,
      tpl1: Object.assign({}, ub3['75 jt']).amount,
    },
    {
      key: '12',
      tpl: Object.assign({}, ub3['80 jt']).max_price,
      tpl1: Object.assign({}, ub3['80 jt']).amount,
    },
    {
      key: '13',
      tpl: Object.assign({}, ub3['85 jt']).max_price,
      tpl1: Object.assign({}, ub3['85 jt']).amount,
    },
    {
      key: '14',
      tpl: Object.assign({}, ub3['90 jt']).max_price,
      tpl1: Object.assign({}, ub3['90 jt']).amount,
    },
    {
      key: '15',
      tpl: Object.assign({}, ub3['95 jt']).max_price,
      tpl1: Object.assign({}, ub3['95 jt']).amount,
    },
    {
      key: '16',
      tpl: Object.assign({}, ub3['100 jt']).max_price,
      tpl1: Object.assign({}, ub3['100 jt']).amount,
    },
  ]
  return (
      <React.Fragment>
        <Card>
          <Row type="flex" justify="space-between" className="mb-2 mr-4">
            <Col xs={24} md={20}>
              <div className="text-primary2">Product - Otomate</div>
            </Col>
            <Col xs={24} md={4}>
              <Button type="primary" className="float-md-right mt-3 mt-md-0" onClick={() => history.push('/product-setting/otomate/edit')}>
                Edit Otomate
              </Button>
            </Col>
          </Row>
          {isFetching && (
              <Loader />
          )}
          {(!isFetching && isEmpty(dataCustomer)) && (
              <>
                <Table columns={columns} dataSource={data} bordered size="small" pagination={false} className="mb-4" />
                <Collapse defaultActiveKey={['1']} accordion="true" onChange={callback} expandIconPosition="right">
                  <Panel header="Bencana Alam" key="1">
                    <Table columns={columnsBencana} dataSource={dataBencana} pagination={false} bordered size="large" />
                  </Panel>
                </Collapse>
                <Collapse defaultActiveKey={['2']} onChange={callback} className="mt-4" expandIconPosition="right">
                  <Panel header="Persnal Accident" key="2">
                    <Table columns={columnsPA} dataSource={dataPA} pagination={false} bordered size="small" />
                  </Panel>
                </Collapse>
                <Collapse defaultActiveKey={['3']} onChange={callback} className="mt-4" expandIconPosition="right">
                  <Panel header="TPL" key="3">
                    <Table columns={columnsTPL} dataSource={dataTPL} bordered pagination={false} size="small" />
                  </Panel>
                </Collapse>
              </>
          )}
        </Card>
      </React.Fragment>
  )
}

OtomateSetting.propTypes = {
  stateLiability: PropTypes.object,
  dataCustomer: PropTypes.array,
  metaCustomer: PropTypes.object,
  stateCustomer: PropTypes.object,
  handlePage: PropTypes.func,
  loadCustomer: PropTypes.func,
  setStateCustomer: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  handleFilter: PropTypes.func,
}

export default OtomateSetting

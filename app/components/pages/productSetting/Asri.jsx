import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Table, Collapse,
  Card,
  Button,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse

const columns = [
  {
    title: 'Description',
    dataIndex: 'desc',
    key: 'desc',
    render: (text, row, index) => {
      if (index > 0) {
        return <a>{text}</a>;
      }
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: 1,
        },
      };
    },
  },
  {
    title: 'Kelas 1',
    children: [
      {
        title: 'Zona I',
        dataIndex: 'zona11',
        key: 'zona11',
      },
      {
        title: 'Zona II',
        dataIndex: 'zona12',
        key: 'zona12',
      },
      {
        title: 'Zona III',
        dataIndex: 'zona13',
        key: 'zona13',
      },
      {
        title: 'Zona IV',
        dataIndex: 'zona14',
        key: 'zona14',
      },
      {
        title: 'Zona V',
        dataIndex: 'zona15',
        key: 'zona15',
      },
    ],
  },
  {
    title: 'Kelas 2',
    children: [
      {
        title: 'Zona I',
        dataIndex: 'zona21',
        key: 'zona21',
      },
      {
        title: 'Zona II',
        dataIndex: 'zona22',
        key: 'zona22',
      },
    ],
  },
]

const columnsBencana = [
  {
    title: 'Description',
    dataIndex: 'desc',
    key: 'desc',
  },
  {
    title: 'Kelas 1',
    dataIndex: 'kelas1',
    key: 'kelas1',
  },
  {
    title: 'Kelas 2',
    dataIndex: 'kelas2',
    key: 'kelas2',
  },
]

const AsriSetting = ({
  stateLiability,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const data = [stateLiability.list]
  const dataStandard = [
    {
      key: '1',
      desc: 'Flexas',
      kelas1: '0,294',
      kelas2: '0,424',
    },
    {
      key: '2',
      desc: 'RSMDCC',
      kelas1: '0.108',
      kelas2: '0.236',
    },
    {
      key: '3',
      desc: 'TPL',
      kelas1: '0.02',
      kelas2: '0.02',
    },
    {
      key: '4',
      desc: 'TS',
      kelas1: '0.35',
      kelas2: '0.14',
    },
    {
      key: '5',
      desc: 'Accommodation Cost',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '6',
      desc: 'Personal Accident - Death only (due to risk covered)',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '7',
      desc: 'Medical expense (due to risk covered)',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '8',
      desc: 'Architects, Surveyor & Consulting Engineers Expense',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '9',
      desc: 'Public Authorities ',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '10',
      desc: 'Fire Brigades Charges',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '11',
      desc: 'Removal Debris',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '12',
      desc: 'Vehicle Impact',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '13',
      desc: 'Burglary',
      kelas1: '0.100',
      kelas2: '0.100',
    },
    {
      key: '14',
      desc: 'Total',
      kelas1: Object.assign({}, Object.assign({}, data[0].asri_standard)[1]).rate,
      kelas2: Object.assign({}, Object.assign({}, data[0].asri_standard)[0]).rate,
    },
  ]
  const dataTSFWD = [
    {
      key: '1',
      desc: 'Flexas',
      kelas1: '0,294',
      kelas2: '0,424',
    },
    {
      key: '2',
      desc: 'RSMDCC',
      kelas1: '0.108',
      kelas2: '0.236',
    },
    {
      key: '3',
      desc: 'TPL',
      kelas1: '0.02',
      kelas2: '0.02',
    },
    {
      key: '4',
      desc: 'TS',
      kelas1: '0.35',
      kelas2: '0.14',
    },
    {
      key: '5',
      desc: 'Accommodation Cost',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '6',
      desc: 'Personal Accident - Death only (due to risk covered)',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '7',
      desc: 'Medical expense (due to risk covered)',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '8',
      desc: 'Architects, Surveyor & Consulting Engineers Expense',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '9',
      desc: 'Public Authorities ',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '10',
      desc: 'Fire Brigades Charges',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '11',
      desc: 'Removal Debris',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '12',
      desc: 'Vehicle Impact',
      kelas1: '0.001',
      kelas2: '0.010',
    },
    {
      key: '13',
      desc: 'Burglary',
      kelas1: '0.100',
      kelas2: '0.100',
    },
    {
      key: '14',
      desc: 'TSFWD',
      kelas1: '0.500',
      kelas2: '0.500',
    },
    {
      key: '15',
      desc: 'Total',
      kelas1: Object.assign({}, Object.assign({}, data[0].asri_tsfwd)[0]).rate,
      kelas2: Object.assign({}, Object.assign({}, data[0].asri_tsfwd)[1]).rate,
    },
  ]
  const dataEqvet = [
    {
      key: '1',
      desc: 'Flexas',
      zona11: '0.294',
      zona12: '0.294',
      zona13: '0.294',
      zona14: '0.294',
      zona15: '0.294',
      zona21: '0.424',
      zona22: '0.424',
    },
    {
      key: '2',
      desc: 'RSMDCC',
      zona11: '0.108',
      zona12: '0.108',
      zona13: '0.108',
      zona14: '0.108',
      zona15: '0.108',
      zona21: '0.236',
      zona22: '0.236',
    },
    {
      key: '3',
      desc: 'TPL',
      zona11: '0.020',
      zona12: '0.020',
      zona13: '0.020',
      zona14: '0.020',
      zona15: '0.020',
      zona21: '0.020',
      zona22: '0.020',
    },
    {
      key: '4',
      desc: 'TS',
      zona11: '0.350',
      zona12: '0.350',
      zona13: '0.350',
      zona14: '0.350',
      zona15: '0.350',
      zona21: '0.140',
      zona22: '0.140',
    },
    {
      key: '5',
      desc: 'Accommodation Cost',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '6',
      desc: 'Personal Accident - Death only (due to risk covered)',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '7',
      desc: 'Medical expense (due to risk covered)',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '8',
      desc: 'Architects, Surveyor & Consulting Engineers Expense',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '9',
      desc: 'Public Authorities ',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '10',
      desc: 'Fire Brigades Charges',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '11',
      desc: 'Removal Debris',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '12',
      desc: 'Vehicle Impact',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '13',
      desc: 'Burglary',
      zona11: '0.190',
      zona12: '0.260',
      zona13: '0.210',
      zona14: '0.100',
      zona15: '0.120',
      zona21: '0.190',
      zona22: '0.260',
    },
    {
      key: '14',
      desc: 'EQVET',
      zona11: '0.760',
      zona12: '0.790',
      zona13: '1.040',
      zona14: '1.350',
      zona15: '1.600',
      zona21: '0.760',
      zona22: '0.790',
    },
    {
      key: '15',
      desc: 'Total',
      zona11: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[0]).rate,
      zona12: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[1]).rate,
      zona13: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[2]).rate,
      zona14: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[3]).rate,
      zona15: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[4]).rate,
      zona21: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[5]).rate,
      zona22: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[6]).rate,
    },
  ]
  const dataEqvetTsfwd = [
    {
      key: '1',
      desc: 'Flexas',
      zona11: '0.294',
      zona12: '0.294',
      zona13: '0.294',
      zona14: '0.294',
      zona15: '0.294',
      zona21: '0.424',
      zona22: '0.424',
    },
    {
      key: '2',
      desc: 'RSMDCC',
      zona11: '0.108',
      zona12: '0.108',
      zona13: '0.108',
      zona14: '0.108',
      zona15: '0.108',
      zona21: '0.236',
      zona22: '0.236',
    },
    {
      key: '3',
      desc: 'TPL',
      zona11: '0.020',
      zona12: '0.020',
      zona13: '0.020',
      zona14: '0.020',
      zona15: '0.020',
      zona21: '0.020',
      zona22: '0.020',
    },
    {
      key: '4',
      desc: 'TS',
      zona11: '0.350',
      zona12: '0.350',
      zona13: '0.350',
      zona14: '0.350',
      zona15: '0.350',
      zona21: '0.140',
      zona22: '0.140',
    },
    {
      key: '5',
      desc: 'Accommodation Cost',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '6',
      desc: 'Personal Accident - Death only (due to risk covered)',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '7',
      desc: 'Medical expense (due to risk covered)',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '8',
      desc: 'Architects, Surveyor & Consulting Engineers Expense',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '9',
      desc: 'Public Authorities ',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '10',
      desc: 'Fire Brigades Charges',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '11',
      desc: 'Removal Debris',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '12',
      desc: 'Vehicle Impact',
      zona11: '0.001',
      zona12: '0.001',
      zona13: '0.001',
      zona14: '0.001',
      zona15: '0.001',
      zona21: '0.010',
      zona22: '0.010',
    },
    {
      key: '13',
      desc: 'Burglary',
      zona11: '0.190',
      zona12: '0.260',
      zona13: '0.210',
      zona14: '0.120',
      zona15: '0.120',
      zona21: '0.190',
      zona22: '0.260',
    },
    {
      key: '14',
      desc: 'TSFWD',
      zona11: '0.500',
      zona12: '0.500',
      zona13: '0.500',
      zona14: '0.500',
      zona15: '0.500',
      zona21: '0.500',
      zona22: '0.500',
    },
    {
      key: '15',
      desc: 'EQVET',
      zona11: '0.760',
      zona12: '0.790',
      zona13: '1.040',
      zona14: '1.350',
      zona15: '1.600',
      zona21: '0.760',
      zona22: '0.790',
    },
    {
      key: '16',
      desc: 'Total',
      zona11: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[0]).rate,
      zona12: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[1]).rate,
      zona13: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[2]).rate,
      zona14: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[3]).rate,
      zona15: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[4]).rate,
      zona21: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[5]).rate,
      zona22: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[6]).rate,
    },
  ]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <h4 className="text-primary2">Product - Rate Asri</h4>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="float-md-right mt-3 mt-md-0" onClick={() => history.push('/product-setting/asri/edit')}>
              Edit Rate Asri
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && !isEmpty(data)) && (
            <>
              <Table columns={columnsBencana} dataSource={dataStandard} pagination={false} bordered size="small" className="mb-4" />
              <Collapse defaultActiveKey={['1']} accordion="true" expandIconPosition="right">
                <Panel header="Rate ASRI + TSFWD" key="1">
                  <Table columns={columnsBencana} dataSource={dataTSFWD} pagination={false} bordered size="small" className="mb-4" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['2']} className="mt-4" expandIconPosition="right">
                <Panel header="Rate ASRI + EQVET" key="2">
                  <Table columns={columns} dataSource={dataEqvet} bordered size="small" pagination={false} />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['3']} className="mt-4" expandIconPosition="right">
                <Panel header="RATE ASRI + EQVET + TSFWD" key="3">
                  <Table columns={columns} dataSource={dataEqvetTsfwd} bordered size="small" pagination={false} />
                </Panel>
              </Collapse>
            </>
        )}
      </Card>
    </React.Fragment>
  )
}

AsriSetting.propTypes = {
  stateLiability: PropTypes.object,
  isFetching: PropTypes.bool,
}

export default AsriSetting

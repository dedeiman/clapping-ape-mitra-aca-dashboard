/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Button, Input,
  Row, Col, Upload, Modal,
  Select, DatePicker, Divider,
} from 'antd'
import Webcam from 'react-webcam'
import { Form } from '@ant-design/compatible'
import { InboxOutlined, DeleteTwoTone } from '@ant-design/icons'
import Helper from 'utils/Helper'
import moment from 'moment'

const { Dragger } = Upload
const videoConstraints = {
  width: 600,
  height: 600,
  facingMode: 'user',
}


const CardForm = ({
  data, handleUpload,
  stateForm, stateCard,
  stateModalWebcam, setImgSrc,
  setStateModalWebcam, stateFile,
  toggle, form, onSubmit, imgSrc,
  handleSearchProduct, setStateFile,
  handleDeleteWebcamPict,
}) => {
  const webcamRef = React.useRef(null)
  const { getFieldDecorator, getFieldValue, setFieldsValue } = form
  const { title, isEdit, data: currentData } = data

  const handleCapture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot()
    setImgSrc(imageSrc)
    setStateFile({ ...stateFile, file: imageSrc })
    setStateModalWebcam({ ...stateModalWebcam, visible: false })
  }, [webcamRef, setImgSrc])

  return (
    <>
      <Form onSubmit={onSubmit}>
        {(title.toLowerCase() === 'product') && (
          <Row gutter={24}>
            <Col span={24}>
              <p className="mb-0">Product</p>
              <Form.Item>
                {getFieldDecorator('product_id', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.product ? currentData.product.id : undefined,
                })(
                  <Select
                    showSearch
                    size="large"
                    placeholder="Select Product"
                    onSearch={handleSearchProduct}
                    disabled={isEdit}
                    filterOption={(input, option) => (option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)}
                  >
                    {(stateForm.productList || []).map(item => (
                      <Select.Option
                        key={item.id}
                        value={item.id}
                        disabled={(stateCard['restricted-products'] || []).map(productCard => productCard.product.id).includes(item.id)}
                      >
                        {`${(item.display_name).toUpperCase()} - ${(item.type.name).toUpperCase()}`}
                      </Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <p className="mb-0">Restricted Date</p>
              <Form.Item>
                {getFieldDecorator('restricted_date', {
                  rules: Helper.fieldRules(getFieldValue('status') === 'restricted' ? ['required'] : []),
                  initialValue: currentData.restricted_date ? moment(currentData.restricted_date) : undefined,
                })(
                  <DatePicker
                    size="large"
                    className="w-100"
                    disabled={isEdit && getFieldValue('status') !== 'restricted'}
                    format="DD MMMM YYYY"
                    disabledDate={current => (current && (current < moment().subtract(1, 'day')))}
                  />,
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <p className="mb-0">Unrestricted Date</p>
              <Form.Item>
                {getFieldDecorator('unrestricted_date', {
                  rules: Helper.fieldRules(getFieldValue('status') === 'unrestricted' ? ['required'] : []),
                  initialValue: currentData.unrestricted_date ? moment(currentData.unrestricted_date) : undefined,
                })(
                  <DatePicker
                    size="large"
                    className="w-100"
                    disabled={!isEdit || getFieldValue('status') !== 'unrestricted'}
                    format="DD MMMM YYYY"
                    disabledDate={current => (current && (current < moment().subtract(0, 'day')))}
                  />,
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <p className="mb-0">Status</p>
              <Form.Item>
                {getFieldDecorator('status', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.status || 'restricted',
                  getValueFromEvent: (e) => {
                    if (isEdit) {
                      if (e === 'restricted') {
                        setFieldsValue({
                          restricted_date: currentData.restricted_date ? moment(currentData.restricted_date) : undefined,
                          unrestricted_date: undefined,
                        })
                      } else {
                        setFieldsValue({
                          unrestricted_date: currentData.unrestricted_date ? moment(currentData.unrestricted_date) : undefined,
                          restricted_date: undefined,
                        })
                      }
                    }

                    return e
                  },
                })(
                  <Select
                    size="large"
                    className="w-100"
                  >
                    {([{ name: 'unrestricted', id: 1 }, { name: 'restricted', id: 2 }] || []).map(item => (
                      <Select.Option disabled={isEdit ? false : item.name === 'unrestricted'} key={item.id} value={item.name}>{(item.name || '').toUpperCase()}</Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <p className="mb-0">Reason</p>
              <Form.Item>
                {getFieldDecorator('reason', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.reason || '',
                })(
                  <Input.TextArea rows={4} placeholder="Input Reason" className="uppercase" size="large" style={{ 'text-transform': 'uppercase' }} />,
                )}
              </Form.Item>
            </Col>
          </Row>
        )}

        {(title.toLowerCase() === 'agent') && (
          <Row gutter={24}>
            <Col span={24}>
              <p className="mb-0">Agent ID</p>
              <Form.Item>
                {getFieldDecorator('agent_id', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.agent.agent_id || '',
                })(
                  <Input size="large" disabled />,
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <p className="mb-0">Agent Name</p>
              <Form.Item>
                {getFieldDecorator('agent_name', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.agent.name || '',
                })(
                  <Input size="large" disabled />,
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <p className="mb-0">Branch</p>
              <Form.Item>
                {getFieldDecorator('branch', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.agent.branch.name || '',
                })(
                  <Input size="large" disabled />,
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <p className="mb-0">Status</p>
              <Form.Item>
                {getFieldDecorator('status', {
                  rules: Helper.fieldRules(['required']),
                  initialValue: currentData.status || undefined,
                })(
                  <Select
                    size="large"
                    className="w-100"
                    placeholder="Select Status"
                    loading={stateForm.bankLoad}
                  >
                    <Select.Option key="active" value="active">Active</Select.Option>
                    <Select.Option key="inactive" value="inactive">Inactive</Select.Option>
                  </Select>,
                )}
              </Form.Item>
            </Col>
          </Row>
        )}

        {(title.toLowerCase() === 'document') && (
          <>
            <Row gutter={24}>
              <Col span={24}>
                <p className="mb-0">Document Category</p>
                <Form.Item>
                  {getFieldDecorator('document_category_id', {
                    rules: Helper.fieldRules(['required']),
                    initialValue: currentData.document_category_id || undefined,
                  })(
                    <Select
                      size="large"
                      className="w-100"
                      placeholder="Select Type"
                      loading={stateForm.documentLoad}
                    >
                      {(stateForm.documentList || []).map(item => (
                        <Select.Option key={item.id} value={item.id}>{(item.name).toUpperCase()}</Select.Option>
                      ))}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={24}>
                <p className="mb-0">Description</p>
                <Form.Item>
                  {getFieldDecorator('description', {
                    rules: Helper.fieldRules(['required']),
                    initialValue: currentData.description || '',
                  })(
                    <Input.TextArea rows={4} size="large" style={{ 'text-transform': 'uppercase' }} />,
                  )}
                </Form.Item>
              </Col>
              <Col span={24}>
                <p className="mb-0">Browse</p>
                <Form.Item>
                  {getFieldDecorator('file', {
                    rules: Helper.fieldRules(!isEdit && !stateFile.file ? ['required'] : []),
                  })(
                    <Dragger
                      name="file"
                      multiple="true"
                      listType="picture-card"
                      beforeUpload={() => false}
                      showUploadList={{ showRemoveIcon: true, showPreviewIcon: false }}
                      onChange={info => handleUpload(info, 'file')}
                      style={{ 'margin-bottom': '10px' }}
                    >
                      <div>
                        <p className="ant-upload-drag-icon">
                          <InboxOutlined />
                        </p>
                        <p className="ant-upload-text">Upload Document</p>
                        <p className="ant-upload-hint">
                          Support for a single or bulk upload.
                        </p>
                      </div>
                    </Dragger>,
                  )}
                </Form.Item>
              </Col>
              <Col>
                <p className="mb-0">Camera</p>
                <Button
                  ghost
                  size="large"
                  type="primary"
                  className="d-flex align-items-center mb-3"
                  onClick={() => setStateModalWebcam({ ...stateModalWebcam, visible: true })}
                >
                  Capture Photo
                </Button>
              </Col>
            </Row>
            <Row>
              {imgSrc && (
                <>
                  <img
                    src={imgSrc}
                    style={{
                      width: 100,
                      height: 100,
                      borderRadius: 4,
                    }}
                  />
                  <DeleteTwoTone
                    className="ml-1"
                    onClick={handleDeleteWebcamPict}
                    style={{ cursor: 'pointer' }}
                  />
                </>
              )}
            </Row>
          </>
        )}
        <Divider />
        <Row gutter={24}>
          <Col span={24} className="d-flex justify-content-end align-items-center">
            <Button
              size="large"
              type="primary"
              htmlType="submit"
              className="mr-2"
            >
              Submits
            </Button>
            <Button
              ghost
              size="large"
              type="primary"
              onClick={() => {
                form.resetFields()
                setImgSrc(null)
                toggle()
              }}
            >
              Cancel
            </Button>
          </Col>
        </Row>
      </Form>

      <Modal
        title={<p className="mb-0 title-card">Capture Photo</p>}
        visible={stateModalWebcam.visible}
        footer={null}
        closable={false}
        width={650}
        onCancel={() => setStateModalWebcam({ ...stateModalWebcam, visible: false })}
      >
        <Webcam
          videoConstraints={videoConstraints}
          audio={false}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
        />
        <Button onClick={handleCapture}>Capture Photo</Button>
      </Modal>
    </>
  )
}

CardForm.propTypes = {
  toggle: PropTypes.func,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  imgSrc: PropTypes.object,
  stateForm: PropTypes.object,
  data: PropTypes.object,
  stateCard: PropTypes.object,
  handleUpload: PropTypes.func,
  setImgSrc: PropTypes.func,
  stateFile: PropTypes.object,
  setStateFile: PropTypes.func,
  stateModalWebcam: PropTypes.object,
  setStateModalWebcam: PropTypes.func,
  handleSearchProduct: PropTypes.func,
  handleDeleteWebcamPict: PropTypes.func,
}

export default CardForm

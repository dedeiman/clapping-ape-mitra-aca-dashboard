import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Table, Collapse,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse;
const { Column, ColumnGroup } = Table

const PAAmanahSetting = ({
  stateLiability,
  loadCustomer,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const ub1 = Object.assign({}, stateLiability.list.A)
  const ub2 = Object.assign({}, stateLiability.list.B)
  const ub3 = Object.assign({}, stateLiability.list.C)
  const dataTable = [
    {
      bu: Object.assign({}, ub1).class,
      rate: Object.assign({}, ub1).rate_percentage,
    },
    {
      bu: Object.assign({}, ub2).class,
      rate: Object.assign({}, ub2).rate_percentage,
    },
    {
      bu: Object.assign({}, ub3).class,
      rate: Object.assign({}, ub3).rate_percentage,
    },
  ]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <div className="text-primary2">Product - PA Amanah</div>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="float-md-right mt-3 mt-md-0" onClick={() => history.push('/product-setting/paamanah/edit')}>
              Edit PA Amanah
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {!isEmpty(stateLiability)
          ? (
            <>
              <Table pagination={false} bordered dataSource={dataTable}>
                <Column title="Class" dataIndex="bu" key="bu" />
                <Column title="Rate (%)" dataIndex="rate" key="rate" />
              </Table>
            </>
          )
          : <div className="py-5" />
        }
      </Card>
    </React.Fragment>
  )
}

PAAmanahSetting.propTypes = {
  stateLiability: PropTypes.object,
  currentUser: PropTypes.object,
  dataCustomer: PropTypes.array,
  metaCustomer: PropTypes.object,
  stateCustomer: PropTypes.object,
  handlePage: PropTypes.func,
  loadCustomer: PropTypes.func,
  setStateCustomer: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  handleFilter: PropTypes.func,
}

export default PAAmanahSetting

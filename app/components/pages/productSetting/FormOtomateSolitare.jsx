/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button,
  Row, Col,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormOtomateSolitare = ({
  handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields } = form
  const dataPremi = Object.assign({}, stateLiability.list.premi_rates)
  const dataWW = Object.assign({}, stateLiability.list.natural_disaster_rates)
  const dataTPL = Object.assign({}, stateLiability.list.third_party_liability_rates)
  const bencana1 = Object.assign({}, dataWW.flood)
  const bencana2 = Object.assign({}, dataWW.eq)
  const bencana3 = Object.assign({}, dataWW.rscc)
  const bencana4 = Object.assign({}, dataWW.ts)
  const asia1 = Object.assign({}, dataPremi['>100jt-125jt'])
  const asia2 = Object.assign({}, dataPremi['>125jt-200jt'])
  const asia3 = Object.assign({}, dataPremi['>200jt-400jt'])
  const asia4 = Object.assign({}, dataPremi['>400jt-800jt'])
  const asia5 = Object.assign({}, dataPremi['>800jt'])
  return (
    <Card title="Edit Product Setting - Otomate Solitare">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <h6>Otomate Solitare</h6>
        <Row gutter={24}>
          <Col span={5}>
            <label>Jaminan</label>
          </Col>
          <Col span={6}>
            <label>Wilayah 1</label>
          </Col>
          <Col span={6}>
            <label>Wilayah 2</label>
          </Col>
          <Col span={6}>
            <label>Wilayah 3</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'> 100 s/d 125 juta'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will11}>
              {getFieldDecorator('will11', {
                initialValue: Object.assign({}, asia1[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will12}>
              {getFieldDecorator('will12', {
                initialValue: Object.assign({}, asia1[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will13}>
              {getFieldDecorator('will13', {
                initialValue: Object.assign({}, asia1[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'> 125 s/d 200 juta'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will21}>
              {getFieldDecorator('will21', {
                initialValue: Object.assign({}, asia2[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will22}>
              {getFieldDecorator('will22', {
                initialValue: Object.assign({}, asia2[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will23}>
              {getFieldDecorator('will23', {
                initialValue: Object.assign({}, asia2[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will23'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'200 s/d 400 juta'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will31}>
              {getFieldDecorator('will31', {
                initialValue: Object.assign({}, asia3[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will32}>
              {getFieldDecorator('will32', {
                initialValue: Object.assign({}, asia3[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will33}>
              {getFieldDecorator('will33', {
                initialValue: Object.assign({}, asia3[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will33'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'> 400 s/d 800 juta'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will41}>
              {getFieldDecorator('will41', {
                initialValue: Object.assign({}, asia4[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will42}>
              {getFieldDecorator('will42', {
                initialValue: Object.assign({}, asia4[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will43}>
              {getFieldDecorator('will43', {
                initialValue: Object.assign({}, asia4[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will43'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'> 800 juta'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will51}>
              {getFieldDecorator('will51', {
                initialValue: Object.assign({}, asia5[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will51'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will52}>
              {getFieldDecorator('will52', {
                initialValue: Object.assign({}, asia5[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will52'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.will53}>
              {getFieldDecorator('will53', {
                initialValue: Object.assign({}, asia5[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'will53'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        {/*bencana alam*/}
        <h6>Bencana Alam (TSI Bencana Alam : 10% dari TSI)</h6>
        <Row gutter={24} className="mt-2">
          <Col span={5}>
            <label></label>
          </Col>
          <Col span={6}>
            <label>Wilayah 1 (%)</label>
          </Col>
          <Col span={6}>
            <label>Wilayah 2 (%)</label>
          </Col>
          <Col span={6}>
            <label>Wilayah 3 (%)</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'' +
                'Angin Topan, Badai, Hujan Es, Banjir dan atau Tanah Longsor'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana11}>
              {getFieldDecorator('bencana11', {
                initialValue: Object.assign({}, bencana1[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana12}>
              {getFieldDecorator('bencana12', {
                initialValue: Object.assign({}, bencana1[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana13}>
              {getFieldDecorator('bencana13', {
                initialValue: Object.assign({}, bencana1[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'' +
                'Gempa Bumi, Tsunami dan atau Letusan Gunung Berapi'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana21}>
              {getFieldDecorator('bencana21', {
                initialValue: Object.assign({}, bencana2[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana22}>
              {getFieldDecorator('bencana22', {
                initialValue: Object.assign({}, bencana2[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana23}>
              {getFieldDecorator('bencana23', {
                initialValue: Object.assign({}, bencana2[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana23'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'' +
                'Huru-hara dan kerusuhan (SRCC)'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana31}>
              {getFieldDecorator('bencana31', {
                initialValue: Object.assign({}, bencana3[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana32}>
              {getFieldDecorator('bencana32', {
                initialValue: Object.assign({}, bencana3[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana33}>
              {getFieldDecorator('bencana33', {
                initialValue: Object.assign({}, bencana3[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana33'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{'' +
                'Huru-hara dan kerusuhan (SRCC) & Terorisme dan Sabotase'}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana41}>
              {getFieldDecorator('bencana41', {
                initialValue: Object.assign({}, bencana4[0]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana42}>
              {getFieldDecorator('bencana42', {
                initialValue: Object.assign({}, bencana4[1]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.bencana43}>
              {getFieldDecorator('bencana43', {
                initialValue: Object.assign({}, bencana4[2]).rate || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'bencana43'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <h6>TPL</h6>
        <Row gutter={24} className="mt-2">
          <Col span={5}>
            <label>TPL</label>
          </Col>
          <Col span={6}>
            <label></label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['30 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl1}>
              {getFieldDecorator('tpl1', {
                initialValue: Object.assign({}, dataTPL['30 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['35 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl2}>
              {getFieldDecorator('tpl2', {
                initialValue: Object.assign({}, dataTPL['35 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['40 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl3}>
              {getFieldDecorator('tpl3', {
                initialValue: Object.assign({}, dataTPL['40 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['45 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl4}>
              {getFieldDecorator('tpl4', {
                initialValue: Object.assign({}, dataTPL['45 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl4'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['50 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl5}>
              {getFieldDecorator('tpl5', {
                initialValue: Object.assign({}, dataTPL['50 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl5'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['55 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl6}>
              {getFieldDecorator('tpl6', {
                initialValue: Object.assign({}, dataTPL['55 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl6'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['60 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl7}>
              {getFieldDecorator('tpl7', {
                initialValue: Object.assign({}, dataTPL['60 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl7'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['65 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl8}>
              {getFieldDecorator('tpl8', {
                initialValue: Object.assign({}, dataTPL['65 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl8'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['70 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl9}>
              {getFieldDecorator('tpl9', {
                initialValue: Object.assign({}, dataTPL['70 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl9'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['75jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl10}>
              {getFieldDecorator('tpl10', {
                initialValue: Object.assign({}, dataTPL['75jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl10'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['80 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl11}>
              {getFieldDecorator('tpl11', {
                initialValue: Object.assign({}, dataTPL['80 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['85 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl12}>
              {getFieldDecorator('tpl12', {
                initialValue: Object.assign({}, dataTPL['85 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['90 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl13}>
              {getFieldDecorator('tpl13', {
                initialValue: Object.assign({}, dataTPL['90 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['95 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl14}>
              {getFieldDecorator('tpl14', {
                initialValue: Object.assign({}, dataTPL['95 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl14'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={5} className="mt-1">
            <label className="mt-1">{Object.assign({}, dataTPL['100 jt']).max_price}</label>
          </Col>
          <Col span={6}>
            <Form.Item {...errorsField.tpl15}>
              {getFieldDecorator('tpl15', {
                initialValue: Object.assign({}, dataTPL['100 jt']).amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'tpl15'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormOtomateSolitare.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormOtomateSolitare)

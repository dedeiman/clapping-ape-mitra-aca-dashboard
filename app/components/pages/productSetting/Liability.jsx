import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Table,
  Card, Button,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty } from 'lodash'
import history from 'utils/history'

const { Column } = Table


const LiabilitySetting = ({
  stateLiability,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const data = [stateLiability.list]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <div className="text-primary2">Product - Liability</div>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="float-md-right mt-3 mt-md-0" htmlType="submit" onClick={() => history.push('/product-setting/liability/edit')}>
              Edit Liability
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && isEmpty(stateLiability)) && (
          <Table bordered dataSource={data}>
            <Column title="Premi" dataIndex="premium_amount" key="premium_amount" />
            <Column title="Nilai Pertanggungan" dataIndex="coverage_amount" key="coverage_amount" />
          </Table>
        )}
        {!isEmpty(stateLiability)
          ? (
            <>
              <Table pagination={false} bordered dataSource={data}>
                <Column title="Premi" dataIndex="premium_amount" key="premium_amount" />
                <Column title="Nilai Pertanggungan" dataIndex="coverage_amount" key="coverage_amount" />
              </Table>
            </>
          )
          : <div className="py-5" />
          }
      </Card>
    </React.Fragment>
  )
}

LiabilitySetting.propTypes = {
  stateLiability: PropTypes.object,
  isFetching: PropTypes.bool,
}

export default LiabilitySetting

/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'
import NumberFormat from 'react-number-format'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormTravelInternational = ({
  detailProduct, handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields } = form
  const dataAsia = Object.assign({}, stateLiability.list.travel_international_asia)
  const dataWW = Object.assign({}, stateLiability.list.travel_international_worldwide)
  const asia1 = Object.assign({}, dataAsia['1-4'])
  const asia2 = Object.assign({}, dataAsia['5-6'])
  const asia3 = Object.assign({}, dataAsia['7-8'])
  const asia4 = Object.assign({}, dataAsia['9-10'])
  const asia5 = Object.assign({}, dataAsia['11-15'])
  const asia6 = Object.assign({}, dataAsia['16-20'])
  const asia7 = Object.assign({}, dataAsia['21-25'])
  const asia8 = Object.assign({}, dataAsia['26-31'])
  const asia9 = Object.assign({}, dataAsia.additional_week)
  const asia10 = Object.assign({}, dataAsia.annual)
  const ww1 = Object.assign({}, dataWW['1-4'])
  const ww2 = Object.assign({}, dataWW['5-6'])
  const ww3 = Object.assign({}, dataWW['7-8'])
  const ww4 = Object.assign({}, dataWW['9-10'])
  const ww5 = Object.assign({}, dataWW['11-15'])
  const ww6 = Object.assign({}, dataWW['16-20'])
  const ww7 = Object.assign({}, dataWW['21-25'])
  const ww8 = Object.assign({}, dataWW['26-31'])
  const ww9 = Object.assign({}, dataWW.additional_week)
  const ww10 = Object.assign({}, dataWW.annual)
  return (
    <Card title="Edit Product Setting - Travel International">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <h6>Travelsafe International ASIA</h6>
        <Row gutter={24}>
          <Col span={3}>
            <label>Trip Duration</label>
          </Col>
          <Col span={2}>
            <label>Individu (VIP)</label>
          </Col>
          <Col span={2}>
            <label>Family (VIP)</label>
          </Col>
          <Col span={3}>
            <label>Individu (Executive)</label>
          </Col>
          <Col span={3}>
            <label>Family (Executive)</label>
          </Col>
          <Col span={2}>
            <label>Individu (Deluxe)</label>
          </Col>
          <Col span={2}>
            <label>Family (Deluxe)</label>
          </Col>
          <Col span={2}>
            <label>Individu (Superior)</label>
          </Col>
          <Col span={2}>
            <label>Family (Superior)</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 1 - 4</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind1}>
              {getFieldDecorator('asia_ind1', {
                initialValue: Object.assign({}, asia1[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam1}>
              {getFieldDecorator('asia_fam1', {
                initialValue: Object.assign({}, asia1[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind2}>
              {getFieldDecorator('asia_ind2', {
                initialValue: Object.assign({}, asia1[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam2}>
              {getFieldDecorator('asia_fam2', {
                initialValue: Object.assign({}, asia1[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind3}>
              {getFieldDecorator('asia_ind3', {
                initialValue: Object.assign({}, asia1[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam3}>
              {getFieldDecorator('asia_fam3', {
                initialValue: Object.assign({}, asia1[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind4}>
              {getFieldDecorator('asia_ind4', {
                initialValue: Object.assign({}, asia1[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind4'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam4}>
              {getFieldDecorator('asia_fam4', {
                initialValue: Object.assign({}, asia1[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam4'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 5 - 6</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind11}>
              {getFieldDecorator('asia_ind11', {
                initialValue: Object.assign({}, asia2[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam11}>
              {getFieldDecorator('asia_fam11', {
                initialValue: Object.assign({}, asia2[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind21}>
              {getFieldDecorator('asia_ind21', {
                initialValue: Object.assign({}, asia2[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam21}>
              {getFieldDecorator('asia_fam21', {
                initialValue: Object.assign({}, asia2[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind31}>
              {getFieldDecorator('asia_ind31', {
                initialValue: Object.assign({}, asia2[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam31}>
              {getFieldDecorator('asia_fam31', {
                initialValue: Object.assign({}, asia2[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind41}>
              {getFieldDecorator('asia_ind41', {
                initialValue: Object.assign({}, asia2[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam41}>
              {getFieldDecorator('asia_fam41', {
                initialValue: Object.assign({}, asia2[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
                getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 7 - 8</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind12}>
              {getFieldDecorator('asia_ind12', {
                initialValue: Object.assign({}, asia3[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam12}>
              {getFieldDecorator('asia_fam12', {
                initialValue: Object.assign({}, asia3[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind22}>
              {getFieldDecorator('asia_ind22', {
                initialValue: Object.assign({}, asia3[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam22}>
              {getFieldDecorator('asia_fam22', {
                initialValue: Object.assign({}, asia3[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind32}>
              {getFieldDecorator('asia_ind32', {
                initialValue: Object.assign({}, asia3[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam32}>
              {getFieldDecorator('asia_fam32', {
                initialValue: Object.assign({}, asia3[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind42}>
              {getFieldDecorator('asia_ind42', {
                initialValue: Object.assign({}, asia3[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam42}>
              {getFieldDecorator('asia_fam42', {
                initialValue: Object.assign({}, asia3[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 9 - 10</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind13}>
              {getFieldDecorator('asia_ind13', {
                initialValue: Object.assign({}, asia4[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam13}>
              {getFieldDecorator('asia_fam13', {
                initialValue: Object.assign({}, asia4[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind23}>
              {getFieldDecorator('asia_ind23', {
                initialValue: Object.assign({}, asia4[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind23'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam23}>
              {getFieldDecorator('asia_fam23', {
                initialValue: Object.assign({}, asia4[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam23'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind33}>
              {getFieldDecorator('asia_ind33', {
                initialValue: Object.assign({}, asia4[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind33'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam33}>
              {getFieldDecorator('asia_fam33', {
                initialValue: Object.assign({}, asia4[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam33'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind43}>
              {getFieldDecorator('asia_ind43', {
                initialValue: Object.assign({}, asia4[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind43'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              getValueFromEvent: e => Number(e.target.value.replace('Rp. ', '').replace(/,/g, '')),
              })(
                  <NumberFormat
                      autoComplete="off"
                      className="ant-input field-lg"
                      thousandSeparator
                      prefix=""
                  />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam43}>
              {getFieldDecorator('asia_fam43', {
                initialValue: Object.assign({}, asia4[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam43'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 11 - 15</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind14}>
              {getFieldDecorator('asia_ind14', {
                initialValue: Object.assign({}, asia5[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind14'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam14}>
              {getFieldDecorator('asia_fam14', {
                initialValue: Object.assign({}, asia5[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam14'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind24}>
              {getFieldDecorator('asia_ind24', {
                initialValue: Object.assign({}, asia5[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind24'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam24}>
              {getFieldDecorator('asia_fam24', {
                initialValue: Object.assign({}, asia5[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam24'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind34}>
              {getFieldDecorator('asia_ind34', {
                initialValue: Object.assign({}, asia5[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind34'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam34}>
              {getFieldDecorator('asia_fam34', {
                initialValue: Object.assign({}, asia5[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam34'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind44}>
              {getFieldDecorator('asia_ind44', {
                initialValue: Object.assign({}, asia5[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind44'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam44}>
              {getFieldDecorator('asia_fam44', {
                initialValue: Object.assign({}, asia5[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam44'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 16 - 20</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind15}>
              {getFieldDecorator('asia_ind15', {
                initialValue: Object.assign({}, asia6[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind15'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam15}>
              {getFieldDecorator('asia_fam15', {
                initialValue: Object.assign({}, asia6[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam15'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind25}>
              {getFieldDecorator('asia_ind25', {
                initialValue: Object.assign({}, asia6[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind25'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam25}>
              {getFieldDecorator('asia_fam25', {
                initialValue: Object.assign({}, asia6[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam25'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind35}>
              {getFieldDecorator('asia_ind35', {
                initialValue: Object.assign({}, asia6[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind35'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam35}>
              {getFieldDecorator('asia_fam35', {
                initialValue: Object.assign({}, asia6[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam35'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind45}>
              {getFieldDecorator('asia_ind45', {
                initialValue: Object.assign({}, asia6[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind45'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam45}>
              {getFieldDecorator('asia_fam45', {
                initialValue: Object.assign({}, asia6[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam45'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 21 - 25</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind16}>
              {getFieldDecorator('asia_ind16', {
                initialValue: Object.assign({}, asia7[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind16'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam16}>
              {getFieldDecorator('asia_fam16', {
                initialValue: Object.assign({}, asia7[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam16'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind26}>
              {getFieldDecorator('asia_ind26', {
                initialValue: Object.assign({}, asia7[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind26'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam26}>
              {getFieldDecorator('asia_fam26', {
                initialValue: Object.assign({}, asia7[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam26'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind36}>
              {getFieldDecorator('asia_ind36', {
                initialValue: Object.assign({}, asia7[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind36'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam36}>
              {getFieldDecorator('asia_fam36', {
                initialValue: Object.assign({}, asia7[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam36'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind46}>
              {getFieldDecorator('asia_ind46', {
                initialValue: Object.assign({}, asia7[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind46'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam46}>
              {getFieldDecorator('asia_fam46', {
                initialValue: Object.assign({}, asia7[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam46'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 26 - 31</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind17}>
              {getFieldDecorator('asia_ind17', {
                initialValue: Object.assign({}, asia8[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind17'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam17}>
              {getFieldDecorator('asia_fam17', {
                initialValue: Object.assign({}, asia8[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam17'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind27}>
              {getFieldDecorator('asia_ind27', {
                initialValue: Object.assign({}, asia8[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind27'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam27}>
              {getFieldDecorator('asia_fam27', {
                initialValue: Object.assign({}, asia8[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam27'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind37}>
              {getFieldDecorator('asia_ind37', {
                initialValue: Object.assign({}, asia8[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind37'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam37}>
              {getFieldDecorator('asia_fam37', {
                initialValue: Object.assign({}, asia8[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam37'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind47}>
              {getFieldDecorator('asia_ind47', {
                initialValue: Object.assign({}, asia8[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind47'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam47}>
              {getFieldDecorator('asia_fam47', {
                initialValue: Object.assign({}, asia8[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam47'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> Additional Week</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind18}>
              {getFieldDecorator('asia_ind18', {
                initialValue: Object.assign({}, asia9[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind18'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam18}>
              {getFieldDecorator('asia_fam18', {
                initialValue: Object.assign({}, asia9[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam18'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind28}>
              {getFieldDecorator('asia_ind28', {
                initialValue: Object.assign({}, asia9[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind28'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam28}>
              {getFieldDecorator('asia_fam28', {
                initialValue: Object.assign({}, asia9[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam28'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind38}>
              {getFieldDecorator('asia_ind38', {
                initialValue: Object.assign({}, asia9[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind38'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam38}>
              {getFieldDecorator('asia_fam38', {
                initialValue: Object.assign({}, asia9[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam38'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind48}>
              {getFieldDecorator('asia_ind48', {
                initialValue: Object.assign({}, asia9[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind48'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam48}>
              {getFieldDecorator('asia_fam48', {
                initialValue: Object.assign({}, asia9[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam48'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> Annual</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind19}>
              {getFieldDecorator('asia_ind19', {
                initialValue: Object.assign({}, asia10[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind19'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam19}>
              {getFieldDecorator('asia_fam19', {
                initialValue: Object.assign({}, asia10[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam19'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_ind29}>
              {getFieldDecorator('asia_ind29', {
                initialValue: Object.assign({}, asia10[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind29'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.asia_fam29}>
              {getFieldDecorator('asia_fam29', {
                initialValue: Object.assign({}, asia10[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam29'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind39}>
              {getFieldDecorator('asia_ind39', {
                initialValue: Object.assign({}, asia10[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind39'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam39}>
              {getFieldDecorator('asia_fam39', {
                initialValue: Object.assign({}, asia10[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam39'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_ind49}>
              {getFieldDecorator('asia_ind49', {
                initialValue: Object.assign({}, asia10[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind49'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.asia_fam49}>
              {getFieldDecorator('asia_fam49', {
                initialValue: Object.assign({}, asia10[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_fam49'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <h6>Travelsafe International WorldWide</h6>
        <Row gutter={24}>
          <Col span={3}>
            <label>Trip Duration</label>
          </Col>
          <Col span={2}>
            <label>Individu (VIP)</label>
          </Col>
          <Col span={2}>
            <label>Family (VIP)</label>
          </Col>
          <Col span={3}>
            <label>Individu (Executive)</label>
          </Col>
          <Col span={3}>
            <label>Family (Executive)</label>
          </Col>
          <Col span={2}>
            <label>Individu (Deluxe)</label>
          </Col>
          <Col span={2}>
            <label>Family (Deluxe)</label>
          </Col>
          <Col span={2}>
            <label>Individu (Superior)</label>
          </Col>
          <Col span={2}>
            <label>Family (Superior)</label>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 1 - 4</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind1}>
              {getFieldDecorator('ww_ind1', {
                initialValue: Object.assign({}, ww1[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam1}>
              {getFieldDecorator('ww_fam1', {
                initialValue: Object.assign({}, ww1[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam1'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind2}>
              {getFieldDecorator('ww_ind2', {
                initialValue: Object.assign({}, ww1[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam2}>
              {getFieldDecorator('ww_fam2', {
                initialValue: Object.assign({}, ww1[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam2'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind3}>
              {getFieldDecorator('ww_ind3', {
                initialValue: Object.assign({}, ww1[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam3}>
              {getFieldDecorator('ww_fam3', {
                initialValue: Object.assign({}, ww1[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam3'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind4}>
              {getFieldDecorator('ww_ind4', {
                initialValue: Object.assign({}, ww1[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind4'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam4}>
              {getFieldDecorator('ww_fam4', {
                initialValue: Object.assign({}, ww1[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam4'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 5 - 6</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind11}>
              {getFieldDecorator('ww_ind11', {
                initialValue: Object.assign({}, ww2[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam11}>
              {getFieldDecorator('ww_fam11', {
                initialValue: Object.assign({}, ww2[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam11'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind21}>
              {getFieldDecorator('ww_ind21', {
                initialValue: Object.assign({}, ww2[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam21}>
              {getFieldDecorator('ww_fam21', {
                initialValue: Object.assign({}, ww2[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam21'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind31}>
              {getFieldDecorator('ww_ind31', {
                initialValue: Object.assign({}, ww2[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam31}>
              {getFieldDecorator('ww_fam31', {
                initialValue: Object.assign({}, ww2[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam31'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind41}>
              {getFieldDecorator('ww_ind41', {
                initialValue: Object.assign({}, ww2[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam41}>
              {getFieldDecorator('ww_fam41', {
                initialValue: Object.assign({}, ww2[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam41'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 7 - 8</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind12}>
              {getFieldDecorator('ww_ind12', {
                initialValue: Object.assign({}, ww3[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'asia_ind12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam12}>
              {getFieldDecorator('ww_fam12', {
                initialValue: Object.assign({}, ww3[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam12'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind22}>
              {getFieldDecorator('ww_ind22', {
                initialValue: Object.assign({}, ww3[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam22}>
              {getFieldDecorator('ww_fam22', {
                initialValue: Object.assign({}, ww3[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam22'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind32}>
              {getFieldDecorator('ww_ind32', {
                initialValue: Object.assign({}, ww3[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam32}>
              {getFieldDecorator('ww_fam32', {
                initialValue: Object.assign({}, ww3[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam32'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind42}>
              {getFieldDecorator('ww_ind42', {
                initialValue: Object.assign({}, ww3[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam42}>
              {getFieldDecorator('ww_fam42', {
                initialValue: Object.assign({}, ww3[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam42'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 9 - 10</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind13}>
              {getFieldDecorator('ww_ind13', {
                initialValue: Object.assign({}, ww4[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam13}>
              {getFieldDecorator('ww_fam13', {
                initialValue: Object.assign({}, ww4[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam13'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind23}>
              {getFieldDecorator('ww_ind23', {
                initialValue: Object.assign({}, ww4[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind23'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam23}>
              {getFieldDecorator('ww_fam23', {
                initialValue: Object.assign({}, ww4[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam23'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind33}>
              {getFieldDecorator('ww_ind33', {
                initialValue: Object.assign({}, ww4[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind33'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam33}>
              {getFieldDecorator('ww_fam33', {
                initialValue: Object.assign({}, ww4[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam33'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind43}>
              {getFieldDecorator('ww_ind43', {
                initialValue: Object.assign({}, ww4[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind43'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam43}>
              {getFieldDecorator('ww_fam43', {
                initialValue: Object.assign({}, ww4[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam43'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 11 - 15</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind14}>
              {getFieldDecorator('ww_ind14', {
                initialValue: Object.assign({}, ww5[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind14'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam14}>
              {getFieldDecorator('ww_fam14', {
                initialValue: Object.assign({}, ww5[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam14'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind24}>
              {getFieldDecorator('ww_ind24', {
                initialValue: Object.assign({}, ww5[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind24'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam24}>
              {getFieldDecorator('ww_fam24', {
                initialValue: Object.assign({}, ww5[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam24'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind34}>
              {getFieldDecorator('ww_ind34', {
                initialValue: Object.assign({}, ww5[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind34'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam34}>
              {getFieldDecorator('ww_fam34', {
                initialValue: Object.assign({}, ww5[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam34'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind44}>
              {getFieldDecorator('ww_ind44', {
                initialValue: Object.assign({}, ww5[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind44'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam44}>
              {getFieldDecorator('ww_fam44', {
                initialValue: Object.assign({}, ww5[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam44'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 16 - 20</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind15}>
              {getFieldDecorator('ww_ind15', {
                initialValue: Object.assign({}, ww6[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind15'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam15}>
              {getFieldDecorator('ww_fam15', {
                initialValue: Object.assign({}, ww6[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam15'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind25}>
              {getFieldDecorator('ww_ind25', {
                initialValue: Object.assign({}, ww6[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind25'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam25}>
              {getFieldDecorator('ww_fam25', {
                initialValue: Object.assign({}, ww6[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam25'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind35}>
              {getFieldDecorator('ww_ind35', {
                initialValue: Object.assign({}, ww6[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind35'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam35}>
              {getFieldDecorator('ww_fam35', {
                initialValue: Object.assign({}, ww6[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam35'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind45}>
              {getFieldDecorator('ww_ind45', {
                initialValue: Object.assign({}, ww6[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind45'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam45}>
              {getFieldDecorator('ww_fam45', {
                initialValue: Object.assign({}, ww6[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam45'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 21 - 25</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind16}>
              {getFieldDecorator('ww_ind16', {
                initialValue: Object.assign({}, ww7[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind16'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam16}>
              {getFieldDecorator('ww_fam16', {
                initialValue: Object.assign({}, ww7[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam16'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind26}>
              {getFieldDecorator('ww_ind26', {
                initialValue: Object.assign({}, ww7[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind26'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam26}>
              {getFieldDecorator('ww_fam26', {
                initialValue: Object.assign({}, ww7[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam26'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind36}>
              {getFieldDecorator('ww_ind36', {
                initialValue: Object.assign({}, ww7[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind36'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam36}>
              {getFieldDecorator('ww_fam36', {
                initialValue: Object.assign({}, ww7[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam36'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind46}>
              {getFieldDecorator('ww_ind46', {
                initialValue: Object.assign({}, ww7[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind46'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam46}>
              {getFieldDecorator('ww_fam46', {
                initialValue: Object.assign({}, ww7[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam46'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> 26 - 31</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind17}>
              {getFieldDecorator('ww_ind17', {
                initialValue: Object.assign({}, ww8[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind17'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam17}>
              {getFieldDecorator('ww_fam17', {
                initialValue: Object.assign({}, ww8[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam17'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind27}>
              {getFieldDecorator('ww_ind27', {
                initialValue: Object.assign({}, ww8[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind27'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam27}>
              {getFieldDecorator('ww_fam27', {
                initialValue: Object.assign({}, ww8[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam27'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind37}>
              {getFieldDecorator('ww_ind37', {
                initialValue: Object.assign({}, ww8[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind37'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam37}>
              {getFieldDecorator('ww_fam37', {
                initialValue: Object.assign({}, ww8[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam37'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind47}>
              {getFieldDecorator('ww_ind47', {
                initialValue: Object.assign({}, ww8[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind47'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam47}>
              {getFieldDecorator('ww_fam47', {
                initialValue: Object.assign({}, ww8[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam47'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> Additional Week</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind18}>
              {getFieldDecorator('ww_ind18', {
                initialValue: Object.assign({}, ww9[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind18'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam18}>
              {getFieldDecorator('ww_fam18', {
                initialValue: Object.assign({}, ww9[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam18'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind28}>
              {getFieldDecorator('ww_ind28', {
                initialValue: Object.assign({}, ww9[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind28'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam28}>
              {getFieldDecorator('ww_fam28', {
                initialValue: Object.assign({}, ww9[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam28'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind38}>
              {getFieldDecorator('ww_ind38', {
                initialValue: Object.assign({}, ww9[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind38'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam38}>
              {getFieldDecorator('ww_fam38', {
                initialValue: Object.assign({}, ww9[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam38'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind48}>
              {getFieldDecorator('ww_ind48', {
                initialValue: Object.assign({}, ww9[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind48'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam48}>
              {getFieldDecorator('ww_fam48', {
                initialValue: Object.assign({}, ww9[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam48'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={3} className="mt-1">
            <label className="mt-1"> Annual</label>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind19}>
              {getFieldDecorator('ww_ind19', {
                initialValue: Object.assign({}, ww10[3]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind19'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam19}>
              {getFieldDecorator('ww_fam19', {
                initialValue: Object.assign({}, ww10[3]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam19'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_ind29}>
              {getFieldDecorator('ww_ind29', {
                initialValue: Object.assign({}, ww10[1]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind29'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item {...errorsField.ww_fam29}>
              {getFieldDecorator('ww_fam29', {
                initialValue: Object.assign({}, ww10[1]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam29'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind39}>
              {getFieldDecorator('ww_ind39', {
                initialValue: Object.assign({}, ww10[0]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind39'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam39}>
              {getFieldDecorator('ww_fam39', {
                initialValue: Object.assign({}, ww10[0]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam39'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_ind49}>
              {getFieldDecorator('ww_ind49', {
                initialValue: Object.assign({}, ww10[2]).individual_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_ind49'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item {...errorsField.ww_fam49}>
              {getFieldDecorator('ww_fam49', {
                initialValue: Object.assign({}, ww10[2]).family_amount || 0,
                rules: [
                  ...Helper.fieldRules(['required'], 'ww_fam49'),
                  {
                    validator: (rule, value) => {
                      if (!value) return Promise.resolve()

                      if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                      if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                      return Promise.resolve()
                    },
                  }],
              })(
                  <Input />,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormTravelInternational.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormTravelInternational)

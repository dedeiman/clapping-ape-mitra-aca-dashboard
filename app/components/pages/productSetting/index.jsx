import PropTypes from 'prop-types'
import { Forbidden } from 'components/elements'
import SelectProduct from 'containers/pages/productSetting/SelectProduct'
import OtomateSetting from 'containers/pages/productSetting/Otomate'
import OtomateSmartSetting from 'containers/pages/productSetting/OtomateSmart'
import OtomateSolitaireSetting from 'containers/pages/productSetting/OtomateSolitaire'
import ComprehensiveSetting from 'containers/pages/productSetting/Comprehensive'
import TLOSetting from 'containers/pages/productSetting/TLO'
import CargoSetting from 'containers/pages/productSetting/Cargo'
import LiabilitySetting from 'containers/pages/productSetting/Liability'
import TravelDomesticSetting from 'containers/pages/productSetting/TravelDomestic'
import TravelInternationalSetting from 'containers/pages/productSetting/TravelInternational'
import FormLiability from 'containers/pages/productSetting/FormLiability'
import FormWellWoman from 'containers/pages/productSetting/FormWellWoman'
import FormPAAmanah from 'containers/pages/productSetting/FormPAAmanah'
import FormTravelDomestic from 'containers/pages/productSetting/FormTravelDomestic'
import FormTravelInternational from 'containers/pages/productSetting/FormTravelInternational'
import FormOtomateSmart from 'containers/pages/productSetting/FormOtomateSmart'
import FormOtomateSolitare from 'containers/pages/productSetting/FormOtomateSolitare'
import FormOtomate from 'containers/pages/productSetting/FormOtomate'
import FormAsri from 'containers/pages/productSetting/FormAsri'
import FormCargo from 'containers/pages/productSetting/FormCargo'
import WellWomanSetting from 'containers/pages/productSetting/WellWoman'
import PAAmanahSetting from 'containers/pages/productSetting/PAAmanah'
import AsriSetting from 'containers/pages/productSetting/Asri'

const productSetting = ({ location }) => {
  if (location.pathname === '/product-setting') {
    return <SelectProduct location={location} />
  }
  if (location.pathname === '/product-setting/liability/edit') {
    return <FormLiability location={location} />
  }
  if (location.pathname === '/product-setting/travel-domestic/edit') {
    return <FormTravelDomestic location={location} />
  }
  if (location.pathname === '/product-setting/travel-international/edit') {
    return <FormTravelInternational location={location} />
  }
  if (location.pathname === '/product-setting/otomate-smart/edit') {
    return <FormOtomateSmart location={location} />
  }
  if (location.pathname === '/product-setting/otomate-solitare/edit') {
    return <FormOtomateSolitare location={location} />
  }
  if (location.pathname === '/product-setting/otomate/edit') {
    return <FormOtomate location={location} />
  }
  if (location.pathname === '/product-setting/wellwoman/edit') {
    return <FormWellWoman location={location} />
  }
  if (location.pathname === '/product-setting/paamanah/edit') {
    return <FormPAAmanah location={location} />
  }
  if (location.pathname === '/product-setting/cargo/edit') {
    return <FormCargo location={location} />
  }
  if (location.pathname === '/product-setting/asri/edit') {
    return <FormAsri location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/otomate')) {
    return <OtomateSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/otomate-smart')) {
    return <OtomateSmartSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/otomate-solitaire')) {
    return <OtomateSolitaireSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/tlo')) {
    return <TLOSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/comprehensive')) {
    return <ComprehensiveSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/cargo')) {
    return <CargoSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/travel-domestic')) {
    return <TravelDomesticSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/travel-international')) {
    return <TravelInternationalSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/liability')) {
    return <LiabilitySetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/wellwoman')) {
    return <WellWomanSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/paamanah')) {
    return <PAAmanahSetting location={location} />
  }
  if (location.pathname.includes('/edit') || (location.pathname === '/product-setting/asri')) {
    return <AsriSetting location={location} />
  }

  return ''
}

productSetting.propTypes = {
  currentUser: PropTypes.object,
  groupRole: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
}

export default productSetting

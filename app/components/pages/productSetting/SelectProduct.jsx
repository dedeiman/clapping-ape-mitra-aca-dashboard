import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card,
  Button, Select,
  Radio, Input,
} from 'antd'
import { Form } from '@ant-design/compatible'
import Helper from 'utils/Helper'

const CreateCOB = ({
  stateCategory, handleProduct,
  handleProductType, getProduct,
  onSubmit, form, stateCategorySyariah,
}) => {
  const { getFieldDecorator, setFieldsValue, getFieldValue } = form
  return (
    <>
      <Row className="pt-5 mt-5" gutter={16} type="flex" justify="center">
        <Col xs={24} md={20}>
          <Card className="m-0 auto">
            <Row>
              <Col md={12}>
                <h4 className="text-primary2 mb-5">Select Product</h4>
              </Col>
              <Col md={12} offset={4}>
                <Form
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 16 }}
                  onSubmit={onSubmit}
                >
                  <Form.Item>
                    {getFieldDecorator('type_product', {
                      initialValue: 'konvensional',
                      rules: Helper.fieldRules(['required']),
                    })(
                      <Radio.Group
                        size="large"
                        className="field-lg mb-3 w-100"
                        onChange={(e) => {
                          handleProductType(e.target.value)
                          setFieldsValue({
                            cob: undefined,
                            product: undefined,
                          })
                        }}
                      >
                        <Radio value="konvensional">Konvensional</Radio>
                        <Radio value="syariah">Syariah</Radio>
                      </Radio.Group>,
                    )}
                  </Form.Item>
                  <Form.Item label="Class Bussiness">
                    {getFieldDecorator('cob', {
                      rules: Helper.fieldRules(['required'], 'Class Bussiness'),
                    })(
                      <Select
                        onChange={(e) => {
                          handleProduct(e)
                        }}
                        style={{ width: '350px' }}
                        loading={stateCategory.loading}
                        placeholder="Select COB"
                        className="field-lg w-100"
                        disabled={getProduct.type === ''}
                        onSelect={() => {
                          setFieldsValue({
                            product_id: '',
                            product: undefined,
                            product_name: undefined,
                          })
                        }}
                      >
                        {getFieldValue('type_product') === 'konvensional'
                            && (stateCategory.list || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))
                        }
                        {getFieldValue('type_product') === 'syariah'
                            && (stateCategorySyariah.list || []).map(item => (
                              <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                            ))
                        }
                      </Select>,
                    )}
                  </Form.Item>
                  <Form.Item label="Product" className="mt-4">
                    {getFieldDecorator('product', {
                      rules: Helper.fieldRules(['required'], 'Product'),
                    })(
                      <Select
                        style={{ width: '350px' }}
                        loading={getProduct.loading}
                        placeholder="Select Product"
                        className="field-lg w-100"
                        disabled={!getFieldValue('cob')}
                        onSelect={(val) => {
                          const data = getProduct.list.find(item => item.name === val)
                          setFieldsValue({
                            product_id: data.id,
                            product: val,
                          })
                        }}
                      >
                        {(getProduct.list || []).map(item => (
                          <Select.Option key={item.id} value={item.name}>{item.display_name}</Select.Option>
                        ))}
                      </Select>,
                    )}
                  </Form.Item>
                  <Form.Item label="Product ID" className="d-none">
                    {getFieldDecorator('product_id', {
                      rules: Helper.fieldRules(['required']),
                    })(
                      <Input
                        className="field-lg"
                        placeholder=""
                        disabled
                      />,
                    )}
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary button-lg mt-4 mb-3" htmlType="submit" disabled={false}>
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  )
}

CreateCOB.propTypes = {
  stateCategory: PropTypes.object,
  stateCategorySyariah: PropTypes.object,
  handleProduct: PropTypes.func,
  handleProductType: PropTypes.func,
  getProduct: PropTypes.object,
  onSubmit: PropTypes.func,
  form: PropTypes.any,
}

export default CreateCOB

import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Table, Collapse,
  Card,
  Button,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse

const columns = [
  {
    title: 'Trip Duration',
    dataIndex: 'desc',
    key: 'desc',
    render: (text, row, index) => {
      if (index > 0) {
        return <a>{text}</a>;
      }
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: 1,
        },
      };
    },
  },
  {
    title: 'VIP',
    children: [
      {
        title: 'Individu',
        dataIndex: 'individu1',
        key: 'individu1',
      },
      {
        title: 'Family',
        dataIndex: 'family1',
        key: 'family1',
      },
    ],
  },
  {
    title: 'Executive',
    children: [
      {
        title: 'Individu',
        dataIndex: 'individu2',
        key: 'individu2',
      },
      {
        title: 'Family',
        dataIndex: 'family2',
        key: 'family2',
      },
    ],
  },
  {
    title: 'Deluxe',
    children: [
      {
        title: 'Individu',
        dataIndex: 'individu3',
        key: 'individu3',
      },
      {
        title: 'Family',
        dataIndex: 'family3',
        key: 'family3',
      },
    ],
  },
  {
    title: 'Superior',
    children: [
      {
        title: 'Individu',
        dataIndex: 'individu4',
        key: 'individu4',
      },
      {
        title: 'Family',
        dataIndex: 'family4',
        key: 'family4',
      },
    ],
  },
]

const TravelInternationalSetting = ({
  stateLiability,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const dataAsia = Object.assign({}, stateLiability.list.travel_international_asia)
  const dataWW = Object.assign({}, stateLiability.list.travel_international_worldwide)
  const asia1 = Object.assign({}, dataAsia['1-4'])
  const asia2 = Object.assign({}, dataAsia['5-6'])
  const asia3 = Object.assign({}, dataAsia['7-8'])
  const asia4 = Object.assign({}, dataAsia['9-10'])
  const asia5 = Object.assign({}, dataAsia['11-15'])
  const asia6 = Object.assign({}, dataAsia['16-20'])
  const asia7 = Object.assign({}, dataAsia['21-25'])
  const asia8 = Object.assign({}, dataAsia['26-31'])
  const asia9 = Object.assign({}, dataAsia.additional_week)
  const asia10 = Object.assign({}, dataAsia.annual)
  const ww1 = Object.assign({}, dataWW['1-4'])
  const ww2 = Object.assign({}, dataWW['5-6'])
  const ww3 = Object.assign({}, dataWW['7-8'])
  const ww4 = Object.assign({}, dataWW['9-10'])
  const ww5 = Object.assign({}, dataWW['11-15'])
  const ww6 = Object.assign({}, dataWW['16-20'])
  const ww7 = Object.assign({}, dataWW['21-25'])
  const ww8 = Object.assign({}, dataWW['26-31'])
  const ww9 = Object.assign({}, dataWW.additional_week)
  const ww10 = Object.assign({}, dataWW.annual)
  const dataTSFWD = [
    {
      key: '1',
      desc: '1 - 4',
      individu1: Object.assign({}, asia1[3]).individual_amount,
      family1: Object.assign({}, asia1[3]).family_amount,
      individu2: Object.assign({}, asia1[1]).individual_amount,
      family2: Object.assign({}, asia1[1]).family_amount,
      individu3: Object.assign({}, asia1[0]).individual_amount,
      family3: Object.assign({}, asia1[0]).family_amount,
      individu4: Object.assign({}, asia1[2]).individual_amount,
      family4: Object.assign({}, asia1[2]).family_amount,
    },
    {
      key: '2',
      desc: '5 - 6',
      individu1: Object.assign({}, asia2[3]).individual_amount,
      family1: Object.assign({}, asia2[3]).family_amount,
      individu2: Object.assign({}, asia2[1]).individual_amount,
      family2: Object.assign({}, asia2[1]).family_amount,
      individu3: Object.assign({}, asia2[0]).individual_amount,
      family3: Object.assign({}, asia2[0]).family_amount,
      individu4: Object.assign({}, asia2[2]).individual_amount,
      family4: Object.assign({}, asia2[2]).family_amount,
    },
    {
      key: '3',
      desc: '7 - 8',
      individu1: Object.assign({}, asia3[3]).individual_amount,
      family1: Object.assign({}, asia3[3]).family_amount,
      individu2: Object.assign({}, asia3[1]).individual_amount,
      family2: Object.assign({}, asia3[1]).family_amount,
      individu3: Object.assign({}, asia3[0]).individual_amount,
      family3: Object.assign({}, asia3[0]).family_amount,
      individu4: Object.assign({}, asia3[2]).individual_amount,
      family4: Object.assign({}, asia3[2]).family_amount,
    },
    {
      key: '4',
      desc: '9 - 10',
      individu1: Object.assign({}, asia4[3]).individual_amount,
      family1: Object.assign({}, asia4[3]).family_amount,
      individu2: Object.assign({}, asia4[1]).individual_amount,
      family2: Object.assign({}, asia4[1]).family_amount,
      individu3: Object.assign({}, asia4[0]).individual_amount,
      family3: Object.assign({}, asia4[0]).family_amount,
      individu4: Object.assign({}, asia4[2]).individual_amount,
      family4: Object.assign({}, asia4[2]).family_amount,
    },
    {
      key: '5',
      desc: '11 - 15',
      individu1: Object.assign({}, asia5[3]).individual_amount,
      family1: Object.assign({}, asia5[3]).family_amount,
      individu2: Object.assign({}, asia5[1]).individual_amount,
      family2: Object.assign({}, asia5[1]).family_amount,
      individu3: Object.assign({}, asia5[0]).individual_amount,
      family3: Object.assign({}, asia5[0]).family_amount,
      individu4: Object.assign({}, asia5[2]).individual_amount,
      family4: Object.assign({}, asia5[2]).family_amount,
    },
    {
      key: '6',
      desc: '16 - 20',
      individu1: Object.assign({}, asia6[3]).individual_amount,
      family1: Object.assign({}, asia6[3]).family_amount,
      individu2: Object.assign({}, asia6[1]).individual_amount,
      family2: Object.assign({}, asia6[1]).family_amount,
      individu3: Object.assign({}, asia6[0]).individual_amount,
      family3: Object.assign({}, asia6[0]).family_amount,
      individu4: Object.assign({}, asia6[2]).individual_amount,
      family4: Object.assign({}, asia6[2]).family_amount,
    },
    {
      key: '7',
      desc: '21 - 25',
      individu1: Object.assign({}, asia7[3]).individual_amount,
      family1: Object.assign({}, asia7[3]).family_amount,
      individu2: Object.assign({}, asia7[1]).individual_amount,
      family2: Object.assign({}, asia7[1]).family_amount,
      individu3: Object.assign({}, asia7[0]).individual_amount,
      family3: Object.assign({}, asia7[0]).family_amount,
      individu4: Object.assign({}, asia7[2]).individual_amount,
      family4: Object.assign({}, asia7[2]).family_amount,
    },
    {
      key: '8',
      desc: '26 - 31',
      individu1: Object.assign({}, asia8[3]).individual_amount,
      family1: Object.assign({}, asia8[3]).family_amount,
      individu2: Object.assign({}, asia8[1]).individual_amount,
      family2: Object.assign({}, asia8[1]).family_amount,
      individu3: Object.assign({}, asia8[0]).individual_amount,
      family3: Object.assign({}, asia8[0]).family_amount,
      individu4: Object.assign({}, asia8[2]).individual_amount,
      family4: Object.assign({}, asia8[2]).family_amount,
    },
    {
      key: '9',
      desc: 'Additional Week',
      individu1: Object.assign({}, asia9[3]).individual_amount,
      family1: Object.assign({}, asia9[3]).family_amount,
      individu2: Object.assign({}, asia9[1]).individual_amount,
      family2: Object.assign({}, asia9[1]).family_amount,
      individu3: Object.assign({}, asia9[0]).individual_amount,
      family3: Object.assign({}, asia9[0]).family_amount,
      individu4: Object.assign({}, asia9[2]).individual_amount,
      family4: Object.assign({}, asia9[2]).family_amount,
    },
    {
      key: '10',
      desc: 'Annual',
      individu1: Object.assign({}, asia10[3]).individual_amount,
      family1: Object.assign({}, asia10[3]).family_amount,
      individu2: Object.assign({}, asia10[1]).individual_amount,
      family2: Object.assign({}, asia10[1]).family_amount,
      individu3: Object.assign({}, asia10[0]).individual_amount,
      family3: Object.assign({}, asia10[0]).family_amount,
      individu4: Object.assign({}, asia10[2]).individual_amount,
      family4: Object.assign({}, asia10[2]).family_amount,
    },
  ]
  const dataEqvet = [
    {
      key: '1',
      desc: '1 - 4',
      individu1: Object.assign({}, ww1[3]).individual_amount,
      family1: Object.assign({}, ww1[3]).family_amount,
      individu2: Object.assign({}, ww1[1]).individual_amount,
      family2: Object.assign({}, ww1[1]).family_amount,
      individu3: Object.assign({}, ww1[0]).individual_amount,
      family3: Object.assign({}, ww1[0]).family_amount,
      individu4: Object.assign({}, ww1[2]).individual_amount,
      family4: Object.assign({}, ww1[2]).family_amount,
    },
    {
      key: '2',
      desc: '5 - 6',
      individu1: Object.assign({}, ww2[3]).individual_amount,
      family1: Object.assign({}, ww2[3]).family_amount,
      individu2: Object.assign({}, ww2[1]).individual_amount,
      family2: Object.assign({}, ww2[1]).family_amount,
      individu3: Object.assign({}, ww2[0]).individual_amount,
      family3: Object.assign({}, ww2[0]).family_amount,
      individu4: Object.assign({}, ww2[2]).individual_amount,
      family4: Object.assign({}, ww2[2]).family_amount,
    },
    {
      key: '3',
      desc: '7 - 8',
      individu1: Object.assign({}, ww3[3]).individual_amount,
      family1: Object.assign({}, ww3[3]).family_amount,
      individu2: Object.assign({}, ww3[1]).individual_amount,
      family2: Object.assign({}, ww3[1]).family_amount,
      individu3: Object.assign({}, ww3[0]).individual_amount,
      family3: Object.assign({}, ww3[0]).family_amount,
      individu4: Object.assign({}, ww3[2]).individual_amount,
      family4: Object.assign({}, ww3[2]).family_amount,
    },
    {
      key: '4',
      desc: '9 - 10',
      individu1: Object.assign({}, ww4[3]).individual_amount,
      family1: Object.assign({}, ww4[3]).family_amount,
      individu2: Object.assign({}, ww4[1]).individual_amount,
      family2: Object.assign({}, ww4[1]).family_amount,
      individu3: Object.assign({}, ww4[0]).individual_amount,
      family3: Object.assign({}, ww4[0]).family_amount,
      individu4: Object.assign({}, ww4[2]).individual_amount,
      family4: Object.assign({}, ww4[2]).family_amount,
    },
    {
      key: '5',
      desc: '11 - 15',
      individu1: Object.assign({}, ww5[3]).individual_amount,
      family1: Object.assign({}, ww5[3]).family_amount,
      individu2: Object.assign({}, ww5[1]).individual_amount,
      family2: Object.assign({}, ww5[1]).family_amount,
      individu3: Object.assign({}, ww5[0]).individual_amount,
      family3: Object.assign({}, ww5[0]).family_amount,
      individu4: Object.assign({}, ww5[2]).individual_amount,
      family4: Object.assign({}, ww5[2]).family_amount,
    },
    {
      key: '6',
      desc: '16 - 20',
      individu1: Object.assign({}, ww6[3]).individual_amount,
      family1: Object.assign({}, ww6[3]).family_amount,
      individu2: Object.assign({}, ww6[1]).individual_amount,
      family2: Object.assign({}, ww6[1]).family_amount,
      individu3: Object.assign({}, ww6[0]).individual_amount,
      family3: Object.assign({}, ww6[0]).family_amount,
      individu4: Object.assign({}, ww6[2]).individual_amount,
      family4: Object.assign({}, ww6[2]).family_amount,
    },
    {
      key: '7',
      desc: '21 - 25',
      individu1: Object.assign({}, ww7[3]).individual_amount,
      family1: Object.assign({}, ww7[3]).family_amount,
      individu2: Object.assign({}, ww7[1]).individual_amount,
      family2: Object.assign({}, ww7[1]).family_amount,
      individu3: Object.assign({}, ww7[0]).individual_amount,
      family3: Object.assign({}, ww7[0]).family_amount,
      individu4: Object.assign({}, ww7[2]).individual_amount,
      family4: Object.assign({}, ww7[2]).family_amount,
    },
    {
      key: '8',
      desc: '26 - 31',
      individu1: Object.assign({}, ww8[3]).individual_amount,
      family1: Object.assign({}, ww8[3]).family_amount,
      individu2: Object.assign({}, ww8[1]).individual_amount,
      family2: Object.assign({}, ww8[1]).family_amount,
      individu3: Object.assign({}, ww8[0]).individual_amount,
      family3: Object.assign({}, ww8[0]).family_amount,
      individu4: Object.assign({}, ww8[2]).individual_amount,
      family4: Object.assign({}, ww8[2]).family_amount,
    },
    {
      key: '9',
      desc: 'Additional Week',
      individu1: Object.assign({}, ww9[3]).individual_amount,
      family1: Object.assign({}, ww9[3]).family_amount,
      individu2: Object.assign({}, ww9[1]).individual_amount,
      family2: Object.assign({}, ww9[1]).family_amount,
      individu3: Object.assign({}, ww9[0]).individual_amount,
      family3: Object.assign({}, ww9[0]).family_amount,
      individu4: Object.assign({}, ww9[2]).individual_amount,
      family4: Object.assign({}, ww9[2]).family_amount,
    },
    {
      key: '10',
      desc: 'Annual',
      individu1: Object.assign({}, ww10[3]).individual_amount,
      family1: Object.assign({}, ww10[3]).family_amount,
      individu2: Object.assign({}, ww10[1]).individual_amount,
      family2: Object.assign({}, ww10[1]).family_amount,
      individu3: Object.assign({}, ww10[0]).individual_amount,
      family3: Object.assign({}, ww10[0]).family_amount,
      individu4: Object.assign({}, ww10[2]).individual_amount,
      family4: Object.assign({}, ww10[2]).family_amount,
    },
  ]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <div className="text-primary2">Product - Travel international</div>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="float-md-right mt-3 mt-md-0" onClick={() => history.push('/product-setting/travel-international/edit')}>
              Edit Travel International
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {(!isFetching && !isEmpty(stateLiability)) && (
            <>
              <Collapse className="mt-3" defaultActiveKey={['1']} accordion="true" expandIconPosition="right">
                <Panel header="Travelsafe International ASIA" key="1">
                  <Table columns={columns} dataSource={dataTSFWD} pagination={false} bordered size="small" className="mb-4" />
                </Panel>
              </Collapse>
              <Collapse defaultActiveKey={['2']} className="mt-4" expandIconPosition="right">
                <Panel header="Travelsafe International Worldwide" key="2">
                  <Table columns={columns} dataSource={dataEqvet} bordered size="small" pagination={false} />
                </Panel>
              </Collapse>
            </>
        )}
      </Card>
    </React.Fragment>
  )
}

TravelInternationalSetting.propTypes = {
  stateLiability: PropTypes.object,
  isFetching: PropTypes.bool,
}

export default TravelInternationalSetting

/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-dupe-keys */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card, Form, Input,
  Button, Select,
  Row, Col,
  Table,
} from 'antd'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { Loader } from '../../elements'

const columns = [
  {
    title: 'Country',
    dataIndex: 'name',
  },
]


const FormAsri = ({
  detailProduct, handleSubmit,
  form, stateLiability, isFetching,
  errorsField,
}) => {
  const { getFieldDecorator, validateFields, getFieldValue } = form
  const data = [stateLiability.list]

  return (
    <Card title="Edit Product Setting - Liability">
      {isFetching && <Loader />}
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          validateFields((err, values) => {
            if (!err) {
              handleSubmit(values)
            }
          })
        }}
      >
        <Card title="Rate Asri (Total)">
          <Row gutter={24}>
            <Col span={10}>
              <Form.Item label="Kelas 1" {...errorsField.std_kls1}>
                {getFieldDecorator('std_kls1', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_standard)[1]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'std_kls1'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                  <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item label="Kelas 2" {...errorsField.std_kls2}>
                {getFieldDecorator('std_kls2', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_standard)[0]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'std_kls2'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                  <Input />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Rate ASRI + TSFWD (Total)" className="mt-2">
          <Row gutter={24}>
            <Col span={10}>
              <Form.Item label="Kelas 1" {...errorsField.tsfwd_kls1}>
                {getFieldDecorator('tsfwd_kls1', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd)[0]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'tsfwd_kls1'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                  <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item label="Kelas 2" {...errorsField.tsfwd_kls2}>
                {getFieldDecorator('tsfwd_kls2', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd)[1]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'tsfwd_kls2'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                  <Input />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="Rate ASRI + EQVET (Total)" className="mt-2">
          <Row gutter={24}>
            <Col span={4}>
              <Form.Item label="Zona I (Kelas 1)" {...errorsField.eqvet_11}>
                {getFieldDecorator('eqvet_11', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[0]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_11'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                  <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona II (Kelas 1)" {...errorsField.eqvet_21}>
                {getFieldDecorator('eqvet_21', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[1]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_21'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona III (Kelas 1)" {...errorsField.eqvet_31}>
                {getFieldDecorator('eqvet_31', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[2]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_31'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona IV (Kelas 1)" {...errorsField.eqvet_41}>
                {getFieldDecorator('eqvet_41', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[3]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_41'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona V (Kelas 1)" {...errorsField.eqvet_51}>
                {getFieldDecorator('eqvet_51', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[4]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_51'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={5}>
              <Form.Item label="Zona I (Kelas 2)" {...errorsField.eqvet_12}>
                {getFieldDecorator('eqvet_12', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[5]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_12'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={5}>
              <Form.Item label="Zona II (Kelas 2)" {...errorsField.eqvet_22}>
                {getFieldDecorator('eqvet_22', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[6]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_22'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Card title="RATE ASRI + EQVET + TSFWD (Total)" className="mt-2 mb-3">
          <Row gutter={24}>
            <Col span={4}>
              <Form.Item label="Zona I (Kelas 1)" {...errorsField.eqvet_tsfwd_11}>
                {getFieldDecorator('eqvet_tsfwd_11', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[0]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_11'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona II (Kelas 1)" {...errorsField.eqvet_tsfwd_21}>
                {getFieldDecorator('eqvet_tsfwd_21', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[1]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_21'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona III (Kelas 1)" {...errorsField.eqvet_tsfwd_31}>
                {getFieldDecorator('eqvet_tsfwd_31', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[2]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_31'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona IV (Kelas 1)" {...errorsField.eqvet_tsfwd_41}>
                {getFieldDecorator('eqvet_tsfwd_41', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[3]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_41'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item label="Zona V (Kelas 1)" {...errorsField.eqvet_tsfwd_51}>
                {getFieldDecorator('eqvet_tsfwd_51', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[4]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_51'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={5}>
              <Form.Item label="Zona I (Kelas 2)" {...errorsField.eqvet_tsfwd_12}>
                {getFieldDecorator('eqvet_tsfwd_12', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[5]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_12'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
            <Col span={5}>
              <Form.Item label="Zona II (Kelas 2)" {...errorsField.eqvet_tsfwd_22}>
                {getFieldDecorator('eqvet_tsfwd_22', {
                  initialValue: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[6]).rate,
                  rules: [
                    ...Helper.fieldRules(['required'], 'eqvet_tsfwd_22'),
                    {
                      validator: (rule, value) => {
                        if (!value) return Promise.resolve()

                        if (isNaN(value)) return Promise.reject('*Harus menggunakan angka')

                        if (Number(value) < 0) return Promise.reject('*Nilai harus positif')

                        return Promise.resolve()
                      },
                    }],
                })(
                    <Input />,
                )}
              </Form.Item>
            </Col>
          </Row>
        </Card>
        <Form.Item className="text-right">
          <Button type="secondary" className="mr-3" onClick={() => history.goBack()}>Cancel</Button>
          <Button type="primary" htmlType="submit">Submit Change</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

FormAsri.propTypes = {
  stateLiability: PropTypes.object,
  detailProduct: PropTypes.object,
  handleSubmit: PropTypes.func,
  form: PropTypes.any,
  isEdit: PropTypes.bool,
  handleSelect: PropTypes.func,
  handleGetCountries: PropTypes.func,
  handleChecked: PropTypes.func,
  getCountries: PropTypes.object,
  loadCOB: PropTypes.bool,
  handlePage: PropTypes.func,
  metaCountries: PropTypes.object,
  handleFilter: PropTypes.func,
  errorsField: PropTypes.object,
  setErrorsField: PropTypes.func,
  goToDetailCountry: PropTypes.func,
  selectOption: PropTypes.array,
  isFetching: PropTypes.bool,
}

export default Form.create({ name: 'productForm' })(FormAsri)

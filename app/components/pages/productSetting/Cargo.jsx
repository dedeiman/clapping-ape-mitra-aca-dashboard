import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Table,
  Card, Button,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty } from 'lodash'
import history from 'utils/history'

const { Column, ColumnGroup } = Table


const CargoSetting = ({
  stateLiability,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const data = [stateLiability.list]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <div className="text-primary2">Product - Cargo</div>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="float-md-right mt-3 mt-md-0" onClick={() => history.push('/product-setting/cargo/edit')}>
              Edit Cargo
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {!isEmpty(stateLiability)
          ? (
            <>
              <Table pagination={false} bordered dataSource={data}>
                <Column title="Min Rate (%)" dataIndex="min_rate" key="min_rate" />
                <Column title="Max Rate (%)" dataIndex="max_rate" key="max_rate" />
              </Table>
            </>
          )
          : <div className="py-5" />
          }
      </Card>
    </React.Fragment>
  )
}

CargoSetting.propTypes = {
  stateLiability: PropTypes.object,
  isFetching: PropTypes.bool,
}

export default CargoSetting

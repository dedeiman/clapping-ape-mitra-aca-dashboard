/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Input, Button,
  AutoComplete,
  Upload, Avatar,
} from 'antd'
import { InboxOutlined } from '@ant-design/icons'
import { Form } from '@ant-design/compatible'

const ChangeAgent = ({
  toggle, form, onSubmit,
  loadSubmitBtn, handleSearchAgentBefore,
  handleSearchAgentAfter,
  stateSelects, detailCustomer,
  stateFile, handleUpload,
}) => {
  const { getFieldDecorator, setFieldsValue } = form
  return (
    <Form onSubmit={onSubmit}>
      <Row gutter={24}>
        <Col span={11}>
          <p className="mb-1">Agent ID Before</p>
          <Form.Item>
            {getFieldDecorator('agent_id_before', {
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
              getValueFromEvent: value => (value || '').toUpperCase(),
            })(
              <AutoComplete
                allowClear
                onSelect={(val) => {
                  const dataAgent = (stateSelects.agentListBefore || []).filter(item => item.agent_id === val)

                  setFieldsValue({
                    profile_id_before: dataAgent[0].profile_id,
                  })
                }}
                onSearch={handleSearchAgentBefore}
                options={(stateSelects.agentListBefore || []).map(item => ({ value: item.agent_id, label: `${item.agent_id} - ${(item.name).toUpperCase()}` }))}
              >
                <Input className="field-lg capitalize" placeholder="Input Agent ID" />
              </AutoComplete>,
            )}
          </Form.Item>
        </Col>
        <Col span={2} className="d-flex align-items-center"><i className="las la-random mr-2" /></Col>
        <Col span={11}>
          <p className="mb-1">Agent ID After</p>
          <Form.Item>
            {getFieldDecorator('agent_id_after', {
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
              getValueFromEvent: value => (value || '').toUpperCase(),
            })(
              <AutoComplete
                allowClear
                onSelect={(val) => {
                  const dataAgent = (stateSelects.agentListAfter || []).filter(item => item.agent_id === val)

                  setFieldsValue({
                    profile_id_after: dataAgent[0].profile_id,
                  })
                }}
                onSearch={handleSearchAgentAfter}
                options={(stateSelects.agentListAfter || []).map(item => ({ value: item.agent_id, label: `${item.agent_id} - ${(item.name).toUpperCase()}` }))}
              >
                <Input className="field-lg capitalize" placeholder="Input Agent ID" />
              </AutoComplete>,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={11}>
          <p className="mb-1">Customer Number</p>
          <Form.Item>
            {getFieldDecorator('customer_number', {
              initialValue: detailCustomer.customer_number,
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
            })(
              <Input
                className="field-lg"
                placeholder="Input Customer Number"
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={2} />
        <Col span={11}>
          <p className="mb-1">Customer ID</p>
          <Form.Item>
            {getFieldDecorator('customer_id', {
              initialValue: detailCustomer.id,
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
            })(
              <Input
                disabled
                className="field-lg"
                placeholder="Customer ID"
              />,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={11}>
          <p className="mb-1">Profile ID Before</p>
          <Form.Item>
            {getFieldDecorator('profile_id_before', {
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
            })(
              <Input
                className="field-lg"
                placeholder="Profile ID"
              />,
            )}
          </Form.Item>
        </Col>
        <Col span={2} />
        <Col span={11}>
          <p className="mb-1">Profile ID After</p>
          <Form.Item>
            {getFieldDecorator('profile_id_after', {
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
            })(
              <Input
                className="field-lg"
                placeholder="Profile ID"
              />,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={24}>
          <p className="mb-0">File</p>
          <Form.Item>
            {getFieldDecorator('file', {
              rules: [
                {
                  validator: (rule, value) => {
                    if (!value) return Promise.resolve()

                    return Promise.resolve()
                  },
                },
              ],
            })(
              <Upload
                accept="image/*,.pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                name="icon"
                listType="picture-card"
                className="avatar-uploader banner-content upload-doc"
                showUploadList={false}
                beforeUpload={() => false}
                onChange={info => handleUpload(info, 'file')}
              >
                {stateFile.file
                  ? (
                    <Avatar src={stateFile.file} shape="square" className="banner-preview" />
                  )
                  : (
                    <div>
                      <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                      </p>
                      <p className="ant-upload-text">Upload Document</p>
                    </div>
                  )
                }
              </Upload>,
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={24} className="d-flex justify-content-end align-items-center">
          <Button
            size="large"
            type="primary"
            htmlType="submit"
            className="mr-2"
            disabled={loadSubmitBtn}
          >
            Submit
          </Button>
          <Button
            ghost
            size="large"
            type="primary"
            onClick={() => {
              form.resetFields()
              toggle()
            }}
          >
            Cancel
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

ChangeAgent.propTypes = {
  toggle: PropTypes.func,
  form: PropTypes.any,
  onSubmit: PropTypes.func,
  stateSelects: PropTypes.object,
  handleSearchAgentBefore: PropTypes.func,
  handleSearchAgentAfter: PropTypes.func,
  handleUpload: PropTypes.func,
  stateFile: PropTypes.object,
  loadSubmitBtn: PropTypes.bool,
}

export default ChangeAgent

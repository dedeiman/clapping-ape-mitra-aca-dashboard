import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Table, Collapse,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse;
const { Column, ColumnGroup } = Table

function callback(key) {
  console.log(key);
}

const columnsBencana = [

  {
    title: 'Batasan Usia',
    dataIndex: 'bu',
    key: 'bu',
  },
  {
    title: 'Plan 1',
    dataIndex: 'plan1',
    key: 'plan1',
  },
  {
    title: 'Plan 2',
    dataIndex: 'plan2',
    key: 'plan2',
  },
  {
    title: 'Plan 3',
    dataIndex: 'plan3',
    key: 'plan3',
  },
]

const WellWomanSetting = ({
  stateLiability,
  loadCustomer,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const ub1 = Object.assign({}, stateLiability.list['18-40'])
  const ub2 = Object.assign({}, stateLiability.list['41-55'])
  const ub3 = Object.assign({}, stateLiability.list['56-64'])
  const dataTable = [
    {
      bu: '18-40',
      plan1: Object.assign({}, ub1[0]).premium_amount,
      plan2: Object.assign({}, ub1[1]).premium_amount,
      plan3: Object.assign({}, ub1[2]).premium_amount,
    },
    {
      bu: '41-55',
      plan1: Object.assign({}, ub2[0]).premium_amount,
      plan2: Object.assign({}, ub2[1]).premium_amount,
      plan3: Object.assign({}, ub2[2]).premium_amount,
    },
    {
      bu: '56-64',
      plan1: Object.assign({}, ub3[0]).premium_amount,
      plan2: Object.assign({}, ub3[1]).premium_amount,
      plan3: Object.assign({}, ub3[2]).premium_amount,
    },
  ]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2 mr-4">
          <Col xs={24} md={20}>
            <div className="text-primary2">Product - WellWoman</div>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="float-md-right mt-3 mt-md-0"
              onClick={() => history.push('/product-setting/wellwoman/edit')}>
              Edit WellWoman
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {!isEmpty(stateLiability)
          ? (
            <>
              <Table pagination={false} bordered dataSource={dataTable}>
                <Column title="Batasan Usia" dataIndex="bu" key="bu" />
                <Column title="Plan 1" dataIndex="plan1" key="plan1" />
                <Column title="Plan 2" dataIndex="plan2" key="plan2" />
                <Column title="Plan 3" dataIndex="plan3" key="plan3" />
              </Table>
            </>
          )
          : <div className="py-5" />
        }
      </Card>
    </React.Fragment>
  )
}

WellWomanSetting.propTypes = {
  stateLiability: PropTypes.object,
  currentUser: PropTypes.object,
  dataCustomer: PropTypes.array,
  metaCustomer: PropTypes.object,
  stateCustomer: PropTypes.object,
  handlePage: PropTypes.func,
  loadCustomer: PropTypes.func,
  setStateCustomer: PropTypes.func,
  updatePerPage: PropTypes.func,
  isFetching: PropTypes.bool,
  handleFilter: PropTypes.func,
}

export default WellWomanSetting

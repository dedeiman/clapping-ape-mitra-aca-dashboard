import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col, Input, Table, Collapse,
  Card, Divider,
  Descriptions,
  Pagination,
  Button, Select,
  Empty, Typography,
} from 'antd'
import { Loader } from 'components/elements'
import { isEmpty, capitalize } from 'lodash'
import history from 'utils/history'

const { Panel } = Collapse;
const { Column, ColumnGroup } = Table

function callback(key) {
  console.log(key);
}

const columnsBencana = [

  {
    title: 'Batasan Usia',
    dataIndex: 'bu',
    key: 'bu',
  },
  {
    title: 'Plan 1',
    dataIndex: 'plan1',
    key: 'plan1',
  },
  {
    title: 'Plan 2',
    dataIndex: 'plan2',
    key: 'plan2',
  },
  {
    title: 'Plan 3',
    dataIndex: 'plan3',
    key: 'plan3',
  },
]

const TravelDomesticSetting = ({
  stateLiability,
  loadCustomer,
  isFetching,
}) => {
  const isMobile = window.innerWidth < 768
  const dataTravelDomestic = Object.assign({}, stateLiability.list.travel_domestic)
  const ub1 = Object.assign({}, dataTravelDomestic['1-4'])
  const ub2 = Object.assign({}, dataTravelDomestic['5-11'])
  const ub3 = Object.assign({}, dataTravelDomestic['12-20'])
  const ub4 = Object.assign({}, dataTravelDomestic['21-31'])
  const ub5 = Object.assign({}, dataTravelDomestic.additional_week)

  const dataTable = [
    {
      bu: '1-4',
      plan1: Object.assign({}, ub1[0]).nusantara_1,
      plan2: Object.assign({}, ub1[0]).nusantara_2,
    },
    {
      bu: '5-11',
      plan1: Object.assign({}, ub2[0]).nusantara_1,
      plan2: Object.assign({}, ub2[0]).nusantara_2,
    },
    {
      bu: '12-20',
      plan1: Object.assign({}, ub3[0]).nusantara_1,
      plan2: Object.assign({}, ub3[0]).nusantara_2,
    },
    {
      bu: '21-31',
      plan1: Object.assign({}, ub4[0]).nusantara_1,
      plan2: Object.assign({}, ub4[0]).nusantara_2,
    },
    {
      bu: 'Tambahan Per Minggu',
      plan1: Object.assign({}, ub5[0]).nusantara_1,
      plan2: Object.assign({}, ub5[0]).nusantara_2,
    },
  ]
  return (
    <React.Fragment>
      <Card>
        <Row type="flex" justify="space-between" className="mb-2">
          <Col xs={24} md={20}>
            <div className="text-primary2">Product - Travel Domestic</div>
          </Col>
          <Col xs={24} md={4}>
            <Button type="primary" className="float-md-right mt-3 mt-md-0" onClick={() => history.push('/product-setting/travel-domestic/edit')}>
              Edit Travel Domestic
            </Button>
          </Col>
        </Row>
        {isFetching && (
          <Loader />
        )}
        {!isEmpty(stateLiability)
          ? (
            <>
              <Table pagination={false} bordered dataSource={dataTable}>
                <Column title="Lama Perjalanan" dataIndex="bu" key="bu" />
                <Column title="Nusantara 1" dataIndex="plan1" key="plan1" />
                <Column title="Nusantara 2" dataIndex="plan2" key="plan2" />
              </Table>
            </>
          )
          : <div className="py-5" />
        }
      </Card>
    </React.Fragment>
  )
}

TravelDomesticSetting.propTypes = {
  stateLiability: PropTypes.object,
  loadCustomer: PropTypes.func,
  isFetching: PropTypes.bool,
}

export default TravelDomesticSetting

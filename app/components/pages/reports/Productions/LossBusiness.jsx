import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Select, Table, DatePicker, Input, Button, Checkbox } from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
  
const LossBusinessReport = ({
  dataList, branchList, cobList, productList,
  representativeBranchList, isLoading, handleFilter,
  handleSubmitFilter, handlePage, handleDownload
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: 'no',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => (text || '-')
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => (text || '-')
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-')
    }, {
      title: 'Branch',
      dataIndex: 'branch',
      key: 'branch',
      render: text => (text || '-')
    }, {
      title: 'Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => (text || '-')
    }, {
      title: 'Class of Bussiness',
      dataIndex: 'classof_bussiness',
      key: 'classof_bussiness',
      render: text => (text || '-')
    }, {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      render: text => (text || '-')
    }, {
      title: 'Policy No',
      dataIndex: 'policy_no',
      key: 'policy_no',
      render: text => (text || '-')
    }, {
      title: 'Start Period',
      dataIndex: 'start_period',
      key: 'start_period',
      render: text => (text || '-')
    }, {
      title: 'End Period',
      dataIndex: 'end_period',
      key: 'end_period',
      render: text => (text || '-')
    }, {
      title: 'Insured Name',
      dataIndex: 'insured_name',
      key: 'end_period',
      render: text => (text || '-')
    }, {
      title: 'Insured Address',
      dataIndex: 'insured_address',
      key: 'insured_address',
      render: text => (text || '-')
    }, {
      title: 'Email Insured',
      dataIndex: 'insured_email',
      key: 'insured_email',
      render: text => (text || '-')
    }, {
      title: 'Hp Insured',
      dataIndex: 'insured_phone',
      key: 'insured_phone',
      render: text => (text || '-')
    }, {
      title: 'TSI in IDR',
      dataIndex: 'tsi_in_idr',
      key: 'tsi_in_idr',
      render: text => (text || '-')
    }, {
      title: 'Premium Rate',
      dataIndex: 'premium_rate',
      key: 'premium_rate',
      render: text => (text || '-')
    }, {
      title: 'Premium',
      dataIndex: 'premium',
      key: 'premium',
      render: text => (text || '-')
    }, {
      title: 'Discount',
      dataIndex: 'discount',
      key: 'discount',
      render: text => (text || '-')
    }, {
      title: 'Object Information 1',
      dataIndex: 'object_info_1',
      key: 'object_info_1',
      render: text => (text || '-')
    }, {
      title: 'Object Information 2',
      dataIndex: 'object_info_2',
      key: 'object_info_2',
      render: text => (text || '-')
    }, {
      title: 'Object Information 3',
      dataIndex: 'object_info_3',
      key: 'object_info_3',
      render: text => (text || '-')
    }, {
      title: 'Object Information 4',
      dataIndex: 'object_info_4',
      key: 'object_info_4',
      render: text => (text || '-')
    }, {
      title: 'Object Information 5',
      dataIndex: 'object_info_6',
      key: 'object_info_5',
      render: text => (text || '-')
    }, {
      title: 'Object Information 6',
      dataIndex: 'object_info_6',
      key: 'object_info_6',
      render: text => (text || '-')
    }, {
      title: 'Object Information 7',
      dataIndex: 'object_info_7',
      key: 'object_info_7',
      render: text => (text || '-')
    }, {
      title: 'Object Information 8',
      dataIndex: 'object_info_8',
      key: 'object_info_8',
      render: text => (text || '-')
    }, {
      title: 'Object Information 9',
      dataIndex: 'object_info_9',
      key: 'object_info_9',
      render: text => (text || '-')
    }, {
      title: 'Object Information 10',
      dataIndex: 'object_info_10',
      key: 'object_info_10',
      render: text => (text || '-')
    }, {
      title: 'Object Information 11',
      dataIndex: 'object_info_11',
      key: 'object_info_11',
      render: text => (text || '-')
    }, {
      title: 'Object Information 12',
      dataIndex: 'object_info_12',
      key: 'object_info_12',
      render: text => (text || '-')
    }, {
      title: 'Object Information 13',
      dataIndex: 'object_info_13',
      key: 'object_info_13',
      render: text => (text || '-')
    }, {
      title: 'Object Information 14',
      dataIndex: 'object_info_14',
      key: 'object_info_14',
      render: text => (text || '-')
    }, {
      title: 'Object Information 15',
      dataIndex: 'object_info_15',
      key: 'object_info_15',
      render: text => (text || '-')
    }
  ]

  return (
    <React.Fragment>
      <Card
        title={(
          <Row type="flex">
            <Col xs={24} md={20}>
              <h4 className="text-primary2 m-0">Report Loss Business</h4>
            </Col>
          </Row>
        )}
      >
        <Row gutter={24}>
          <Col xs={24}>
            <form onSubmit={handleSubmitFilter}>
              <Row gutter={24}>
                <Col xs={24} md={24}>
                  <label htmlFor="">Date of Policy End Periode</label>
                </Col>
                <Col xs={24} md={8}>
                  <Form.Item className="">
                    <DatePicker.RangePicker
                      format="DD MMM YYYY"
                      className="w-100"
                      onChange={e => handleFilter(e, 'policyDate')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={8}>
                  <Form.Item className="">
                    <Input
                      allowClear
                      className="w-100"
                      placeholder="Search Agent ID / Profile Id / Name Agent"
                      onChange={e => handleFilter(e.target.value, 'agent_keyword')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={8}>
                  <Form.Item className="">
                    <Input
                      allowClear
                      className="w-100"
                      placeholder="Search Policy Number"
                      onChange={e => handleFilter(e.target.value, 'policy_no')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={8}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Class of Business -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'cob_id')}
                    >
                      {
                        !isEmpty(cobList) && cobList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={8}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Product -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'product_id')}
                    >
                      {
                        !isEmpty(productList) && productList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={8}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Branch -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'branch_id')}
                    >
                      {
                        !isEmpty(branchList) && branchList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={8}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Representative -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'branch_perwakilan_id')}
                    >
                      {
                        !isEmpty(representativeBranchList) && representativeBranchList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24} className="mt-4">
                  <Row>
                    <Col span={6} className="mt-4">
                      <Button type="primary" className="button-lg btn-block" htmlType="submit">
                        Search
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Card>
      <Row gutter={24} className="mt-5" type="flex" justify="end">
        <Col md={{ span: 5 }}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={(e) => handleDownload(e, 'pdf')} disabled={isLoading}>
            Export As PDF
          </Button>
        </Col>
        <Col md={{ span: 5 }}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={(e) => handleDownload(e, 'excel')} disabled={isLoading}>
            Export As Excel
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          rowKey={(record, index) => index}
          dataSource={ !isEmpty(dataList) ? dataList.data : [] }
          loading={isLoading}
          columns={columns}
          scroll={{ x: 'max-content' }}
          pagination={{
            current: !isEmpty(dataList) ? dataList.meta.current_page : 1,
            total: !isEmpty(dataList) ? dataList.meta.total_count : 0,
            simple: isMobile,
            onChange: (current) => {
              handlePage(current)
            }
          }}
        />
      </div>
    </React.Fragment>
  )
}

LossBusinessReport.propTypes = {
  dataList: PropTypes.object,
  branchList: PropTypes.array,
  cobList: PropTypes.array,
  productList: PropTypes.array,
  representativeBranchList: PropTypes.array,
  isLoading: PropTypes.bool,
  handleFilter: PropTypes.func,
  handleSubmitFilter: PropTypes.func,
  handlePage: PropTypes.func,
  handleDownload: PropTypes.func
}

export default LossBusinessReport

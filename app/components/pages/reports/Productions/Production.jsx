import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button,
  Checkbox,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const { Option } = Select

const ProductionReport = ({
  handleFilter,
  changeState,
  handleTableChange,
  state, reportProduct,
  handleBranchPerwakilan,
  handleReport, handleChecked,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: 'no',
      render: (text, record, idx) => (reportProduct.list.pagination.current_page > 1 ? (reportProduct.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
    }, {
      title: 'Policy Status',
      dataIndex: 'policy_status',
      render: text => (text || '').toUpperCase(),
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
    }, {
      title: 'Agent Status',
      dataIndex: 'agent_status',
      render: text => (text || '').toUpperCase(),
    }, {
      title: 'Branch',
      dataIndex: 'branch',
    }, {
      title: 'Representative',
      dataIndex: 'branch_representative',
    }, {
      title: 'Class of Bussiness',
      dataIndex: 'class_of_business',
    }, {
      title: 'Product',
      dataIndex: 'product',
    }, {
      title: 'Renewed',
      dataIndex: 'renewed',
    }, {
      title: 'PrevPolicy',
      dataIndex: 'previous_policy_number',
    }, {
      title: 'SPPA Type',
      dataIndex: 'sppa_type',
    }, {
      title: 'SPPA No',
      dataIndex: 'sppa_number',
    }, {
      title: 'Policy No',
      dataIndex: 'policy_number',
    }, {
      title: 'Start Period',
      dataIndex: 'start_period',
    }, {
      title: 'End Period',
      dataIndex: 'end_period',
    }, {
      title: 'Insured Name',
      dataIndex: 'insured_name',
    }, {
      title: 'Currency of TSI',
      dataIndex: 'currency_of_tsi',
    }, {
      title: 'Rate TSI',
      dataIndex: 'rate_of_tsi',
    }, {
      title: 'TSI',
      dataIndex: 'tsi',
    }, {
      title: 'TSI in IDR',
      dataIndex: 'tsi_in_idr',
    }, {
      title: 'Nilai Premi',
      dataIndex: 'premi',
    }, {
      title: 'Nilai Komisi',
      dataIndex: 'commision',
    }, {
      title: 'Nilai Diskon',
      dataIndex: 'discount',
    }, {
      title: 'Nilai Akusisi',
      dataIndex: 'acquisition',
    }, {
      title: 'Object Information 1',
      dataIndex: 'object_info_1',
    }, {
      title: 'Object Information 2',
      dataIndex: 'object_info_2',
    }, {
      title: 'Object Information 3',
      dataIndex: 'object_info_3',
    }, {
      title: 'Object Information 4',
      dataIndex: 'object_info_4',
    }, {
      title: 'Object Information 5',
      dataIndex: 'object_info_5',
    }, {
      title: 'Object Information 6',
      dataIndex: 'object_info_6',
    }, {
      title: 'Object Information 7',
      dataIndex: 'object_info_7',
    }, {
      title: 'Object Information 8',
      dataIndex: 'object_info_8',
    }, {
      title: 'Object Information 9',
      dataIndex: 'object_info_9',
    }, {
      title: 'Object Information 10',
      dataIndex: 'object_info_10',
    }, {
      title: 'Object Information 11',
      dataIndex: 'object_info_11',
    }, {
      title: 'Object Information 12',
      dataIndex: 'object_info_12',
    }, {
      title: 'Object Information 13',
      dataIndex: 'object_info_13',
    }, {
      title: 'Object Information 14',
      dataIndex: 'object_info_14',
    }, {
      title: 'Object Information 15',
      dataIndex: 'object_info_15',
    },
  ]

  const columnsSummary = [
    {
      title: 'No',
      dataIndex: 'No',
      render: (text, record, idx) => (reportProduct.list.pagination.current_page > 1 ? (reportProduct.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
    }, {
      title: 'Policy Status',
      dataIndex: 'policy_status',
      render: text => (text || '').toUpperCase(),
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
    }, {
      title: 'Branch',
      dataIndex: 'branch',
    }, {
      title: 'Representative',
      dataIndex: 'representative',
    }, {
      title: 'Total TSI in IDR',
      dataIndex: 'total_tsi_in_idr',
    }, {
      title: 'Total Premium',
      dataIndex: 'total_premi',
    }, {
      title: 'Total Commision',
      dataIndex: 'total_commision',
    }, {
      title: 'Total Discount',
      dataIndex: 'total_discount',
    }, {
      title: 'Total Akusisi',
      dataIndex: 'total_acquisition',
    }, {
      title: 'Policy Count',
      dataIndex: 'policy_count',
    },
  ]

  return (
    <React.Fragment>
      <Card
        title={(
          <Row type="flex" justify="flex-between" align="end">
            <Col xs={24} md={24}>
              <h4 className="text-primary2 m-0">Report Produksi</h4>
            </Col>
          </Row>
        )}
      >
        <Row gutter={24}>
          <Col xs={24}>
            <form onSubmit={handleFilter}>
              <Row gutter={24}>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Periode">
                    <DatePicker.RangePicker
                      allowClear
                      format="DD MMM YYYY"
                      // value={!isEmpty(state.issued_to) ? [moment(state.issued_to), moment(state.issued_from)] : ''}
                      onChange={(date) => {
                        changeState({
                          ...state,
                          issued_from: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                          issued_to: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                        })
                      }}
                    />
                  </Form.Item>

                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Tanggal Periode Polis Awal">
                    <DatePicker.RangePicker
                      allowClear
                      format="DD MMM YYYY"
                      // value={!isEmpty(state.start_period_to) ? [moment(state.start_period_to), moment(state.start_period_from)] : ''}
                      onChange={(date) => {
                        changeState({
                          ...state,
                          start_period_from: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                          start_period_to: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Search Produksi">
                    <Input
                      allowClear
                      className="w-100"
                      value={state.search || undefined}
                      placeholder="Search Agent ID / Profile Id / Name Agent"
                      onChange={e => changeState({ ...state, search: e.target.value })}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Search Policy Number">
                    <Input
                      allowClear
                      className="w-100"
                      value={state.policy_number || undefined}
                      placeholder="- Policy No -"
                      onChange={e => changeState({ ...state, policy_number: e.target.value })}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Pilih COB">
                    <Select
                      allowClear
                      placeholder="- Class of Business -"
                      className="w-100"
                      value={state.cob_id || undefined}
                      onChange={val => changeState({ ...state, cob_id: val })}
                    >
                      <Option value="">All COB</Option>
                      {(state.cobList || []).map(key => (
                        <Option value={key.id}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Pilih Status">
                    <Select
                      allowClear
                      placeholder="- Agent Status -"
                      className="w-100"
                      value={state.agent_status || undefined}
                      onChange={val => changeState({ ...state, agent_status: val })}
                    >
                      <Option value="">All Status</Option>
                      {(state.agentStatusList || []).map(key => (
                        <Option value={key.slug}>{key.display_name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Pilih Produk">
                    <Select
                      allowClear
                      placeholder="- Product -"
                      className="w-100"
                      value={state.product_id || undefined}
                      onChange={val => changeState({ ...state, product_id: val })}
                    >
                      <Option value="">All Product</Option>
                      {(state.productList || []).map(key => (
                        <Option value={key.id}>{(key.display_name || '').toUpperCase()}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Pilih Branch">
                    <Select
                      allowClear
                      placeholder="- Branch -"
                      className="w-100"
                      value={state.branch_id || undefined}
                      onChange={(val) => {
                        handleBranchPerwakilan(val)
                      }}
                    >
                      <Option value="">All Branch</Option>
                      {(state.branchList || []).map(key => (
                        <Option value={key.id}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2" label="Pilih Branch Representative">
                    <Select
                      allowClear
                      placeholder="- Branch Representative -"
                      className="w-100"
                      value={state.branch_perwakilan_id || undefined}
                      disabled={(state.branch_id === '') || (state.branch_id === undefined)}
                      onChange={val => changeState({ ...state, branch_perwakilan_id: val })}
                    >
                      <Option value="">All Representative</Option>
                      {(state.branchPerList || []).map(key => (
                        <Option value={key.id}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24} align="end">
                  <Button type="primary" className="button-lg px-4" htmlType="submit">
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Card>
      <Row gutter={24} className="mt-5">
          <Col md={5}>
            <Checkbox
              checked={state.is_detail}
              onChange={handleChecked}
            >
              Show Detail
            </Checkbox>
          </Col>
          <Col md={{ span: 5, offset: 14 }}>
            <Button
              ghost
              type="primary"
              className="btn-border-mitra btn-block"
              onClick={() => handleReport()}
            >
              Export As Excel
            </Button>
          </Col>
        </Row>
      <div className="table-list">
        <Table
          dataSource={reportProduct.list.data}
          loading={false}
          columns={state.is_detail ? columns : columnsSummary}
          pagination={{
            position: ['bottomRight'],
            current: reportProduct.list.pagination.current_page,
            total: reportProduct.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
          scroll={{ x: 'max-content' }}
        />
      </div>
    </React.Fragment>
  )
}

ProductionReport.propTypes = {
  handleFilter: PropTypes.func,
  changeState: PropTypes.func,
  handleReport: PropTypes.func,
  handleTableChange: PropTypes.func,
  state: PropTypes.object,
  reportProduct: PropTypes.any,
  handleChecked: PropTypes.func,
  handleBranchPerwakilan: PropTypes.func,
}

export default ProductionReport

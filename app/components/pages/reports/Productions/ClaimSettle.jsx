import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button,
  Checkbox,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const { Option } = Select

const columns = [
    {
      title: 'No',
      dataIndex: 'no',
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
    }, {
      title: 'Agent Name',
      dataIndex: 'name',
    }, {
      title: 'Branch',
      dataIndex: ['branch', 'name'],
    }, {
      title: 'Representative',
      dataIndex: ['branch_perwakilan', 'name'],
    }, {
      title: 'Class of Bussiness',
      dataIndex: 'cob',
    }, {
      title: 'Product',
      dataIndex: 'product',
    }, {
      title: 'Policy No',
      dataIndex: 'policy',
    }, {
      title: 'Start Period',
      dataIndex: 'start-period',
    }, {
      title: 'End Period',
      dataIndex: 'end-period',
    }, {
      title: 'Insured Name',
      dataIndex: 'insured-name',
    }, {
      title: 'Currency of TSI',
      dataIndex: 'currency-tsi',
    }, {
      title: 'Rate TSI',
      dataIndex: 'rate_tsi',
    }, {
      title: 'TSI',
      dataIndex: 'tsi',
    }, {
      title: 'TSI in IDR',
      dataIndex: 'tsi_idr',
    }, {
      title: 'Date of Loss',
      dataIndex: 'date_loss',
    }, {
      title: 'Cause of Loss',
      dataIndex: 'cause_loss',
    }, {
      title: 'Currency of Claim',
      dataIndex: 'currency_claim',
    }, {
      title: 'Rate of Claim',
      dataIndex: 'rate_claim',
    }, {
      title: 'Claim Paid',
      dataIndex: 'claim_paid',
    }, {
      title: 'Claim Paid IDR',
      dataIndex: 'claim_paid_idr',
    }, {
      title: 'Date of Settlement',
      dataIndex: 'date_settlement',
    }, {
      title: 'Object Information 1',
      dataIndex: 'object-1',
    }, {
      title: 'Object Information 2',
      dataIndex: 'object-2',
    }, {
      title: 'Object Information 3',
      dataIndex: 'object-3',
    }, {
      title: 'Object Information 4',
      dataIndex: 'object-4',
    }, {
      title: 'Object Information 5',
      dataIndex: 'object-5',
    }, {
      title: 'Object Information 6',
      dataIndex: 'object-6',
    }, {
      title: 'Object Information 7',
      dataIndex: 'object-7',
    }, {
      title: 'Object Information 8',
      dataIndex: 'object-8',
    }, {
      title: 'Object Information 9',
      dataIndex: 'object-9',
    }, {
      title: 'Object Information 10',
      dataIndex: 'object-10',
    }, {
      title: 'Object Information 11',
      dataIndex: 'object-11',
    }, {
      title: 'Object Information 12',
      dataIndex: 'object-12',
    }, {
      title: 'Object Information 13',
      dataIndex: 'object-13',
    }, {
      title: 'Object Information 14',
      dataIndex: 'object-14',
    }, {
      title: 'Object Information 15',
      dataIndex: 'object-15',
    },
  ]
  
  const columnsSummary = [
    {
      title: 'No',
      dataIndex: 'No',
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
    }, {
      title: 'Agent Name',
      dataIndex: 'name',
    }, {
      title: 'Branch',
      dataIndex: 'branch',
    }, {
      title: 'Representative',
      dataIndex: 'representative',
    }, {
      title: 'Number of Policy Claim',
      dataIndex: 'number_policy_claim',
    }, {
      title: 'Total TSI in IDR',
      dataIndex: 'total_tsi_idr',
    }, {
      title: 'Number of Claim',
      dataIndex: 'number_claim',
    }, {
      title: 'Total Claim Paid IDR',
      dataIndex: 'total_claim_paid',
    },
  ]
  
const ClaimSettleReport = ({
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
        <Card
            title={(
            <Row type="flex">
                <Col xs={24} md={20}>
                    <h4 className="text-primary2 m-0">Report Claim Settle</h4>
                </Col>
            </Row>
            )}
        >
            <Row gutter={24}>
                <Col xs={24}>
                    <form onSubmit=''>
                        <Row gutter={24}>
                            <Col xs={24} md={24}>
                                <label htmlFor="">Date of Settled</label>
                            </Col>
                            <Col xs={24} md={10}>
                                <Form.Item className="">
                                    <DatePicker.RangePicker
                                        format="DD MMM YYYY"
                                        className="w-100"
                                        value=''
                                        onChange=''
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={14}>
                                <Form.Item className="">
                                    <Input
                                        allowClear
                                        className="w-100"
                                        value=''
                                        placeholder="Search Agent ID / Profile Id / Name Agent"
                                        onChange=''
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={6}>
                                <Form.Item className="mt-3">
                                    <Input
                                        allowClear
                                        className="w-100"
                                        value=''
                                        placeholder="- Policy No -"
                                        onChange=''
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={6}>
                                <Form.Item className="mt-3">
                                    <Select
                                        allowClear
                                        placeholder="- Class of Business -"
                                        className="w-100"
                                        value=''
                                        onChange=''
                                    >
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={6}>
                                <Form.Item className="mt-3">
                                    <Select
                                        allowClear
                                        placeholder="- Product -"
                                        className="w-100"
                                        value=''
                                        onChange=''
                                    >
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={6}>
                                <Form.Item className="mt-3">
                                    <Select
                                        allowClear
                                        placeholder="- Branch -"
                                        className="w-100"
                                        value=''
                                        onChange=''
                                    >
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={6}>
                                <Form.Item className="mt-3">
                                    <Select
                                        allowClear
                                        placeholder="- Representative -"
                                        className="w-100"
                                        value=''
                                        onChange=''
                                    >
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={6}>
                                <Form.Item className="mt-4">
                                    <Checkbox
                                        checked=''
                                        onChange=''
                                    >
                                        Detail
                                    </Checkbox>
                                </Form.Item>
                            </Col>
                            <Col span={24} align="end">
                                <Button type="primary" className="button-lg px-5" htmlType="submit">
                                    Search
                                </Button>
                            </Col>
                        </Row>
                    </form>
                </Col>
            </Row>
        </Card>
        <Row gutter={24} className="mt-5">
            <Col md={{ span: 5, offset: 14 }}>
                <Button
                    ghost
                    type="primary"
                    className="btn-border-mitra btn-block"
                    onClick=''
                >
                    Export As Excel
                </Button>
            </Col>
            <Col md={{ span: 5 }}>
                <Button
                    ghost
                    type="primary"
                    className="btn-border-mitra btn-block"
                    onClick=''
                >
                    Export As PDF
                </Button>
            </Col>
        </Row>
        <div className="table-list">
            <Table
            dataSource={[]}
            loading={false}
            columns={0 ? columns : columnsSummary}
            pagination={{
                position: ['bottomRight'],
                current: 1,
                total: 1,
                simple: isMobile,
            }}
            onChange=''
            scroll={{ x: 'max-content' }}
            />
        </div>
    </React.Fragment>
  )
}

ClaimSettleReport.propTypes = {
}

export default ClaimSettleReport

import React from 'react'
import PropTypes from 'prop-types'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button,
} from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const { Option } = Select
const OutstandingClaim = ({
  outstandingClaimReport, handleTableChange,
  state, changeState, handleReport, handleSearch, handleProduct,
  handleRepresentative,
}) => {

  const isMobile = window.innerWidth < 768

  const columns = [{
    title: 'No',
    dateIndex: '',
    render: (text, record, idx) => (outstandingClaimReport.list.pagination.current_page > 1 ? (outstandingClaimReport.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
  },{
    title: 'Agent ID',
    dataIndex: 'agent_id',
  }, {
    title: 'Profile ID',
    dataIndex: 'profile_id',
  }, {
    title: 'Agent Name',
    dataIndex: 'agent_name',
  }, {
    title: 'Branch',
    dataIndex: 'branch',
  }, {
    title: 'Agent Representative',
    dataIndex: ['representative'],
  }, {
    title: 'Class of Bussiness',
    dataIndex: 'cob_id',
  }, {
    title: 'Product',
    dataIndex: 'product',
  }, {
    title: 'Policy No',
    dataIndex: 'policy_number',
  }, {
    title: 'Start Period',
    dataIndex: 'start_periode',
    render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
  }, {
    title: 'End Period',
    dataIndex: 'end_periode',
    render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
  }, {
    title: 'Insured Name',
    dataIndex: 'insured_name',
  }, {
    title: 'Currency of TSI',
    dataIndex: 'currency_of_tsi',
  }, {
    title: 'Rate TSI',
    dataIndex: 'rate_of_tsi',
  }, {
    title: 'TSI',
    dataIndex: 'tsi',
  }, {
    title: 'TSI in IDR',
    dataIndex: 'tsi_in_idr',
  }, {
    title: 'Date of Loss',
    dataIndex: 'date_of_loss',
    render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
  }, {
    title: 'Cause of Loss',
    dataIndex: 'cause_of_loss',
  }, {
    title: 'Currency of Claim',
    dataIndex: 'currency_of_claim',
  }, {
    title: 'Estimate Outstanding Claim',
    dataIndex: 'estimate_outstanding_claim',
  }, {
    title: 'Object Information 1',
    dataIndex: 'object_info_1',
  }, {
    title: 'Object Information 2',
    dataIndex: 'object_info_2',
  }, {
    title: 'Object Information 3',
    dataIndex: 'object_info_3',
  }, {
    title: 'Object Information 4',
    dataIndex: 'object_info_4',
  }, {
    title: 'Object Information 5',
    dataIndex: 'object_info_5',
  }, {
    title: 'Object Information 6',
    dataIndex: 'object_info_6',
  }, {
    title: 'Object Information 7',
    dataIndex: 'object_info_7',
  }, {
    title: 'Object Information 8',
    dataIndex: 'object_info_8',
  }, {
    title: 'Object Information 9',
    dataIndex: 'object_info_9',
  }, {
    title: 'Object Information 10',
    dataIndex: 'object_info_10',
  }, {
    title: 'Object Information 11',
    dataIndex: 'object_info_11',
  }, {
    title: 'Object Information 12',
    dataIndex: 'object_info_12',
  }, {
    title: 'Object Information 13',
    dataIndex: 'object_info_13',
  }, {
    title: 'Object Information 14',
    dataIndex: 'object_info_14',
  }, {
    title: 'Object Information 15',
    dataIndex: 'object_info_15',
  },
]
  return (
    <React.Fragment>
      <Row gutter={24}>
        <Col xs={24}>
          <Card
            className="card-box-filter"
            title={(
              <Row type="flex">
                <Col xs={24} md={20}>
                  <h4 className="text-primary2 m-0">Report Outstanding Claim</h4>
                </Col>
              </Row>
            )}
          >
            <form onSubmit={handleSearch}>
              <Row gutter={24}>
                <Col xs={24} md={12}>
                  <Form.Item className="mb-2">
                    <DatePicker
                      className="w-100"
                      format="DD MMM YYYY"
                      placeholder="AS AT"
                      value={state.start_date}
                      disabled
                      value={ moment(state.start_date)}

                      onChange={(date) => {
                        changeState({
                          ...state,
                          start_date: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={12}>
                  <Form.Item className="mb-2">
                    <Input
                      allowClear
                      className="w-100"
                      value={state.keyword}
                      placeholder="Search Agent ID / Profile ID / Name Agent..."
                      onChange={e => changeState({ ...state, keyword: e.target.value })}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2">
                    <Input
                      allowClear
                      className="w-100"
                      value={state.policy_no}
                      placeholder="- Policy No -"
                      onChange={e => changeState({ ...state, policy_no: e.target.value })}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2">
                  <Select
                      allowClear
                      placeholder="- Class of Business -"
                      className="w-100"
                      value={state.cob || undefined}
                      onChange={val => handleProduct(val)}
                    >
                      <Option value="">All COB</Option>
                      {(state.cobList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Product -"
                      className="w-100"
                      disabled={state.cob === '' || state.cob === undefined}
                      value={state.product || undefined}
                      onChange={val => changeState({ ...state, product: val })}
                    >
                      <Option value="">All Product</Option>
                      {(state.productList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{(key.display_name || '').toUpperCase()}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Agent Branch -"
                      className="w-100"
                      value={state.branch || undefined}
                      onChange={val => handleRepresentative(val)}
                    >
                      <Option value="">All Branch</Option>
                      {(state.branchList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Agent Representative -"
                      className="w-100"
                      value={state.agent_representative || undefined}
                      disabled={state.branch === '' || state.branch === undefined}
                      onChange={val => changeState({ ...state, agent_representative: val })}
                    >
                      <Option value="">All Representative</Option>
                      {(state.agentRepresentList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24} className="mt-2">
                <Col span={6}>
                  <Button type="primary" className="button-lg btn-block" htmlType="submit">
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Card>
        </Col>
      </Row>
      <Row gutter={24} className="mt-5">
        <Col md={{ span: 5, offset: 14 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={() => handleReport('excel')}
          >
            Export As Excel
          </Button>
        </Col>
        <Col md={{ span: 5 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={() => handleReport('pdf')}
          >
            Export As PDF
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          dataSource={outstandingClaimReport.list.data}
          columns={columns}
          loading={outstandingClaimReport.list.isFetching}
          scroll={{ x: 'max-content' }}
          pagination={{
            position: ['bottomRight'],
            current: outstandingClaimReport.list.pagination.current_page,
            total: outstandingClaimReport.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
        />
      </div>
    </React.Fragment>
  )
}

OutstandingClaim.propTypes = {
  outstandingClaimReport: PropTypes.any,
  handleTableChange: PropTypes.func,
  state: PropTypes.any,
  changeState: PropTypes.func,
  handleReport: PropTypes.func,
  handleSearch: PropTypes.func,
  handleProduct: PropTypes.func,
  handleRepresentative: PropTypes.func,
}

export default OutstandingClaim

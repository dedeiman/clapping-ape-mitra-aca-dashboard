import React from 'react'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button, Alert,
} from 'antd'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const { Option } = Select
const columns = [
  {
    title: 'No',
    dataIndex: 'no',
  },
  {
    title: 'Agent ID',
    dataIndex: ['agent_id'],
  },
  {
    title: 'Agent Profile ID',
    dataIndex: 'profile_id',
  },
  {
    title: 'Agent Name',
    dataIndex: ['agent_name'],
  },
  {
    title: 'Agent Level',
    dataIndex: ['agent_level'],
  },
  {
    title: 'Agent Branch',
    dataIndex: 'agent_branch',
  },
  {
    title: 'Agent Representative',
    dataIndex: ['agent_representative'],
  },
  {
    title: 'Class of Bussines',
    dataIndex: 'class_of_business',
  },
  {
    title: 'Product',
    dataIndex: 'product',
  },
  {
    title: 'Tipe SPPA',
    dataIndex: 'sppa_type',
  },
  {
    title: 'SPPA No.',
    dataIndex: 'sppa_number',
  },
  {
    title: 'Insured Name',
    dataIndex: 'insured_name',
  },
  {
    title: 'Premi (IDR)',
    dataIndex: 'premi',
  },
  {
    title: 'SPPA Create Date',
    dataIndex: 'sppa_create_date',
  },
  {
    title: 'SPPA Status',
    dataIndex: 'sppa_status',
  },
]

const ReportSPPA = ({
  sppaReport, state, changeState,
  handleSearch, handleTableChange,
  handleReport, handleRepresentative,
}) => {
  const isMobile = window.innerWidth < 768

  return (
    <React.Fragment>
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 m-0">Report SPPA</h4>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="float-md-right mt-3 mt-md-0"
              onClick={handleReport}
            >
              Export Data
            </Button>
          </Col>
        </Row>
      )}
    >
      <Row gutter={24}>
        <Col xs={24}>
          <form onSubmit={handleSearch}>
            <Row gutter={24}>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Periode">
                  <DatePicker.RangePicker
                    format="DD MMM YYYY"
                    value={!isEmpty(state.join_from_date) ? [moment(state.join_from_date), moment(state.join_to_date)] : ''}
                    onChange={(date) => {
                      changeState({
                        ...state,
                        join_from_date: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                        join_to_date: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                      })
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Search SPPA">
                  <Input
                    allowClear
                    className="w-100"
                    value={state.keyword}
                    placeholder="Search Agent ID / Profile ID / Name Agent..."
                    onChange={e => changeState({ ...state, keyword: e.target.value })}
                  />
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Agent Branch">
                  <Select
                    allowClear
                    placeholder="- Agent Branch -"
                    className="w-100"
                    value={state.branch || undefined}
                    onChange={val => handleRepresentative(val)}
                  >
                    {
                      (state.branchList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Agent Representative">
                  <Select
                    allowClear
                    placeholder="- Agent Representative -"
                    className="w-100"
                    value={state.agent_representative || undefined}
                    disabled={state.branch === '' || state.branch === undefined}
                    onChange={val => changeState({ ...state, agent_representative: val })}
                  >
                    {
                      (state.agentRepresentList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Class of Business">
                  <Select
                    allowClear
                    placeholder="- Class of Business -"
                    className="w-100"
                    value={state.cob || undefined}
                    onChange={val => changeState({ ...state, cob: val })}
                  >
                    {
                      (state.cobList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Product">
                  <Select
                    allowClear
                    placeholder="- Product -"
                    className="w-100"
                    value={state.product || undefined}
                    onChange={val => changeState({ ...state, product: val })}
                  >
                    {
                      (state.productList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.display_name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col span={24} align="end">
                <Button type="primary" className="button-lg px-4" htmlType="submit">
                  Search
                </Button>
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
    </Card>

      <div className="table-list mt-5">
        <Table
          dataSource={sppaReport.list.data}
          loading={sppaReport.list.isFetching}
          columns={columns}
          pagination={{
            position: ['bottomRight'],
            current: sppaReport.list.pagination.current_page,
            total: sppaReport.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
          scroll={{ x: 'max-content' }}
        />
      </div>
    </React.Fragment>
  )
}

ReportSPPA.propTypes = {
  sppaReport: PropTypes.any,
  state: PropTypes.object,
  changeState: PropTypes.func,
  handleTableChange: PropTypes.func,
  handleSearch: PropTypes.func,
  handleReport: PropTypes.func,
  handleRepresentative: PropTypes.func,
}

export default ReportSPPA

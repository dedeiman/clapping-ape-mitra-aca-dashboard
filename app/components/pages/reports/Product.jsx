import React from 'react'
import {
  Row, Col, Card,
  Button, Select, Table,
} from 'antd'

import PropTypes from 'prop-types'

const { Option } = Select

const columns = [{
  title: 'Agent ID',
  dataIndex: 'agent_id',
}, {
  title: 'Agent Profile ID',
  dataIndex: 'profile_id',
}, {
  title: 'Agent Name',
  dataIndex: 'agent_name',
}, {
  title: 'Agent Level',
  dataIndex: 'agent_level',
}, {
  title: 'Agent Branch',
  dataIndex: 'agent_branch',
}, {
  title: 'Agent Representative',
  dataIndex: 'agent_representative',
}, {
  title: 'COB',
  dataIndex: 'class_of_business',
}, {
  title: 'Product',
  dataIndex: 'product',
}, {
  title: 'Expiry Date',
  dataIndex: 'expiry_date',
}]

const RestrictedProductList = ({
  restrictedProduct, handleTableChange,
  state, changeState, handleReport, handleSearch,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Report - Agent Restricted Product</div>
        </Col>
      </Row>
      <Card className="card-box-filter mt-3">
        <Row gutter={24}>
          <Col xs={24}>
            <form onSubmit={handleSearch}>
              <Row gutter={24}>
                <Col md={10}>
                  <Select
                    showSearch
                    placeholder="- Product -"
                    optionFilterProp="children"
                    className="mitra-select-filter"
                    value={state.product || undefined}
                    onChange={val => changeState({ ...state, product: val })}
                    suffixIcon={<img src="/assets/dropdown.svg" alt="icondrop" />}
                  >
                    <Option value="" key={Math.random()}>- Product -</Option>
                    {
                      (state.productList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.display_name}</Option>
                      ))
                    }
                  </Select>
                </Col>
                <Col md={10}>
                  <Select
                    showSearch
                    placeholder="- Branch -"
                    optionFilterProp="children"
                    className="mitra-select-filter"
                    value={state.branch || undefined}
                    onChange={val => changeState({ ...state, branch: val })}
                    suffixIcon={<img src="/assets/dropdown.svg" alt="icondrop" />}
                  >
                    <Option value="" key={Math.random()}>- Branch -</Option>
                    {
                      (state.branchList || []).map(key => (
                        <Option value={key.id} key={key.id}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Col>
                <Col md={4}>
                  <Button
                    type="primary"
                    className="mitra-submit-filter w-100"
                    htmlType="submit"
                  >
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Card>

      <Row gutter={24} className="mt-5">
        <Col md={{ span: 5, offset: 19 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={handleReport}
          >
            Export As Excel
          </Button>
        </Col>
      </Row>

      <div className="table-list">
        <Table
          dataSource={restrictedProduct.list.data}
          columns={columns}
          loading={restrictedProduct.list.isFetching}
          scroll={{ x: 'max-content' }}
          pagination={{
            position: ['bottomRight'],
            current: restrictedProduct.list.pagination.current_page,
            total: restrictedProduct.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
        />
      </div>
    </React.Fragment>
  )
}

RestrictedProductList.propTypes = {
  restrictedProduct: PropTypes.any,
  handleTableChange: PropTypes.func,
  state: PropTypes.any,
  changeState: PropTypes.func,
  handleReport: PropTypes.func,
  handleSearch: PropTypes.func,
}

export default RestrictedProductList

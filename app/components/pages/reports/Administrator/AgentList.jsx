import React from 'react'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Input, Button,
} from 'antd'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const AgentList = ({
  agentList, state, changeState,
  handleSearch, handleTableChange,
  handleReport, submitted,
}) => {
const isMobile = window.innerWidth < 768

const { Option } = Select

const columns = [
  {
    title: 'No',
    dateIndex: '',
    render: (text, record, idx) => (agentList.list.pagination.current_page > 1 ? (agentList.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
  },{
    title: 'Agent ID',
    dataIndex: 'agent_id',
  },
  {
    title: 'Profile ID',
    dataIndex: 'profile_id',
  },
  {
    title: 'Agent Name',
    dataIndex: 'name',
  },
  {
    title: 'Agent Branch',
    dataIndex: ['branch', 'name'],
  },
  {
    title: 'Agent Representative',
    dataIndex: ['branch_perwakilan', 'name'],
  },
  {
    title: 'Agent ID Upline',
    dataIndex: 'upline_id',
  },
  {
    title: 'Upline Name',
    dataIndex: 'upline',
  },
  {
    title: 'Profile ID Upline',
    dataIndex: ['upline', 'profile_id'],
  },
  {
    title: 'Agent Level',
    dataIndex: ['level', 'name'],
  },
  {
    title: 'Agent Type',
    dataIndex: 'type',
  },
  {
    title: 'Agent Status',
    dataIndex: 'status',
  },
  {
    title: 'Gender',
    dataIndex: ['gender', 'name'],
  },
  {
    title: 'Birth Place',
    dataIndex: 'birth_place',
  },
  {
    title: 'Birth Date',
    dataIndex: 'birthdate',
  },
  {
    title: 'NPWP No',
    dataIndex: 'npwp',
  },
  {
    title: 'Status PTKP',
    dataIndex: 'status',
  },
  {
    title: 'e-KTP No',
    dataIndex: 'no_ktp',
  },
  {
    title: 'AAUI Certificate No',
    dataIndex: 'licenses',
    render: licenses => (
      <>
        {licenses.map(license => <p>{license.license_number}</p>)}
      </>
    ),
  },
  {
    title: 'Contract No.',
    dataIndex: 'contracts',
    render: contracts => (
      <>
        {contracts.map(contract => <p>{contract.contract_number}</p>)}
      </>
    ),
  },
  {
    title: 'Contract Expired Date',
    dataIndex: 'contracts',
    render: contracts => (
      <>
        {contracts.map(contract => <p>{moment(contract.end_date).format('YYYY-MM-DD')}</p>)}
      </>
    ),
  },
  {
    title: 'Join Date',
    dataIndex: 'created_at',
    render: createdAt => (
      <p>{moment(createdAt).format('YYYY-MM-DD')}</p>
    ),
  },
  {
    title: 'KTP Address',
    dataIndex: 'address',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Status PTKP',
    dataIndex: 'age',
  },
  {
    title: 'Phone No',
    dataIndex: 'phone_number',
  },
  {
    title: 'Email Address',
    dataIndex: 'email',
  },
  {
    title: 'Religion',
    dataIndex: 'religion',
  },
  {
    title: 'Employment',
    dataIndex: 'employment',
  },
]
return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Report - Agent List</div>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24}>
          <Card className="card-box-filter">
            <form onSubmit={handleSearch}>
              <label htmlFor="">Join Date</label>
              <Row gutter={24}>
                <Col xs={12} md={12}>
                  <Form.Item className="mb-2">
                    <DatePicker.RangePicker
                    className="w-100"
                      format="DD MMM YYYY"
                      value={!isEmpty(state.join_from_date) ? [moment(state.join_from_date), moment(state.join_to_date)] : ''}
                      onChange={(date) => {
                        changeState({
                          ...state,
                          join_from_date: date ? moment(date[0]).utc().format('YYYY-MM-DD') : '',
                          join_to_date: date ? moment(date[1]).utc().format('YYYY-MM-DD') : '',
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={12}>
                  <Form.Item className="mb-2">
                    <Input
                      allowClear
                      className="w-100"
                      value={state.search}
                      placeholder="Search Agent ID / Profile ID / Name Agent..."
                      onChange={e => changeState({ ...state, search: e.target.value })}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={12}>
                  <Form.Item className="mb-2" label="Expired Type">
                    <Select
                      placeholder="- Expired Type -"
                      className="w-100"
                      value={state.license_type || undefined}
                      onChange={val => changeState({ ...state, license_type: val })}
                      allowClear
                    >
                      <Option value="aaui" key={Math.random()}>AAUI</Option>
                      <Option value="mou" key={Math.random()}>MOU</Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={12} xxl={9}>
                  <Form.Item className="mb-2" label="From To">
                    <DatePicker.RangePicker
                      className="w-100"
                      disabled={isEmpty(state.license_type)}
                      format="DD MMM YYYY"
                      value={!isEmpty(state.license_from_date) ? [moment(state.license_from_date), moment(state.license_to_date)] : ''}
                      onChange={(date) => {
                        changeState({
                          ...state,
                          license_from_date: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                          license_to_date: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Branch -"
                      className="w-100"
                      value={state.branch || undefined}
                      onChange={val => changeState({ ...state, branch: val })}
                    >
                      {
                        (state.branchList || []).map(key => (
                          <Option value={key.id} key={key.id}>{key.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Agent Status -"
                      className="w-100"
                      value={state.status || undefined}
                      onChange={val => changeState({ ...state, status: val })}
                    >
                      <Option value="active" key={Math.random()}>Active</Option>
                      <Option value="inactive" key={Math.random()}>Inactive</Option>
                      <Option value="passed-away" key={Math.random()}>Passed Away</Option>
                      <Option value="restricted" key={Math.random()}>Restricted</Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Agent Type -"
                      className="w-100"
                      value={state.agent_type || undefined}
                      onChange={val => changeState({ ...state, agent_type: val })}
                    >
                      {
                        (state.agent_typeList || []).map(key => (
                          <Option value={key.value} key={Math.random()}>{key.label}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Level -"
                      className="w-100"
                      value={state.level || undefined}
                      onChange={val => changeState({ ...state, level: val })}
                    >
                      {
                        (state.levelList || []).map(key => (
                          <Option value={key.id} key={Math.random()}>{key.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Birth Month -"
                      className="w-100"
                      value={state.mob || undefined}
                      onChange={val => changeState({ ...state, mob: val })}
                    >
                      {(moment.months() || []).map((item, idx) => (
                        <Select.Option key={Math.random()} value={idx + 1}>{item}</Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mb-2">
                    <Select
                      allowClear
                      placeholder="- Religion -"
                      className="w-100"
                      value={state.religion || undefined}
                      onChange={val => changeState({ ...state, religion: val })}
                    >
                      <Option value="islam" key={Math.random()}>Islam</Option>
                      <Option value="protestan" key={Math.random()}>Protestan</Option>
                      <Option value="katolik" key={Math.random()}>Katolik</Option>
                      <Option value="hindu" key={Math.random()}>Hindu</Option>
                      <Option value="budha" key={Math.random()}>Budha</Option>
                      <Option value="khonghucu" key={Math.random()}>Khonghucu</Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={24} align="end">
                  <Button type="primary" className="button-lg px-4" htmlType="submit">
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Card>
        </Col>
      </Row>
      <Row gutter={24} className="mt-5">
        <Col md={{ span: 5, offset: 19 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={handleReport}
            loading={submitted}
          >
            {!submitted ? 'Export As Excel' : 'Loading ...'}
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          dataSource={agentList.list.data}
          columns={columns}
          loading={agentList.list.isFetching}
          scroll={{ x: 'max-content' }}
          pagination={{
            current: agentList.list.pagination.current_page,
            total: agentList.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
        />
      </div>
    </React.Fragment>
  )
}

AgentList.propTypes = {
  agentList: PropTypes.any,
  state: PropTypes.object,
  changeState: PropTypes.func,
  handleTableChange: PropTypes.func,
  handleSearch: PropTypes.func,
  handleReport: PropTypes.func,
  submitted: PropTypes.bool,
}

export default AgentList

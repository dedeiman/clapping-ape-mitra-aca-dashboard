import React from 'react'
import {
  Row, Col,
  Card, Select,
  Table, DatePicker,
  Button,
} from 'antd'
import PropTypes from 'prop-types'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
import moment from 'moment'

const { Option } = Select

const columns = [
  {
    title: 'SPPA',
    dataIndex: ['sppa', 'sppa_number'],
  },
  {
    title: 'Protect Type',
    dataIndex: 'approval_infos',
  },
  {
    title: 'Class of Bussiness',
    dataIndex: ['creator', 'class_of_bussiness'],
  },
  {
    title: 'Product',
    dataIndex: ['sppa', 'product', 'name'],
  },
  {
    title: 'Approval Status',
    dataIndex: 'approval',
  },
  {
    title: 'Approval By',
    dataIndex: ['creator', 'name'],
  },
  {
    title: 'Approval Date',
    dataIndex: 'created_at',
  },
  {
    title: 'Approval Reason',
    dataIndex: 'reason',
  },
  {
    title: 'Cancellation By',
    dataIndex: 'cancellation_by',
  },
  {
    title: 'Cancellation Date',
    dataIndex: 'cancellation_date',
  },
  {
    title: 'Cancellation Reason',
    dataIndex: 'cancellation_reason',
  },
]

const ApprovalReport = ({
  approvalReport, state, changeState,
  handleSearch, handleTableChange,
  handleReport,
}) => {
  const isMobile = window.innerWidth < 768
  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 m-0">Report Approval</h4>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="float-md-right mt-3 mt-md-0"
              onClick={handleReport}
            >
              Export Data
            </Button>
          </Col>
        </Row>
      )}
    >
      <Row gutter={24}>
        <Col xs={24}>
          <form onSubmit={handleSearch}>
            <Row gutter={24}>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Approval Period">
                  <DatePicker.RangePicker
                    format="DD MMM YYYY"
                    value={!isEmpty(state.join_from_date) ? [moment(state.join_from_date), moment(state.join_to_date)] : ''}
                    onChange={(date) => {
                      changeState({
                        ...state,
                        join_from_date: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                        join_to_date: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                      })
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Pilih Status">
                  <Select
                    allowClear
                    placeholder="- Status -"
                    className="w-100"
                    value={state.status || undefined}
                    onChange={val => changeState({ ...state, status: val })}
                  >
                    <Option value="approved" key={Math.random()}>Approve</Option>
                    <Option value="pending" key={Math.random()}>Pending</Option>
                    <Option value="rejected" key={Math.random()}>Rejected</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Pilih COB">
                  <Select
                    allowClear
                    placeholder="- Class of Business -"
                    className="w-100"
                    value={state.cob || undefined}
                    onChange={val => changeState({ ...state, cob: val })}
                  >
                    {
                      (state.cobList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Pilih Produk">
                  <Select
                    allowClear
                    placeholder="- Product -"
                    className="w-100"
                    value={state.product || undefined}
                    onChange={val => changeState({ ...state, product: val })}
                  >
                    {
                      (state.productList || []).map(key => (
                        <Option value={key.id} key={Math.random()}>{key.display_name}</Option>
                      ))
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={12} md={6}>
                <Form.Item className="mb-2" label="Pilih Tipe Perlindungan">
                  <Select
                    allowClear
                    placeholder="- Protect Type -"
                    className="w-100"
                    value={state.level || undefined}
                    disabled
                    onChange={() => false}
                  />
                </Form.Item>
              </Col>
              <Col span={24} align="end">
                <Button type="primary" className="button-lg px-4" htmlType="submit">
                  Search
                </Button>
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
      <div className="table-list mt-3">
        <Table
          dataSource={approvalReport.list.data}
          loading={approvalReport.list.isFetching}
          columns={columns}
          pagination={{
            position: ['bottomRight'],
            current: approvalReport.list.pagination.current_page,
            total: approvalReport.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
          scroll={{ x: 'max-content' }}
        />
      </div>
    </Card>
  )
}

ApprovalReport.propTypes = {
  approvalReport: PropTypes.any,
  state: PropTypes.object,
  changeState: PropTypes.func,
  handleTableChange: PropTypes.func,
  handleSearch: PropTypes.func,
  handleReport: PropTypes.func,
}

export default ApprovalReport

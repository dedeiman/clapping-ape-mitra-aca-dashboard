import React from 'react'
import {
  Row, Col, Form,
  Button, Select, Table,
  DatePicker, Card, Input,
} from 'antd'
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import moment from 'moment'

const { Option } = Select
const ExpiredPolicy = ({
  expiredReport, handleTableChange,
  state, changeState, handleReport, handleSearch,
}) => {
const isMobile = window.innerWidth < 768
const columns = [{
  title: 'No',
  dateIndex: '',
  render: (text, record, idx) => (expiredReport.list.pagination.current_page > 1 ? (expiredReport.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
},{
  title: 'Agent ID',
  dataIndex: 'agent_id',
}, {
  title: 'Profile ID',
  dataIndex: 'profile_id',
}, {
  title: 'Agent Name',
  dataIndex: 'agent_name',
}, {
  title: 'Branch',
  dataIndex: 'branch_code',
}, {
  title: 'Branch Name',
  dataIndex: 'branch_name',
},{
  title: 'Representative',
  dataIndex: '',
},{
  title: 'Class of Bussiness',
  dataIndex: 'class_of_business',
},{
  title: 'Product',
  dataIndex: 'product',
},{
  title: 'Insured Name',
  dataIndex: 'insured_name',
},{
  title: 'SPPA No',
  dataIndex: 'sppa_number',
},{
  title: 'Policy No',
  dataIndex: 'policy_number',
},{
  title: 'Start Period',
  dataIndex: 'start_period',
  render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
},{
  title: 'End Period',
  dataIndex: 'end_period',
  render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
},{
  title: 'Currency',
  dataIndex: 'currency',
},{
  title: 'Rate',
  dataIndex: 'rate',
},{
  title: 'Premi',
  dataIndex: 'premi',
},{
  title: 'Premi in IDR',
  dataIndex: 'premi_in_idr',
},{
  title: 'Policy Charge',
  dataIndex: '',
},{
  title: 'Stamp Duty',
  dataIndex: '',
},{
  title: 'Discount (%)',
  dataIndex: '',
},{
  title: 'Discount',
  dataIndex: '',
},{
  title: 'Commision (%)',
  dataIndex: '',
},{
  title: 'Commision',
  dataIndex: '',
},{
  title: 'Total Premi',
  dataIndex: '',
},{
  title: 'Type of Payment',
  dataIndex: '',
},{
  title: 'Tax',
  dataIndex: '',
},{
  title: 'Commision After Tax',
  dataIndex: '',
},{
  title: 'Payment Date',
  dataIndex: '',
}]
  return (
    <React.Fragment>
        <Card
            title={(
            <Row type="flex">
                <Col xs={24} md={20}>
                    <h4 className="text-primary2 m-0">Report Expired Policy</h4>
                </Col>
            </Row>
            )}
        >
            <form onSubmit={handleSearch}>
                <Row gutter={12}>
                    <Col xs={24} md={8}>
                        <label htmlFor="">Class of Business</label>
                        <Form.Item className="mb-0">
                            <Select
                                allowClear
                                placeholder="- Class of Business -"
                                className="w-100"
                                value={state.cob || undefined}
                                onChange={val => changeState({ ...state, cob: val })}
                            >
                                {
                                (state.cobList || []).map(key => (
                                    <Option value={key.id} key={Math.random()}>{key.name}</Option>
                                ))
                                }
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={8}>
                        <label htmlFor="">Tanggal Pembayaran</label>
                        <Form.Item className="mb-0">
                            <DatePicker.RangePicker
                                format="DD MMM YYYY"
                                className="w-100"
                                value={!isEmpty(state.payment_date_from) ? [moment(state.payment_date_from), moment(state.payment_date_to)] : ''}
                                onChange={(date) => {
                                changeState({
                                    ...state,
                                    payment_date_from: date ? moment(date[0]).format('YYYY-MM-DD') : '',
                                    payment_date_to: date ? moment(date[1]).format('YYYY-MM-DD') : '',
                                })
                                }}
                            />
                        </Form.Item>
                    </Col>
                    <Col xs={24} md={8}>
                        <label htmlFor="">SPPA No / Policy No</label>
                        <Form.Item className="mb-0">
                            <Input
                                allowClear
                                className="w-100"
                                value={state.policy_number}
                                placeholder="Search SPPA No / Policy No ..."
                                onChange={e => changeState({ ...state, policy_number: e.target.value })}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24} className="mt-3">
                    <Col span={8}>
                        <Button type="primary" className="button-lg btn-block" htmlType="submit">
                            Search
                        </Button>
                    </Col>
                </Row>
            </form>
        </Card>
        <Row gutter={24} className="mt-5">
            <Col md={{ span: 5, offset: 19 }}>
                <Button
                    ghost
                    type="primary"
                    className="btn-border-mitra btn-block"
                    onClick=''
                >
                    Export As Excel
                </Button>
            </Col>
        </Row>

        <div className="table-list">
          <Table
            dataSource={expiredReport.list.data}
            columns={columns}
            loading={expiredReport.list.isFetching}
            scroll={{ x: 'max-content' }}
            pagination={{
              current: expiredReport.list.pagination.current_page,
              total: expiredReport.list.pagination.total_count,
              simple: isMobile,
            }}
            onChange={handleTableChange}
          />
        </div>
    </React.Fragment>
  )
}

ExpiredPolicy.propTypes = {
    expiredReport: PropTypes.any,
    handleTableChange: PropTypes.func,
    state: PropTypes.any,
    changeState: PropTypes.func,
    handleReport: PropTypes.func,
    handleSearch: PropTypes.func,
}
  
export default ExpiredPolicy

import React from 'react'
import {
  Row, Col, Card,
  Button, Select, Table,
} from 'antd'

import PropTypes from 'prop-types'

const { Option } = Select


const ContestWinnerList = ({
  optionYear, contestWinner, handleTableChange,
  state, changeState, handleReport, handleSearch,
  optionContest,
}) => {

  const isMobile = window.innerWidth < 768
  const columns = [{
    title: 'No',
    dataIndex: '',
    key: 'no',
    // width: 80,
    render: (text, record, idx) => (contestWinner.list.pagination.current_page > 1 ? (contestWinner.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
  }, {
    title: 'Agent ID',
    dataIndex: 'agent_id',
  }, {
    title: 'Agent Profile ID',
    dataIndex: 'agent_profile_id',
  }, {
    title: 'Agent Name',
    dataIndex: 'agent_name',
  }, {
    title: 'Agent Level',
    dataIndex: 'agent_level',
  }, {
    title: 'Agent Branch',
    dataIndex: 'agent_branch',
  }, {
    title: 'Agent Representative',
    dataIndex: 'agent_representative',
  }, {
    title: 'Contest',
    dataIndex: 'contest_name',
  }, {
    title: 'Reward',
    dataIndex: 'reward',
  }, {
    title: 'Contest Year',
    dataIndex: 'contest_year',
  }]

  return (
    <React.Fragment>
      <Row gutter={[24, 24]} className={isMobile ? 'pt-5' : ''}>
        <Col span={24} className="d-flex justify-content-between align-items-center">
          <div className="title-page">Report - Agent Contest Winner</div>
        </Col>
      </Row>
      <Card className="card-box-filter mt-3">
        <Row gutter={24}>
          <Col xs={24}>
            <form onSubmit={handleSearch}>
              <Row gutter={24}>
                <Col md={10}>
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    placeholder="- Contest ID -"
                    className="mitra-select-filter"
                    value={state.contest_id || undefined}
                    onChange={val => changeState({ ...state, contest_id: val })}
                    suffixIcon={<img src="/assets/dropdown.svg" alt="icondrop" />}
                  >
                    {
                      optionContest.map(key => (
                        <Option value={key.id}>{key.name}</Option>
                      ))
                    }
                  </Select>
                </Col>
                <Col md={10}>
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    placeholder="- Contest Year -"
                    className="mitra-select-filter"
                    value={state.year || undefined}
                    onChange={val => changeState({ ...state, year: val })}
                    suffixIcon={<img src="/assets/dropdown.svg" alt="icondrop" />}
                  >
                    {
                      optionYear.map(key => (
                        <Option value={key.contest_year}>{key.contest_year}</Option>
                      ))
                    }
                  </Select>
                </Col>
                <Col md={4}>
                  <Button
                    type="primary"
                    className="mitra-submit-filter w-100"
                    htmlType="submit"
                  >
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Card>

      <Row gutter={24} className="mt-5">
        <Col md={{ span: 5, offset: 19 }}>
          <Button
            ghost
            type="primary"
            className="align-items-center btn-border-mitra"
            onClick={handleReport}
          >
            Report As Customer
          </Button>
        </Col>
      </Row>

      <div className="table-list">
      <Table
          dataSource={agentList.list.data}
          columns={columns}
          loading={agentList.list.isFetching}
          scroll={{ x: 'max-content' }}
          pagination={{
            current: agentList.list.pagination.current_page,
            total: agentList.list.pagination.total_count,
            simple: isMobile,
          }}
          onChange={handleTableChange}
        />
      </div>
    </React.Fragment>
  )
}

ContestWinnerList.propTypes = {
  optionYear: PropTypes.array,
  contestWinner: PropTypes.any,
  handleTableChange: PropTypes.func,
  state: PropTypes.any,
  optionContest: PropTypes.array,
  changeState: PropTypes.func,
  handleReport: PropTypes.func,
  handleSearch: PropTypes.func,
}

export default ContestWinnerList

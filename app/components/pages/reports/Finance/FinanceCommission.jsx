import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Select, Table, DatePicker, Input, Button, Checkbox } from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
  
const FianceCommissionReport = ({
  dataList, isLoading, handleFilter, isDetail,
  handleSubmitFilter, handleDownload, branchList,
  cobList, statusList, productList, representativeBranchList,
  handleDetail
}) => {
  const isMobile = window.innerWidth < 768
  const columnsSummary = [
    {
      title: 'No',
      dataIndex: 'no',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => (text || '-')
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => (text || '-')
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-')
    }, {
      title: 'Branch',
      dataIndex: 'branch',
      key: 'branch',
      render: text => (text || '-')
    }, {
      title: 'Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => (text || '-')
    }, {
      title: 'Total Commision',
      dataIndex: 'total_commision',
      key: 'total_commision',
      render: text => (text || '-')
    }, {
      title: 'Total Tax',
      dataIndex: 'total_after_commision',
      key: 'total_after_commision',
      render: text => (text || '-')
    }, {
      title: 'Total Commision After Tax',
      dataIndex: 'total_discount',
      key: 'total_discount',
      render: text => (text || '-')
    }
  ]

  const columnsDetail = [
    {
      title: 'No',
      dataIndex: 'no',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => (text || '-')
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => (text || '-')
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-')
    }, {
      title: 'Branch',
      dataIndex: 'branch',
      key: 'branch',
      render: text => (text || '-')
    }, {
      title: 'Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => (text || '-')
    }, {
      title: 'Class of Bussiness',
      dataIndex: 'class_of_business',
      key: 'class_of_business',
      render: text => (text || '-')
    }, {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      render: text => (text || '-')
    }, {
      title: 'Insured Name',
      dataIndex: 'insured_name',
      key: 'insured_name',
      render: text => (text || '-')
    }, {
      title: 'SPPA No',
      dataIndex: 'sppa_number',
      key: 'sppa_number',
      render: text => (text || '-')
    }, {
      title: 'Policy No',
      dataIndex: 'policy_number',
      key: 'policy_number',
      render: text => (text || '-')
    }, {
      title: 'Start Period',
      dataIndex: 'start_period',
      key: 'start_period',
      render: text => (text || '-')
    }, {
      title: 'End Period',
      dataIndex: 'end_period',
      key: 'end_period',
      render: text => (text || '-')
    }, {
      title: 'Rate',
      dataIndex: 'rate_premi',
      key: 'rate_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Premium',
      dataIndex: 'premium',
      key: 'premium',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Premium in IDR',
      dataIndex: 'premium_in_idr',
      key: 'premium_in_idr',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Policy Charge',
      dataIndex: 'policy_charge',
      key: 'policy_charge',
      render: text => (text || '-')
    }, {
      title: 'Stamp Duty',
      dataIndex: 'stamp_duty',
      key: 'stamp_duty',
      render: text => (text || '-')
    }, {
      title: 'Discount (%)',
      dataIndex: 'discount_percent',
      key: 'discount_percent',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Discount',
      dataIndex: 'discount',
      key: 'discount',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision (%)',
      dataIndex: 'commision_percent',
      key: 'commision_percent',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision',
      dataIndex: 'commision',
      key: 'commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Premium',
      dataIndex: 'premium',
      key: 'premium',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Type of Payment',
      dataIndex: 'payment_type',
      key: 'payment_type',
      render: text => (text || '-')
    }, {
      title: 'Payment Date',
      dataIndex: 'payment_date',
      key: 'payment_date',
      render: text => (text || '-')
    }, {
      title: 'Status Policy',
      dataIndex: 'status_policy',
      key: 'status_policy',
      render: text => (text || '-')
    }, {
      title: 'Doc No.',
      dataIndex: 'doc_no',
      key: 'doc_no',
      render: text => (text || '-')
    }, {
      title: 'Premium Note',
      dataIndex: 'premium_note',
      key: 'premium_note',
      render: text => (text || '-')
    }, {
      title: 'Currency Premium',
      dataIndex: 'currency_premium',
      key: 'currency_premium',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Premium',
      dataIndex: 'total_premi',
      key: 'total_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision Note',
      dataIndex: 'commision_note',
      key: 'commision_note',
      render: text => (text || '-')
    }, {
      title: 'Currency Commision',
      dataIndex: 'currency_commision',
      key: 'currency_commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Rate Commision',
      dataIndex: 'rate_commision',
      key: 'rate_commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision',
      dataIndex: 'commision1',
      key: 'commision1',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Tax',
      dataIndex: 'tax',
      key: 'tax',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision After Tax',
      dataIndex: 'total_after_commision',
      key: 'total_after_commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Receipt Voucher',
      dataIndex: 'receipt_voucher',
      key: 'receipt_voucher',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Settle Date',
      dataIndex: 'settle_date',
      key: 'settle_date',
      render: text => (text || '-')
    }
  ]

  return (
    <React.Fragment>
      <Card
        title={(
          <Row type="flex">
            <Col xs={24} md={20}>
              <h4 className="text-primary2 m-0">Report Komisi Finance</h4>
            </Col>
          </Row>
        )}
      >
        <Row gutter={24}>
          <Col xs={24}>
            <form onSubmit={handleSubmitFilter}>
              <Row gutter={24}>
                <Col xs={24} md={12}>
                  <label htmlFor="">Payment Date</label>
                  <Form.Item>
                    <DatePicker.RangePicker
                      format="DD MMM YYYY"
                      className="w-100"
                      onChange={e => handleFilter(e, 'paymentDate')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={12}>
                  <label htmlFor="">Settle Date</label>
                  <Form.Item>
                    <DatePicker.RangePicker
                      format="DD MMM YYYY"
                      className="w-100"
                      onChange={e => handleFilter(e, 'settleDate')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mt-3">
                    <Input
                      allowClear
                      className="w-100"
                      placeholder="Search Agent ID / Profile Id / Name Agent"
                      onChange={e => handleFilter(e.target.value, 'agent_keyword')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mt-3">
                    <Input
                      allowClear
                      className="w-100"
                      placeholder="Search SPPA No / Policy No"
                      onChange={e => handleFilter(e.target.value, 'sppa_policy_no')}
                    />
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Branch -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'branch_id')}
                    >
                      {
                        !isEmpty(branchList) && branchList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Representative -"
                      className="w-100"
                      disabled={isEmpty(representativeBranchList)}
                      onChange={e => handleFilter(e, 'branch_perwakilan_id')}
                    >
                      {
                        !isEmpty(representativeBranchList) && representativeBranchList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Class of Business -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'cob_id')}
                    >
                      {
                        !isEmpty(cobList) && cobList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24} md={6}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Agent Status -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'agent_status')}
                    >
                      {
                        !isEmpty(statusList) && statusList.map(item => (
                          <Option value={item.slug} key={Math.random()}>{item.display_name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={12} md={6}>
                  <Form.Item className="mt-3">
                    <Select
                      allowClear
                      placeholder="- Product -"
                      className="w-100"
                      onChange={e => handleFilter(e, 'product_id')}
                    >
                      {
                        !isEmpty(productList) && productList.map(item => (
                          <Option value={item.id} key={Math.random()}>{item.name}</Option>
                        ))
                      }
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={7} className="mt-4">
                  <Button type="primary" className="button-lg btn-block" htmlType="submit">
                    Search
                  </Button>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Card>
      <Row gutter={24} className="mt-5">
        <Col md={14}>
          <Form.Item className="mt-3">
            <Checkbox onChange={e => handleDetail(e.target.checked, 'is_detail')}>
              Detail
            </Checkbox>
          </Form.Item>
        </Col>
        <Col md={5}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={(e) => handleDownload(e, 'pdf')} disabled={isLoading}>
            Export As PDF
          </Button>
        </Col>
        <Col md={5}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={(e) => handleDownload(e, 'excel')} disabled={isLoading}>
            Export As Excel
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          rowKey={(record, index) => index}
          dataSource={ !isEmpty(dataList) ? dataList.data : [] }
          loading={isLoading}
          columns={isDetail ? columnsDetail : columnsSummary}
          scroll={{ x: 'max-content' }}
          pagination={{
            current: !isEmpty(dataList) ? dataList.meta.current_page : 1,
            total: !isEmpty(dataList) ? dataList.meta.total_count : 0,
            simple: isMobile,
            onChange: (current) => {
              handlePage(current)
            }
          }}
        />
      </div>
    </React.Fragment>
  )
}

FianceCommissionReport.propTypes = {
  dataList: PropTypes.object,
  cobList: PropTypes.array,
  statusList: PropTypes.array,
  branchList: PropTypes.array,
  productList: PropTypes.array,
  representativeBranchList: PropTypes.array,
  isLoading: PropTypes.bool,
  isDetail: PropTypes.bool,
  handleFilter: PropTypes.func,
  handleSubmitFilter: PropTypes.func,
  handlePage: PropTypes.func,
  handleDetail: PropTypes.func,
  handleDownload: PropTypes.func
}

export default FianceCommissionReport

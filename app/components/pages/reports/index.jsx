import React from 'react'
import PropTypes from 'prop-types'
import {
  Table, Card, Button,
  Row, Col, Select, Form,
  DatePicker,
} from 'antd'
import { capitalize } from 'lodash'
import moment from 'moment'

const TitleCard = ({
  stateBranches, stateSubject,
  stateTraining,
  stateTime, handleFilter,
}) => (
  <Row gutter={12} className="w-100">
    <Col xs={24} md={5}>
      <Form.Item label="Filter By Subject" className="m-0 text-truncate">
        <Select
          allowClear
          className="w-100"
          mode="multiple"
          optionFilterProp="children"
          placeholder="Select Subject"
          loading={stateSubject.isFetching}
          onChange={e => handleFilter(e, 'Subject')}
          value={stateSubject.selected}
        >
          {stateSubject.options && stateSubject.options.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
        </Select>
      </Form.Item>
    </Col>
    <Col xs={24} md={5}>
      <Form.Item label="Filter By Training" className="m-0 text-truncate">
        <Select
          allowClear
          className="w-100"
          mode="multiple"
          optionFilterProp="children"
          placeholder="Select Training"
          loading={stateTraining.isFetching}
          onChange={e => handleFilter(e, 'Training')}
          value={stateTraining.selected}
        >
          {(stateTraining.options || []).map(item => (
            <Select.Option key={item.training_id} value={item.training_id}>{item.training_id}</Select.Option>
          ))}
        </Select>
      </Form.Item>
    </Col>
    <Col xs={24} md={5}>
      <Form.Item label="Filter By Branch" className="m-0 text-truncate" style={{ lineHeight: 'normal' }}>
        <Select
          allowClear
          className="w-100"
          mode="multiple"
          optionFilterProp="children"
          placeholder="Select Branch"
          loading={stateBranches.isFetching}
          onChange={e => handleFilter(e, 'Branches')}
          value={stateBranches.selected}
        >
          {stateBranches.options && stateBranches.options.map(item => <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>)}
        </Select>
      </Form.Item>
    </Col>
    <Col xs={24} md={9}>
      <Form.Item label="Filter By Period" className="m-0 text-truncate" style={{ lineHeight: 'normal' }}>
        <DatePicker.RangePicker
          allowClear
          className="w-100"
          placeholder={['Start Date', 'End Date']}
          onChange={e => handleFilter(e, 'Time')}
          value={stateTime}
          // defaultValue={[moment(stateTime[0], 'YYYY-MM-DD'), moment(stateTime[1], 'YYYY-MM-DD')]}
        />
      </Form.Item>
    </Col>
  </Row>
)

const LearningQuiz = ({
  isFetching, handlePage, activeTab,
  stateTime, handleFilter, handleExport,
  handleTab, dataTraining, metaTraining,
  dataTrainingDetail, metaTrainingDetail,
  stateBranches, stateSubject, stateTraining,
}) => {
  const isMobile = window.innerWidth < 768

  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'no',
      // width: 80,
      render: (text, record, idx) => (metaTraining.current_page > 1 ? (metaTraining.current_page - 1) * 10 + (idx + 1) : idx + 1),
    },
    {
      title: 'Subject Name',
      dataIndex: 'subject_name',
      key: 'subject_name',
      render: text => (text || '-'),
    },
    {
      title: 'Branch Organizer',
      dataIndex: 'branch_organizer_name',
      key: 'branch_organizer_name',
      render: text => (text || 0),
    },
    {
      title: 'Total Participants',
      dataIndex: 'total_participants',
      key: 'total_participants',
      render: text => (text || 0),
    },
    {
      title: 'Total Present',
      dataIndex: 'total_present',
      key: 'total_present',
      render: text => (text || 0),
    },
    {
      title: 'Total Absent',
      dataIndex: 'total_absent',
      key: 'total_absent',
      render: text => (text || 0),
    },
    {
      title: 'Quota',
      dataIndex: 'quota',
      key: 'quota',
      render: text => (text || 0),
    },
  ]
  const columnsDetail = [
    {
      title: 'No',
      dataIndex: '',
      key: 'no',
      // width: 50,
      render: (text, record, idx) => (metaTrainingDetail.current_page > 1 ? (metaTrainingDetail.current_page - 1) * 10 + (idx + 1) : idx + 1),
    },
    {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phone_number',
      key: 'phone_number',
    },
    {
      title: 'Level Name',
      dataIndex: 'level_name',
      key: 'level_name',
    },
    {
      title: 'Branch Name',
      dataIndex: 'branch_name',
      key: 'branch_name',
      render: text => (text || '-'),
    },
    {
      title: 'Branch Organizer',
      dataIndex: 'branch_organizer_name',
      key: 'branch_organizer_name',
      render: text => (text || '-'),
    },
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
      render: text => (text || '-'),
    },
    {
      title: 'Register Date',
      dataIndex: 'register_date',
      key: 'register_date',
      render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
    },
    {
      title: 'Training At',
      key: 'training_at',
      children: [
        {
          title: 'Date',
          dataIndex: 'training_date',
          key: 'training_date',
          render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
        },
        {
          title: 'Start',
          dataIndex: 'start_time',
          key: 'start_time',
          render: text => (text || '-'),
        },
        {
          title: 'End',
          dataIndex: 'end_time',
          key: 'end_time',
          render: text => (text || '-'),
        },
      ],
    },
  ]
  const contentList = {
    list: {
      columns,
      rowKey: Math.random(),
      dataSource: dataTraining,
      pagination: {
        showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
        total: metaTraining && metaTraining.total_count,
        current: metaTraining && metaTraining.current_page,
        onChange: (current) => {
          handlePage(current)
        },
        simple: isMobile,
      },
    },
    detail: {
      columns: columnsDetail,
      rowKey: Math.random(),
      dataSource: dataTrainingDetail,
      pagination: {
        showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
        total: metaTrainingDetail && metaTrainingDetail.total_count,
        current: metaTrainingDetail && metaTrainingDetail.current_page,
        onChange: (current) => {
          handlePage(current)
        },
        simple: isMobile,
      },
    },
  }
  return (
    <Card
      tabList={[{ key: 'list', tab: 'List' }, { key: 'detail', tab: 'Detail' }]}
      activeTabKey={activeTab}
      onTabChange={handleTab}
      title={(
        <Row type="flex" justify="space-between" align="bottom">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 m-0">Report Training</h4>
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="float-md-right mt-3 mt-md-0"
              onClick={() => handleExport()}
            >
              {`Export ${capitalize(activeTab)}`}
            </Button>
          </Col>
        </Row>
      )}
    >
      <Table
        bordered
        title={() => TitleCard({
          stateBranches,
          stateSubject,
          stateTraining,
          stateTime,
          handleFilter,
          handleExport,
          type: capitalize(activeTab),
        })}
        loading={isFetching}
        {...contentList[activeTab]}
      />
    </Card>
  )
}

TitleCard.propTypes = {
  stateBranches: PropTypes.object,
  stateSubject: PropTypes.object,
  stateTraining: PropTypes.object,
  stateTime: PropTypes.array,
  handleFilter: PropTypes.func,
}

LearningQuiz.propTypes = {
  isFetching: PropTypes.bool,
  dataTraining: PropTypes.array,
  metaTraining: PropTypes.object,
  dataTrainingDetail: PropTypes.array,
  metaTrainingDetail: PropTypes.object,
  handlePage: PropTypes.func,
  activeTab: PropTypes.string,
  handleTab: PropTypes.func,
  stateBranches: PropTypes.object,
  stateSubject: PropTypes.object,
  stateTraining: PropTypes.object,
  stateTime: PropTypes.array,
  handleFilter: PropTypes.func,
  handleExport: PropTypes.func,
}

export default LearningQuiz

import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Select, Table, DatePicker, Input, Button, Checkbox, InputNumber } from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'
  
const BenefitYearlyReport = ({
  dataList, isLoading, handleFilter,
  branchList, representativeBranchList,
  handleSubmitFilter, handlePage,
  handleDownload
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => (text || '-')
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => (text || '-')
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-')
    }, {
      title: 'Agent Branch Code',
      dataIndex: 'branch_code',
      key: 'branch_code',
      render: text => (text || '-')
    }, {
      title: 'Agent Branch Name',
      dataIndex: 'branch_name',
      key: 'branch_name',
      render: text => (text || '-')
    }, {
      title: 'Agent Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => (text || '-')
    }, {
      title: 'Agent Level',
      dataIndex: 'level',
      key: 'level',
      render: text => (text || '-')
    }, {
      title: '(Pribadi) Total New Business',
      dataIndex: 'total_new_business',
      key: 'total_new_business',
      render: text => (text !== '' ? text : '-')
    }, {
      title: '(Group) Produksi Premi',
      dataIndex: 'produksi_premi',
      key: 'produksi_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: '(Group) Komisi',
      dataIndex: 'komisi',
      key: 'komisi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: '(Group) Claim Settle',
      dataIndex: 'claim_settle',
      key: 'claim_settle',
      render: text => (text !== '' ? text : '-')
    }, {
      title: '(Group) % Claim Ratio',
      dataIndex: 'claim_ratio',
      key: 'claim_ratio',
      render: text => (text !== '' ? text : '-')
    }
  ]

  return (
    <React.Fragment>
      <Card
        title={(
        <Row type="flex">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 m-0">Report Benefit Finance Yearly</h4>
          </Col>
        </Row>
        )}
      >
        <form onSubmit={handleSubmitFilter}>
          <Row gutter={24}>
            <Col xs={24} md={12}>
              <Form.Item className="" label="Production Period">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'productionDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={12}>
              <Form.Item className="" label="Claim Settle Period">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'claimDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mt-2">
                <Input
                  allowClear
                  className="w-100"
                  placeholder="Search Agent ID / Profile Id / Name Agent"
                  onChange={e => handleFilter(e.target.value, 'agent_keyword')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mt-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Claim Ratio (0 - 100)"
                  onChange={e => handleFilter(e, 'claim_ratio')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mt-2">
                <Select
                  allowClear
                  placeholder="- Branch -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'branch_id')}
                >
                  {
                    !isEmpty(branchList) && branchList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mt-2">
                <Select
                  allowClear
                  placeholder="- Representative -"
                  className="w-100"
                  disabled={isEmpty(representativeBranchList)}
                  onChange={e => handleFilter(e, 'branch_perwakilan_id')}
                >
                  {
                    !isEmpty(representativeBranchList) && representativeBranchList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24} className="mt-3">
            <Col span={6}>
              <Button type="primary" className="button-lg btn-block" htmlType="submit">
                Search
              </Button>
            </Col>
          </Row>
        </form>
      </Card>
      <Row gutter={24} className="mt-5" type="flex" justify="end">
        <Col md={{ span: 5 }}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={e => handleDownload(e, 'pdf')} disabled={isLoading}>
            Export As PDF
          </Button>
        </Col>
        <Col md={{ span: 5 }}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={e => handleDownload(e, 'excel')} disabled={isLoading}>
            Export As Excel
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          rowKey={(record, index) => index}
          dataSource={ !isEmpty(dataList) ? dataList.data : [] }
          loading={isLoading}
          columns={columns}
          pagination={{
            current: !isEmpty(dataList) ? dataList.meta.current_page : 1,
            total: !isEmpty(dataList) ? dataList.meta.total_count : 0,
            simple: isMobile,
            onChange: (current) => {
              handlePage(current)
            }
          }}
          scroll={{ x: 'max-content' }}
        />
      </div>
    </React.Fragment>
  )
}

BenefitYearlyReport.propTypes = {
  dataList: PropTypes.object,
  isLoading: PropTypes.bool,
  branchList: PropTypes.array,
  representativeBranchList: PropTypes.array,
  handleFilter: PropTypes.func,
  handleSubmitFilter: PropTypes.func,
  handlePage: PropTypes.func,
  handleDownload: PropTypes.func,
}

export default BenefitYearlyReport

import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Select, Table, DatePicker, Input, Button, Checkbox, InputNumber } from 'antd'
import { Form } from '@ant-design/compatible'
import { isEmpty } from 'lodash'

const { Option } = Select
const BenefitMonthlyReport = ({
  dataList, isLoading, handleFilter, isDetail,
  handleSubmitFilter, handleDownload, branchList,
  cobList, statusList, productList, representativeBranchList,
  handleDetail
}) => {
  const isMobile = window.innerWidth < 768
  const columnsDetail = [
    {
      title: 'No',
      dataIndex: '',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    }, {
      title: 'Agent Status',
      dataIndex: 'agent_status',
      key: 'agent_status',
      render: text => (text || '-')
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => (text || '-')
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => (text || '-')
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-')
    }, {
      title: 'Agent Level',
      dataIndex: 'agent_level',
      key: 'agent_level',
      render: text => (text || '-')
    }, {
      title: 'Branch',
      dataIndex: 'branch',
      key: 'branch',
      render: text => (text || '-')
    }, {
      title: 'Branch Name',
      dataIndex: 'branch_name',
      key: 'branch_name',
      render: text => (text || '-')
    }, {
      title: 'Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => (text || '-')
    }, {
      title: 'Class of Bussiness',
      dataIndex: 'classof_bussiness',
      key: 'classof_bussiness',
      render: text => (text || '-')
    }, {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      render: text => (text || '-')
    }, {
      title: 'Insured Name',
      dataIndex: 'insured_name',
      key: 'insured_name',
      render: text => (text || '-')
    }, {
      title: 'SPPA No',
      dataIndex: 'sppa_no',
      key: 'sppa_no',
      render: text => (text || '-')
    }, {
      title: 'Policy No',
      dataIndex: 'policy_no',
      key: 'policy_no',
      render: text => (text || '-')
    }, {
      title: 'Prev Policy',
      dataIndex: 'prev_policy',
      key: 'prev_policy',
      render: text => (text || '-')
    }, {
      title: 'Start Period',
      dataIndex: 'start_period',
      key: 'start_period',
      render: text => (text || '-')
    }, {
      title: 'End Period',
      dataIndex: 'end_period',
      key: 'end_period',
      render: text => (text || '-')
    }, {
      title: 'Payment Date',
      dataIndex: 'payment_date',
      key: 'payment_date',
      render: text => (text || '-')
    }, {
      title: 'Currency',
      dataIndex: 'currency',
      key: 'currency',
      render: text => (text || '-')
    }, {
      title: 'Rate',
      dataIndex: 'rate',
      key: 'rate',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'TSI',
      dataIndex: 'tsi',
      key: 'tsi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Premium',
      dataIndex: 'premium-note',
      key: 'premium',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Premi in IDR',
      dataIndex: 'premi_in_idr',
      key: 'premi_in_idr',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Policy Charge',
      dataIndex: 'policy_charge',
      key: 'policy_charge',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Stamp Duty',
      dataIndex: 'stamp_duty',
      key: 'stamp_duty',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Discount (%)',
      dataIndex: 'discount_percent',
      key: 'discount_percent',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Discount',
      dataIndex: 'discount',
      key: 'discount',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Discount IDR',
      dataIndex: 'discount_idr',
      key: 'discount_idr',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision (%)',
      dataIndex: 'commision_percent',
      key: 'commision_percent',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision',
      dataIndex: 'commision',
      key: 'commision-detail',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision in IDR',
      dataIndex: 'commision_in_idr',
      key: 'commision_in_idr',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Premi',
      dataIndex: 'total_premi',
      key: 'total_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Type of Payment',
      dataIndex: 'typeof_payment',
      key: 'typeof_payment',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Akusisi',
      dataIndex: 'total_akusisi',
      key: 'total_akusisi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision Payment Date',
      dataIndex: 'commision_payment_date',
      key: 'commision_payment_date',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Status Policy',
      dataIndex: 'status_policy',
      key: 'status_policy',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Doc No.',
      dataIndex: 'doc_no',
      key: 'doc_no',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Booking Date',
      dataIndex: 'booking_date',
      key: 'booking_date',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Premium Note',
      dataIndex: 'premium_note',
      key: 'premium_note',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Currency Premi',
      dataIndex: 'currency_premi',
      key: 'currency_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Rate Premi',
      dataIndex: 'rate_premi',
      key: 'rate_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Premium',
      dataIndex: 'total_premium',
      key: 'total_premium',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision Note',
      dataIndex: 'commision_note',
      key: 'commision_note',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Currency Commision',
      dataIndex: 'currency_commision',
      key: 'currency_commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Rate Commision',
      dataIndex: 'rate_commision',
      key: 'rate_commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision',
      dataIndex: 'commision',
      key: 'commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Tax',
      dataIndex: 'total-tax',
      key: 'tax',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Commision After Tax',
      dataIndex: 'commision_after_tax',
      key: 'commision_after_tax',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Bank Name',
      dataIndex: 'bank_name',
      key: 'bank_name',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Account No',
      dataIndex: 'account_no',
      key: 'account_no',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Account Name',
      dataIndex: 'account_name',
      key: 'account_name',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Receipt Voucher',
      dataIndex: 'receipt_voucher',
      key: 'receipt_voucher',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Settle Date',
      dataIndex: 'settle_date',
      key: 'settle_date',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Mobile Marketing',
      dataIndex: 'mobile_marketing',
      key: 'mobile_marketing',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Entertainment',
      dataIndex: 'entertainment',
      key: 'entertainment',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Agent ID Upline lv 1',
      dataIndex: 'agent_id_uplinelv_1',
      key: 'agent_id_uplinelv_1',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Name Upline lv 1',
      dataIndex: 'name_uplinelv_1',
      key: 'name_uplinelv_1',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Level Upline lv 1',
      dataIndex: 'level_uplinelv_1',
      key: 'level_uplinelv_1',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Train Edu lv 1',
      dataIndex: 'train_edulv_1',
      key: 'train_edulv_1',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Agent ID Upline lv 2',
      dataIndex: 'agent_id_uplinelv_2',
      key: 'agent_id_uplinelv_2',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Name Upline lv 2',
      dataIndex: 'name_uplinelv_2',
      key: 'name_uplinelv_2',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Level Upline lv 2',
      dataIndex: 'level_uplinelv_2',
      key: 'level_uplinelv_2',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Train Edu lv 2',
      dataIndex: 'train_edulv_2',
      key: 'train_edulv_2',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Agent ID Upline lv 3',
      dataIndex: 'agent_id_uplinelv_3',
      key: 'agent_id_uplinelv_3',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Name Upline lv 3',
      dataIndex: 'name_uplinelv_3',
      key: 'name_uplinelv_3',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Level Upline lv 3',
      dataIndex: 'level_uplinelv_3',
      key: 'level_uplinelv_3',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Train Edu lv 3',
      dataIndex: 'train_edulv_3',
      key: 'train_edulv_3',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Agent ID Upline lv 4',
      dataIndex: 'agent_id_uplinelv_4',
      key: 'agent_id_uplinelv_4',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Name Upline lv 4',
      dataIndex: 'name_uplinelv_4',
      key: 'name_uplinelv_4',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Level Upline lv 4',
      dataIndex: 'level_uplinelv_4',
      key: 'level_uplinelv_4',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Train Edu lv 4',
      dataIndex: 'train_edulv_4',
      key: 'train_edulv_4',
      render: text => (text !== '' ? text : '-')
    }
  ]
  
  const columnsSummary = [
    {
      title: 'No',
      dataIndex: 'no',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    }, {
      title: 'Agent Status',
      dataIndex: 'agent_status',
      key: 'agent_status',
      render: text => (text || '-')
    }, {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => (text || '-')
    }, {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => (text || '-')
    }, {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => (text || '-')
    }, {
      title: 'Level',
      dataIndex: 'level',
      key: 'level',
      render: text => (text || '-')
    }, {
      title: 'Branch',
      dataIndex: 'branch',
      key: 'branch',
      render: text => (text || '-')
    }, {
      title: 'Branch Name',
      dataIndex: 'branch-name',
      key: 'branch-name',
      render: text => (text || '-')
    }, {
      title: 'Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => (text || '-')
    }, {
      title: 'Class of Bussiness',
      dataIndex: 'class_of_business',
      key: 'class_of_business',
      render: text => (text || '-')
    }, {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      render: text => (text || '-')
    }, {
      title: 'Bank Name',
      dataIndex: 'bank_name',
      key: 'bank_name',
      render: text => (text || '-')
    }, {
      title: 'Account No',
      dataIndex: 'account_no',
      key: 'account_no',
      render: text => (text || '-')
    }, {
      title: 'Account Name',
      dataIndex: 'account_name',
      key: 'account_name',
      render: text => (text || '-')
    }, {
      title: 'Total Gross Premi (Premi in IDR)',
      dataIndex: 'total_gross_premi',
      key: 'total_gross_premi',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Commision',
      dataIndex: 'total_commision',
      key: 'total_commision',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Entertainment',
      dataIndex: 'total_entertainment',
      key: 'total_entertainment',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Mobile Marketing',
      dataIndex: 'total_mobile_marketing',
      key: 'total_mobile_marketing',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Training & Education Lvl 1',
      dataIndex: 'total_training_edu_1',
      key: 'total_training_edu_1',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Training & Education Lvl 2',
      dataIndex: 'total_training_edu_2',
      key: 'total_training_edu_2',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Training & Education Lvl 3',
      dataIndex: 'total_training_edu_3',
      key: 'total_training_edu_3',
      render: text => (text !== '' ? text : '-')
    }, {
      title: 'Total Training & Education Lvl 4',
      dataIndex: 'total_training_edu_4',
      key: 'total_training_edu_4',
      render: text => (text !== '' ? text : '-')
    }
  ]

  return (
    <React.Fragment>
      <Card
        title={(
        <Row type="flex">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 m-0">Report Benefit Finance Monthly</h4>
          </Col>
        </Row>
        )}
      >
        <form onSubmit={handleSubmitFilter}>
          <Row gutter={24}>
            <Col xs={24} md={8}>
              <label htmlFor="">Booking Date</label>
              <Form.Item className="">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'bookDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={8}>
              <label htmlFor="">Start Date Period</label>
              <Form.Item className="mb-2">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'periodDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={8}>
              <label htmlFor="">Commision Payment Date</label>
              <Form.Item className="mb-2">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'commisionDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
              <Input
                allowClear
                className="w-100"
                placeholder="Search Agent ID / Profile Id / Name Agent"
                onChange={e => handleFilter(e.target.value, 'agent_keyword')}
              />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Input
                  allowClear
                  className="w-100"
                  placeholder="Search SPPA No / Policy No"
                  onChange={e => handleFilter(e.target.value, 'sppa_policy_keyword')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Mobile Marketing (%)"
                  onChange={e => handleFilter(e, 'mobile_marketing')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Entertainment (%)"
                  onChange={e => handleFilter(e, 'entertainment')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Train Edu Lvl 1 (%)"
                  onChange={e => handleFilter(e, 'train_edu1')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Train Edu Lvl 2 (%)"
                  onChange={e => handleFilter(e, 'train_edu2')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Train Edu Lvl 3 (%)"
                  onChange={e => handleFilter(e, 'train_edu3')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <InputNumber
                  allowClear
                  className="w-100"
                  type="number"
                  min={1}
                  max={100}
                  placeholder="Train Edu Lvl 4 (%)"
                  onChange={e => handleFilter(e, 'train_edu4')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Branch -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'branch_id')}
                >
                  {
                    !isEmpty(branchList) && branchList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Representative -"
                  className="w-100"
                  disabled={isEmpty(representativeBranchList)}
                  onChange={e => handleFilter(e, 'branch_perwakilan_id')}
                >
                  {
                    !isEmpty(representativeBranchList) && representativeBranchList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Class of Business -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'cob_id')}
                >
                  {
                    !isEmpty(cobList) && cobList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Product -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'product_id')}
                >
                  {
                    !isEmpty(productList) && productList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Agent Status -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'agent_status')}
                >
                  {
                    !isEmpty(statusList) && statusList.map(item => (
                      <Option value={item.slug} key={Math.random()}>{item.display_name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24} className="mt-3">
            <Col span={6}>
              <Button type="primary" className="button-lg btn-block" htmlType="submit" loading={isLoading}>
                Search
              </Button>
            </Col>
          </Row>
        </form>
      </Card>
      <Row gutter={24} className="mt-5">
        <Col md={14}>
          <Form.Item className="mt-3">
            <Checkbox onChange={e => handleDetail(e.target.checked, 'is_detail')}>
              Detail
            </Checkbox>
          </Form.Item>
        </Col>
        <Col md={5}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={(e) => handleDownload(e, 'pdf')} disabled={isLoading}>
            Export As PDF
          </Button>
        </Col>
        <Col md={5}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={(e) => handleDownload(e, 'excel')} disabled={isLoading}>
            Export As Excel
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          rowKey={(record, index) => index}
          dataSource={ !isEmpty(dataList) ? dataList.data : [] }
          loading={isLoading}
          columns={isDetail ? columnsDetail : columnsSummary}
          scroll={{ x: 'max-content' }}
          pagination={{
            current: !isEmpty(dataList) ? dataList.meta.current_page : 1,
            total: !isEmpty(dataList) ? dataList.meta.total_count : 0,
            simple: isMobile,
            onChange: (current) => {
              handlePage(current)
            }
          }}
        />
      </div>
    </React.Fragment>
  )
}

BenefitMonthlyReport.propTypes = {
  dataList: PropTypes.object,
  cobList: PropTypes.array,
  statusList: PropTypes.array,
  branchList: PropTypes.array,
  productList: PropTypes.array,
  representativeBranchList: PropTypes.array,
  isLoading: PropTypes.bool,
  isDetail: PropTypes.bool,
  handleFilter: PropTypes.func,
  handleSubmitFilter: PropTypes.func,
  handlePage: PropTypes.func,
  handleDownload: PropTypes.func
}

export default BenefitMonthlyReport

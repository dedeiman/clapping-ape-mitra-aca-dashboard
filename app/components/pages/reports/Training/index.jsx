import React from 'react'
import {
  Row, Col,
  Button, Select, Table, Checkbox,
  DatePicker, Card,
} from 'antd'

import PropTypes from 'prop-types'
import moment from 'moment'

const { Option } = Select

const TrainingReport = ({
  trainingReport, handleTableChange,
  state, changeState, handleReport, handleSearch, handleChecked,
  handleFilter,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [{
    title: 'No',
    dateIndex: '',
    render: (text, record, idx) => (trainingReport.list.pagination.current_page > 1 ? (trainingReport.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
  }, {
    title: 'Training Date',
    dataIndex: 'training_date',
    render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
  }, {
    title: 'Training ID',
    dataIndex: 'training_id',
  }, {
    title: 'Training Subject',
    dataIndex: 'subject_name',
  }, {
    title: 'Branch Organizer',
    dataIndex: 'branch_organizer_name',
  }, {
    title: 'Jumlah Terdaftar',
    dataIndex: 'total_participants',
  }, {
    title: 'Jumlah Peserta Hadir',
    dataIndex: 'total_present',
  }, {
    title: 'Jumlah Peserta Tidak Hadir',
    dataIndex: 'total_absent',
  }, {
    title: 'Kuota Peserta',
    dataIndex: 'quota',
  },
  ]

  const columnDetail = [{
    title: 'No',
    dateIndex: '',
    render: (text, record, idx) => (trainingReport.list.pagination.current_page > 1 ? (trainingReport.list.pagination.current_page - 1) * state.per_page + (idx + 1) : idx + 1),
  }, {
    title: 'Training ID',
    dataIndex: 'training_id',
  }, {
    title: 'Register Date',
    dataIndex: 'register_date',
    render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
  }, {
    title: 'Agent ID',
    dataIndex: 'agent_id',
  }, {
    title: 'Profile ID',
    dataIndex: 'profile_id',
  }, {
    title: 'Agent Name',
    dataIndex: 'agent_name',
  }, {
    title: 'Level',
    dataIndex: 'level_name',
  }, {
    title: 'Branch',
    dataIndex: 'branch_name',
  }, {
    title: 'Representative',
    dataIndex: 'representative_branch_name',
  }, {
    title: 'Email',
    dataIndex: 'email',
  }, {
    title: 'Phone Number',
    dataIndex: 'phone_number',
  }, {
    title: 'Training Date',
    dataIndex: 'training_date',
    render: text => (text ? moment(text).format('DD MMMM YYYY') : '-'),
  }, {
    title: 'Start Time',
    dataIndex: 'start_time',
  }, {
    title: 'End Time',
    dataIndex: 'end_time',
  }, {
    title: 'Tipe Training',
    dataIndex: 'training_type',
  }, {
    title: 'Subject Training ',
    dataIndex: 'subject_name',
  }, {
    title: 'Branch Organizer',
    dataIndex: 'branch_organizer_name',
  }, {
    title: 'Lokasi Training',
    dataIndex: 'location',
  }, {
    title: 'Kehadiran',
    dataIndex: '',
    render: (record) => (record.is_present == true ? ('Hadir'): 'Tidak'),
  }, {
    title: 'Ujian',
    dataIndex: '',
    render: (record) => (record.is_quiz_exists == true ? ('Ya'): 'Tidak'),
  }, {
    title: 'Trainer',
    dataIndex: 'trainer_name',
  },
  ]

  return (
    <React.Fragment>
        <Card
            title={(
            <Row type="flex">
                <Col xs={24} md={20}>
                    <h4 className="text-primary2 m-0">Report Training</h4>
                </Col>
            </Row>
            )}
        >
          <form onSubmit={handleSearch}>
            <Row gutter={12}>
              <Col md={8} className="mb-2">
                <Select
                  allowClear
                  placeholder="All Subject"
                  optionFilterProp="children"
                  showSearch
                  className="mitra-select-filter"
                  value={state.subject || undefined}
                  onChange={val => changeState({ ...state, subject: val })}
                >
                  <Option value="" key={Math.random()}>All Subject</Option>
                  {
                    (state.subjectList || []).map(key => (
                      <Option value={key.id} key={Math.random()}>{key.name}</Option>
                    ))
                  }
                </Select>
              </Col>
              <Col md={8} className="mb-2">
                <Select
                  allowClear
                  placeholder="All Training"
                  className="mitra-select-filter"
                  optionFilterProp="children"
                  showSearch
                  value={state.training || undefined}
                  onChange={(val) => {
                    const listTraining = state.trainingList.filter(item => item.training_id === val)

                    if (listTraining.length > 0) {
                      changeState({ ...state, training: listTraining[0].training_id })
                    } else {
                      changeState({ ...state, training: '' })
                    }
                  }}
                >
                  <Option value="" key={Math.random()}>All Training</Option>
                  {
                    (state.trainingList || []).map(key => (
                      <Option value={key.training_id} key={Math.random()}>{key.training_id}</Option>
                    ))
                  }
                </Select>
              </Col>
              <Col md={8} className="mb-2">
                <Select
                  allowClear
                  placeholder="All Branch"
                  optionFilterProp="children"
                  showSearch
                  className="mitra-select-filter"
                  value={state.branch || undefined}
                  onChange={val => changeState({ ...state, branch: val })}
                >
                  <Option value="" key={Math.random()}>All Branch</Option>
                  {
                    (state.branchList || []).map(key => (
                      <Option value={key.id} key={Math.random()}>{key.name}</Option>
                    ))
                  }
                </Select>
              </Col>
              <Col xs={24} md={8}>
                <DatePicker.RangePicker
                  allowClear
                  className="mitra-date-picker"
                  placeholder={['Start Date', 'End Date']}
                  onChange={e => handleFilter(e, 'Time')}
                />
              </Col>
              <Col xs={24} md={8}>
              </Col>
              <Col xs={24} md={8}>
                    <Button
                      type="primary"
                      className="mitra-submit-filter btn-block"
                      htmlType="submit"
                    >
                      Search
                  </Button>
                </Col>
            </Row>
          </form>
        </Card>
        <Row gutter={24} className="mt-5">
          <Col md={5}>
            <Checkbox
              checked={state.isChecked}
              onChange={handleChecked}
            >
              Show Detail
            </Checkbox>
          </Col>
          <Col md={{ span: 5, offset: 14 }}>
            <Button
              ghost
              type="primary"
              className="align-items-center btn-border-mitra w-100"
              onClick={handleReport}
            >
              Export As Excel
            </Button>
          </Col>
        </Row>

        <div className="table-list">
          <Table
            dataSource={trainingReport.list.data}
            columns={state.isChecked ? columnDetail : columns}
            loading={trainingReport.list.isFetching}
            scroll={{ x: 'max-content' }}
            pagination={{
              current: trainingReport.list.pagination.current_page,
              total: trainingReport.list.pagination.total_count,
              simple: isMobile,
            }}
            onChange={handleTableChange}
            rowKey="training_id"
          />
        </div>
    </React.Fragment>
  )
}

TrainingReport.propTypes = {
  trainingReport: PropTypes.any,
  handleTableChange: PropTypes.func,
  state: PropTypes.any,
  changeState: PropTypes.func,
  handleReport: PropTypes.func,
  handleSearch: PropTypes.func,
  handleChecked: PropTypes.func,
  handleFilter: PropTypes.func,
}

export default TrainingReport

import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Select, Table, DatePicker, Input, Button, Checkbox } from 'antd'
import { isEmpty } from 'lodash'
import { Form } from '@ant-design/compatible'
import moment from 'moment'

const { Option } = Select
const CareerReport = ({
  dataList, isLoading, handleFilter,
  state, handleDownloadCareer, branchList,
  levelList, statusList, handleSubmitFilter,
  representativeBranchList, handlePage
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: '',
      render: (text, record, idx) => (!isEmpty(dataList) ? (dataList.meta.current_page - 1) * 10 + (idx + 1) : idx + 1)
    },
    {
      title: 'Agent ID',
      dataIndex: 'agent_id',
      key: 'agent_id',
      render: text => text || '-'
    },
    {
      title: 'Profile ID',
      dataIndex: 'profile_id',
      key: 'profile_id',
      render: text => text || '-'
    },
    {
      title: 'Agent Name',
      dataIndex: 'agent_name',
      key: 'agent_name',
      render: text => text || '-'
    },
    {
      title: 'Branch',
      dataIndex: 'branch',
      key: 'branch',
      render: text => text || '-'
    },
    {
      title: 'Representative',
      dataIndex: 'representative',
      key: 'representative',
      render: text => text || '-'
    },
    {
      title: 'Upline Agent ID',
      dataIndex: 'upline_agent_id',
      key: 'upline_agent_id',
      render: text => text || '-'
    },
    {
      title: 'Upline Agent ProfileID',
      dataIndex: 'upline_profile_id',
      key: 'upline_profile_id',
      render: text => text || '-'
    },
    {
      title: 'Upline Agent Name',
      dataIndex: 'upline_agent_name',
      key: 'upline_agent_name',
      render: text => text || '-'
    },
    {
      title: 'Upline Agent Branch',
      dataIndex: 'upline_branch',
      key: 'upline_branch',
      render: text => text || '-'
    },
    {
      title: 'Upline Agent Representative',
      dataIndex: 'upline_representative',
      key: 'upline_representative',
      render: text => text || '-'
    },
    {
      title: 'Agent Level',
      dataIndex: 'agent_level',
      key: 'agent_level',
      render: text => text || '-'
    },
    {
      title: 'Join Date',
      dataIndex: 'join_date',
      key: 'join_date',
      render: text => !isEmpty(text) ? moment(text).format('DD-MM-YYYY') : '-'
    },
    {
      title: 'Agent Status',
      dataIndex: 'agent_status',
      key: 'agent_status',
      render: text => text || '-'
    },
    {
      title: 'Number of Policy',
      dataIndex: 'policy_no',
      key: 'policy_no',
      render: text => text || '-'
    },
    {
      title: 'Total Premium in IDR',
      dataIndex: 'total_premium_in_idr',
      key: 'total_premium_in_idr',
      render: text => text || '-'
    },
    {
      title: 'Number of Policy Claim',
      dataIndex: 'number_of_policy_claim',
      key: 'number_of_policy_claim',
      render: text => text || '-'
    },
    {
      title: 'Total Claim Settle In IDR',
      dataIndex: 'total_claim_settle_in_idr',
      key: 'total_claim_settle_in_idr',
      render: text => text || '-'
    },
    {
      title: 'Commision Total',
      dataIndex: 'commision_total',
      key: 'commision_total',
      render: text => text || '-'
    },
    {
      title: 'Discount Total',
      dataIndex: 'discount_total',
      key: 'discount_total',
      render: text => text || '-'
    },
    {
      title: 'Akusisi Total',
      dataIndex: 'akusisi_total',
      key: 'akusisi_total',
      render: text => text || '-'
    },
    {
      title: 'Claim Ratio',
      dataIndex: 'claim_ratio',
      key: 'claim_ratio',
      render: text => text || '-'
    }
  ]

  return (
    <React.Fragment>
      <Card
        title={(
        <Row type="flex">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 m-0">Report Benefit Career</h4>
          </Col>
        </Row>
        )}
      >
        <form onSubmit={handleSubmitFilter}>
          <Row gutter={24}>
            <Col xs={24} md={12}>
              <Form.Item className="mb-2" label="Production Period">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'productionDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={12}>
              <Form.Item className="mb-2" label="Claim Settle Period">
                <DatePicker.RangePicker
                  format="DD MMM YYYY"
                  className="w-100"
                  onChange={e => handleFilter(e, 'claimDate')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Input
                  allowClear
                  className="w-100"
                  placeholder="Search Agent ID / Profile Id / Name Agent"
                  onChange={e => handleFilter(e.target.value, 'agent_keyword')}
                />
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Agent Status -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'agent_status')}
                >
                  {
                    !isEmpty(statusList) && statusList.map(item => (
                      <Option value={item.slug} key={Math.random()}>{item.display_name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Level -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'agent_level')}
                >
                  {
                    !isEmpty(levelList) && levelList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Branch -"
                  className="w-100"
                  onChange={e => handleFilter(e, 'branch_id')}
                >
                  {
                    !isEmpty(branchList) && branchList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col xs={12} md={6}>
              <Form.Item className="mb-2">
                <Select
                  allowClear
                  placeholder="- Branch Representative -"
                  className="w-100"
                  disabled={isEmpty(representativeBranchList)}
                  onChange={e => handleFilter(e, 'branch_perwakilan_id')}
                >
                  {
                    !isEmpty(representativeBranchList) && representativeBranchList.map(item => (
                      <Option value={item.id} key={Math.random()}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24} className="mt-3">
            <Col span={6}>
              <Button type="primary" className="button-lg btn-block" htmlType="submit" disabled={isLoading}>
                Search
              </Button>
            </Col>
          </Row>
        </form>
      </Card>
      <Row gutter={24} className="mt-5" type="flex" justify="end">
        <Col md={{ span: 5 }}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={() => handleDownloadCareer('pdf')} disabled={isLoading}>
            Export As PDF
          </Button>
        </Col>
        <Col md={{ span: 5 }}>
          <Button ghost type="primary" className="btn-border-mitra w-100" onClick={() => handleDownloadCareer('excel')} disabled={isLoading}>
            Export As Excel
          </Button>
        </Col>
      </Row>
      <div className="table-list">
        <Table
          rowKey={(record, index) => index}
          dataSource={!isEmpty(dataList) ? dataList.data : []}
          loading={isLoading}
          columns={columns}
          scroll={{ x: 'max-content' }}
          pagination={{
            total: !isEmpty(dataList) ? dataList.meta.total_count : 0,
            current: !isEmpty(dataList) ? dataList.meta.current_page : 1,
            simple: isMobile,
            onChange: (current) => {
              handlePage(current)
            }
          }}
        />
      </div>
    </React.Fragment>
  )
}

CareerReport.propTypes = {
  dataList: PropTypes.object,
  state: PropTypes.object,
  branchList: PropTypes.array,
  levelList: PropTypes.array,
  statusList: PropTypes.array,
  representativeBranchList: PropTypes.array,
  isLoading: PropTypes.bool,
  handleFilter: PropTypes.func,
  handleSubmitFilter: PropTypes.func,
  handlePage: PropTypes.func,
  handleDownloadCareer: PropTypes.func
}

export default CareerReport

import PropTypes from 'prop-types'
import {
  // Row, Col,
  Button, Avatar,
  Statistic,
} from 'antd'
import { Row, Col, Card } from 'react-bootstrap'
import history from 'utils/history'

const Dashboard = ({ stateDashboard }) => (
  <Row className="h-100 wrapper-dashboard">
    <Col lg={6} md={12} className="h-100 mb-4">
      <Card className="h-100">
        <Card.Body className="d-flex align-items-center flex-column">
          <h2 className="text-primary2 w-100 mb-4">
            <Avatar
              size="large"
              shape="square"
              src="/assets/ic-search-agent.svg"
              className="img-contain bg-transparent mr-2"
              style={{ filter: 'invert(1) sepia(1) saturate(5) hue-rotate(175deg)' }}
            />
            Total User
          </h2>
          <Row className="mb-3 w-100">
            {(stateDashboard.totalUsers || []).map(item => (
              <Col lg={4} md={12} className="wrapper-card mb-md-4" key={`user-${Math.random()}`}>
                <Card className="h-100">
                  <Card.Body className="text-capitalize">
                    <Statistic title={(item.group_name).replace('-', ' ')} value={item.total} valueStyle={{ textAlign: 'right' }} />
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>

          <Row className="w-100 justify-content-center mt-auto">
            <Col md={8}>
              <Button
                type="primary"
                className="w-100"
                onClick={() => history.push('/users')}
              >
                View All
              </Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Col>
    <Col lg={6} md={12} className="h-100">
      <Card className="h-100">
        <Card.Body className="d-flex align-items-center flex-column">
          <h2 className="text-primary2 mb-4 w-100">
            <Avatar
              size="large"
              shape="square"
              src="/assets/ic-search-policy.svg"
              className="img-contain bg-transparent mr-2"
              style={{ filter: 'invert(1) sepia(1) saturate(5) hue-rotate(175deg)' }}
            />
            Total Product
          </h2>
          <Row className="mb-3 w-100">
            {(stateDashboard.totalProducts || []).map(item => (
              <Col lg={4} md={12} className="wrapper-card mb-md-4" key={`user-${Math.random()}`}>
                <Card className="h-100">
                  <Card.Body className="text-capitalize">
                    <Statistic title={(item.cob_name).replace('-', ' ')} value={item.total} valueStyle={{ textAlign: 'right' }} />
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
          {/*
          <Row className="h-100 w-100">
            <Col lg={5} md={12} className="h-50 wrapper-card-height mb-md-4">
              <Card className="h-100">
                <Card.Body>
                  <Statistic title="COB" value={11} valueStyle={{ textAlign: 'right' }} />
                </Card.Body>
              </Card>
            </Col>
            <Col lg={7} md={12} className="h-50 wrapper-card-col">
              <Card className="h-100">
                <Card.Body>
                  <Row className="h-100">
                    <Col lg={12} xs={12} className="mb-md-4">
                      <Card className="h-100">
                        <Card.Body>
                          <Statistic title="Product" value={9} valueStyle={{ textAlign: 'right' }} />
                        </Card.Body>
                      </Card>
                    </Col>
                    <Col lg={6} xs={12}>
                      <Card className="h-100">
                        <Card.Body>
                          <Statistic title="Syariah" value={9} valueStyle={{ textAlign: 'right' }} />
                        </Card.Body>
                      </Card>
                    </Col>
                    <Col lg={6} xs={12}>
                      <Card className="h-100">
                        <Card.Body>
                          <Statistic title="Konvensional" value={9} valueStyle={{ textAlign: 'right' }} />
                        </Card.Body>
                      </Card>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          */}
          <Row className="w-100 justify-content-center mt-auto">
            <Col md={8} className="h-100 mx-auto">
              <Button
                type="primary"
                className="w-100"
                onClick={() => history.push('/products')}
              >
                View All
              </Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Col>
  </Row>
)

Dashboard.propTypes = {
  stateDashboard: PropTypes.object,
}
export default Dashboard

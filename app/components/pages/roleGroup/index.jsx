import PropTypes from 'prop-types'
import {
  Card, Table, Col, Row,
  Button, Alert,
  Popconfirm, Input,
} from 'antd'
import history from 'utils/history'

const RoleGroupPage = ({
  isFetching, handlePage,
  metaRoleGroup, dataRoleGroup,
  handleDelete, errorMessage,
  closeError, handleSearch,
  keyword, currentPage, setKeyword,
}) => {
  const isMobile = window.innerWidth < 768
  const columns = [
    {
      title: 'No',
      dataIndex: '',
      key: 'no',
      width: 110,
      render: (text, row, idx) => {
        const numb = idx + 1
        const pageCount = (currentPage && currentPage > 1) ? (currentPage - 1) : ''
        if (numb < 10) return `${pageCount}${numb}`
        if (numb === 10) return `${pageCount + 1}0`
        return numb
      },
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      // width: 110,
      render: (text, record) => (record.display_name || text).split('-').join(' ').toUpperCase(),
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'action',
      width: 110,
      render: (text, record) => (
        <>
          <Button onClick={() => history.push(`/role-group/${record.id}/edit`)} className="px-2">
            <i className="lar la-edit text-primary" style={{ fontSize: '20px' }} />
          </Button>
          <Popconfirm
            title="Are you sure delete this item?"
            okText="Yes"
            cancelText="No"
            placement="topLeft"
            onConfirm={() => handleDelete(record.id)}
          >
            {/* <Button className="px-2">
              <i className="las la-trash text-danger" style={{ fontSize: '20px' }} />
            </Button> */}
          </Popconfirm>
        </>
      ),
    },
  ]

  return (
    <Card
      title={(
        <Row type="flex" justify="flex-between" align="end">
          <Col xs={24} md={20}>
            <h4 className="text-primary2 mb-3">Role Group List</h4>
            <Input.Search
              allowClear
              placeholder="Search..."
              value={keyword}
              onChange={(e) => {
                const { value } = e.target
                if (value === '' || /^[a-zA-Z0-9-_+@. ]+$/.test(value)) {
                  setKeyword(value)
                }
              }}
              onSearch={handleSearch}
              className="w-md-50"
            />
          </Col>
          <Col xs={24} md={4}>
            <Button
              type="primary"
              className="float-md-right mt-3 mt-md-0"
              onClick={() => history.push('/role-group/add')}
            >
              Add Role Group
            </Button>
          </Col>
        </Row>
      )}
    >
      {errorMessage && (
        <Alert
          message="Error"
          description={errorMessage}
          type="error"
          closable
          onClose={closeError}
        />
      )}

      <Table
        bordered
        rowKey="id"
        columns={columns}
        dataSource={dataRoleGroup}
        loading={isFetching}
        pagination={{
          showTotal: (total, range) => `Showing ${range[0]} to ${range[1]} of ${total} entries`,
          total: metaRoleGroup ? metaRoleGroup.total_count : dataRoleGroup.length,
          current: currentPage || 1,
          onChange: handlePage,
          simple: isMobile,
        }}
      />
    </Card>
  )
}

RoleGroupPage.propTypes = {
  isFetching: PropTypes.bool,
  dataRoleGroup: PropTypes.array,
  metaRoleGroup: PropTypes.object,
  handlePage: PropTypes.func,
  handleDelete: PropTypes.func,
  closeError: PropTypes.func,
  errorMessage: PropTypes.string,
  handleSearch: PropTypes.func,
  keyword: PropTypes.string,
  currentPage: PropTypes.string,
  setKeyword: PropTypes.func,
}

export default RoleGroupPage

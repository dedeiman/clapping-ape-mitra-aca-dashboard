/* eslint-disable no-restricted-globals */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Input, Icon,
  Divider, Alert,
  Result, Form,
  Select,
} from 'antd'
import { Button, Col, Row } from 'react-bootstrap'
import qs from 'query-string'
import Helper from 'utils/Helper'
import history from 'utils/history'
import { isEmpty, capitalize } from 'lodash'

const ForgotPassword = ({
  onChange,
  onSubmit,
  form,
  errorMessage,
  isAuthenticating,
  onCloseError,
  isPassword,
  tokenExpired,
  handleResendOTP,
}) => {
  const { getFieldDecorator, getFieldValue } = form
  if (tokenExpired) {
    return (
      <Result
        status="403"
        className="text-capitalize"
        title={tokenExpired}
        subTitle="Sorry, your token has been expired."
        extra={(
          <Button type="primary" onClick={() => history.push('/')}>
            Back To Login
          </Button>
        )}
      />
    )
  }
  return (
    <div className="form-container forgot row m-0">
      <Col md={7} className="d-none d-md-block" />
      <Col>
        <Form className="form-signin" onSubmit={onSubmit} hideRequiredMark layout="vertical">
          {(() => {
            switch (isPassword) {
              case 'forgot':
              case 'reset':
                return (
                  <h3>
                    {`Lupa ${qs.parse(location.search).is_secondary ? 'second' : ''} password Anda`}
                    <br />
                    {' '}
                    kami akan bantu
                  </h3>
                )
              default:
                return <h3>Masukan Kode OTP</h3>
            }
          })()}

          {(() => {
            switch (isPassword) {
              case 'forgot':
                return (
                  <>
                    <Form.Item label="Select Type">
                      {getFieldDecorator('key', {
                        rules: Helper.fieldRules(['required'], 'Select Type'),
                      })(
                        <Select
                          showSearch
                          size="large"
                          placeholder="Select Type"
                        >
                          {(['email', 'phone_number', 'nik']).map(item => (
                            <Select.Option
                              key={item}
                              value={item}
                            >
                              {(item.split('_').join(' ')).toUpperCase()}
                            </Select.Option>
                          ))}
                        </Select>,
                      )}
                    </Form.Item>
                    <Form.Item label={getFieldValue('key') !== 'nik' ? capitalize((getFieldValue('key') || '').split('_').join(' ')) : (getFieldValue('key') || '').split('_').join(' ').toUpperCase()}>
                      {getFieldDecorator('identifier', {
                        rules: Helper.fieldRules(['required'], 'Email / Phone Number / NIK'),
                      })(
                        <Input style={{ borderRadius: '10px' }} onChange={onChange} name="identifier" placeholder={!isEmpty(getFieldValue('key')) ? `Masukan ${getFieldValue('key') !== 'nik' ? capitalize((getFieldValue('key') || '').split('_').join(' ')) : (getFieldValue('key') || '').split('_').join(' ').toUpperCase()}` : 'Masukan Email / Phone Number / NIK'} />,
                      )}
                    </Form.Item>
                  </>
                )
              case 'reset':
                return (
                  <React.Fragment>
                    <Form.Item label="Password Baru Anda">
                      {getFieldDecorator((!qs.parse(location.search).is_secondary ? 'password' : 'alternative_password'), {
                        rules: [
                          ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'Password'),
                          { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' },
                          { pattern: /^.{0,50}$/, message: '*Maksimal 50 karakter' },
                        ],
                      })(
                        <Input.Password
                          onChange={onChange}
                          placeholder="Password"
                          iconRender={visible => (
                            visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                          )}
                        />,
                      )}
                    </Form.Item>
                    <Form.Item label="Konfirmasi Password">
                      {getFieldDecorator((!qs.parse(location.search).is_secondary ? 'password_confirmation' : 'alternative_password_confirmation'), {
                        rules: [
                          ...Helper.fieldRules(['required', 'oneUpperCase', 'oneLowerCase', 'oneNumber'], 'Konfirmasi Password'),
                          { pattern: /^.{8,}$/, message: '*Minimal 8 karakter' },
                          { pattern: /^.{0,50}$/, message: '*Maksimal 50 karakter' },
                        ],
                      })(
                        <Input.Password
                          onChange={onChange}
                          placeholder="Konfirmasi Password"
                          iconRender={visible => (
                            visible ? <img src="/assets/seen-icon.svg" alt="seen" /> : <img src="/assets/unseen-icon.svg" alt="unseen" />
                          )}
                        />,
                      )}
                    </Form.Item>
                  </React.Fragment>
                )
              default:
                return (
                  <React.Fragment>
                    <Form.Item label="Masukan Kode OTP">
                      {getFieldDecorator('otp_code', {
                        rules: Helper.fieldRules(['required'], 'otp_code'),
                      })(
                        <Input style={{ borderRadius: '10px' }} maxLength={6} name="otp_code" placeholder="Masukan Kode OTP" />,
                      )}
                    </Form.Item>
                  </React.Fragment>
                )
            }
          })()}

          {errorMessage && (
            <Alert
              message="Error"
              description={errorMessage}
              type="error"
              closable
              onClose={onCloseError}
              className="mb-3 text-left"
            />
          )}
          <Row>
            <Col>
              <div className="d-flex align-items-center justify-content-between mt-3">
                <Button block className="br-button" ghost type="primary" disabled={isAuthenticating} onClick={() => history.goBack()}>
                  Cancel
                </Button>
              </div>
            </Col>
            <Col>
              <div className="d-flex align-items-center justify-content-between mt-3">
                <Button block variant="primary" disabled={isAuthenticating} type="submit">
                  {isAuthenticating && <Icon type="loading" className="mr-2" />}
                  {(() => {
                    switch (isPassword) {
                      case 'forgot':
                        return 'Kirim Link'
                      default:
                        return 'Submit'
                    }
                  })()}
                </Button>
              </div>
            </Col>
          </Row>

          <div className="pb-3" />

          <Divider />
          <Row>
            <Col xl="8">
              <div className="text-left">
                Butuh bantuan?
                <a href="tel:+39708999" className="ml-1">(021) 39708999</a>
              </div>
            </Col>
            {(() => {
              switch (isPassword) {
                case 'otp':
                  return (
                    <Col xl="4">
                      <div className="text-right">
                        <Button
                          size="sm"
                          onClick={handleResendOTP}
                        >
                          Resend OTP
                        </Button>
                      </div>
                    </Col>
                  )
                default:
                  return ''
              }
            })()}
          </Row>
        </Form>
      </Col>
    </div>
  )
}

ForgotPassword.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCloseError: PropTypes.func,
  form: PropTypes.any,
  errorMessage: PropTypes.string,
  tokenExpired: PropTypes.string,
  isAuthenticating: PropTypes.bool,
  isPassword: PropTypes.string,
  handleResendOTP: PropTypes.func,
}

export default ForgotPassword

import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import {
  Icon, Form, Input,
  Divider, Alert,
} from 'antd'
import { Button, Col } from 'react-bootstrap'

const Login = (props) => {
  const {
    onChange,
    onSubmit,
    form,
    errorMessage,
    isAuthenticating,
    onCloseError,
  } = props

  return (
    <div className="form-container row m-0">
      <Col md={7} className="d-none d-md-block" />
      <Col>
        <Form className="form-signin" onSubmit={onSubmit}>
          {/* <img className="mb-4" src="http://www.clappingape.com/img/logo.svg" alt="" /> */}
          <h3>Silakan masuk ke akun Dashboard Mitraca Anda</h3>
          <Form.Item label="Email / Phone Number / NIK">
            <Input onChange={onChange} value={form.email || ''} name="email" type="text" placeholder="Email / Phone Number / NIK" />
          </Form.Item>
          <Form.Item label="Password">
            <Input.Password onChange={onChange} value={form.password || ''} name="password" type="password" placeholder="Password" />
          </Form.Item>

          {errorMessage && (
            <Alert
              message="Error"
              description={errorMessage}
              type="error"
              closable
              onClose={onCloseError}
              className="mb-3 text-left"
            />
          )}

          <div className="d-flex align-items-center justify-content-between mt-3">
            <Link to="/password/forgot" style={{ width: '50%', textAlign: 'left' }}>Forgot Password</Link>
            <Button block variant="primary" disabled={isAuthenticating} type="submit">
              {isAuthenticating && <Icon type="loading" className="mr-2" />}
              {'Login'}
            </Button>
          </div>

          <div className="pb-3" />

          <Divider />
        </Form>
      </Col>
    </div>
  )
}

Login.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onCloseError: PropTypes.func,
  form: PropTypes.any,
  errorMessage: PropTypes.string,
  isAuthenticating: PropTypes.bool,
}

export default Login

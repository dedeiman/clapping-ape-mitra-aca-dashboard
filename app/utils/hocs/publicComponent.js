import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import Browser from 'utils/Browser'
import Cookies from 'js-cookie'
import config from 'app/config'

export const mapStateToProps = (state) => {
  const {
    currentUser,
  } = state.root.auth

  return {
    currentUser,
  }
}

const publicComponent = (Component, redirect = false, redirectPath = '/dashboard') => compose(
  connect(
    mapStateToProps,
  ),
  lifecycle({
    componentDidMount() {
      if (!!Cookies.get(config.auth_cookie_name) && redirect) {
        Browser.setWindowHref(redirectPath)
      }
    },
  }),
)(Component)

export default publicComponent

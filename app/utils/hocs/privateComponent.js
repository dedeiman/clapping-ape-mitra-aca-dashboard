import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  compose,
  mapProps,
} from 'recompose'

import { omit } from 'lodash'
import { authenticateByToken } from 'actions/Auth'

export const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  authenticateByToken: bindActionCreators(authenticateByToken, dispatch),
})

const privateComponent = SecureComponent => compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  mapProps(
    props => omit(props, ['authenticateByToken']),
  ),
)(SecureComponent)

export default privateComponent

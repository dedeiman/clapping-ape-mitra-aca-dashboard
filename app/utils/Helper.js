import { message } from 'antd'
import Swal from 'sweetalert2'
import Browser from 'utils/Browser'
import { getAccessToken, removeToken } from 'actions/Auth'
import { mainPersistConfig } from 'store/configureStore'
import { purgeStoredState } from 'redux-persist'

export default class Helper {
  static getBase64(img, callback) {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }

  static sessionTimeout() {
    const accessToken = getAccessToken()

    if (accessToken) {
      setTimeout(() => {
        Swal.fire({
          title: 'Session expired',
          text: 'Your will get time out. You want stay?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#2b57b7',
          cancelButtonColor: '#6E7D88',
          confirmButtonText: 'Yes, Logout!',
        }).then((result) => {
          if (result.isConfirmed) {
            purgeStoredState(mainPersistConfig).then(() => {
              removeToken()
              Browser.setWindowHref('/')
            })
          }
        })
      }, 1000 * 60 * 60 * 24)
    }
  }

  static fieldRules(rules = [], name) {
    const formRule = []
    rules.forEach((item) => {
      if (item === 'email') formRule.push({ type: 'email', message: `*${name} tidak valid` })
      if (item === 'number') formRule.push({ type: 'number', transform: value => (value ? Number(value) : ''), message: `*${name} harus menggunakan angka` })
      if (item === 'positiveNumber') {
        formRule.push({
          type: 'number', min: 0, transform: value => (value ? Number(value) : ''), message: `*${name} harus menggunakan angka positif`,
        })
      }
      if (item === 'required') formRule.push({ required: true, message: `*${name} Wajib diisi` })
      if (item === 'requiredDropdown') formRule.push({ required: true, message: `${name} *Wajib dipilih` })
      if (item === 'oneUpperCase') formRule.push({ pattern: /(?=.*[A-Z])/, message: '*Wajib memiliki minimal 1 huruf besar' })
      if (item === 'oneLowerCase') formRule.push({ pattern: /(?=.*[a-z])/, message: '*Wajib memiliki minimal 1 huruf kecil' })
      if (item === 'oneNumber') formRule.push({ pattern: /(?=.*\d)/, message: '*Wajib memiliki minimal 1 angka' })
      if (item === 'notSpecial') formRule.push({ pattern: /^[a-zA-Z., ]+$/, message: '*Tidak boleh menggunakan angka dan spesial karakter' })
      if (item === 'alphabet') formRule.push({ pattern: /^[a-zA-Z]+$/, message: `*${name} hanya boleh menggunakan huruf` })
    })
    return formRule
  }

  static beforeUpload(file) {
    const isFileType = file.type === 'image/jpeg' || file.type === 'image/png'
    if (!isFileType) {
      message.error('You can only upload JPG/PNG file!')
    }

    return false
  }
}

import { connect } from 'react-redux'
import { isEmpty } from 'lodash'
import { bindActionCreators } from 'redux'
import { compose, withHandlers, lifecycle } from 'recompose'
import { authenticateByCredentials, authenticateUserFailure } from 'actions/Auth'
import withForms from 'utils/hocs/withForms'
import LoginView from 'components/login/Login'

export function mapStateToProps(state) {
  const {
    isAuthenticating,
    errorMessage,
  } = isEmpty(state.root.auth)
    ? { isAuthenticating: false, errorMessage: null }
    : state.root.auth

  return {
    isAuthenticating,
    errorMessage,
  }
}

const mapDispatchToProps = dispatch => ({
  loginWithCredentials: bindActionCreators(authenticateByCredentials, dispatch),
  authenticateUserFailure: bindActionCreators(authenticateUserFailure, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withForms(),
  withHandlers({
    onCloseError: props => () => {
      props.authenticateUserFailure('')
    },
    onSubmit: props => (event) => {
      event.preventDefault()
      props.loginWithCredentials(props.form)
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.authenticateUserFailure('')
    },
  }),
)(LoginView)

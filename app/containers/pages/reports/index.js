import { connect } from 'react-redux'
import {
  compose, lifecycle,
  withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchReportTraining } from 'actions/Report'
import { getTraining, getSubject, getBranches } from 'actions/Option'
import {
  isEmpty, capitalize,
  pickBy, identity,
} from 'lodash'
import { message } from 'antd'
import LearningQuizView from 'components/pages/reports'
import history from 'utils/history'
import moment from 'moment'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    isFetching,
    dataTraining,
    metaTraining,
    dataTrainingDetail,
    metaTrainingDetail,
  } = state.root.reports
  return {
    isFetching,
    dataTraining,
    metaTraining,
    dataTrainingDetail,
    metaTrainingDetail,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchReportTraining: bindActionCreators(fetchReportTraining, dispatch),
  getTraining: bindActionCreators(getTraining, dispatch),
  getSubject: bindActionCreators(getSubject, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('activeTab', 'setActiveTab', 'list'),
  withState('stateBranches', 'setStateBranches', {
    isFetching: true,
    options: [],
    selected: [],
  }),
  withState('stateSubject', 'setStateSubject', {
    isFetching: true,
    options: [],
    selected: [],
  }),
  withState('stateTraining', 'setStateTraining', {
    isFetching: true,
    options: [],
    selected: [],
  }),
  withState('stateTime', 'setStateTime', [null, null]),
  withHandlers({
    reloadPage: props => () => {
      const payload = qs.stringify(pickBy({
        subject_id: (props.stateSubject.selected).join(','),
        training_id: (props.stateTraining.selected).join(','),
        branch_organizer_id: (props.stateBranches.selected).join(','),
        from_date: props.stateTime[0] ? moment(props.stateTime[0]).format('YYYY-MM-DD') : '',
        to_date: props.stateTime[1] ? moment(props.stateTime[1]).format('YYYY-MM-DD') : '',
        page: '1',
      }, identity))
      props.fetchReportTraining(`?${payload}&is_detail=${props.activeTab !== 'list'}`, (props.activeTab !== 'list'))
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      const payload = qs.stringify(pickBy({
        subject_id: (props.stateSubject.selected).join(','),
        training_id: (props.stateTraining.selected).join(','),
        branch_organizer_id: (props.stateBranches.selected).join(','),
        from_date: props.stateTime[0] ? moment(props.stateTime[0]).format('YYYY-MM-DD') : '',
        to_date: props.stateTime[1] ? moment(props.stateTime[1]).format('YYYY-MM-DD') : '',
        page,
        per_page: 10,
      }, identity))
      props.fetchReportTraining(`?${payload}&is_detail=${props.activeTab !== 'list'}`, (props.activeTab !== 'list'))
    },
    handleTab: () => (key) => {
      history.push(`/reports/training/${key}`)
    },
    handleFilter: props => (value, type) => {
      if (type === 'Time') {
        props.setStateTime(value)
      } else {
        props[`setState${type}`]({
          ...props[`state${type}`],
          selected: value,
        })
      }
      setTimeout(() => {
        props.reloadPage()
      }, 300)
    },
    handleExport: props => (isPdf) => {
      if (!isEmpty(props.stateTime[0]) && !isEmpty(props.stateTime[1])) {
        const payload = qs.stringify(pickBy({
          format: isPdf ? 'pdf' : 'excel',
          subject_id: props.stateSubject.selected,
          training_id: props.stateTraining.selected,
          branch_organizer_id: props.stateBranches.selected,
          from_date: props.stateTime[0] ? moment(props.stateTime[0]).format('YYYY-MM-DD') : '',
          to_date: props.stateTime[1] ? moment(props.stateTime[1]).format('YYYY-MM-DD') : '',
          is_detail: props.activeTab !== 'list',
        }, identity))
        window.open(
          `${process.env.APP_CONFIG.api_url}/report/training-class/download?${payload}`,
          '_blank',
        )
      } else {
        message.warning('Select Period first to export data')
      }
    },
  }),
  lifecycle({
    componentDidMount() {
      const { type } = this.props.match.params
      const {
        stateTraining, stateSubject, stateBranches,
        setStateTraining, setStateSubject, setStateBranches,
      } = this.props
      this.props.updateSiteConfiguration('breadList', ['Home', `Report Training - ${capitalize(type)}`])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/training/list')
      this.props.setActiveTab(type)

      this.props.fetchReportTraining(`?is_detail=${type !== 'list'}`, (type !== 'list')).then((res) => {
        setStateTraining({
          ...stateTraining,
          options: res,
          isFetching: false,
        })
      })
      this.props.getSubject().then(res => (
        setStateSubject({
          ...stateSubject,
          options: res,
          isFetching: false,
        })
      ))
      this.props.getBranches().then(res => (
        setStateBranches({
          ...stateBranches,
          options: res,
          isFetching: false,
        })
      ))
    },
  }),
)(LearningQuizView)

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchRestrictedProduct } from 'actions/Reports/Administrator/RestrictedProduct'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import RestrictedProduct from 'components/pages/reports/Product'
import QueryString from 'query-string'
import config from 'config'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { message } from 'antd'

export function mapStateToProps(state) {
  const {
    restrictedProduct,
  } = state.root
  return {
    restrictedProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchRestrictedProduct, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    product: '',
    branch: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list } = props

      list({
        page: page.current,
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        product_id: state.product,
        branch_id: state.branch,
      })
    },
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        branch_id: state.branch,
        product_id: state.product,
        format: 'excel',
      })
      window.open(
        `${config.api_url}/report/agent-restricted-product/download?${payload}`,
        '_blank',
      )
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/products?purpose=select', name: 'product' },
        { url: '/branches', name: 'branch' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Report Product'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/restricted-product')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(RestrictedProduct)

/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
/* eslint-disable array-callback-return */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchContestWinner } from 'actions/Reports/Administrator/Contest'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import ContestWinner from 'components/pages/reports/Contest'
import config from 'app/config'
import { getDatas } from 'actions/Option'
import qs from 'query-string'

export function mapStateToProps(state) {
  const {
    contestWinner,
  } = state.root

  return {
    contestWinner,
  }
}

const mapDispatchToProps = dispatch => ({
  list: bindActionCreators(fetchContestWinner, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('optionYear', 'changeOptionYear', []),
  withState('optionContest', 'changeOptionContest', []),
  withState('state', 'changeState', {
    contest_id: '',
    year: '',
    per_page: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list, state, changeState } = props

      changeState({
        ...state,
        per_page: page.pageSize,
      })

      list({
        page: page.current,
        year: state.year,
        per_page: page.pageSize,
        contest_id: state.contest_id,
      })
    },
    handleReport: props => () => {
      const { state } = props

      const payload = qs.stringify({
        year: state.year,
        contest_id: state.contest_id,
        per_page: state.per_page,
        format: 'excel',
      })

      window.open(
        `${config.api_url_content}/report/contest-winners/download?${payload}`,
        '_blank',
      )
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        contest_id: state.contest_id,
        per_page: state.per_page,
        year: state.year,
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const { changeOptionYear, changeOptionContest, list } = this.props

      this.props.getDatas(
        { base: 'apiContent', url: '/report/contest-winners-year', method: 'get' },
      ).then((res) => {
        changeOptionYear(res.data)
      })

      const dataContest = []
      this.props.getDatas(
        { base: 'apiUser', url: '/contests', method: 'get' },
      ).then((res) => {
        [...new Set(res.data.map((item) => {
          dataContest.push({
            id: item.id,
            name: item.name,
          })

          changeOptionContest(dataContest)
        }))]
        this.props.updateSiteConfiguration('breadList', ['Home', 'Report Contest Winners'])
        this.props.updateSiteConfiguration('activeSubPage', 'reports')
        this.props.updateSiteConfiguration('activePage', 'reports/contest-winner')
      })

      list()
    },
  }),
)(ContestWinner)

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchAgentList } from 'actions/Reports/Administrator/AgentList'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import AgentList from 'components/pages/reports/Administrator/AgentList'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import QueryString from 'query-string'
import { message } from 'antd'
import moment from 'moment'
import { isEmpty } from 'lodash'

export function mapStateToProps(state) {
  const {
    agentList,
  } = state.root

  return {
    agentList,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchAgentList, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('optionYear', 'changeOptionYear', []),
  withState('submitted', 'setSubmitted', false),
  withState('state', 'changeState', {
    search: '',
    branch: '',
    agent_type: '',
    level: '',
    status: '',
    mob: '',
    religion_id: '',
    join_from_date: '',
    join_to_date: '',
    license_type: '',
    license_from_date: '',
    license_to_date: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list } = props
      list({
        page: page.current,
        per_page: page.pageSize,
      })
    },
    handleReport: props => () => {
      const { state } = props
      props.setSubmitted(true)
      const payload = QueryString.stringify({
        
        format: 'excel',
        search: state.search,
        branch_id: state.branch,
        agent_type: state.agent_type,
        level_id: state.level,
        status: state.status,
        mob: state.mob,
        religion_id: state.religion,
        join_from_date: state.join_from_date,
        join_to_date: state.join_to_date,
        license_type: state.license_type,
        license_from_date: state.license_from_date,
        license_to_date: state.license_to_date,
      })

      props.getData(
        { base: 'apiUser', url: `/reports/agents/download?${payload}`, method: 'get' },
      ).then((res) => {
        props.setSubmitted(false)
        window.open(
          res.data.file_url,
          '_blank',
        )
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        search: state.search,
        branch_id: state.branch,
        agent_type: state.agent_type,
        level_id: state.level,
        status: state.status,
        mob: state.mob,
        religion_id: state.religion,
        join_from_date: state.join_from_date,
        join_to_date: state.join_to_date,
        license_type: state.license_type,
        license_from_date: state.license_from_date,
        license_to_date: state.license_to_date,
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/branches', name: 'branch' },
        { url: '/search-types', name: 'agent_type' },
        { url: '/levels', name: 'level' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('activePage', 'reports/agent-list')
      this.props.updateSiteConfiguration('breadList', ['Home', 'Report Agent List'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(AgentList)

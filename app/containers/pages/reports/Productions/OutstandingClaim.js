import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchOutstandingClaim } from 'actions/Reports/Productions/OutstandingClaim'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import OutstandingClaim from 'components/pages/reports/Productions/OutstandingClaim'
import QueryString from 'query-string'
import config from 'config'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import moment from 'moment'

export function mapStateToProps(state) {
  const {
    outstandingClaimReport,
    auth,
  } = state.root
  return {
    auth,
    outstandingClaimReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchOutstandingClaim, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('state', 'changeState', {
    cob_id: '',
    start_date: moment().add(-1, 'day').format('YYYY-MM-DD'),
    start_date: moment().add(-1, 'day').format('YYYY-MM-DD'),
    keyword: '',
    branch: '',
    agent_representative: '',
    product: '',
    policy_no: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list, state } = props

      list({
        page: page.current,
        per_page: page.pageSize,
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        cob_id: state.cob,
        start_date: state.start_date,
        start_date: state.start_date,
        agent_keyword: state.keyword,
        branch_id: state.branch,
        branch_perwakilan_id: state.agent_representative,
        product_id: state.product,
        policy_no: state.policy_no,
      })
    },
    handleReport: props => (params) => {
      const { state } = props

      const payload = QueryString.stringify({
        cob_id: state.cob,
        cob_id: state.cob,
        start_date: state.start_date,
        start_date: state.start_date,
        product_id: state.product,
        policy_no: state.policy_no,
        format: params,
      })

      props.getData(
        { base: 'apiUser', url: `/report/insurance-letters/outstanding-claim/export?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
    handleRepresentative: props => (id) => {
      if (id !== '') {
        props.getData(
          { base: 'apiUser', url: `/branch-perwakilan/${id}`, method: 'get' },
        ).then((res) => {
          props.changeState({
            ...props.state,
            agentRepresentLoad: false,
            agentRepresentList: res.data,
            branch: id,
          })
        }).catch((err) => {
          message.error(err)
          props.changeState({
            ...props.state,
            agentRepresentLoad: false,
            agentRepresentList: [],
            branch: id,
          })
        })
      } else {
        props.changeState({
          ...props.state,
          branch: id,
        })
      }
    },
    handleProduct: props => (id) => {
      props.getData(
        { base: 'apiUser', url: `/product/${id}`, method: 'get' },
      ).then((res) => {
        props.changeState({
          ...props.state,
          product: '',
          productLoad: false,
          productList: res.data,
          cob: id,
        })
      }).catch((err) => {
        message.error(err)
        props.changeState({
          ...props.state,
          product: '',
          productLoad: false,
          productList: [],
          cob: id,
        })
      })
    },
  }),
  
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/branches', name: 'branch' },
        { url: '/class-of-business', name: 'cob' },
        { url: '/agent-change-reasons', name: 'agentStatus' },
      ]
     
      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Produksi', 'Outstanding Claim'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/outstanding-claim')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(OutstandingClaim)

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchSPPAReport } from 'actions/Reports/Productions/SPPA'
import {
  compose, lifecycle, withState, withHandlers,
} from 'recompose'
import SPPAReport from 'components/pages/reports/Productions/SPPA'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import QueryString from 'query-string'
import { message } from 'antd'

export function mapStateToProps(state) {
  const {
    sppaReport,
  } = state.root

  return {
    sppaReport,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  list: bindActionCreators(fetchSPPAReport, dispatch),
  getData: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('optionYear', 'changeOptionYear', []),
  withState('state', 'changeState', {
    branch: '',
    keyword: '',
    agent_representative: '',
    join_from_date: '',
    join_to_date: '',
    format: '',
    product: '',
    cob: '',
  }),
  withHandlers({
    handleTableChange: props => (page) => {
      const { list } = props
      list({
        page: page.current,
        per_page: page.pageSize,
      })
    },
    handleReport: props => () => {
      const { state } = props

      const payload = QueryString.stringify({
        format: 'excel',
        from_date: state.join_from_date,
        to_date: state.join_to_date,
        status: state.status,
        product_id: state.product,
        cob_id: state.cob,
      })

      props.getData(
        { base: 'apiUser', url: `/report/insurance-letters/export?${payload}`, method: 'get' },
      ).then((res) => {
        window.open(
          res.data.file_url,
          '_blank',
        )
      }).catch((err) => {
        message.error(err.message)
      })
    },
    handleSearch: props => (e) => {
      e.preventDefault()
      const { list, state } = props

      list({
        page: 1,
        format: '',
        from_date: state.join_from_date,
        to_date: state.join_to_date,
        agent_keyword: state.keyword,
        branch_id: state.branch,
        branch_representative: state.agent_representative,
        product_id: state.product,
        cob_id: state.cob,
      })
    },
    handleRepresentative: props => (id) => {
      props.getData(
        { base: 'apiUser', url: `/branch-perwakilan/${id}`, method: 'get' },
      ).then((res) => {
        props.changeState({
          ...props.state,
          agentRepresentLoad: false,
          agentRepresentList: res.data,
          branch: id,
        })
      }).catch((err) => {
        message.error(err)
        props.changeState({
          ...props.state,
          agentRepresentLoad: false,
          agentRepresentList: [],
          branch: id,
        })
      })
    },
  }),
  lifecycle({
    componentDidMount() {
      const configGetData = [
        { url: '/branches', name: 'branch' },
        { url: '/products', name: 'product' },
        { url: '/class-of-business', name: 'cob' },
      ]

      configGetData.map(item => (
        this.loadMasterData(item.url, item.name)
      ))

      const { list } = this.props

      this.props.updateSiteConfiguration('breadList', ['Home', 'Produksi', 'Report SPPA'])
      this.props.updateSiteConfiguration('activeSubPage', 'reports')
      this.props.updateSiteConfiguration('activePage', 'reports/sppa')
      list()
    },
    loadMasterData(url, name) {
      this.props.getData(
        { base: 'apiUser', url, method: 'get' },
      ).then((res) => {
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.changeState({
          ...this.props.state,
          [`${name}Load`]: false,
          [`${name}List`]: [],
        })
      })
    },
  }),
)(SPPAReport)

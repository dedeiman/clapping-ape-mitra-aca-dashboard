import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import FinanceCommission from 'components/pages/reports/Finance/FinanceCommission'
import { fetchReportCommissionList, fetchDownloadReportCommission } from 'actions/Reports/Finance/FinanceCommission'
import { getBranches, getRepresentativeBranch, getProducts, getAgentStatus, getCOB } from 'actions/Option'
import { compose, lifecycle, withState, withHandlers } from 'recompose'
import qs from 'query-string'
import moment from 'moment'
import { pickBy, identity, first, last } from 'lodash'

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchReportCommissionList: bindActionCreators(fetchReportCommissionList, dispatch),
  fetchDownloadReportCommission: bindActionCreators(fetchDownloadReportCommission, dispatch),
  getBranches: bindActionCreators(getBranches, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
  getAgentStatus: bindActionCreators(getAgentStatus, dispatch),
  getRepresentativeBranch: bindActionCreators(getRepresentativeBranch, dispatch),
  getProducts: bindActionCreators(getProducts, dispatch)
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withState('dataList', 'setDataList', {}),
  withState('branchList', 'setBranchList', []),
  withState('cobList', 'setCobList', []),
  withState('statusList', 'setStatusList', []),
  withState('productList', 'setProductList', []),
  withState('representativeBranchList', 'setRepresentativeBranchList', []),
  withState('isLoading', 'setIsLoading', true),
  withState('isDetail', 'setDetail', false),
  withState('state', 'changeState', {
    payment_start_date: '',
    payment_end_date: '',
    settle_start_date: '',
    settle_end_date: '',
    agent_status: '',
    cob_id: '',
    product_id: '',
    branch_id: '',
    branch_perwakilan_id: '',
    sppa_policy_no: '',
    agent_keyword: '',
    is_detail: false,
    format: 'excel',
    page: '1',
    per_page: '10'
  }),
  withHandlers({
    getList: props => async (params) => {
      const { fetchReportCommissionList, setDataList, setIsLoading } = props
      setIsLoading(true)
      try {
        const res = await fetchReportCommissionList(params)
        setDataList(res)
        setIsLoading(false)
      } catch (err) {
        setDataList()
        setIsLoading(false)
      }
    },
    handleGetBranches: props => async (params) => {
      const { setBranchList, getBranches, setIsLoading } = props
      try {
        const res = await getBranches(params)
        setBranchList(res)
        setIsLoading(false)
      } catch (err) {
        setBranchList([])
        setIsLoading(false)
      }
    },
    handleGetCob: props => async (params) => {
      const { setCobList, getCOB, setIsLoading } = props
      try {
        const res = await getCOB(params)
        setCobList(res)
        setIsLoading(false)
      } catch (err) {
        setCobList([])
        setIsLoading(false)
      }
    },
    handleGetStatus: props => async (params) => {
      const { setStatusList, getAgentStatus, setIsLoading } = props
      try {
        const res = await getAgentStatus(params)
        setStatusList(res)
        setIsLoading(false)
      } catch (err) {
        setStatusList([])
        setIsLoading(false)
      }
    },
    handleGetProduct: props => async (params) => {
      const { setProductList, getProducts, setIsLoading } = props
      try {
        const res = await getProducts(params)
        setProductList(res)
        setIsLoading(false)
      } catch (err) {
        setProductList([])
        setIsLoading(false)
      }
    }
  }),
  withHandlers({
    handleFilter: props => async (value, field) => {
      const { changeState, state, getRepresentativeBranch, setRepresentativeBranchList } = props
      if (field.includes('Date')) {
        if (field === 'paymentDate') {
          changeState({
            ...props.state,
            payment_start_date: moment(value[0]).format('YYYY-MM-DD'),
            payment_end_date: moment(value[1]).format('YYYY-MM-DD')
          })
        } else {
          changeState({
            ...props.state,
            settle_start_date: moment(first(value)).format('YYYY-MM-DD'),
            settle_end_date: moment(last(value)).format('YYYY-MM-DD')
          })
        }
      } else {
        if (field === 'branch_id') {
          try {
            const res = await getRepresentativeBranch(value)
            setRepresentativeBranchList(res)
          } catch {
            setRepresentativeBranchList()
          }
        }

        changeState({
          ...props.state,
          [field]: value
        })
      }
    },
    handleDetail: props => (value, field) => {
      const { changeState, state, getList, setDetail } = props
      const payload = {
        ...state,
        is_detail: value
      }
      getList(`?${qs.stringify(pickBy(payload, identity))}`)
      setDetail(value)
      changeState({
        ...state,
        is_detail: value
      })
    },
    handleSubmitFilter: props => (params) => {
      params.preventDefault()
      const { getList, state, setIsLoading } = props

      setIsLoading(true)
      getList(`?${qs.stringify(pickBy(state, identity))}`)
    },
    handlePage: props => (dataPage) => {
      const { getList, state } = props
      const payload = {
        ...state,
        page: dataPage
      }
      getList(`?${qs.stringify(pickBy(payload, identity))}`)
    },
    handleDownload: props => async (event, field) => {
      const { fetchDownloadReportCommission, state, setIsLoading } = props
      const payload = { ...state, format: field}
      setIsLoading(true)

      try {
        const res = await fetchDownloadReportCommission(`?${qs.stringify(pickBy(payload, identity))}`)
        let anchor = document.createElement('a')
        anchor.href = res.data.file_url
        anchor.target = '_blank'
        anchor.click()
        setIsLoading(false)
      } catch (err) {
        setIsLoading(false)
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { updateSiteConfiguration, getList, state, handleGetBranches, handleGetCob, handleGetStatus, handleGetProduct } = this.props

      getList(`?${qs.stringify(pickBy(state, identity))}`)
      handleGetBranches()
      handleGetCob()
      handleGetStatus()
      handleGetProduct()
      updateSiteConfiguration('breadList', ['Home', 'Finance', 'Komisi Finance'])
      updateSiteConfiguration('activeSubPage', 'reports')
      updateSiteConfiguration('activePage', 'reports/finance-commission')
    },
  }),
)(FinanceCommission)

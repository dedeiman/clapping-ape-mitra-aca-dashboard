import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchUser, deleteUser, userFailure } from 'actions/User'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import qs from 'query-string'
import history from 'utils/history'
import UserView from 'components/pages/users'
import Swal from 'sweetalert2'
import { isEmpty } from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataUser,
    metaUser,
  } = state.root.user

  return {
    isFetching,
    errorMessage,
    dataUser,
    metaUser,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchUser: bindActionCreators(fetchUser, dispatch),
  deleteUser: bindActionCreators(deleteUser, dispatch),
  userFailure: bindActionCreators(userFailure, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('keyword', 'setKeyword', ''),
  withState('stateFilter', 'setFilter', {
    type: 'name',
    prefix: '+62',
    list: [],
  }),
  withState('currentPage', 'setCurrentPage', 1),
  withState('type', 'setType', ''),
  withHandlers({
    handleFilter: props => () => {
      const {
        type, countries, prefix,
      } = props.stateFilter
      const valPrefix = type === 'phone_number' ? countries.filter(item => item.phone_code === prefix)[0].id : ''
      const payload = {
        keyword: props.keyword,
        page: props.currentPage,
        type,
        phone_code_id: `${type === 'phone_number' ? valPrefix : ''}`,
      }
      history.push(`/users?${qs.stringify(payload)}`)
    },
  }),
  withHandlers({
    handleSearch: props => (value) => {
      const { type, prefix, countries } = props.stateFilter
      props.setKeyword(value)
      const valPrefix = type === 'phone_number' ? countries.filter(item => item.phone_code === prefix)[0].id : ''
      setTimeout(() => {
        history.push(`/users?type=${type}${value ? `&keyword=${value || ''}` : ''}${type === 'phone_number' ? `&phone_code_id=${valPrefix}` : ''}`)
      }, 300)
    },
    handlePage: props => (page) => {
      props.setCurrentPage(page)
      setTimeout(() => {
        props.handleFilter()
      }, 300)
    },
    handleDelete: props => (id) => {
      props.deleteUser(id).then(() => {
        Swal.fire({
          icon: 'success',
          title: 'User has been deleted',
          showConfirmButton: false,
          timer: 1500,
        })
      })
    },
    closeError: props => () => props.userFailure(''),
  }),
  lifecycle({
    componentDidMount() {
      const {
        setKeyword, setCurrentPage, location,
        setFilter, stateFilter,
      } = this.props
      const search = qs.parse(location.search) || {}
      setCurrentPage(Number(search.page))
      setKeyword(search.keyword)
      this.props.updateSiteConfiguration('breadList', ['Home', 'User'])
      this.props.updateSiteConfiguration('activePage', 'users')
      this.props.fetchUser(location.search)
      this.props.getDatas(
        { base: 'apiUser', url: '/phone-country-codes', method: 'get' },
      ).then((res) => {
        const prefix = res.data.filter(item => item.id === search.phone_code_id)
        setFilter({
          ...stateFilter,
          type: search.type || 'name',
          prefix: !isEmpty(prefix) ? prefix[0].phone_code : '',
          countries: res.data,
        })
      }).catch((err) => {
        message.error(err.message)
        setFilter({
          ...stateFilter,
          type: search.type || 'name',
          prefix: '+62',
          countries: [],
        })
      })
    },
  }),
)(UserView)

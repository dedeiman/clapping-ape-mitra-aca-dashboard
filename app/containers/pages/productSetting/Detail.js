import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose,
  withState,
  withHandlers,
  lifecycle,
} from 'recompose'
import config from 'config'
import { fetchDetailContest, deleteContest, cancelWinner } from 'actions/Contest'
import { fetchDetailCustomer, deleteCustomer } from 'actions/ProductSetting'
import DetailCustomer from 'components/pages/customers/Detail'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import Swal from 'sweetalert2'
import { message } from 'antd'
import { capitalize, isEmpty } from 'lodash'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  const {
    isFetching,
    detailCustomer,
  } = state.root.customer

  return {
    isFetching,
    detailCustomer,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDetailContest: bindActionCreators(fetchDetailContest, dispatch),
  deleteContest: bindActionCreators(deleteContest, dispatch),
  fetchDetailCustomer: bindActionCreators(fetchDetailCustomer, dispatch),
  deleteCustomer: bindActionCreators(deleteCustomer, dispatch),
  cancelWinner: bindActionCreators(cancelWinner, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('isBlocking', 'setBlocking', false),
  withState('stateButton', 'setStateButton', {
    list: ['Policy', 'Product', 'Agent', 'Document', 'History'],
    active: 'Policy',
    policy: [],
    'restricted-products': [],
    agents: [],
    documents: [],
    histories: [],
    policiesLoaded: false,
    productsLoaded: false,
    agentsLoaded: false,
    documentsLoaded: false,
    historiesLoaded: false,
    isFetching: false,
  }),
  withState('stateModalStatus', 'setStateModalStatus', props => ({
    visible: false,
    data: props.detailCustomer,
  })),
  withState('stateModalAgent', 'setStateModalAgent', {
    visible: false,
  }),
  withState('stateCard', 'setStateCard', {
    visible: false,
    title: '',
    data: {},
  }),
  withHandlers({
    handleBlockNavigation: props => (nextLocation) => {
      if (props.isBlocking) {
        Swal.fire({
          title: 'Are you sure to leave this page?',
          icon: 'warning',
          showCancelButton: true,
        }).then((res) => {
          if (res.value) {
            props.setBlocking(false)
            return history.push(nextLocation.pathname)
          }
          return props.updateSiteConfiguration('activePage', 'agents')
        })
        return false
      }
      return true
    },
    handleCancelCard: props => () => {
      props.setStateButton({
        ...props.stateButton,
        isFetching: true,
      })

      Swal.fire({
        title: 'Anda yakin ingin membatalkan perubahan?',
        text: 'Semua data yang anda perbarui akan terhapus',
        icon: 'warning',
        showCancelButton: true,
      }).then((res) => {
        if (res.value) {
          setTimeout(() => {
            const productExist = props.stateButton['restricted-products'].filter(product => !isEmpty(product.id))
            const documentExist = props.stateButton.documents.filter(document => !isEmpty(document.id))

            props.setStateButton({
              ...props.stateButton,
              'restricted-products': productExist,
              documents: documentExist,
              isFetching: false,
            })
          }, 300)
        } else {
          setTimeout(() => {
            props.setStateButton({
              ...props.stateButton,
              isFetching: false,
            })
          }, 300)
        }
      })
      return false
    },
    handleDetail: props => (type) => {
      const { stateButton, setStateButton, match } = props
      let url = `${type.toLowerCase()}s`
      if (type === 'Policy') url = 'policy'
      if (type === 'Product') url = 'restricted-products'
      if (type === 'History') url = 'histories'

      if (!stateButton[`${url}Loaded`]) {
        setStateButton({
          ...stateButton,
          isFetching: true,
        })
        props.getDatas(
          { base: 'apiUser', url: `/customers/${match.params.id}/${url}`, method: 'get' },
        ).then((res) => {
          setStateButton({
            ...stateButton,
            active: type,
            isFetching: false,
            [url]: url === 'level-histories' ? res.data.histories : res.data,
            [`${url}Loaded`]: true,
          })
        }).catch(() => {
          setStateButton({
            ...stateButton,
            active: type,
            isFetching: false,
            [url]: [],
            [`${url}Loaded`]: true,
          })
        })
      }
    },
    handleUpdateCard: props => () => {
      const {
        'restricted-products': products, documents, agents,
      } = props.stateButton

      const payload = {
        products: (products || []).map(item => ({
          id: item.id || null,
          product_id: item.product ? item.product.id : '',
          restricted_date: item.restricted_date,
          unrestricted_date: item.unrestricted_date,
          reason: item.reason,
          status: item.status,
        })),
        agents: (agents || []).map(item => ({
          id: item.id || null,
          agent_id: item.agent_id,
          status: item.status,
        })),
        documents: (documents || []).map(item => ({
          id: item.id || null,
          document_category_id: item.document_category_id,
          description: item.description,
          file: (item.file_url).includes('https://aca-web.s3.ap-southeast-1.amazonaws.com/') ? null : item.file_url,
        })),
      }

      props.setBlocking(true)
      props.setStateButton({ ...props.stateButton, isFetching: true })

      props.getDatas(
        { base: 'apiUser', url: `/customers/${props.match.params.id}/card`, method: 'put' },
        payload,
      ).then((res) => {
        Swal.fire(
          'Data Berhasil Diubah!',
          '',
          'success',
        ).then(() => {
          props.setBlocking(false)
          props.setStateButton({
            ...props.stateButton,
            isFetching: false,
            'restricted-products': res.data.products,
            agents: res.data.agents,
            documents: res.data.documents,
          })
        })
      }).catch((error) => {
        props.setBlocking(false)
        props.setStateButton({ ...props.stateButton, isFetching: false })
        message.error(error.message)
      })
    },
    donwloadDocument: props => () => {
      window.open(
        `${config.api_url}/customers/${props.match.params.id}/documents/download`,
        '_blank',
      )
    },
    handleDelete: props => (id) => {
      props.setBlocking(true)
      props.setStateButton({ ...props.stateButton, isFetching: true })
      props.getDatas(
        { base: 'apiUser', url: `/customers/${id}/documents`, method: 'delete' },
      ).then((res) => {
        Swal.fire(
          capitalize(res.meta.message),
          '',
          'success',
        ).then(() => {
          props.setBlocking(false)
          props.setStateButton({ ...props.stateButton, isFetching: false })
        })
      }).catch((err) => {
        props.setBlocking(false)
        props.setStateButton({ ...props.stateButton, isFetching: false })
        message.error(err.message)
      })
    },
    collectDataCard: props => (data, title, isEdit) => {
      const {
        stateButton, setStateButton,
        stateCard, setStateCard,
      } = props
      let currentState = ''
      if (title.toLowerCase() === 'product') currentState = 'restricted-products'
      if (title.toLowerCase() === 'contract') currentState = 'contracts'
      if (title.toLowerCase() === 'license') currentState = 'licenses'
      if (title.toLowerCase() === 'taxation') currentState = 'taxation'
      if (title.toLowerCase() === 'document') currentState = 'documents'
      if (title.toLowerCase() === 'bank') currentState = 'bank_accounts'

      const newState = stateButton[currentState]

      if (isEdit) {
        const newData = {
          ...((stateButton[currentState] || []).find(item => item.id === data.id) || {}),
          ...data,
        }
        const arrKey = (stateButton[currentState] || []).findIndex(item => item.id === data.id)
        newState[arrKey] = newData
        setStateButton({ ...stateButton, [currentState]: newState })
      } else {
        newState.push(data)
        setStateButton({ ...stateButton, [currentState]: newState })
      }

      setStateCard({
        ...stateCard, visible: false, title: '', data: {},
      })

      props.setBlocking(false)
    },
  }),
  lifecycle({
    componentDidMount() {
      const { match } = this.props
      this.props.fetchDetailCustomer(match.params.id)
      this.props.handleDetail('Policy')
    },
  }),
)(DetailCustomer)

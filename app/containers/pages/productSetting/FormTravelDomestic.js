import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  productDetail, fetchDetailProduct, createProduct, updateProduct,
} from 'actions/Product'
import { getCOB, getDatas } from 'actions/Option'
import { message } from 'antd'
import FormTravelDomestic from 'components/pages/productSetting/FormTravelDomestic'
import history from 'utils/history'
import _ from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    detailProduct,
  } = state.root.products

  return {
    isFetching,
    detailProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createProduct: bindActionCreators(createProduct, dispatch),
  updateProduct: bindActionCreators(updateProduct, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  productDetail: bindActionCreators(productDetail, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
})
const dataSelect = [
  { id: '9ec8a103-aef9-4193-bebf-2883d3be0fee', label: 'BCA' },
]

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateLiability', 'setStateLiability', {
    load: true,
    list: [],
    search: '',
    filter: '',
  }),
  withState('isEdit', 'setIsEdit', false),
  withState('errorsField', 'setErrorsField', {}),
  withState('loadCOB', 'setLoadCOB', true),
  withState('selectedRows', 'setSelectedRows', []),
  withState('metaCountries', 'setMetaCountries', null),
  withState('keyword', 'setKeyword', ''),
  withState('getCountries', 'setCountries', {
    list: [],
    isLoadCountries: false,
  }),
  withState('selectOption', 'setSelectOption', dataSelect),
  withHandlers({
    handleSubmit: props => (values) => {
      const dataTravelDomestic = Object.assign({}, props.stateLiability.list.travel_domestic)
      const ub1 = Object.assign({}, dataTravelDomestic['1-4'])
      const ub2 = Object.assign({}, dataTravelDomestic['5-11'])
      const ub3 = Object.assign({}, dataTravelDomestic['12-20'])
      const ub4 = Object.assign({}, dataTravelDomestic['21-31'])
      const ub5 = Object.assign({}, dataTravelDomestic.additional_week)
      props.getDatas({ base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'put' }, {
        product_type: 'travel-domestic',
        rates_data: {
          travel_domestic_settings: [
            {
              id: Object.assign({}, ub1[0]).id || null,
              nusantara_1_amount: Number(values.bu11) || 0,
              nusantara_2_amount: Number(values.bu12) || 0,
            },
            {
              id: Object.assign({}, ub2[0]).id || null,
              nusantara_1_amount: Number(values.bu21) || 0,
              nusantara_2_amount: Number(values.bu22) || 0,
            },
            {
              id: Object.assign({}, ub3[0]).id || null,
              nusantara_1_amount: Number(values.bu31) || 0,
              nusantara_2_amount: Number(values.bu32) || 0,
            },
            {
              id: Object.assign({}, ub4[0]).id || null,
              nusantara_1_amount: Number(values.bu41) || 0,
              nusantara_2_amount: Number(values.bu42) || 0,
            },
            {
              id: Object.assign({}, ub5[0]).id || null,
              nusantara_1_amount: Number(values.bu51) || 0,
              nusantara_2_amount: Number(values.bu52) || 0,
            },
          ],
        },
      }).then(() => {
        message.success('Product Setting Travel Domestic has been updated')
        history.push('/product-setting/travel-domestic')
      }).catch((err) => {
        let objError = {}
        Object.keys(err.errors).forEach((item) => {
          objError = {
            ...objError,
            [item]: {
              validateStatus: 'error',
              help: _.startCase((err.errors[item]).replace('_id', '').replace('_', ' ')),
            },
          }
          return props.setErrorsField(objError)
        })
      })
    },
    handleGetCountries: props => (page, key) => {
      props.setCountries({
        ...props.getCountries,
        isLoadCountries: true,
      })

      props.getDatas(
        { base: 'apiUser', url: `/countries/list?page=${page}&limit=10&keyword=${key}`, method: 'get' },
      ).then((res) => {
        props.setCountries({
          ...props.getCountries,
          list: res.data,
          isLoadCountries: false,
        })

        props.setMetaCountries({
          ...props.metaCountries,
          total_count: res.meta.total_count,
        })

        if (props.match.params.id) {
          const countriesKey = res.data.filter(item => item.is_schengen_area)

          props.setSelectedRows(countriesKey.map(country => country.id))
        }
      })
    },
    handleChecked: props => (selectedRowKeys) => {
      props.setSelectedRows(selectedRowKeys)
    },
    goToDetailCountry: props => (dataCountry) => {
      if (props.match.params.id) {
        history.push(`/detail-country/${dataCountry.id}/edit`)
      }
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      props.handleGetCountries(page, props.keyword || '')
    },
    handleFilter: props => (key) => {
      props.handleGetCountries(1, key)
      props.setKeyword(key)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        match, setStateLiability,
        stateLiability, setLoadCOB,
      } = this.props
      this.props.updateSiteConfiguration('breadList', ['Home', 'Product Setting'])
      this.props.updateSiteConfiguration('activePage', 'product-setting')
      this.props.getDatas(
        { base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'get' },
      ).then((res) => {
        this.props.setStateLiability({
          ...this.props.stateLiability,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateLiability({})
      })
    },
  }),
)(FormTravelDomestic)

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {pickBy, identity, filter} from 'lodash'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import OtomateSetting from 'components/pages/productSetting/Otomate'
import { fetchPs } from 'actions/ProductSetting'
import { getDatas } from 'actions/Option'
import qs from 'query-string'
import { Form, message } from 'antd'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role
  const { currentUser } = state.root.auth

  const {
    isFetching,
    dataCustomer,
    metaCustomer,
  } = state.root.auth

  return {
    isFetching,
    dataCustomer,
    metaCustomer,
    groupRole,
    currentUser,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  fetchPs: bindActionCreators(fetchPs, dispatch),
})
export default Form.create({ name: 'liabilityForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateLiability', 'setStateLiability', {
      load: true,
      list: [],
      search: '',
      filter: '',
    }),
    withHandlers({
      handleFilter: props => (val) => {
        const { stateCustomer, setStateCustomer } = props
        setStateCustomer({
          ...stateCustomer,
          filter: val,
        })
      },
      handlePage: props => (value) => {
        const { stateCustomer, setStateCustomer, loadCustomer } = props
        setStateCustomer({
          ...stateCustomer,
          page: value,
        })
        setTimeout(() => {
          loadCustomer()
        }, 300)
      },
      updatePerPage: props => (current, value) => {
        const { stateCustomer, setStateCustomer } = props
        setStateCustomer({
          ...stateCustomer,
          perPage: value,
        })
        setTimeout(() => {
          const payload = {
            per_page: value,
            customer_status: stateCustomer.search ? stateCustomer.filter : '',
            search: stateCustomer.search,
          }
          props.fetchPs(`?${qs.stringify(pickBy(payload, identity))}`)
        }, 300)
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.getDatas(
          { base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'get' },
        ).then((res) => {
          this.props.setStateLiability({
            ...this.props.stateLiability,
            loading: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateLiability({})
        })
      },
    }),
  )(OtomateSetting),
)

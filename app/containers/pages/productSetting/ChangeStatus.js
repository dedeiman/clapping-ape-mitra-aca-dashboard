import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
  withPropsOnChange,
} from 'recompose'
import { message } from 'antd'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import { debounce, isEmpty } from 'lodash'
import moment from 'moment'
import Swal from 'sweetalert2'
import ChangeStatusForm from 'components/pages/customers/ChangeStatus'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    detailCustomer,
  } = state.root.customer

  return {
    detailCustomer,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formChangeStatus' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateForm', 'setStateForm', {
      statusLoad: false,
      statusList: [],
    }),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withHandlers({
      handleSearchAgent: props => (val) => {
        props.getDatas(
          { base: 'apiUser', url: `/agents?search_type=agent_id&search=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateForm({
            ...props.stateForm,
            agentLoad: false,
            agentList: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setStateForm({
            ...props.stateForm,
            agentLoad: false,
            agentList: [],
          })
        })
      },
      getDownline: props => (id) => {
        props.getDatas(
          { base: 'apiUser', url: `/agents/${id}/downlines`, method: 'get' },
        ).then((res) => {
          if (isEmpty(res.data)) {
            props.setStateForm({
              ...props.stateForm,
              downlineLoad: false,
              downlineList: res.data,
            })
          } else {
            props.getDatas(
              { base: 'apiUser', url: '/agent-change-status-actions', method: 'get' },
            ).then((result) => {
              props.setStateForm({
                ...props.stateForm,
                downlineLoad: false,
                downlineList: res.data,
                downlineStatusList: result.data,
              })
            })
          }
        }).catch((err) => {
          message.error(err)
          props.setStateForm({
            ...props.stateForm,
            downlineLoad: false,
            downlineList: [],
          })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            const { statusList } = props.stateForm
            props.setLoadSubmitBtn(true)

            const payload = {
              customer_id: props.id,
              reason_id: (statusList || []).find(item => item.slug === values.status_active).id,
              effective_date: values.effective_date ? moment(values.effective_date).format('YYYY-MM-DD') : '',
            }

            props.getDatas(
              { base: 'apiUser', url: '/customer-change-status', method: 'put' },
              payload,
            ).then(() => {
              props.setLoadSubmitBtn(false)
              Swal.fire('Status has been successfully changed', '', 'success')
                .then(() => {
                  props.toggle()
                  window.location.reload()
                })
            }).catch((error) => {
              props.setLoadSubmitBtn(false)

              message.error(error.message)
            })
          }
        })
      },
    }),
    withPropsOnChange(
      ['handleSearchAgent'],
      ({ handleSearchAgent }) => ({
        handleSearchAgent: debounce(handleSearchAgent, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        const masterData = [
          {
            base: 'apiUser',
            url: '/customer-change-reasons',
            method: 'get',
            name: 'status',
          },
        ]

        masterData.map(item => this.loadMasterData(item))
      },
      loadMasterData(data) {
        this.props.getDatas(
          { base: data.base, url: data.url, method: data.method },
        ).then((res) => {
          this.props.setStateForm({
            ...this.props.stateForm,
            [`${data.name}Load`]: false,
            [`${data.name}List`]: res.data,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateForm({
            ...this.props.stateForm,
            [`${data.name}Load`]: false,
            [`${data.name}List`]: [],
          })
        })
      },
    }),
  )(ChangeStatusForm),
)

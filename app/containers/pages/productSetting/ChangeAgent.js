import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
} from 'recompose'
import { Form } from '@ant-design/compatible'
import { getDatas } from 'actions/Option'
import { message } from 'antd'
import { capitalize, isEmpty } from 'lodash'
import Swal from 'sweetalert2'
import Helper from 'utils/Helper'
import ChangeStatusForm from 'components/pages/customers/ChangeAgent'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  return {
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
})
export default Form.create({ name: 'formChangeAgent' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateForm', 'setStateForm', {
      statusLoad: false,
      statusList: [],
    }),
    withState('stateSelects', 'setStateSelects', {
      agentListAfter: [],
      agentListBefore: [],
    }),
    withState('stateFile', 'setStateFile', {}),
    withState('loadSubmitBtn', 'setLoadSubmitBtn', false),
    withHandlers({
      handleUpload: props => (info, type) => {
        const { stateFile, setStateFile } = props

        Helper.getBase64(info.file, file => setStateFile({ ...stateFile, [type]: file }))
      },
      handleSearchAgentBefore: props => (val) => {
        props.getDatas(
          { base: 'apiUser', url: `/upline-agents?upline_agent_id=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            agentLoad: false,
            agentListBefore: res.data,
          })
        }).catch(() => {
          props.setStateSelects({
            ...props.stateSelects,
            agentLoad: false,
            agentListBefore: [],
          })
        })
      },
      handleSearchAgentAfter: props => (val) => {
        props.getDatas(
          { base: 'apiUser', url: `/upline-agents?upline_agent_id=${val}`, method: 'get' },
        ).then((res) => {
          props.setStateSelects({
            ...props.stateSelects,
            agentLoad: false,
            agentListAfter: res.data,
          })
        }).catch(() => {
          props.setStateSelects({
            ...props.stateSelects,
            agentLoad: false,
            agentListAfter: [],
          })
        })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields((err, values) => {
          const agentIdAfter = props.stateSelects.agentListAfter.filter(item => item.agent_id === values.agent_id_after)
          const agentIdBefore = props.stateSelects.agentListBefore.filter(item => item.agent_id === values.agent_id_before)

          if (!err) {
            const payload = {
              agent_id_after: !isEmpty(agentIdAfter) ? agentIdAfter[0].id : '',
              agent_id_before: !isEmpty(agentIdBefore) ? agentIdBefore[0].id : '',
              customer_number: values.customer_number,
              customer_id: values.customer_id,
              profile_id_after: values.profile_id_after,
              profile_id_before: values.profile_id_before,
              file: props.stateFile.file,
            }

            props.getDatas(
              { base: 'apiUser', url: '/customers/customer-change-agent', method: 'post' },
              payload,
            ).then(() => {
              props.setLoadSubmitBtn(false)
              Swal.fire('Status has been successfully changed', '', 'success')
                .then(() => {
                  props.toggle()
                  window.location.reload()
                })
            }).catch((error) => {
              props.setLoadSubmitBtn(false)

              if (error.errors.agent_id_after) {
                props.form.setFields({
                  agent_id_after: {
                    value: values.agent_id_after,
                    errors: [new Error((capitalize(error.errors.agent_id_after).split('_').join(' ')))],
                  },
                })
              }
              if (error.errors.agent_id_before) {
                props.form.setFields({
                  agent_id_before: {
                    value: values.agent_id_before,
                    errors: [new Error((capitalize(error.errors.agent_id_before).split('_').join(' ')))],
                  },
                })
              }
              if (error.errors.profile_id_after) {
                props.form.setFields({
                  profile_id_after: {
                    value: values.profile_id_after,
                    errors: [new Error((capitalize(error.errors.profile_id_after).split('_').join(' ')))],
                  },
                })
              }
              if (error.errors.profile_id_before) {
                props.form.setFields({
                  profile_id_before: {
                    value: values.profile_id_before,
                    errors: [new Error((capitalize(error.errors.profile_id_before).split('_').join(' ')))],
                  },
                })
              }
              if (error.errors.file) {
                props.form.setFields({
                  file: {
                    value: values.file,
                    errors: [new Error((capitalize(error.errors.file).split('_').join(' ')))],
                  },
                })
              }
              if (isEmpty(error.errors)) {
                message.error(error.message)
              }
            })
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        const {
          setStateSelects, stateSelects, detailCustomer,
        } = this.props

        this.props.getDatas(
          { base: 'apiUser', url: `/customers?search_by=customer_number&keyword=${detailCustomer.customer_number}`, method: 'get' },
        ).then((res) => {
          setStateSelects({
            ...stateSelects,
            customerLoad: false,
            customerList: res.data,
          })
        }).catch(() => {
          setStateSelects({
            ...stateSelects,
            customerLoad: false,
            customerList: [],
          })
        })
      },
    }),
  )(ChangeStatusForm),
)

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Form } from '@ant-design/compatible'
import { updateSiteConfiguration } from 'actions/Site'
import {
  compose,
  withHandlers,
  lifecycle,
  withState,
} from 'recompose'
import SelectProduct from 'components/pages/productSetting/SelectProduct'
import { message } from 'antd'
import { getDatas } from 'actions/Option'
import history from 'utils/history'
import { filter } from 'lodash'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  const {
    isFetching,
  } = state.root.role

  return {
    isFetching,
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  getDatas: bindActionCreators(getDatas, dispatch),
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
})

export default Form.create({ name: 'createCOBForm' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('stateCategory', 'setStateCategory', {
      loading: false,
      list: [],
    }),
    withState('stateCategorySyariah', 'setStateCategorySyariah', {
      loading: false,
      list: [],
    }),
    withState('getProduct', 'setProduct', {
      loading: false,
      type: 'konvensional',
      list: [],
    }),
    withHandlers({
      handleProductType: props => (value) => {
        props.setProduct({
          ...props.getProduct,
          type: value,
        })
      },
      handleProduct: props => (id) => {
        props.getDatas(
          { base: 'apiUser', url: `/product/${id}?product_type=${props.getProduct.type}`, method: 'get' },
        ).then((res) => {
          props.setProduct({
            ...props.getProduct,
            loading: false,
            list: res.data,
          })
        }).catch((err) => {
          message.error(err)
          props.setProduct({})
        })
      },
      onSubmit: props => (e) => {
        e.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            console.log(values)
            if (values.cob === 1) {
              if (values.product === 'otomate') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/otomate')
              } else if (values.product === 'otomate_smart') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/otomate-smart')
              } else if (values.product === 'otomate_solitare' || values.product === 'otomate_solitaire') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/otomate-solitaire')
              } else if (values.product === 'tlo') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/tlo')
              } else if (values.product === 'comprehensive') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/comprehensive')
              }
            } else if (values.cob === 2) {
              localStorage.setItem('product_id', values.product_id)
              history.push('product-setting/asri')
            } else if (values.cob === 3) {
              localStorage.setItem('product_id', values.product_id)
              history.push('product-setting/liability')
            } else if (values.cob === 4) {
              localStorage.setItem('product_id', values.product_id)
              history.push('product-setting/cargo')
            } else if (values.cob === 5) {
              localStorage.setItem('product_id', values.product_id)
              if (values.product === 'travel_safe_domestic') {
                history.push('product-setting/travel-domestic')
              } else if (values.product === 'travel_safe_international') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/travel-international')
              }
            } else if (values.cob === 6) {
              if (values.product === 'wellwoman') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/wellwoman')
              } else if (values.product === 'pa_amanah') {
                localStorage.setItem('product_id', values.product_id)
                history.push('product-setting/paamanah')
              }
            }
          }
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        this.props.updateSiteConfiguration('breadList', ['Home', 'Product Setting'])
        this.props.getDatas(
          { base: 'apiUser', url: '/class-of-business', method: 'get' },
        ).then((res) => {
          this.props.setStateCategory({
            ...this.props.stateCategory,
            loading: false,
            list: res.data,
          })

          // Filter manual syariah product
          const listSyariah = []
          filter(res.data, (item) => {
            if (item.name !== 'Travel') if (item.name !== 'Liability') if (item.name !== 'Cargo') listSyariah.push(item)
          })
          this.props.setStateCategorySyariah({
            ...this.props.stateCategorySyariah,
            loading: false,
            list: listSyariah,
          })
        }).catch((err) => {
          message.error(err)
          this.props.setStateCategory({})
        })
      },
    }),
  )(SelectProduct),
)

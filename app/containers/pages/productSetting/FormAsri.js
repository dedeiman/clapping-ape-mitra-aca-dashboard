import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  productDetail, fetchDetailProduct, createProduct, updateProduct,
} from 'actions/Product'
import { getCOB, getDatas } from 'actions/Option'
import { message } from 'antd'
import FormAsri from 'components/pages/productSetting/FormAsri'
import history from 'utils/history'
import _ from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    detailProduct,
  } = state.root.products

  return {
    isFetching,
    detailProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createProduct: bindActionCreators(createProduct, dispatch),
  updateProduct: bindActionCreators(updateProduct, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  productDetail: bindActionCreators(productDetail, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
})
const dataSelect = [
  { id: '9ec8a103-aef9-4193-bebf-2883d3be0fee', label: 'BCA' },
]

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateLiability', 'setStateLiability', {
    load: true,
    list: [],
    search: '',
    filter: '',
  }),
  withState('isEdit', 'setIsEdit', false),
  withState('errorsField', 'setErrorsField', {}),
  withState('loadCOB', 'setLoadCOB', true),
  withState('selectedRows', 'setSelectedRows', []),
  withState('metaCountries', 'setMetaCountries', null),
  withState('keyword', 'setKeyword', ''),
  withState('getCountries', 'setCountries', {
    list: [],
    isLoadCountries: false,
  }),
  withState('selectOption', 'setSelectOption', dataSelect),
  withHandlers({
    handleSubmit: props => (values) => {
      const data = [props.stateLiability.list]
      props.getDatas({ base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'put' }, {
        product_type: 'asri',
        rates_data: {
          asri_settings: [
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_standard)[1]).id || null,
              rate_percentage: Number(values.std_kls1) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_standard)[0]).id || null,
              rate_percentage: Number(values.std_kls2) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd)[0]).id || null,
              rate_percentage: Number(values.tsfwd_kls1) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd)[1]).id || null,
              rate_percentage: Number(values.tsfwd_kls2) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[0]).id || null,
              rate_percentage: Number(values.eqvet_11) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[1]).id || null,
              rate_percentage: Number(values.eqvet_21) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[2]).id || null,
              rate_percentage: Number(values.eqvet_31) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[3]).id || null,
              rate_percentage: Number(values.eqvet_41) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[4]).id || null,
              rate_percentage: Number(values.eqvet_51) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[5]).id || null,
              rate_percentage: Number(values.eqvet_12) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_eqvet)[6]).id || null,
              rate_percentage: Number(values.eqvet_22) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[0]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_11) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[1]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_21) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[2]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_31) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[3]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_41) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[4]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_51) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[5]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_12) || 0,
            },
            {
              id: Object.assign({}, Object.assign({}, data[0].asri_tsfwd_eqvet)[6]).id || null,
              rate_percentage: Number(values.eqvet_tsfwd_22) || 0,
            },
          ],
        },
      }).then(() => {
        message.success('Product Setting Asri has been updated')
        history.push('/product-setting/asri')
      }).catch((err) => {
        let objError = {}
        Object.keys(err.errors).forEach((item) => {
          objError = {
            ...objError,
            [item]: {
              validateStatus: 'error',
              help: _.startCase((err.errors[item]).replace('_id', '').replace('_', ' ')),
            },
          }
          return props.setErrorsField(objError)
        })
      })
    },
    handleGetCountries: props => (page, key) => {
      props.setCountries({
        ...props.getCountries,
        isLoadCountries: true,
      })

      props.getDatas(
        { base: 'apiUser', url: `/countries/list?page=${page}&limit=10&keyword=${key}`, method: 'get' },
      ).then((res) => {
        props.setCountries({
          ...props.getCountries,
          list: res.data,
          isLoadCountries: false,
        })

        props.setMetaCountries({
          ...props.metaCountries,
          total_count: res.meta.total_count,
        })

        if (props.match.params.id) {
          const countriesKey = res.data.filter(item => item.is_schengen_area)

          props.setSelectedRows(countriesKey.map(country => country.id))
        }
      })
    },
    handleChecked: props => (selectedRowKeys) => {
      props.setSelectedRows(selectedRowKeys)
    },
    goToDetailCountry: props => (dataCountry) => {
      if (props.match.params.id) {
        history.push(`/detail-country/${dataCountry.id}/edit`)
      }
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      props.handleGetCountries(page, props.keyword || '')
    },
    handleFilter: props => (key) => {
      props.handleGetCountries(1, key)
      props.setKeyword(key)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        match, setStateLiability,
        stateLiability, setLoadCOB,
      } = this.props
      this.props.updateSiteConfiguration('breadList', ['Home', 'Product Setting'])
      this.props.updateSiteConfiguration('activePage', 'product-setting')
      this.props.getDatas(
        { base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'get' },
      ).then((res) => {
        this.props.setStateLiability({
          ...this.props.stateLiability,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateLiability({})
      })
    },
  }),
)(FormAsri)

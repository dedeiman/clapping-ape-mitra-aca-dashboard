import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  productDetail, fetchDetailProduct, createProduct, updateProduct,
} from 'actions/Product'
import { getCOB, getDatas } from 'actions/Option'
import { message } from 'antd'
import FormTravelInternational from 'components/pages/productSetting/FormTravelInternational'
import history from 'utils/history'
import _ from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    detailProduct,
  } = state.root.products

  return {
    isFetching,
    detailProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createProduct: bindActionCreators(createProduct, dispatch),
  updateProduct: bindActionCreators(updateProduct, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  productDetail: bindActionCreators(productDetail, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
})
const dataSelect = [
  { id: '9ec8a103-aef9-4193-bebf-2883d3be0fee', label: 'BCA' },
]

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateLiability', 'setStateLiability', {
    load: true,
    list: [],
    search: '',
    filter: '',
  }),
  withState('isEdit', 'setIsEdit', false),
  withState('errorsField', 'setErrorsField', {}),
  withState('loadCOB', 'setLoadCOB', true),
  withState('selectedRows', 'setSelectedRows', []),
  withState('metaCountries', 'setMetaCountries', null),
  withState('keyword', 'setKeyword', ''),
  withState('getCountries', 'setCountries', {
    list: [],
    isLoadCountries: false,
  }),
  withState('selectOption', 'setSelectOption', dataSelect),
  withHandlers({
    handleSubmit: props => (values) => {
      console.log('values')
      const dataAsia = Object.assign({}, props.stateLiability.list.travel_international_asia)
      const dataWW = Object.assign({}, props.stateLiability.list.travel_international_worldwide)
      const asia1 = Object.assign({}, dataAsia['1-4'])
      const asia2 = Object.assign({}, dataAsia['5-6'])
      const asia3 = Object.assign({}, dataAsia['7-8'])
      const asia4 = Object.assign({}, dataAsia['9-10'])
      const asia5 = Object.assign({}, dataAsia['11-15'])
      const asia6 = Object.assign({}, dataAsia['16-20'])
      const asia7 = Object.assign({}, dataAsia['21-25'])
      const asia8 = Object.assign({}, dataAsia['26-31'])
      const asia9 = Object.assign({}, dataAsia.additional_week)
      const asia10 = Object.assign({}, dataAsia.annual)
      const ww1 = Object.assign({}, dataWW['1-4'])
      const ww2 = Object.assign({}, dataWW['5-6'])
      const ww3 = Object.assign({}, dataWW['7-8'])
      const ww4 = Object.assign({}, dataWW['9-10'])
      const ww5 = Object.assign({}, dataWW['11-15'])
      const ww6 = Object.assign({}, dataWW['16-20'])
      const ww7 = Object.assign({}, dataWW['21-25'])
      const ww8 = Object.assign({}, dataWW['26-31'])
      const ww9 = Object.assign({}, dataWW.additional_week)
      const ww10 = Object.assign({}, dataWW.annual)
      props.getDatas({ base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'put' }, {
        product_type: 'travel-international',
        rates_data: {
          travel_international_settings: [
            {
              id: Object.assign({}, asia1[3]).id || null,
              individual_amount: Number(values.asia_ind1) || 0,
              family_amount: Number(values.asia_fam1) || 0,
            },
            {
              id: Object.assign({}, asia1[1]).id || null,
              individual_amount: Number(values.asia_ind2) || 0,
              family_amount: Number(values.asia_fam2) || 0,
            },
            {
              id: Object.assign({}, asia1[0]).id || null,
              individual_amount: Number(values.asia_ind3) || 0,
              family_amount: Number(values.asia_fam3) || 0,
            },
            {
              id: Object.assign({}, asia1[2]).id || null,
              individual_amount: Number(values.asia_ind4) || 0,
              family_amount: Number(values.asia_fam4) || 0,
            },
            {
              id: Object.assign({}, asia2[3]).id || null,
              individual_amount: Number(values.asia_ind11) || 0,
              family_amount: Number(values.asia_fam11) || 0,
            },
            {
              id: Object.assign({}, asia2[1]).id || null,
              individual_amount: Number(values.asia_ind21) || 0,
              family_amount: Number(values.asia_fam21) || 0,
            },
            {
              id: Object.assign({}, asia2[0]).id || null,
              individual_amount: Number(values.asia_ind31) || 0,
              family_amount: Number(values.asia_fam31) || 0,
            },
            {
              id: Object.assign({}, asia2[2]).id || null,
              individual_amount: Number(values.asia_ind41) || 0,
              family_amount: Number(values.asia_fam41) || 0,
            },
            {
              id: Object.assign({}, asia3[3]).id || null,
              individual_amount: Number(values.asia_ind12) || 0,
              family_amount: Number(values.asia_fam12) || 0,
            },
            {
              id: Object.assign({}, asia3[1]).id || null,
              individual_amount: Number(values.asia_ind22) || 0,
              family_amount: Number(values.asia_fam22) || 0,
            },
            {
              id: Object.assign({}, asia3[0]).id || null,
              individual_amount: Number(values.asia_ind32) || 0,
              family_amount: Number(values.asia_fam32) || 0,
            },
            {
              id: Object.assign({}, asia3[2]).id || null,
              individual_amount: Number(values.asia_ind42) || 0,
              family_amount: Number(values.asia_fam42) || 0,
            },
            {
              id: Object.assign({}, asia4[3]).id || null,
              individual_amount: Number(values.asia_ind13) || 0,
              family_amount: Number(values.asia_fam13) || 0,
            },
            {
              id: Object.assign({}, asia4[1]).id || null,
              individual_amount: Number(values.asia_ind23) || 0,
              family_amount: Number(values.asia_fam23) || 0,
            },
            {
              id: Object.assign({}, asia4[0]).id || null,
              individual_amount: Number(values.asia_ind33) || 0,
              family_amount: Number(values.asia_fam33) || 0,
            },
            {
              id: Object.assign({}, asia4[2]).id || null,
              individual_amount: Number(values.asia_ind43) || 0,
              family_amount: Number(values.asia_fam43) || 0,
            },
            {
              id: Object.assign({}, asia5[3]).id || null,
              individual_amount: Number(values.asia_ind14) || 0,
              family_amount: Number(values.asia_fam14) || 0,
            },
            {
              id: Object.assign({}, asia5[1]).id || null,
              individual_amount: Number(values.asia_ind24) || 0,
              family_amount: Number(values.asia_fam24) || 0,
            },
            {
              id: Object.assign({}, asia5[0]).id || null,
              individual_amount: Number(values.asia_ind34) || 0,
              family_amount: Number(values.asia_fam34) || 0,
            },
            {
              id: Object.assign({}, asia5[2]).id || null,
              individual_amount: Number(values.asia_ind44) || 0,
              family_amount: Number(values.asia_fam44) || 0,
            },
            {
              id: Object.assign({}, asia6[3]).id || null,
              individual_amount: Number(values.asia_ind15) || 0,
              family_amount: Number(values.asia_fam15) || 0,
            },
            {
              id: Object.assign({}, asia6[1]).id || null,
              individual_amount: Number(values.asia_ind25) || 0,
              family_amount: Number(values.asia_fam25) || 0,
            },
            {
              id: Object.assign({}, asia6[0]).id || null,
              individual_amount: Number(values.asia_ind35) || 0,
              family_amount: Number(values.asia_fam35) || 0,
            },
            {
              id: Object.assign({}, asia6[2]).id || null,
              individual_amount: Number(values.asia_ind45) || 0,
              family_amount: Number(values.asia_fam45) || 0,
            },
            {
              id: Object.assign({}, asia7[3]).id || null,
              individual_amount: Number(values.asia_ind16) || 0,
              family_amount: Number(values.asia_fam16) || 0,
            },
            {
              id: Object.assign({}, asia7[1]).id || null,
              individual_amount: Number(values.asia_ind26) || 0,
              family_amount: Number(values.asia_fam26) || 0,
            },
            {
              id: Object.assign({}, asia7[0]).id || null,
              individual_amount: Number(values.asia_ind36) || 0,
              family_amount: Number(values.asia_fam36) || 0,
            },
            {
              id: Object.assign({}, asia7[2]).id || null,
              individual_amount: Number(values.asia_ind46) || 0,
              family_amount: Number(values.asia_fam46) || 0,
            },
            {
              id: Object.assign({}, asia8[3]).id || null,
              individual_amount: Number(values.asia_ind17) || 0,
              family_amount: Number(values.asia_fam17) || 0,
            },
            {
              id: Object.assign({}, asia8[1]).id || null,
              individual_amount: Number(values.asia_ind27) || 0,
              family_amount: Number(values.asia_fam27) || 0,
            },
            {
              id: Object.assign({}, asia8[0]).id || null,
              individual_amount: Number(values.asia_ind37) || 0,
              family_amount: Number(values.asia_fam37) || 0,
            },
            {
              id: Object.assign({}, asia8[2]).id || null,
              individual_amount: Number(values.asia_ind47) || 0,
              family_amount: Number(values.asia_fam47) || 0,
            },
            {
              id: Object.assign({}, asia9[3]).id || null,
              individual_amount: Number(values.asia_ind18) || 0,
              family_amount: Number(values.asia_fam18) || 0,
            },
            {
              id: Object.assign({}, asia9[1]).id || null,
              individual_amount: Number(values.asia_ind28) || 0,
              family_amount: Number(values.asia_fam28) || 0,
            },
            {
              id: Object.assign({}, asia9[0]).id || null,
              individual_amount: Number(values.asia_ind38) || 0,
              family_amount: Number(values.asia_fam38) || 0,
            },
            {
              id: Object.assign({}, asia9[2]).id || null,
              individual_amount: Number(values.asia_ind48) || 0,
              family_amount: Number(values.asia_fam48) || 0,
            },
            {
              id: Object.assign({}, asia10[3]).id || null,
              individual_amount: Number(values.asia_ind19) || 0,
              family_amount: Number(values.asia_fam19) || 0,
            },
            {
              id: Object.assign({}, asia10[1]).id || null,
              individual_amount: Number(values.asia_ind29) || 0,
              family_amount: Number(values.asia_fam29) || 0,
            },
            {
              id: Object.assign({}, asia10[0]).id || null,
              individual_amount: Number(values.asia_ind39) || 0,
              family_amount: Number(values.asia_fam39) || 0,
            },
            {
              id: Object.assign({}, asia10[2]).id || null,
              individual_amount: Number(values.asia_ind49) || 0,
              family_amount: Number(values.asia_fam49) || 0,
            },
            {
              id: Object.assign({}, ww1[3]).id || null,
              individual_amount: Number(values.ww_ind1) || 0,
              family_amount: Number(values.ww_fam1) || 0,
            },
            {
              id: Object.assign({}, ww1[1]).id || null,
              individual_amount: Number(values.ww_ind2) || 0,
              family_amount: Number(values.ww_fam2) || 0,
            },
            {
              id: Object.assign({}, ww1[0]).id || null,
              individual_amount: Number(values.ww_ind3) || 0,
              family_amount: Number(values.ww_fam3) || 0,
            },
            {
              id: Object.assign({}, ww1[2]).id || null,
              individual_amount: Number(values.ww_ind4) || 0,
              family_amount: Number(values.ww_fam4) || 0,
            },
            {
              id: Object.assign({}, ww2[3]).id || null,
              individual_amount: Number(values.ww_ind11) || 0,
              family_amount: Number(values.ww_fam11) || 0,
            },
            {
              id: Object.assign({}, ww2[1]).id || null,
              individual_amount: Number(values.ww_ind21) || 0,
              family_amount: Number(values.ww_fam21) || 0,
            },
            {
              id: Object.assign({}, ww2[0]).id || null,
              individual_amount: Number(values.ww_ind31) || 0,
              family_amount: Number(values.ww_fam31) || 0,
            },
            {
              id: Object.assign({}, ww2[2]).id || null,
              individual_amount: Number(values.ww_ind41) || 0,
              family_amount: Number(values.ww_fam41) || 0,
            },
            {
              id: Object.assign({}, ww3[3]).id || null,
              individual_amount: Number(values.ww_ind12) || 0,
              family_amount: Number(values.ww_fam12) || 0,
            },
            {
              id: Object.assign({}, ww3[1]).id || null,
              individual_amount: Number(values.ww_ind22) || 0,
              family_amount: Number(values.ww_fam22) || 0,
            },
            {
              id: Object.assign({}, ww3[0]).id || null,
              individual_amount: Number(values.ww_ind32) || 0,
              family_amount: Number(values.ww_fam32) || 0,
            },
            {
              id: Object.assign({}, ww3[2]).id || null,
              individual_amount: Number(values.ww_ind42) || 0,
              family_amount: Number(values.ww_fam42) || 0,
            },
            {
              id: Object.assign({}, ww4[3]).id || null,
              individual_amount: Number(values.ww_ind13) || 0,
              family_amount: Number(values.ww_fam13) || 0,
            },
            {
              id: Object.assign({}, ww4[1]).id || null,
              individual_amount: Number(values.ww_ind23) || 0,
              family_amount: Number(values.ww_fam23) || 0,
            },
            {
              id: Object.assign({}, ww4[0]).id || null,
              individual_amount: Number(values.ww_ind33) || 0,
              family_amount: Number(values.ww_fam33) || 0,
            },
            {
              id: Object.assign({}, ww4[2]).id || null,
              individual_amount: Number(values.ww_ind43) || 0,
              family_amount: Number(values.ww_fam43) || 0,
            },
            {
              id: Object.assign({}, ww5[3]).id || null,
              individual_amount: Number(values.ww_ind14) || 0,
              family_amount: Number(values.ww_fam14) || 0,
            },
            {
              id: Object.assign({}, ww5[1]).id || null,
              individual_amount: Number(values.ww_ind24) || 0,
              family_amount: Number(values.ww_fam24) || 0,
            },
            {
              id: Object.assign({}, ww5[0]).id || null,
              individual_amount: Number(values.ww_ind34) || 0,
              family_amount: Number(values.ww_fam34) || 0,
            },
            {
              id: Object.assign({}, ww5[2]).id || null,
              individual_amount: Number(values.ww_ind44) || 0,
              family_amount: Number(values.ww_fam44) || 0,
            },
            {
              id: Object.assign({}, ww6[3]).id || null,
              individual_amount: Number(values.ww_ind15) || 0,
              family_amount: Number(values.ww_fam15) || 0,
            },
            {
              id: Object.assign({}, ww6[1]).id || null,
              individual_amount: Number(values.ww_ind25) || 0,
              family_amount: Number(values.ww_fam25) || 0,
            },
            {
              id: Object.assign({}, ww6[0]).id || null,
              individual_amount: Number(values.ww_ind35) || 0,
              family_amount: Number(values.ww_fam35) || 0,
            },
            {
              id: Object.assign({}, ww6[2]).id || null,
              individual_amount: Number(values.ww_ind45) || 0,
              family_amount: Number(values.ww_fam45) || 0,
            },
            {
              id: Object.assign({}, ww7[3]).id || null,
              individual_amount: Number(values.ww_ind16) || 0,
              family_amount: Number(values.ww_fam16) || 0,
            },
            {
              id: Object.assign({}, ww7[1]).id || null,
              individual_amount: Number(values.ww_ind26) || 0,
              family_amount: Number(values.ww_fam26) || 0,
            },
            {
              id: Object.assign({}, ww7[0]).id || null,
              individual_amount: Number(values.ww_ind36) || 0,
              family_amount: Number(values.ww_fam36) || 0,
            },
            {
              id: Object.assign({}, ww7[2]).id || null,
              individual_amount: Number(values.ww_ind46) || 0,
              family_amount: Number(values.ww_fam46) || 0,
            },
            {
              id: Object.assign({}, ww8[3]).id || null,
              individual_amount: Number(values.ww_ind17) || 0,
              family_amount: Number(values.ww_fam17) || 0,
            },
            {
              id: Object.assign({}, ww8[1]).id || null,
              individual_amount: Number(values.ww_ind27) || 0,
              family_amount: Number(values.ww_fam27) || 0,
            },
            {
              id: Object.assign({}, ww8[0]).id || null,
              individual_amount: Number(values.ww_ind37) || 0,
              family_amount: Number(values.ww_fam37) || 0,
            },
            {
              id: Object.assign({}, ww8[2]).id || null,
              individual_amount: Number(values.ww_ind47) || 0,
              family_amount: Number(values.ww_fam47) || 0,
            },
            {
              id: Object.assign({}, ww9[3]).id || null,
              individual_amount: Number(values.ww_ind18) || 0,
              family_amount: Number(values.ww_fam18) || 0,
            },
            {
              id: Object.assign({}, ww9[1]).id || null,
              individual_amount: Number(values.ww_ind28) || 0,
              family_amount: Number(values.ww_fam28) || 0,
            },
            {
              id: Object.assign({}, ww9[0]).id || null,
              individual_amount: Number(values.ww_ind38) || 0,
              family_amount: Number(values.ww_fam38) || 0,
            },
            {
              id: Object.assign({}, ww9[2]).id || null,
              individual_amount: Number(values.ww_ind48) || 0,
              family_amount: Number(values.ww_fam48) || 0,
            },
            {
              id: Object.assign({}, ww10[3]).id || null,
              individual_amount: Number(values.ww_ind19) || 0,
              family_amount: Number(values.ww_fam19) || 0,
            },
            {
              id: Object.assign({}, ww10[1]).id || null,
              individual_amount: Number(values.ww_ind29) || 0,
              family_amount: Number(values.ww_fam29) || 0,
            },
            {
              id: Object.assign({}, ww10[0]).id || null,
              individual_amount: Number(values.ww_ind39) || 0,
              family_amount: Number(values.ww_fam39) || 0,
            },
            {
              id: Object.assign({}, ww10[2]).id || null,
              individual_amount: Number(values.ww_ind49) || 0,
              family_amount: Number(values.ww_fam49) || 0,
            },
          ],
        },
      }).then(() => {
        message.success('Product Setting Travel international has been updated')
        history.push('/product-setting/travel-international')
      }).catch((err) => {
        let objError = {}
        Object.keys(err.errors).forEach((item) => {
          objError = {
            ...objError,
            [item]: {
              validateStatus: 'error',
              help: _.startCase((err.errors[item]).replace('_id', '').replace('_', ' ')),
            },
          }
          return props.setErrorsField(objError)
        })
      })
    },
    handleGetCountries: props => (page, key) => {
      props.setCountries({
        ...props.getCountries,
        isLoadCountries: true,
      })

      props.getDatas(
        { base: 'apiUser', url: `/countries/list?page=${page}&limit=10&keyword=${key}`, method: 'get' },
      ).then((res) => {
        props.setCountries({
          ...props.getCountries,
          list: res.data,
          isLoadCountries: false,
        })

        props.setMetaCountries({
          ...props.metaCountries,
          total_count: res.meta.total_count,
        })

        if (props.match.params.id) {
          const countriesKey = res.data.filter(item => item.is_schengen_area)

          props.setSelectedRows(countriesKey.map(country => country.id))
        }
      })
    },
    handleChecked: props => (selectedRowKeys) => {
      props.setSelectedRows(selectedRowKeys)
    },
    goToDetailCountry: props => (dataCountry) => {
      if (props.match.params.id) {
        history.push(`/detail-country/${dataCountry.id}/edit`)
      }
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      props.handleGetCountries(page, props.keyword || '')
    },
    handleFilter: props => (key) => {
      props.handleGetCountries(1, key)
      props.setKeyword(key)
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        match, setStateLiability,
        stateLiability, setLoadCOB,
      } = this.props
      this.props.updateSiteConfiguration('breadList', ['Home', 'Product Setting'])
      this.props.updateSiteConfiguration('activePage', 'product-setting')
      this.props.getDatas(
        { base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'get' },
      ).then((res) => {
        this.props.setStateLiability({
          ...this.props.stateLiability,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateLiability({})
      })
    },
  }),
)(FormTravelInternational)

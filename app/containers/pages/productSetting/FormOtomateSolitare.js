import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import {
  productDetail, fetchDetailProduct, createProduct, updateProduct,
} from 'actions/Product'
import { getCOB, getDatas } from 'actions/Option'
import { message } from 'antd'
import FormOtomateSolitare from 'components/pages/productSetting/FormOtomateSolitare'
import history from 'utils/history'
import _ from 'lodash'

export function mapStateToProps(state) {
  const {
    isFetching,
    detailProduct,
  } = state.root.products

  return {
    isFetching,
    detailProduct,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchDetailProduct: bindActionCreators(fetchDetailProduct, dispatch),
  createProduct: bindActionCreators(createProduct, dispatch),
  updateProduct: bindActionCreators(updateProduct, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
  productDetail: bindActionCreators(productDetail, dispatch),
  getCOB: bindActionCreators(getCOB, dispatch),
})
const dataSelect = [
  { id: '9ec8a103-aef9-4193-bebf-2883d3be0fee', label: 'BCA' },
]

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('stateLiability', 'setStateLiability', {
    load: true,
    list: [],
    search: '',
    filter: '',
  }),
  withState('isEdit', 'setIsEdit', false),
  withState('errorsField', 'setErrorsField', {}),
  withState('loadCOB', 'setLoadCOB', true),
  withState('selectedRows', 'setSelectedRows', []),
  withState('metaCountries', 'setMetaCountries', null),
  withState('keyword', 'setKeyword', ''),
  withState('getCountries', 'setCountries', {
    list: [],
    isLoadCountries: false,
  }),
  withState('selectOption', 'setSelectOption', dataSelect),
  withHandlers({
    handleSubmit: props => (values) => {
      const dataPremi = Object.assign({}, props.stateLiability.list.premi_rates)
      const dataWW = Object.assign({}, props.stateLiability.list.natural_disaster_rates)
      const dataTPL = Object.assign({}, props.stateLiability.list.third_party_liability_rates)
      const bencana1 = Object.assign({}, dataWW.flood)
      const bencana2 = Object.assign({}, dataWW.eq)
      const bencana3 = Object.assign({}, dataWW.rscc)
      const bencana4 = Object.assign({}, dataWW.ts)
      const asia1 = Object.assign({}, dataPremi['>100jt-125jt'])
      const asia2 = Object.assign({}, dataPremi['>125jt-200jt'])
      const asia3 = Object.assign({}, dataPremi['>200jt-400jt'])
      const asia4 = Object.assign({}, dataPremi['>400jt-800jt'])
      const asia5 = Object.assign({}, dataPremi['>800jt'])

      props.getDatas({ base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'put' }, {
        product_type: 'otomate-solitare',
        rates_data: {
          motorcar_settings: [
            {
              id: Object.assign({}, asia1[0]).id || null,
              rate_percentage: Number(values.will11) || 0,
            },
            {
              id: Object.assign({}, asia1[1]).id || null,
              rate_percentage: Number(values.will12) || 0,
            },
            {
              id: Object.assign({}, asia1[2]).id || null,
              rate_percentage: Number(values.will13) || 0,
            },
            {
              id: Object.assign({}, asia2[0]).id || null,
              rate_percentage: Number(values.will21) || 0,
            },
            {
              id: Object.assign({}, asia2[1]).id || null,
              rate_percentage: Number(values.will22) || 0,
            },
            {
              id: Object.assign({}, asia2[2]).id || null,
              rate_percentage: Number(values.will23) || 0,
            },
            {
              id: Object.assign({}, asia3[0]).id || null,
              rate_percentage: Number(values.will31) || 0,
            },
            {
              id: Object.assign({}, asia3[1]).id || null,
              rate_percentage: Number(values.will32) || 0,
            },
            {
              id: Object.assign({}, asia3[2]).id || null,
              rate_percentage: Number(values.will33) || 0,
            },
            {
              id: Object.assign({}, asia4[0]).id || null,
              rate_percentage: Number(values.will41) || 0,
            },
            {
              id: Object.assign({}, asia4[1]).id || null,
              rate_percentage: Number(values.will42) || 0,
            },
            {
              id: Object.assign({}, asia4[2]).id || null,
              rate_percentage: Number(values.will43) || 0,
            },
            {
              id: Object.assign({}, asia5[0]).id || null,
              rate_percentage: Number(values.will51) || 0,
            },
            {
              id: Object.assign({}, asia5[1]).id || null,
              rate_percentage: Number(values.will52) || 0,
            },
            {
              id: Object.assign({}, asia5[2]).id || null,
              rate_percentage: Number(values.will53) || 0,
            },
          ],
          natural_disasters: [
            {
              id: Object.assign({}, bencana1[0]).id || null,
              rate_percentage: Number(values.bencana11) || 0,
            },
            {
              id: Object.assign({}, bencana1[1]).id || null,
              rate_percentage: Number(values.bencana12) || 0,
            },
            {
              id: Object.assign({}, bencana1[2]).id || null,
              rate_percentage: Number(values.bencana13) || 0,
            },
            {
              id: Object.assign({}, bencana2[0]).id || null,
              rate_percentage: Number(values.bencana21) || 0,
            },
            {
              id: Object.assign({}, bencana2[1]).id || null,
              rate_percentage: Number(values.bencana22) || 0,
            },
            {
              id: Object.assign({}, bencana2[2]).id || null,
              rate_percentage: Number(values.bencana23) || 0,
            },
            {
              id: Object.assign({}, bencana3[0]).id || null,
              rate_percentage: Number(values.bencana31) || 0,
            },
            {
              id: Object.assign({}, bencana3[1]).id || null,
              rate_percentage: Number(values.bencana32) || 0,
            },
            {
              id: Object.assign({}, bencana3[2]).id || null,
              rate_percentage: Number(values.bencana33) || 0,
            },
            {
              id: Object.assign({}, bencana4[0]).id || null,
              rate_percentage: Number(values.bencana41) || 0,
            },
            {
              id: Object.assign({}, bencana4[1]).id || null,
              rate_percentage: Number(values.bencana42) || 0,
            },
            {
              id: Object.assign({}, bencana4[2]).id || null,
              rate_percentage: Number(values.bencana43) || 0,
            },
          ],
          third_party_liability: [
            {
              id: Object.assign({}, dataTPL['30 jt']).id || null,
              rate_percentage: Number(values.tpl1) || 0,
            },
            {
              id: Object.assign({}, dataTPL['35 jt']).id || null,
              rate_percentage: Number(values.tpl2) || 0,
            },
            {
              id: Object.assign({}, dataTPL['40 jt']).id || null,
              rate_percentage: Number(values.tpl3) || 0,
            },
            {
              id: Object.assign({}, dataTPL['45 jt']).id || null,
              rate_percentage: Number(values.tpl4) || 0,
            },
            {
              id: Object.assign({}, dataTPL['50 jt']).id || null,
              rate_percentage: Number(values.tpl5) || 0,
            },
            {
              id: Object.assign({}, dataTPL['55 jt']).id || null,
              rate_percentage: Number(values.tpl6) || 0,
            },
            {
              id: Object.assign({}, dataTPL['60 jt']).id || null,
              rate_percentage: Number(values.tpl7) || 0,
            },
            {
              id: Object.assign({}, dataTPL['65 jt']).id || null,
              rate_percentage: Number(values.tpl8) || 0,
            },
            {
              id: Object.assign({}, dataTPL['70 jt']).id || null,
              rate_percentage: Number(values.tpl9) || 0,
            },
            {
              id: Object.assign({}, dataTPL['75jt']).id || null,
              rate_percentage: Number(values.tpl10) || 0,
            },
            {
              id: Object.assign({}, dataTPL['80 jt']).id || null,
              rate_percentage: Number(values.tpl11) || 0,
            },
            {
              id: Object.assign({}, dataTPL['85 jt']).id || null,
              rate_percentage: Number(values.tpl12) || 0,
            },
            {
              id: Object.assign({}, dataTPL['90 jt']).id || null,
              rate_percentage: Number(values.tpl13) || 0,
            },
            {
              id: Object.assign({}, dataTPL['95 jt']).id || null,
              rate_percentage: Number(values.tpl14) || 0,
            },
            {
              id: Object.assign({}, dataTPL['100 jt']).id || null,
              rate_percentage: Number(values.tpl15) || 0,
            },
          ],
        },
      }).then(() => {
        message.success('Product Setting Travel international has been updated')
        history.push('/product-setting/travel-international')
      }).catch((err) => {
        let objError = {}
        Object.keys(err.errors).forEach((item) => {
          objError = {
            ...objError,
            [item]: {
              validateStatus: 'error',
              help: _.startCase((err.errors[item]).replace('_id', '').replace('_', ' ')),
            },
          }
          return props.setErrorsField(objError)
        })
      })
    },
    handleGetCountries: props => (page, key) => {
      props.setCountries({
        ...props.getCountries,
        isLoadCountries: true,
      })

      props.getDatas(
        { base: 'apiUser', url: `/countries/list?page=${page}&limit=10&keyword=${key}`, method: 'get' },
      ).then((res) => {
        props.setCountries({
          ...props.getCountries,
          list: res.data,
          isLoadCountries: false,
        })

        props.setMetaCountries({
          ...props.metaCountries,
          total_count: res.meta.total_count,
        })

        if (props.match.params.id) {
          const countriesKey = res.data.filter(item => item.is_schengen_area)

          props.setSelectedRows(countriesKey.map(country => country.id))
        }
      })
    },
    handleChecked: props => (selectedRowKeys) => {
      props.setSelectedRows(selectedRowKeys)
    },
    goToDetailCountry: props => (dataCountry) => {
      if (props.match.params.id) {
        history.push(`/detail-country/${dataCountry.id}/edit`)
      }
    },
  }),
  withHandlers({
    handlePage: props => (page) => {
      props.handleGetCountries(page, props.keyword || '')
    },
    handleFilter: props => (key) => {
      props.handleGetCountries(1, key)
      props.setKeyword(key)
    },
  }),
  lifecycle({
    componentDidMount() {
      this.props.updateSiteConfiguration('breadList', ['Home', 'Product Setting'])
      this.props.updateSiteConfiguration('activePage', 'product-setting')
      this.props.getDatas(
        { base: 'apiUser', url: `/product-settings/${localStorage.getItem('product_id')}`, method: 'get' },
      ).then((res) => {
        this.props.setStateLiability({
          ...this.props.stateLiability,
          loading: false,
          list: res.data,
        })
      }).catch((err) => {
        message.error(err)
        this.props.setStateLiability({})
      })
    },
  }),
)(FormOtomateSolitare)

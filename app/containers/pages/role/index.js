import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { fetchRole, deleteRole, roleFailure } from 'actions/Role'
import qs from 'query-string'
import history from 'utils/history'
import RoleView from 'components/pages/role'
import Swal from 'sweetalert2'

export function mapStateToProps(state) {
  const {
    isFetching,
    errorMessage,
    dataRole,
    metaRole,
  } = state.root.role

  return {
    isFetching,
    errorMessage,
    dataRole,
    metaRole,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  fetchRole: bindActionCreators(fetchRole, dispatch),
  deleteRole: bindActionCreators(deleteRole, dispatch),
  roleFailure: bindActionCreators(roleFailure, dispatch),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withState('keyword', 'setKeyword', ''),
  withState('currentPage', 'setCurrentPage', ''),
  withHandlers({
    handleFilter: props => () => {
      const payload = {
        keyword: props.keyword,
        page: props.currentPage,
      }
      history.push(`/role?sort_by=name&order_by=asc&${qs.stringify(payload)}`)
    },
  }),
  withHandlers({
    handleSearch: props => (value) => {
      props.setKeyword(value)
      setTimeout(() => {
        history.push(`/role?sort_by=name&order_by=asc${value ? `&keyword=${value}` : ''}`)
      }, 300)
    },
    handlePage: props => (page) => {
      props.setCurrentPage(page)
      setTimeout(() => {
        props.handleFilter()
      }, 300)
    },
    handleDelete: props => (id) => {
      props.deleteRole(id).then(() => {
        Swal.fire({
          icon: 'success',
          title: 'Role has been deleted',
          showConfirmButton: false,
          timer: 1500,
        })
      })
    },
    closeError: props => () => props.roleFailure(''),
  }),
  lifecycle({
    componentDidMount() {
      const { setKeyword, setCurrentPage, location } = this.props
      const search = qs.parse(location.search) || {}
      setCurrentPage(Number(search.page))
      setKeyword(search.keyword)
      this.props.updateSiteConfiguration('breadList', ['Home', 'Role'])
      this.props.updateSiteConfiguration('activePage', 'role')
      this.props.fetchRole(location.search)
    },
  }),
)(RoleView)

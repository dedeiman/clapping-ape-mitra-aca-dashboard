import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  compose, lifecycle,
  withState, withHandlers,
  withPropsOnChange,
} from 'recompose'
import { resetPassword, verifyPassword } from 'actions/Auth'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import { debounce, isEmpty } from 'lodash'
import Swal from 'sweetalert2'
import FirstPasswordChange from 'components/pages/password/FirstPassword'

export function mapStateToProps(state) {
  const {
    group: groupRole,
  } = state.root.role

  return {
    groupRole,
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  resetPassword: bindActionCreators(resetPassword, dispatch),
  verifyPassword: bindActionCreators(verifyPassword, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'changeFirstPassword' })(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps,
    ),
    withState('isLoading', 'setLoading', false),
    withState('isBlocking', 'setBlocking', true),
    withState('stateError', 'setStateError', {}),
    withHandlers({
      handleBlockNavigation: props => (nextLocation) => {
        const url = `${nextLocation.pathname}${nextLocation.hash}`
        const currentUrl = `${props.location.pathname}${props.location.hash}`
        if (props.isBlocking && (currentUrl !== url)) {
          Swal.fire({
            title: '<span style="color:#2b57b7;font-weight:bold">Maaf, anda harus mengubah password terlebih dahulu.</span>',
            icon: 'warning',
          }).then(() => props.updateSiteConfiguration('activePage', 'profile/change#first'))
          return false
        }
        return true
      },
      onVerify: props => (value, isPrimary) => {
        const { stateError, setStateError } = props
        props.verifyPassword({ [`${isPrimary ? '' : 'alternative_'}password`]: value })
          .catch(() => {
            setStateError({ ...stateError, [`old_${isPrimary ? '' : 'alternative_'}password`]: { validateStatus: 'error', help: "Password is doesn't match" } })
          })
      },
      onSubmit: props => (event) => {
        event.preventDefault()
        props.form.validateFields(async (err, values) => {
          if (err) {
            Swal.fire(
              '',
              '<span style="color:#2b57b7;font-weight:bold">Mohon Periksa kembali data Anda !</span>',
              'error',
            )
          }
          props.setLoading(true)
          if (!err && isEmpty(props.stateError.old_password)) {
            await props.resetPassword({ ...values, change_password: true, change_alternative_password: true }, true)
              .catch((error) => {
                props.setStateError({
                  ...props.stateError,
                  message: error,
                })
                Swal.fire('', `<span style="color:#2b57b7;font-weight:bold">${error}</span>`, 'error')
                  .then(() => props.setLoading(false))
              })
          } else {
            props.setLoading(false)
          }
          props.setLoading(false)
        })
      },
    }),
    withPropsOnChange(
      ['onVerify'],
      ({ onVerify }) => ({
        onVerify: debounce(onVerify, 300),
      }),
    ),
    lifecycle({
      componentDidMount() {
        this.props.updateSiteConfiguration('activeSubPage', 'password')
        this.props.updateSiteConfiguration('activePage', 'my-password/#first')
        // this.props.getDatas(
        //   { base: 'apiUser', url: '/passwords/first-passwords', method: 'get' },
        // ).then((res) => {
        //   this.props.form.setFieldsValue({
        //     old_password: res.data.password,
        //     old_alternative_password: res.data.alternative_password,
        //   })
        // })
      },
    }),
  )(FirstPasswordChange),
)

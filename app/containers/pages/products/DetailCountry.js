import { connect } from 'react-redux'
import {
  compose, lifecycle, withHandlers, withState,
} from 'recompose'
import { bindActionCreators } from 'redux'
import { updateSiteConfiguration } from 'actions/Site'
import { getDatas } from 'actions/Option'
import { Form } from '@ant-design/compatible'
import { message } from 'antd'
import history from 'utils/history'
import DetailCountry from 'components/pages/products/DetailCountry'
import _ from 'lodash'
import Swal from 'sweetalert2'

const mapDispatchToProps = dispatch => ({
  updateSiteConfiguration: bindActionCreators(updateSiteConfiguration, dispatch),
  getDatas: bindActionCreators(getDatas, dispatch),
})

export default Form.create({ name: 'countryForm' })(
  compose(
    connect(
      null,
      mapDispatchToProps,
    ),
    withState('isEdit', 'setIsEdit', false),
    withState('errorsField', 'setErrorsField', {}),
    withState('detailCountry', 'setDetailCountry', {
      code: '',
      name: '',
      zone: '',
    }),
    withHandlers({
      handleSubmit: props => (values) => {
        const {
          params,
        } = props.match

        props.getDatas(
          { base: 'apiUser', url: params.id ? `/countries/update/${params.id}` : '/countries/store', method: params.id ? 'put' : 'post' }, values,
        ).then(() => {
          Swal.fire(
            `Product has been ${params.id ? 'upd' : 'cre'}ated`,
            '',
            'success',
          ).then(() => {
            history.goBack()
          })
        }).catch((err) => {
          let objError = {}
          Object.keys(err.errors).forEach((item) => {
            objError = {
              ...objError,
              [item]: {
                validateStatus: 'error',
                help: _.startCase((err.errors[item]).replace('_id', '').replace('_', ' ')),
              },
            }
            return props.setErrorsField(objError)
          })
        })
      },
      getDetailCountries: props => (id) => {
        props.getDatas(
          { base: 'apiUser', url: `/countries/detail/${id}`, method: 'get' },
        ).then((res) => {
          props.setDetailCountry({ ...res.data })
          props.form.setFieldsValue({
            is_schengen_area: res.data.is_schengen_area,
            is_closed_country: res.data.is_closed_country,
          })
        })
      },
    }),
    lifecycle({
      componentDidMount() {
        const {
          setIsEdit, match,
          getDetailCountries,
        } = this.props
        this.props.updateSiteConfiguration('breadList', ['Home', `Product - Form - ${match.params.id ? 'Edit' : 'Create'} Country`])
        this.props.updateSiteConfiguration('activePage', 'products')

        if (match.params.id) {
          setIsEdit(true)
          getDetailCountries(match.params.id)
        }
      },
    }),
  )(DetailCountry),
)

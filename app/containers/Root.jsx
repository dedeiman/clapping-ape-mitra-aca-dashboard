import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import { Router, Route, Switch } from 'react-router-dom'
import history from 'utils/history'
import privateComponent from 'utils/hocs/privateComponent'
import publicComponent from 'utils/hocs/publicComponent'
import PublicLayout from 'containers/layouts/PublicLayout'
import PrivateLayout from 'containers/layouts/PrivateLayout'

import Login from 'containers/login/Login'
import Forgot from 'containers/login/Forgot'
import Password from 'containers/pages/password'
import NotFound from 'containers/NotFound'
import Dashboard from 'containers/pages/dashboard'
import User from 'containers/pages/users'
import RoleGroup from 'containers/pages/roleGroup'
import FormRoleGroup from 'containers/pages/roleGroup/Form'
import Role from 'containers/pages/role'
import FormRole from 'containers/pages/role/Form'
import FormUser from 'containers/pages/users/Form'
import Product from 'containers/pages/products'
import FormProduct from 'containers/pages/products/Form'
import ReportTraining from 'containers/pages/reports'
import ReportProduct from 'containers/pages/reports/Products'
import ReportContest from 'containers/pages/reports/Contest'
import ReportAgentList from 'containers/pages/reports/Administrator/AgentList'
import ReportApproval from 'containers/pages/reports/Administrator/Approval'
import ReportSPPA from 'containers/pages/reports/Productions/SPPA'
import ReportProduction from 'containers/pages/reports/Productions/Production'
import ReportClaimSettle from 'containers/pages/reports/Productions/ClaimSettle'
import ReportLossBusiness from 'containers/pages/reports/Productions/LossBusiness'
import ReportOutstandingClaim from 'containers/pages/reports/Productions/OutstandingClaim'
import ReportFinanceCommission from 'containers/pages/reports/Finance/FinanceCommission'
import ReportBenefitMonthly from 'containers/pages/reports/Benefit/Monthly'
import ReportBenefitYearly from 'containers/pages/reports/Benefit/Yearly'
import ReportCareer from 'containers/pages/reports/Career'
import ReportDetailCommision from 'containers/pages/reports/Agent/DetailCommision'
import ReportExpiredPolicy from 'containers/pages/reports/Agent/ExpiredPolicy'
import ReportTrainingData from 'containers/pages/reports/Training'
import DetailCountry from 'containers/pages/products/DetailCountry'
import ProductSetting from 'containers/pages/productSetting'

const PublicRoute = (props) => {
  const { 
    component: Component,
    redirect,
    redirectPath,
    ...rest
  } = props

  const PublicLayoutView = publicComponent(PublicLayout, redirect, redirectPath)

  return (
    <Route
      {...rest}
      render={matchProps => (
        <PublicLayoutView>
          <Component {...matchProps} />
        </PublicLayoutView>
      )}
    />
  )
}

const PrivateRoute = ({ component: Component, ...rest }) => {
  const PrivateLayoutView = privateComponent(PrivateLayout)

  return (
    <Route
      {...rest}
      render={matchProps => (
        <PrivateLayoutView>
          <Component {...matchProps} />
        </PrivateLayoutView>
      )}
    />
  )
}

const Root = ({ store, persistor }) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>

      <Router history={history}>
        <Switch>
          <PublicRoute redirect exact path="/" component={Login} />
          <PublicRoute redirect exact path="/password/:type/:tkn" component={Forgot} />
          <PublicRoute redirect exact path="/password/:type" component={Forgot} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <PrivateRoute path="/my-password" component={Password} />
          <PrivateRoute exact path="/user/add" component={FormUser} />
          <PrivateRoute exact path="/user/:id/edit" component={FormUser} />
          <PrivateRoute exact path="/users" component={User} />
          <PrivateRoute exact path="/product/add" component={FormProduct} />
          <PrivateRoute exact path="/product/:id/edit" component={FormProduct} />
          <PrivateRoute exact path="/products" component={Product} />
          <PrivateRoute exact path="/detail-country/add" component={DetailCountry} />
          <PrivateRoute exact path="/detail-country/:id/edit" component={DetailCountry} />
          <PrivateRoute exact path="/role-group" component={RoleGroup} />
          <PrivateRoute exact path="/role-group/add" component={FormRoleGroup} />
          <PrivateRoute exact path="/role-group/:id/edit" component={FormRoleGroup} />
          <PrivateRoute exact path="/role" component={Role} />
          <PrivateRoute exact path="/role/add" component={FormRole} />
          <PrivateRoute exact path="/role/:id/edit" component={FormRole} />
          <PrivateRoute exact path="/reports/training/:type" component={ReportTraining} />
          <PrivateRoute exact path="/reports/restricted-product" component={ReportProduct} />
          <PrivateRoute exact path="/reports/contest-winner" component={ReportContest} />
          <PrivateRoute exact path="/reports/agent-list" component={ReportAgentList} />
          <PrivateRoute exact path="/reports/sppa" component={ReportSPPA} />
          <PrivateRoute exact path="/reports/production" component={ReportProduction} />
          <PrivateRoute exact path="/reports/claim-settle" component={ReportClaimSettle} />
          <PrivateRoute exact path="/reports/loss-business" component={ReportLossBusiness} />
          <PrivateRoute exact path="/reports/outstanding-claim" component={ReportOutstandingClaim} />
          <PrivateRoute exact path="/reports/finance-commission" component={ReportFinanceCommission} />
          <PrivateRoute exact path="/reports/benefit-monthly" component={ReportBenefitMonthly} />
          <PrivateRoute exact path="/reports/benefit-yearly" component={ReportBenefitYearly} />
          <PrivateRoute exact path="/reports/career" component={ReportCareer} />
          <PrivateRoute exact path="/reports/agent/detail-commision" component={ReportDetailCommision} />
          <PrivateRoute exact path="/reports/agent/expired-policy" component={ReportExpiredPolicy} />
          <PrivateRoute exact path="/reports/training" component={ReportTrainingData} />
          <PrivateRoute exact path="/reports/approval" component={ReportApproval} />
          <PrivateRoute path="/product-setting/:id/edit" component={ProductSetting} />
          <PrivateRoute path="/product-setting/otomate" component={ProductSetting} />
          <PrivateRoute path="/product-setting/cargo" component={ProductSetting} />
          <PrivateRoute path="/product-setting/liability" component={ProductSetting} />
          <PrivateRoute path="/product-setting/wellwoman" component={ProductSetting} />
          <PrivateRoute path="/product-setting/travel-domestic" component={ProductSetting} />
          <PrivateRoute path="/product-setting/travel-international" component={ProductSetting} />
          <PrivateRoute path="/product-setting/asri" component={ProductSetting} />
          <PrivateRoute path="/product-setting/:id" component={ProductSetting} />
          <PrivateRoute path="/product-setting" component={ProductSetting} />

          <PublicRoute component={NotFound} />
        </Switch>
      </Router>

    </PersistGate>
  </Provider>
)

Root.propTypes = {
  store: PropTypes.shape().isRequired,
  persistor: PropTypes.shape().isRequired,
}

PrivateRoute.propTypes = {
  component: PropTypes.any,
}

PublicRoute.propTypes = {
  component: PropTypes.any,
  redirect: PropTypes.bool,
  redirectPath: PropTypes.string,
}

export default Root

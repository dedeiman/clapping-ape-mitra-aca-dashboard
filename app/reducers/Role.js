import {
  ROLE_REQUEST,
  ROLE_SUCCESS,
  ROLE_FAILURE,
  ROLE_UPDATED,
  ROLE_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataRole: [],
  detailRole: {},
  metaRole: {
    total_count: 0,
    current: 0,
  },
}

export default function products(state = initialState, action) {
  switch (action.type) {
    case ROLE_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case ROLE_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataRole: action.data,
        metaRole: action.meta,
      }
    case ROLE_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    case ROLE_UPDATED:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataRole: state.dataRole.filter(item => item.id !== action.data.id),
      }
    case ROLE_DETAIL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        detailRole: action.data,
      }
    default:
      return state
  }
}

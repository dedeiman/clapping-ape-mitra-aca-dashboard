import {
  REPORT_TRAINING_REQUEST,
  REPORT_TRAINING_SUCCESS,
  REPORT_TRAINING_DETAIL_SUCCESS,
  REPORT_TRAINING_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataProduct: [],
  metaProduct: {
    total_count: 0,
    current: 0,
  },
  dataTrainingDetail: [],
  metaTrainingDetail: {
    total_count: 0,
    current: 0,
  },
  dataTraining: [],
  metaTraining: {
    total_count: 0,
    current: 0,
  },
}

export default function reports(state = initialState, action) {
  switch (action.type) {
    case REPORT_TRAINING_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case REPORT_TRAINING_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataTraining: action.data,
        metaTraining: action.meta,
      }
    case REPORT_TRAINING_DETAIL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataTrainingDetail: action.data,
        metaTrainingDetail: action.meta,
      }
    case REPORT_TRAINING_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    default:
      return state
  }
}

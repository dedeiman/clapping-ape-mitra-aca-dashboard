import {
  PRODUCT_REQUEST,
  PRODUCT_SUCCESS,
  PRODUCT_FAILURE,
  PRODUCT_UPDATED,
  PRODUCT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataProduct: [],
  detailProduct: {},
  metaProduct: {
    total_count: 0,
    current: 0,
  },
}

export default function products(state = initialState, action) {
  switch (action.type) {
    case PRODUCT_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case PRODUCT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataProduct: action.data,
        metaProduct: action.meta,
      }
    case PRODUCT_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    case PRODUCT_UPDATED:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataProduct: state.dataProduct.filter(item => item.id !== action.data.id),
      }
    case PRODUCT_DETAIL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        detailProduct: action.data,
      }
    default:
      return state
  }
}

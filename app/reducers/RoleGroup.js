import {
  ROLE_GROUP_REQUEST,
  ROLE_GROUP_SUCCESS,
  ROLE_GROUP_FAILURE,
  ROLE_GROUP_UPDATED,
  ROLE_GROUP_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  isFetching: false,
  dataRoleGroup: [],
  detailRoleGroup: {},
  metaRoleGroup: {
    total_count: 0,
    current: 0,
  },
}

export default function products(state = initialState, action) {
  switch (action.type) {
    case ROLE_GROUP_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case ROLE_GROUP_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataRoleGroup: action.data,
        metaRoleGroup: action.meta,
      }
    case ROLE_GROUP_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.errorMessage,
      }
    case ROLE_GROUP_UPDATED:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        dataRoleGroup: state.dataRoleGroup.filter(item => item.id !== action.data.id),
      }
    case ROLE_GROUP_DETAIL_SUCCESS:
      return {
        ...state,
        isFetching: false,
        errorMessage: '',
        detailRoleGroup: action.data,
      }
    default:
      return state
  }
}

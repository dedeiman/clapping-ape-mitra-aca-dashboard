import {
  COMMISION_REPORT_REQUEST,
  COMMISION_REPORT_SUCCESS,
  COMMISION_REPORT_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  list: {
    isFetching: true,
    data: [],
    error: '',
    pagination: {
      current_page: 1,
      total_count: 1,
    },
  },
}

export default function commisionReport(state = initialState, action) {
  switch (action.type) {
    case COMMISION_REPORT_REQUEST:
      return {
        ...state,
        list: {
          isFetching: true,
          data: [],
          error: '',
          pagination: {
            current_page: 1,
            total_count: 1,
          },
        },
      }
    case COMMISION_REPORT_SUCCESS:
      return {
        ...state,
        list: {
          isFetching: false,
          data: action.data,
          error: '',
          pagination: {
            current_page: action.meta.current_page,
            total_count: action.meta.total_count,
          },
        },
      }
    case COMMISION_REPORT_FAILURE:
      return {
        ...state,
        list: {
          isFetching: false,
          data: [],
          error: action.error,
          pagination: {
            current_page: 1,
            total_count: 1,
          },
        },
      }
    default:
      return state
  }
}

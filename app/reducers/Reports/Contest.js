import {
    CONTEST_WINNER_REQUEST,
    CONTEST_WINNER_SUCCESS,
    CONTEST_WINNER_FAILURE,
} from 'constants/ActionTypes'
  
  const initialState = {
    list: {
      isFetching: true,
      data: [],
      error: '',
      pagination: {
        current_page: 1,
        total_count: 1,
      },
    },
  }
  
  export default function contest(state = initialState, action) {
    switch (action.type) {
      case CONTEST_WINNER_REQUEST:
        return {
          ...state,
          list: {
            isFetching: true,
            data: [],
            error: '',
            pagination: {
              current_page: 1,
              total_count: 1,
            },
          },
        }
      case CONTEST_WINNER_SUCCESS:
        return {
          ...state,
          list: {
            isFetching: false,
            data: action.data,
            error: '',
            pagination: {
              current_page: action.meta.current_page,
              total_count: action.meta.total_count,
            },
          },
        }
      case CONTEST_WINNER_FAILURE:
        return {
          ...state,
          list: {
            isFetching: false,
            data: [],
            error: action.error,
            pagination: {
              current_page: 1,
              total_count: 1,
            },
          },
        }
      default:
        return state
    }
  }
  
import {
  AGENT_LIST_REQUEST,
  AGENT_LIST_SUCCESS,
  AGENT_LIST_FAILURE,
} from 'constants/ActionTypes'

const initialState = {
  list: {
    isFetching: true,
    data: [],
    error: '',
    pagination: {
      current_page: 1,
      total_count: 1,
    },
  },
}

export default function agentList(state = initialState, action) {
  switch (action.type) {
    case AGENT_LIST_REQUEST:
      return {
        ...state,
        list: {
          isFetching: true,
          data: [],
          error: '',
          pagination: {
            current_page: 1,
            total_count: 1,
          },
        },
      }
    case AGENT_LIST_SUCCESS:
      return {
        ...state,
        list: {
          isFetching: false,
          data: action.data,
          error: '',
          pagination: {
            current_page: action.meta.current_page,
            total_count: action.meta.total_count,
          },
        },
      }
    case AGENT_LIST_FAILURE:
      return {
        ...state,
        list: {
          isFetching: false,
          data: [],
          error: action.error,
          pagination: {
            current_page: 1,
            total_count: 1,
          },
        },
      }
    default:
      return state
  }
}

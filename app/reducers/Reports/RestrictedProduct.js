import {
    RESTRICTED_PRODUCT_REQUEST,
    RESTRICTED_PRODUCT_SUCCESS,
    RESTRICTED_PRODUCT_FAILURE,
  } from 'constants/ActionTypes'
  
  const initialState = {
    list: {
      isFetching: true,
      data: [],
      error: '',
      pagination: {
        current_page: 1,
        total_count: 1,
      },
    },
  }
  
  export default function contest(state = initialState, action) {
    switch (action.type) {
      case RESTRICTED_PRODUCT_REQUEST:
        return {
          ...state,
          list: {
            isFetching: true,
            data: [],
            error: '',
            pagination: {
              current_page: 1,
              total_count: 1,
            },
          },
        }
      case RESTRICTED_PRODUCT_SUCCESS:
        return {
          ...state,
          list: {
            isFetching: false,
            data: action.data,
            error: '',
            pagination: {
              current_page: action.meta.current_page,
              total_count: action.meta.total_count,
            },
          },
        }
      case RESTRICTED_PRODUCT_FAILURE:
        return {
          ...state,
          list: {
            isFetching: false,
            data: [],
            error: action.error,
            pagination: {
              current_page: 1,
              total_count: 1,
            },
          },
        }
      default:
        return state
    }
  }
  
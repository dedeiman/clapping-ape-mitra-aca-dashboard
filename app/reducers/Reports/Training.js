import {
  REPORT_TRAINING_SUCCESS,
  REPORT_TRAINING_REQUEST,
  REPORT_TRAINING_FAILURE,
  REPORT_TRAINING_DETAIL_SUCCESS,
} from 'constants/ActionTypes'

const initialState = {
  list: {
    isFetching: true,
    data: [],
    error: '',
    pagination: {
      current_page: 1,
      total_count: 1,
    },
  },
}

export default function trainingReport(state = initialState, action) {
  switch (action.type) {
    case REPORT_TRAINING_REQUEST:
      return {
        ...state,
        list: {
          isFetching: true,
          data: [],
          error: '',
          pagination: {
            current_page: 1,
            total_count: 1,
          },
        },
      }
    case REPORT_TRAINING_SUCCESS:
      return {
        ...state,
        list: {
          isFetching: false,
          data: action.data,
          error: '',
          pagination: {
            current_page: action.meta.current_page,
            total_count: action.meta.total_count,
          },
        },
      }
    case REPORT_TRAINING_DETAIL_SUCCESS:
      return {
        ...state,
        isFetching: true,
        errorMessage: '',
        dataTraining: action.data,
        metaTrainingDetail: action.meta,
      }
    case REPORT_TRAINING_FAILURE:
      return {
        ...state,
        list: {
          isFetching: false,
          data: [],
          error: action.error,
          pagination: {
            current_page: 1,
            total_count: 1,
          },
        },
      }
    default:
      return state
  }
}

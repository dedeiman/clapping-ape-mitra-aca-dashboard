import { combineReducers } from 'redux'
import site from 'reducers/Site'
import auth from 'reducers/Auth'
import user from 'reducers/User'
import role from 'reducers/Role'
import roleGroup from 'reducers/RoleGroup'
import products from 'reducers/Products'
import reports from 'reducers/Reports'
import agentList from 'reducers/Reports/AgentList'
import contestWinner from 'reducers/Reports/Contest'
import restrictedProduct from 'reducers/Reports/RestrictedProduct'
import sppaReport from 'reducers/Reports/SPPA'
import reportProduct from 'reducers/Reports/Product'
import approvalReport from 'reducers/Reports/Approval'
import trainingReport from 'reducers/Reports/Training'
import commisionReport from 'reducers/Reports/DetailCommision'
import expiredReport from 'reducers/Reports/ExpiredPolicy'
import outstandingClaimReport from 'reducers/Reports/OutstandingClaim'

export default combineReducers({
  site,
  auth,
  user,
  role,
  roleGroup,
  products,
  reports,
  agentList,
  contestWinner,
  restrictedProduct,
  sppaReport,
  reportProduct,
  approvalReport,
  trainingReport,
  commisionReport,
  expiredReport,
  outstandingClaimReport,
})

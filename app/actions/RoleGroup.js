import API from 'utils/API'
import {
  ROLE_GROUP_REQUEST,
  ROLE_GROUP_SUCCESS,
  ROLE_GROUP_FAILURE,
  ROLE_GROUP_UPDATED,
  ROLE_GROUP_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import { message } from 'antd'
import Helper from '../utils/Helper'

export const roleGroupRequest = () => ({
  type: ROLE_GROUP_REQUEST,
})

export const roleGroupSuccess = (data, meta) => ({
  type: ROLE_GROUP_SUCCESS,
  data,
  meta,
})

export const roleGroupFailure = (errorMessage, errorObject) => ({
  type: ROLE_GROUP_FAILURE,
  errorMessage,
  errorObject,
})

export const roleGroupUpdate = data => ({
  type: ROLE_GROUP_UPDATED,
  data,
})

export const roleGroupDetail = data => ({
  type: ROLE_GROUP_DETAIL_SUCCESS,
  data,
})

export const fetchRoleGroup = params => (
  (dispatch) => {
    dispatch(roleGroupRequest())
    Helper.sessionTimeout()
    const url = `/role-group${params || '?sort_by=name&order_by=asc'}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(roleGroupSuccess(data, meta))
        } else {
          dispatch(roleGroupFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(roleGroupFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailRoleGroup = id => (
  (dispatch) => {
    dispatch(roleGroupRequest())
    Helper.sessionTimeout()
    const url = `/role-group/detail/${id}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(roleGroupDetail(data))
        } else {
          dispatch(roleGroupFailure(meta.message))
        }
        return response
      },
    ).catch((err) => {
      dispatch(roleGroupFailure(err.message)) // eslint-disable-line no-console
      return err
    })
  }
)

export const deleteRoleGroup = id => (
  (dispatch) => {
    dispatch(roleGroupRequest())

    return API.delete(`/role-group/delete/${id}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(roleGroupUpdate(data))
        } else {
          dispatch(roleGroupFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(roleGroupFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const createRoleGroup = payload => (
  dispatch => new Promise((resolve, reject) => (
    API.post('/role-group', payload).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          dispatch(roleGroupFailure(meta.message, meta.errors))
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      dispatch(roleGroupFailure(err.message, err.errors))
      message.error(err.message)
      reject(err)
    })
  ))
)

export const updateRoleGroup = (payload, id) => (
  dispatch => new Promise((resolve, reject) => (
    API.put(`/role-group/update/${id}`, payload).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          dispatch(roleGroupFailure(meta.message, meta.errors))
          message.message(meta.message)
        }
      },
    ).catch((err) => {
      dispatch(roleGroupFailure(err.message, err.errors))
      message.error(err.message)
      reject(err)
    })
  ))
)

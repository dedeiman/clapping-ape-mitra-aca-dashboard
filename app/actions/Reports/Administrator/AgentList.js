/* eslint-disable camelcase */
import API from 'utils/API'
import config from 'app/config'
import {
  AGENT_LIST_REQUEST,
  AGENT_LIST_SUCCESS,
  AGENT_LIST_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../../utils/Helper'

const apiUserURL = config.api_url

export const agentListRequest = () => ({
  type: AGENT_LIST_REQUEST,
})

export const agentListSuccess = (data, meta) => ({
  type: AGENT_LIST_SUCCESS,
  data,
  meta,
})

export const agentListFailure = error => ({
  type: AGENT_LIST_FAILURE,
  error,
})

export const fetchAgentList = ({
  page = 1,
  per_page = 10,
  branch_id = '',
  join_from_date = '',
  join_to_date = '',
  license_type = '',
  license_from_date = '',
  license_to_date = '',
  branch_perwakilan_id = '',
  status = '',
  agent_type = '',
  level_id = '',
  mob = '',
  religion = '',
  search = '',
} = {}) => (
  (dispatch) => {
    dispatch(agentListRequest())
    Helper.sessionTimeout()
    
    return API.get(`${apiUserURL}/reports/agents?page=${page}&per_page=${per_page}&branch_id=${branch_id}&join_from_date=${join_from_date}&join_to_date=${join_to_date}&license_type=${license_type}&license_from_date=${license_from_date}&license_to_date=${license_to_date}&search=${search}&branch_perwakilan_id=${branch_perwakilan_id}&status=${status}&agent_type=${agent_type}&level_id=${level_id}&mob=${mob}&religion=${religion}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(agentListSuccess(data, meta))
        } else {
          dispatch(agentListFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(agentListFailure(err.message))
    })
  }
)

/* eslint-disable camelcase */
import API from 'utils/API'
import {
  CONTEST_WINNER_REQUEST,
  CONTEST_WINNER_SUCCESS,
  CONTEST_WINNER_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../../utils/Helper'

export const contestWinnerRequest = () => ({
  type: CONTEST_WINNER_REQUEST,
})

export const contestWinnerSuccess = (data, meta) => ({
  type: CONTEST_WINNER_SUCCESS,
  data,
  meta,
})

export const contestWinnerFailure = error => ({
  type: CONTEST_WINNER_FAILURE,
  error,
})

export const fetchContestWinner = ({
  page = 1,
  contest_id = '',
  year = '',
  per_page = 10,
} = {}) => (
  (dispatch) => {
    dispatch(contestWinnerRequest())
    Helper.sessionTimeout()

    return API.get(`${process.env.APP_CONFIG.api_url_content}/report/contest-winners?page=${page}&per_page=${per_page}&contest_id=${contest_id}&year=${year}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(contestWinnerSuccess(data, meta))
        } else {
          dispatch(contestWinnerFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(contestWinnerFailure(err.message))
    })
  }
)

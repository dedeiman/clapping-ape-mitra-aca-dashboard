/* eslint-disable camelcase */
import API from 'utils/API'
import {
  RESTRICTED_PRODUCT_REQUEST,
  RESTRICTED_PRODUCT_SUCCESS,
  RESTRICTED_PRODUCT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../../utils/Helper'

export const RestrictedProductRequest = () => ({
  type: RESTRICTED_PRODUCT_REQUEST,
})

export const RestrictedProductSuccess = (data, meta) => ({
  type: RESTRICTED_PRODUCT_SUCCESS,
  data,
  meta,
})

export const RestrictedProductFailure = error => ({
  type: RESTRICTED_PRODUCT_FAILURE,
  error,
})

export const fetchRestrictedProduct = ({
  page = 1,
  product_id = '',
  branch_id = '',
} = {}) => (
  (dispatch) => {
    dispatch(RestrictedProductRequest())
    Helper.sessionTimeout()

    return API.get(`/report/agent-restricted-product?page=${page}&per_page=10&product_id=${product_id}&branch_id=${branch_id}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(RestrictedProductSuccess(data, meta))
        } else {
          dispatch(RestrictedProductFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(RestrictedProductFailure(err.message))
    })
  }
)

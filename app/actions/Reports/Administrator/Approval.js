/* eslint-disable camelcase */
import API from 'utils/API'
import {
  APPROVAL_REPORT_REQUEST,
  APPROVAL_REPORT_SUCCESS,
  APPROVAL_REPORT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../../utils/Helper'

export const approvalReportRequest = () => ({
  type: APPROVAL_REPORT_REQUEST,
})

export const approvalReportSuccess = (data, meta) => ({
  type: APPROVAL_REPORT_SUCCESS,
  data,
  meta,
})

export const approvalReportFailure = error => ({
  type: APPROVAL_REPORT_FAILURE,
  error,
})

export const fetchApprovalReport = ({
  page = 1,
  per_page = 10,
  format = 'excel',
  from_date = '',
  to_date = '',
  status = '',
  product_id = '',
  cob_id = '',
} = {}) => (
  (dispatch) => {
    dispatch(approvalReportRequest())
    Helper.sessionTimeout()

    return API.get(`/report/approvals/history?page=${page}&per_page=${per_page}&format=${format}&from_date=${from_date}&to_date=${to_date}&status=${status}&product_id=${product_id}&cob_id=${cob_id}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(approvalReportSuccess(data, meta))
        } else {
          dispatch(approvalReportFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(approvalReportFailure(err.message))
    })
  }
)

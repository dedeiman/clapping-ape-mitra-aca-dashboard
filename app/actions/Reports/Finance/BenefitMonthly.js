import API from 'utils/API'
import Helper from '../../../utils/Helper'

export const fetchHeaderReportList = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/benefit-monthly${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)

export const fetchDownloadReport = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/benefit-monthly/export${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)
/* eslint-disable camelcase */
import API from 'utils/API'
import Helper from '../../../utils/Helper'

export const fetchCareerList = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/jenjang-karir${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)

export const fetchDownloadCareer = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/jenjang-karir/export${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)

import API from 'utils/API'
import Helper from '../../../utils/Helper'

export const fetchReportCommissionList = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`/report/insurance-letters/komisi-finance${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)

export const fetchDownloadReportCommission = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/komisi-finance/export${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)
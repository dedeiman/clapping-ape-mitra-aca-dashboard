/* eslint-disable camelcase */
import API from 'utils/API'
import {
  PRODUCTION_REQUEST,
  PRODUCTION_SUCCESS,
  PRODUCTION_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../../utils/Helper'

export const productionRequest = () => ({
  type: PRODUCTION_REQUEST,
})

export const productionSuccess = (data, meta) => ({
  type: PRODUCTION_SUCCESS,
  data,
  meta,
})

export const productionFailure = error => ({
  type: PRODUCTION_FAILURE,
  error,
})

export const fetchProduction = ({
  page = '',
  cob_id = '',
  product_id = '',
  issued_to = '',
  issued_from = '',
  start_period_to = '',
  start_period_from = '',
  agent_status = '',
  policy_status = '',
  branch_id = '',
  branch_perwakilan_id = '',
  policy_number = '',
  is_detail = '',
  per_page = '',
} = {}) => (
  (dispatch) => {
    dispatch(productionRequest())
    Helper.sessionTimeout()

    return API.get(`/report/production?issued_from=${issued_from}&issued_to=${issued_to}&start_period_from=${start_period_from}&start_period_to=${start_period_to}&agent_status=${agent_status}&policy_status=${policy_status}&cob_id=${cob_id}&product_id=${product_id}&branch_id=${branch_id}&branch_perwakilan_id=${branch_perwakilan_id}&policy_number=${policy_number}&is_detail=${is_detail}&page=${page}&per_page=${per_page}`).then(
      (res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(productionSuccess(data, meta))
        } else {
          dispatch(productionFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(productionFailure(err.message))
    })
  }
)

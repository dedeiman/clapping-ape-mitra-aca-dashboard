import API from 'utils/API'
import Helper from '../../../utils/Helper'

export const fetchReporLossBusinesstList = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`report/insurance-letters/loss-business${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)

export const fetchDownloadReportLossBusiness = params => (
  () => new Promise((resolve, reject) => {
    Helper.sessionTimeout()
    return API.get(`/report/insurance-letters/loss-business/export${params || ''}`).then(
      (response) => {
        const { data } = response
        if (data.meta.status) {
          resolve(data)
        } else {
          reject(data.meta.message)
        }
      },
    ).catch((err) => {
      reject(err.message) // eslint-disable-line no-console
    })
  })
)
/* eslint-disable camelcase */
import API from 'utils/API'
import {
  COMMISION_REPORT_REQUEST,
  COMMISION_REPORT_SUCCESS,
  COMMISION_REPORT_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../../utils/Helper'

export const CommisionRequest = () => ({
  type: COMMISION_REPORT_REQUEST,
})

export const CommisionSuccess = (data, meta) => ({
  type: COMMISION_REPORT_SUCCESS,
  data,
  meta,
})

export const CommisionFailure = error => ({
  type: COMMISION_REPORT_FAILURE,
  error,
})

export const fetchCommision = ({
  policy_number = '',
  payment_date_from = '',
  payment_date_to = '',
  periode_date_from = '',
  periode_date_to = '',
  cob_id = '',
  page = 1,
  per_page = 10,
} = {}) => (
  (dispatch) => {
    dispatch(CommisionRequest())
    Helper.sessionTimeout()
    return API.get(`agent-commision-report?page=${page}&per_page=${per_page}&cob_id=${cob_id}&payment_date_from=${payment_date_from}&payment_date_to=${payment_date_to}&policy_number=${policy_number}&periode_date_from=${periode_date_from}&periode_date_to=${periode_date_to}`)
      .then((res) => {
        const { meta, data } = res.data

        if (meta.status) {
          dispatch(CommisionSuccess(data, meta))
        } else {
          dispatch(CommisionFailure(meta.message))
        }
      }).catch((err) => {
        dispatch(CommisionFailure(err.message))
      })
  }
)

import API from 'utils/API'
import {
  PRODUCT_REQUEST,
  PRODUCT_SUCCESS,
  PRODUCT_FAILURE,
  PRODUCT_UPDATED,
  PRODUCT_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import { message } from 'antd'
import Helper from '../utils/Helper'
// import { reject } from 'lodash'

export const productRequest = () => ({
  type: PRODUCT_REQUEST,
})

export const productSuccess = (data, meta) => ({
  type: PRODUCT_SUCCESS,
  data,
  meta,
})

export const productFailure = (errorMessage, errorObject) => ({
  type: PRODUCT_FAILURE,
  errorMessage,
  errorObject,
})

export const productUpdate = data => ({
  type: PRODUCT_UPDATED,
  data,
})

export const productDetail = data => ({
  type: PRODUCT_DETAIL_SUCCESS,
  data,
})

export const fetchProduct = params => (
  (dispatch) => {
    dispatch(productRequest())
    Helper.sessionTimeout()
    const url = `/products${params || ''}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(productSuccess(data, meta))
        } else {
          dispatch(productFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(productFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailProduct = id => (
  (dispatch) => {
    dispatch(productRequest())
    Helper.sessionTimeout()
    const url = `/products/${id}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(productDetail(data))
        } else {
          dispatch(productFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(productFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const deleteProduct = id => (
  (dispatch) => {
    dispatch(productRequest())

    return API.delete(`/products/${id}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(productUpdate(data))
        } else {
          dispatch(productFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(productFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const createProduct = payload => (
  dispatch => new Promise((resolve, reject) => (
    API.post('/products', payload).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          dispatch(productFailure(meta.message, meta.errors))
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      dispatch(productFailure(err.message, err.errors))
      message.error(err.message)
      reject(err)
    })
  ))
)

export const updateProduct = (payload, id) => (
  dispatch => new Promise((resolve, reject) => (
    API.put(`/products/${id}`, payload).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          dispatch(productFailure(meta.message, meta.errors))
          message.message(meta.message)
        }
      },
    ).catch((err) => {
      dispatch(productFailure(err.message, err.errors))
      message.error(err.message)
      reject(err)
    })
  ))
)

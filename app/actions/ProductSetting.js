import API from 'utils/API'
import {
  PS_REQUEST,
  PS_SUCCESS,
  PS_FAILURE,
  PS_UPDATED,
  PS_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
// import { message } from 'antd'

export const psRequest = () => ({
  type: PS_REQUEST,
})

export const psSuccess = (data, meta) => ({
  type: PS_SUCCESS,
  data,
  meta,
})

export const psFailure = (errorMessage, errorObject) => ({
  type: PS_FAILURE,
  errorMessage,
  errorObject,
})

export const psUpdate = data => ({
  type: PS_UPDATED,
  data,
})

export const psDetail = data => ({
  type: PS_DETAIL_SUCCESS,
  data,
})

export const fetchPs = params => (
  (dispatch) => {
    dispatch(psRequest())
    const url = `customers${params || ''}`
    return new Promise((resolve, reject) => {
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(psSuccess(data, meta))
          } else {
            dispatch(psFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(psFailure(err.message)) // eslint-disable-line no-console
      })
    })
  }
)

export const fetchDetailPs = id => (
  (dispatch) => {
    dispatch(psRequest())
    const url = `customers/${id}`

    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          resolve(response)
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(psDetail(data))
          } else {
            dispatch(psFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err)
        dispatch(psFailure(err.message)) // eslint-disable-line no-console
      })
    ))
  }
)

export const deletePs = id => (
  (dispatch) => {
    dispatch(psRequest())

    return new Promise((resolve, reject) => (
      API.delete(`customers/${id}`).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(psUpdate(data))
            resolve(data)
          } else {
            dispatch(psFailure(meta.message))
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(psFailure(err.message)) // eslint-disable-line no-console
        reject(err.message)
      })
    ))
  }
)

export const createPs = payload => (
  (dispatch) => {
    dispatch(psRequest())

    return new Promise((resolve, reject) => (
      API.post('customers', payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(psFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(psFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

export const updatePs = (payload, id) => (
  (dispatch) => {
    dispatch(psRequest())

    return new Promise((resolve, reject) => (
      API.put(`customers/${id}`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(psFailure(meta.message, meta.errors))
            // message.error(meta.message)
            reject(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(psFailure(err.message, err.errors))
        // message.error(err.message)
        reject(err.message)
      })
    ))
  }
)

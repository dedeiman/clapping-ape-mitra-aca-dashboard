import API from 'utils/API'
import {
  ROLE_REQUEST,
  ROLE_SUCCESS,
  ROLE_FAILURE,
  ROLE_UPDATED,
  ROLE_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import { message } from 'antd'
import Helper from '../utils/Helper'

export const roleRequest = () => ({
  type: ROLE_REQUEST,
})

export const roleSuccess = (data, meta) => ({
  type: ROLE_SUCCESS,
  data,
  meta,
})

export const roleFailure = (errorMessage, errorObject) => ({
  type: ROLE_FAILURE,
  errorMessage,
  errorObject,
})

export const roleUpdate = data => ({
  type: ROLE_UPDATED,
  data,
})

export const roleDetail = data => ({
  type: ROLE_DETAIL_SUCCESS,
  data,
})

export const fetchRole = params => (
  (dispatch) => {
    dispatch(roleRequest())
    Helper.sessionTimeout()
    const url = `/role${params || '?sort_by=name&order_by=asc'}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(roleSuccess(data, meta))
        } else {
          dispatch(roleFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(roleFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

export const fetchDetailRole = id => (
  (dispatch) => {
    dispatch(roleRequest())
    Helper.sessionTimeout()
    const url = `/role/detail/${id}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(roleDetail(data))
        } else {
          dispatch(roleFailure(meta.message))
        }
        return response
      },
    ).catch((err) => {
      dispatch(roleFailure(err.message)) // eslint-disable-line no-console
      return err
    })
  }
)

export const deleteRole = id => (
  (dispatch) => {
    dispatch(roleRequest())

    return API.delete(`/role/delete/${id}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(roleUpdate(data))
        } else {
          dispatch(roleFailure(meta.message))
        }
        return response
      },
    ).catch((err) => {
      dispatch(roleFailure(err.message)) // eslint-disable-line no-console
      return err
    })
  }
)

export const createRole = payload => (
  dispatch => new Promise((resolve, reject) => (
    API.post('/role', payload).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          dispatch(roleFailure(meta.message, meta.errors))
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      dispatch(roleFailure(err.message, err.errors))
      message.error(err.message)
      reject(err)
    })
  ))
)

export const updateRole = (payload, id) => (
  dispatch => new Promise((resolve, reject) => (
    API.put(`/role/update/${id}`, payload).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          dispatch(roleFailure(meta.message, meta.errors))
          message.message(meta.message)
        }
      },
    ).catch((err) => {
      dispatch(roleFailure(err.message, err.errors))
      message.error(err.message)
      reject(err)
    })
  ))
)

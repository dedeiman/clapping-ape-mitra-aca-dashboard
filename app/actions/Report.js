import API from 'utils/API'
import {
  REPORT_CONTEST_REQUEST,
  REPORT_CONTEST_SUCCESS,
  REPORT_CONTEST_FAILURE,
  REPORT_TRAINING_REQUEST,
  REPORT_TRAINING_SUCCESS,
  REPORT_TRAINING_DETAIL_SUCCESS,
  REPORT_TRAINING_FAILURE,
} from 'constants/ActionTypes'
import Helper from '../utils/Helper'

export const reportContestRequest = () => ({
  type: REPORT_CONTEST_REQUEST,
})

export const reportContestSuccess = (data, meta) => ({
  type: REPORT_CONTEST_SUCCESS,
  data,
  meta,
})

export const reportContestFailure = errorMessage => ({
  type: REPORT_CONTEST_FAILURE,
  errorMessage,
})

export const reportTrainingRequest = () => ({
  type: REPORT_TRAINING_REQUEST,
})

export const reportTrainingSuccess = (data, meta) => ({
  type: REPORT_TRAINING_SUCCESS,
  data,
  meta,
})

export const reportTrainingDetailSuccess = (data, meta) => ({
  type: REPORT_TRAINING_DETAIL_SUCCESS,
  data,
  meta,
})

export const reportTrainingFailure = errorMessage => ({
  type: REPORT_TRAINING_FAILURE,
  errorMessage,
})

export const fetchReportTraining = (params, isDetail) => (
  (dispatch) => {
    dispatch(reportTrainingRequest())
    Helper.sessionTimeout()

    return new Promise((resolve, reject) => (
      API.get(`/report/training-class${params || ''}`).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            if (isDetail) {
              resolve(data)
              dispatch(reportTrainingDetailSuccess(data, meta))
            } else {
              resolve(data)
              dispatch(reportTrainingSuccess(data, meta))
            }
          } else {
            reject(meta.message)
            dispatch(reportTrainingFailure(meta.message))
          }
        },
      ).catch((err) => {
        reject(err.message)
        dispatch(reportTrainingFailure(err.message)) // eslint-disable-line no-console
      })
    ))
  }
)

export const fetchReportContest = params => (
  (dispatch) => {
    dispatch(reportContestRequest())
    Helper.sessionTimeout()
    const url = `${process.env.APP_CONFIG.api_url_content}/report/contest-winners${params || ''}`
    return API.get(url).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          dispatch(reportContestSuccess(data, meta))
        } else {
          dispatch(reportContestFailure(meta.message))
        }
      },
    ).catch((err) => {
      dispatch(reportContestFailure(err.message)) // eslint-disable-line no-console
    })
  }
)

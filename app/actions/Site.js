import { SET_SITE_CONFIGURATION } from 'constants/ActionTypes'

export const updateSiteConfiguration = (dataKey, dataValue) => (
  {
    type: SET_SITE_CONFIGURATION,
    dataKey,
    dataValue,
  }
)

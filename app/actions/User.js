import API from 'utils/API'
import {
  USER_REQUEST,
  USER_SUCCESS,
  USER_FAILURE,
  USER_UPDATED,
  USER_DETAIL_SUCCESS,
} from 'constants/ActionTypes'
import { message } from 'antd'
import Helper from '../utils/Helper'

export const userRequest = () => ({
  type: USER_REQUEST,
})

export const userSuccess = (data, meta) => ({
  type: USER_SUCCESS,
  data,
  meta,
})

export const userFailure = (errorMessage, errorObject) => ({
  type: USER_FAILURE,
  errorMessage,
  errorObject,
})

export const userUpdate = data => ({
  type: USER_UPDATED,
  data,
})

export const userDetail = data => ({
  type: USER_DETAIL_SUCCESS,
  data,
})

export const fetchUser = params => (
  (dispatch) => {
    dispatch(userRequest())
    Helper.sessionTimeout()
    const url = `/users${params || ''}`

    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(userSuccess(data, meta))
          } else {
            dispatch(userFailure(meta.message))
          }

          resolve(response)
        },
      ).catch((err) => {
        dispatch(userFailure(err.message)) // eslint-disable-line no-console
        reject(err)
      })
    ))
  }
)

export const fetchDetailUser = id => (
  (dispatch) => {
    Helper.sessionTimeout()
    dispatch(userRequest())

    const url = `/users/${id}`
    return new Promise((resolve, reject) => (
      API.get(url).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(userDetail(data))
          } else {
            dispatch(userFailure(meta.message))
          }

          resolve(response)
        },
      ).catch((err) => {
        dispatch(userFailure(err.message)) // eslint-disable-line no-console
        reject(err)
      })
    ))
  }
)

export const deleteUser = id => (
  (dispatch) => {
    dispatch(userRequest())

    return new Promise((resolve, reject) => (
      API.delete(`/users/${id}`).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            dispatch(userUpdate(data))
          } else {
            dispatch(userFailure(meta.message))
          }

          resolve(response)
        },
      ).catch((err) => {
        dispatch(userFailure(err.message)) // eslint-disable-line no-console
        reject(err)
      })
    ))
  }
)

export const createUser = payload => (
  (dispatch) => {
    dispatch(userRequest())

    return new Promise(resolve => (
      API.post('/users', payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(userFailure(meta.message, meta.errors))
            message.error(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(userFailure(err.message, err.errors))
        message.error(err.message)
      })
    ))
  }
)

export const updateUser = (payload, id) => (
  (dispatch) => {
    dispatch(userRequest())

    return new Promise(resolve => (
      API.put(`/users/${id}`, payload).then(
        (response) => {
          const { data, meta } = response.data
          if (meta.status) {
            resolve(data)
          } else {
            dispatch(userFailure(meta.message, meta.errors))
            message.error(meta.message)
          }
        },
      ).catch((err) => {
        dispatch(userFailure(err.message, err.errors))
        message.error(err.message)
      })
    ))
  }
)

export const getRole = param => (
  () => new Promise(resolve => (
    API.get(`/role${param}`).then(
      (response) => {
        const { data, meta } = response.data
        if (meta.status) {
          resolve(data)
        } else {
          message.error(meta.message)
        }
      },
    ).catch((err) => {
      message.error(err.message)
    })
  ))
)
